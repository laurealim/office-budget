<?php

use App\Http\Controllers\InternalBudgetExpenseController;
use App\Models\AssignFlat;
use App\Models\Flat;
use App\Models\OfficeBudget;
use App\Role;
use App\Models\District;
use App\Models\Upazila;
use App\Models\Union;
use App\Models\Applicant;
use App\Models\Land;
use App\Models\FlatRate;
use App\Models\AssignUser;
use App\Models\Tenant;
use App\Models\Ledger;
use App\Models\Bill;
use App\Models\Institute;
use App\Models\BudgetCode;
use App\Models\BudgetType;
use App\Models\Item;
use App\Models\OfficeBudgetBalance;
use App\Models\InternalBudget;
use App\Models\InternalBudgetBalance;
use Illuminate\Support\Facades\DB;
use App\Models\OfficeBudgetExpense;
use App\Models\InternalBudgetExpense;
use App\Models\BudgetSubType;
use Illuminate\Support\Facades\File;
use Rakibhstu\Banglanumber\NumberToBangla;

function check_view_permission($role)
{

}

function check_view_role($role)
{
    $roleArr = array($role);
    if (auth()->check() && auth()->user()->hasRole($roleArr)) {
        return true;
    } else {
        return false;
    }
}

function getDistNameById($id)
{
    $distName = District::where('id', '=', $id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function getUpazilaNameById($id)
{
    $distName = Upazila::where('id', '=', $id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function getUnionNameById($id)
{
    $distName = Union::where('id', '=', $id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function getLandById($id)
{
    try {
        if ($id) {
            $res = Land::where('id', '=', $id)
                ->where('is_active', '!=', config('constants.status.Deleted'))
                ->get()->toArray();
            $res[0]['dist'] = getDistNameById($id);
            $res[0]['upa'] = getUpazilaNameById($id);
            return $res;
        }
    } catch (Exception $exception) {
//        dd($exception);
    }
}

function getClientInfoByIds($ids)
{
    $idArr = explode(',', $ids);
    $clientListArray = Applicant::whereIn('id', $idArr)->get();
    return $clientListArray;
}

function updateFlateRate($flatId, $rent, $charge)
{
    try {
        $flatRate = FlatRate::where('flat_id', '=', $flatId)->get();
        if (($flatRate[0]->flat_rent != $rent) || ($flatRate[0]->flat_charge != $charge)) {

            try {
                $flatRate[0]->status = config('constants.status.Inactive');
                $flatRate[0]->updated_by = auth()->user()->id;
//                dd($flatRate);
                $flatRate[0]->save();

                $flatRateModel = new FlatRate();
                $flatRateModel->flat_id = $flatId;
                $flatRateModel->flat_rent = $rent;
                $flatRateModel->flat_charge = $charge;
                $flatRateModel->year = date("Y");
                $flatRateModel->month = date("n");
                $flatRateModel->created_by = auth()->user()->id;
                $flatRateModel->save();
            } catch (Exception $rentExcp) {
                dd($rentExcp);
            }
        }
        return true;

    } catch (Exception $exception) {
        dd($exception);
        return false;
    }
}

function assignFlat($tenantId, $flatId)
{
    $assFlat = "";
    $assignFlatModel = new AssignFlat();
    $resultData = AssignFlat::where('tenant_id', '=', $tenantId)->first();
    try {
        if (!empty($resultData)) {
            $resultData['flat_ids'] = $resultData['flat_ids'] . ',' . $flatId;
            $resultData['flat_ids'] = trim($resultData['flat_ids'], ",");
            $assignFlatModel->where('tenant_id', '=', $tenantId)
                ->update(['flat_ids' => $resultData['flat_ids']]);
        } else {
            $assignFlatModel->tenant_id = $tenantId;
            $assignFlatModel->flat_ids = $flatId;
            $assignFlatModel->status = config('constants.status.Active');
            $assignFlatModel->save();
        }
    } catch (Exception $exception) {
//        pr($exception);
        dd($exception);
    }
}

function assignUser($tenantId, $flatId)
{
    $assFlat = "";
    $assignUserModel = new AssignUser();
    $resultData = AssignUser::where('flat_id', '=', $flatId)->first();
    try {
        if (!empty($resultData)) {
            $resultData['tenant_ids'] = $resultData['tenant_ids'] . ',' . $tenantId;
            $resultData['tenant_ids'] = trim($resultData['tenant_ids'], ",");
            $assignUserModel->where('flat_id', '=', $flatId)
                ->update(['tenant_ids' => $resultData['tenant_ids']]);
        } else {
            $assignUserModel->flat_id = $flatId;
            $assignUserModel->tenant_ids = $tenantId;
            $assignUserModel->status = config('constants.status.Active');
            $assignUserModel->save();
        }
    } catch (Exception $exception) {
//        pr($exception);
        dd($exception);
    }
}

function unAssignUser($flatId)
{
    $flatUpdateData = [
        'tenant_ids' => '',
    ];
    $resultData = AssignUser::where('flat_id', '=', $flatId)->first();
    $tId = explode(',', $resultData['tenant_ids']);
    try {
        AssignUser::where('flat_id', '=', $flatId)->update($flatUpdateData);
        return $tId;
    } catch (Exception $exception) {
//        pr($exception);
        dd($exception);
    }
}

function unAssignFlat($tenantId, $flatId)
{
    $list = "";
    $assignFlatModel = new AssignFlat();
    $resultData = AssignFlat::where('tenant_id', '=', $tenantId)->first();
    try {
        if (!empty($resultData)) {
            $flatIdArr = explode(',', $resultData['flat_ids']);
            $pos = array_search($flatId, $flatIdArr);
            if ($pos !== false) {
                unset($flatIdArr[$pos]);
            }
            $list = implode(',', $flatIdArr);
            $assignFlatModel->where('tenant_id', '=', $tenantId)->update(['flat_ids' => $list]);
            return true;
        }
    } catch (Exception $exception) {
        pr($exception);
        dd($exception);
    }
}

function getUnassignedFlatList($createdBy = null)
{
    $unassignedFlatList = [];
    try {
        $flatModel = new Flat();
        $query = $flatModel::query();
        $query->where('status', '=', config('constants.flat_status.Free'));
        $query->where('tenant_id', '=', config('constants.is_assigned.Unassigned'));
        if (!empty($createdBy))
            $query->where('created_by', '=', $createdBy);

        $unassignedFlatList = $query->get()->pluck('flat_name', 'id');
    } catch (Exception $exception) {

    }
    return $unassignedFlatList;
}

function getAssignedFlatString($tenanrId)
{
    $listArr = getAssignedFlatList($tenanrId);
    $totalFlat = count($listArr);
    $strFlat = ' ';
    foreach ($listArr as $key => $flat_name) {
        $strFlat .= ' ' . $flat_name . '।  ';
    }
    return ['list' => $strFlat, 'count' => $totalFlat];
}

function getAssignedFlatList($tenanrId = null)
{
    $assignedFlatList = [];
    try {
        $condition = [
            ['status', '=', config('constants.flat_status.Booked')],
        ];
        if (!empty($tenanrId)) {
            array_push($condition, ['tenant_id', '=', $tenanrId]);
        }
        $assignedFlatList = Flat::where($condition)->get()->pluck('flat_name', 'id');
    } catch (Exception $exception) {

    }
    return $assignedFlatList;
}

function getTenantList()
{
    $tenantListArr = Tenant::where('status', '=', config('constants.status.Active'))->get()->pluck('name', 'id');
    return $tenantListArr;
}

function getBillableFlatList($flatQwnerId = null)
{
    $currentMonth = (int)date('m');
    $currentYear = (int)date('Y');

    $flatModel = new Flat();

    $query = $flatModel::query();
    $query->select('flats.flat_id', 'flats.address', 'flats.flat', 'flats.id')
        ->where('flats.status', '=', config('constants.flat_status.Booked'))
        ->where('flats.tenant_id', '!=', config('constants.is_assigned.Unassigned'))
        ->whereNotIn('id', function ($query) use ($currentYear, $currentMonth) {
            $query->select(DB::raw('flat_id'))
                ->from('bills')
                ->whereRaw('bills.year = ' . $currentYear)
                ->whereRaw('bills.month = ' . $currentMonth)
                ->groupBy('bills.flat_id');
        });

    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
        $query->where('flats.created_by', '=', auth()->user()->id);
    }

    $billableFlatList = $query->get();
    return $billableFlatList;
}

function getPreviousDueByFlatAndTenantId($flatId, $tenantId)
{
    $ledgerModel = new Ledger();
    $total_bill = $total_payment = $prv_due = 0;
    $ledgerData = $ledgerModel->select(DB::raw('SUM(total_bill) as total_bill,SUM(total_payment) as total_payment,SUM(prv_due) as prv_due'))
        ->where('flat_id', '=', $flatId)
        ->where('tenant_id', '=', $tenantId)
//        ->groupBy('invoice_code')
        ->get()
        ->toArray();
    $ledgerData = $ledgerData[0];
    if (!empty($ledgerData['total_bill'])) {
        $total_bill = $ledgerData['total_bill'];
    }
    if (!empty($ledgerData['total_payment'])) {
        $total_payment = $ledgerData['total_payment'];
    }
    if (!empty($ledgerData['prv_due'])) {
        $prv_due = $ledgerData['prv_due'];
    }

    $totalDue = ($total_payment + $prv_due) - $total_bill;
    return $totalDue;
}

function getFlatNameById($flatId)
{
    $flatModel = new Flat();
    $name = "";
    $nameArr = $flatModel->select('flat_id', 'flat', 'address')
        ->where('id', '=', $flatId)
        ->get();
    if (empty($nameArr))
        return $name;
    else
        $nameArr = $nameArr[0];

    $name = $nameArr['flat_id'] . '-' . $nameArr['flat'] . ' (' . $nameArr['flat_id'] . ')';
    return $name;
//    return $nameArr;
}

function uniqueBillId($billId)
{
    $billModel = new Bill();
    $billData = $billModel->where('invoice_code', '=', $billId)->get()->toArray();
//    $billData = $billModel->where('invoice_code','=','INVPLL1OOY')->get()->toArray();
    if (empty($billData)) {
        return 200;
    } else {
        return 400;
    }
}

function getUnpaidBillList($invoice_id = null, $month = null, $year = null)
{
    $billModel = new Bill();
    if (empty($month)) {
        $month = date('m');
    }
    if (empty($year)) {
        $year = date('Y');
    }
    $query = $billModel::query();
    $query->where('bills.status', '!=', config('constants.bill_status.Full'))
        ->where('month', '=', $month)
        ->where('year', '=', $year)
        ->leftJoin('flats', 'bills.flat_id', 'flats.id');
    if (!empty($invoice_id)) {
        $query->where('invoice_code', '=', $invoice_id);
    }

    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
        $query->where('flats.created_by', '=', auth()->user()->id);
    }

    $unpaidBilList = $query->pluck('bills.invoice_code', 'bills.id');
    return $unpaidBilList;
}

function ledgerUpdateAfterPayment($ledgerData, $formData)
{
    $f_rent = $ledgerData->f_rent;
    $f_charge = $ledgerData->f_charge;
    $adv_rent = $ledgerData->adv_rent;
    $other_charge = $ledgerData->other_charge;
    $prev_due = $ledgerData->prev_due;
    $total_bill = $ledgerData->total_bill;
    $total_prev_payment = $ledgerData->total_payment;

    $pay_rent_amount = $ledgerData->pay_rent_amount;
    $pay_serv_charge_amount = $ledgerData->pay_serv_charge_amount;
    $pay_other_amount = $ledgerData->pay_other_amount;
    $pay_adv_rent = $ledgerData->pay_adv_rent;

    $total_pay = $formData['total_pay'];
    $invoice_code = $formData['invoice_code'];
    $flat_id = $formData['flat_id'];
    $tenant_id = $formData['tenant_id'];
    $year = $formData['year'];
    $month = $formData['month'];
    $total_payment = $total_pay;
    $count = true;

    $counter = 0;
    $searchValue = [
        'pay_rent_amount',
        'pay_serv_charge_amount',
        'pay_other_amount',
        'pay_adv_rent',
    ];

    $finalLedgerValue = [
        'pay_rent_amount' => 0,
        'pay_serv_charge_amount' => 0,
        'pay_other_amount' => 0,
        'pay_adv_rent' => 0,
    ];

    if ($ledgerData->status == config('constants.ledger_status.Partial')) {
        $finalLedgerValue = [
            'pay_rent_amount' => $pay_rent_amount,
            'pay_serv_charge_amount' => $pay_serv_charge_amount,
            'pay_other_amount' => $pay_other_amount,
            'pay_adv_rent' => $pay_adv_rent,
        ];
    }

    $checkingDataValue = [
        "f_rent" => $f_rent,
        "f_charge" => $f_charge,
        "other_charge" => $other_charge,
        "adv_rent" => $adv_rent
    ];

    if ($total_pay >= 1) {

        foreach ($checkingDataValue as $key => $value) {
            if ($ledgerData->status == config('constants.ledger_status.Partial')) {
                $payDataString = $searchValue[$counter];
                if (($ledgerData->$payDataString < $value) && $count) { // if the previous pay data is only smaller than the real data.
                    $extraAmount = $value - $ledgerData->$payDataString;
                    /*
                     * if extra shortage amount is smaller or equal to the total payment amount.
                     */
                    if ($extraAmount <= $total_pay) {

                        /*
                         *  only the extra amount will be added to the final amount including the previous payed amount.
                         */
                        $finalLedgerValue[$searchValue[$counter]] = $extraAmount + $ledgerData->$payDataString;
                        $total_pay = $total_pay - $extraAmount;
                        if ($total_pay < 1) { // if total payment amount remains 0
                            $count = flase;
                        }
                    } else { // extra amount is bigger than the payment amount, than have to add the full payment amount to this payment section.
                        $finalLedgerValue[$searchValue[$counter]] = $total_pay + $ledgerData->$payDataString;
                        $count = flase;
                    }
//                    $counter++;
//                    $ledgerData->$searchValue[$counter] + ($value - $ledgerData->$searchValue[$counter]);
                }
            } else {
                if ($count && ($total_pay - $value > -1)) {
                    $total_pay = $total_pay - $value;
                    $finalLedgerValue[$searchValue[$counter]] = $value;
                    if ($total_pay < 1) {
                        $count = false;
                    }
                } else {
                    $finalLedgerValue[$searchValue[$counter]] = $value - $total_pay;
                    $count = false;
                }
            }

            if (!$count) {
                break;
            }
            $counter++;
        }
    }

    $cur_due = $total_payment + $total_prev_payment - $total_bill;

    $ledgerUpdateArr = [
        'pay_rent_amount' => $finalLedgerValue['pay_rent_amount'],
        'pay_serv_charge_amount' => $finalLedgerValue['pay_serv_charge_amount'],
        'pay_adv_rent' => $finalLedgerValue['pay_adv_rent'],
        'pay_other_amount' => $finalLedgerValue['pay_other_amount'],
        'total_payment' => $total_payment + $total_prev_payment,
        'cur_due_adv' => $cur_due,
        'status' => ($total_bill == ($total_payment + $total_prev_payment)) ? config('constants.ledger_status.Full') : ((($total_bill > ($total_payment + $total_prev_payment)) && ($total_payment >= 1)) ? config('constants.ledger_status.Partial') : config('constants.ledger_status.Unpaid'))
    ];
    return $ledgerUpdateArr;
}

function getTenantAddress($tenent_id)
{
    $tenentData = Tenant::where('id', '=', $tenent_id)
        ->select('dist', 'upaz', 'union', 'address')
        ->get()->toArray();
    $union = $tenentData[0]['union'] >= 1 ? getUnionNameById($tenentData[0]['union']) : 'পৌরসভা';
    $full_address = $tenentData[0]['address'] . ',<br/> ' . $union . ',<br/>  ' . getUpazilaNameById($tenentData[0]['upaz']) . ',<br/>  ' . getUpazilaNameById($tenentData[0]['dist']);
    return $full_address;
}

function getInstituteList($createdBy = null)
{
    $instituteList = [];
    try {
        $instituteModel = new Institute();
        $query = $instituteModel::query();
        $query->where('status', '=', config('constants.status.Active'));
        if (!empty($createdBy))
            $query->where('created_by', '=', $createdBy);

//        $instituteList = $query->get()->pluck('institute_name', 'id');
        $instituteList = $query->pluck('office_name', 'id');
    } catch (Exception $exception) {
    }
    return $instituteList;
}

function getFullInstituteList($createdBy = null)
{
    $instituteList = [];
    try {
//        $instituteList = DB::table('institutes')->selectRaw("id, office_name, dist, upaz")->orderBy('id')->get()->toArray();
        $newListArr = [];

//        dd(auth()->user()->user_type);
        $instituteModel = new Institute();
        $query = $instituteModel::query();
        $query->select("id", "office_name", "dist", "upaz");
        $query->where('status', '=', config('constants.status.Active'));
        if (!empty($createdBy)) {
            $query->where('created_by', '=', $createdBy);
        } else {
            if (auth()->user()->user_type != config('constants.userType.SuperUser')) {
                $query->where('created_by', '=', auth()->user()->id);
            }
        }

        $newListArr = $query->get();

        foreach ($newListArr as $key => $vals) {
            $str = '';
            if ($vals->upaz < 1) {
                $str = $vals->office_name . ', জেলা কার্যালয়,' . getDistNameById($vals->dist);
            } else {
                $str = $vals->office_name . ', ' . getUpazilaNameById($vals->upaz) . ', ' . getDistNameById($vals->dist);
            }
            $instituteList[$vals->id] = $str;
        }
    } catch (Exception $exception) {
    }
    return $instituteList;
}

function getInstituteNameById($id, $createdBy = false)
{
    $instituteList = [];
    try {
        $instituteModel = new Institute();
        $query = $instituteModel::query();
        $query->where('id', '=', $id);
        $query->where('status', '=', config('constants.status.Active'));
        if (!empty($createdBy))
            $query->where('created_by', '=', $createdBy);

        $instituteList = $query->select('office_name', 'dist', 'upaz')->get();
        $instituteList = $instituteList[0];
//        dd($instituteList[0]->office_name);

    } catch (Exception $exception) {
    }
    return $instituteList;
}

function getFullInstituteNameById($id, $createdBy = false)
{
    $instituteList = [];
    try {
        $instituteModel = new Institute();
        $query = $instituteModel::query();
        $query->select("id", "office_name", "dist", "upaz");
        $query->where('id', '=', $id);
        $query->where('status', '=', config('constants.status.Active'));
        if (!empty($createdBy))
            $query->where('created_by', '=', $createdBy);

        $newListArr = $query->get();

        foreach ($newListArr as $key => $vals) {
            $str = '';
            if ($vals->upaz < 1) {
                $str = $vals->office_name . ', জেলা কার্যালয়,' . getDistNameById($vals->dist);
            } else {
                $str = $vals->office_name . ', ' . getUpazilaNameById($vals->upaz) . ', ' . getDistNameById($vals->dist);
            }
            $instituteList[$key] = $str;
        }
        $instituteList = $instituteList[0];
//        dd($instituteList[0]->office_name);

    } catch (Exception $exception) {
    }
    return $instituteList;
}

function getParentItemList()
{
    $parentItemListArr = BudgetCode::where('budget_status', '=', config('constants.status.Active'))
        ->where('is_item_parent', '=', config('constants.is_item_parent.Yes'))->get()->pluck('budget_code_name', 'id');
//    $tenantListArr = Tenant::where('status', '=', config('constants.status.Active'))->get()->pluck('name', 'id');
    return $parentItemListArr;
}

function getBudgetCodeList()
{
    $budgetCode = BudgetCode::get()->pluck('budget_code_name', 'id')->toArray();
    return $budgetCode;
}

function getBudgetCodeById($id)
{
    if (!empty($id)) {
        $budgetCode = BudgetCode::where('id', '=', $id)->get()->toArray();
        $budgetCode = $budgetCode[0];
        return $budgetCode;
    }
}

function getBudgetCodeByBudgetType($typeId)
{
    $budgetCode = [];
    if (!empty($typeId)) {
        $budgetCode = BudgetCode::select('id', 'bgt_type', 'bgt_sub_type', 'budget_code', 'full_budget_code', 'budget_title', 'vat', 'tax')
            ->where('bgt_type', '=', $typeId)
            ->orderBy('bgt_type', 'ASC')
            ->orderBy('bgt_sub_type', 'ASC')
            ->get()->toArray();
        return $budgetCode;
    }
    return $budgetCode;
}

function getBudgetCodeListByBudgetType($typeId)
{
    $budgetCodeList = [];
    if (!empty($typeId)) {
        $budgetCodeList = BudgetCode::select('id', 'bgt_type', 'budget_code', 'budget_title', 'vat', 'tax')
            ->where('bgt_type', '=', $typeId)->get()->pluck('budget_code_name', 'id')->toArray();
        return $budgetCodeList;
    }
    return $budgetCodeList;
}

function getBudgetTypeList($isNull = true)
{
    $query = BudgetType::query();
    $query->where('budget_type_status', '=', config('constants.status.Active'));
    if ($isNull) {
        $query->whereNull('budget_type_code');
    } else {
        $query->whereNotNull('budget_type_code');
    }
    $budgetTypeList = $query->get()->pluck('budget_type_names', 'id');
    return $budgetTypeList;

//    $budgetTypeList = BudgetType::where('budget_type_status', '!=', config('constants.status.Deleted'))
//        ->whereNull('budget_type_code')
//        ->pluck('budget_type', 'id');
}

function getBudgetTypeListById($id = null)
{
    $query = BudgetType::query();
    $query->where('budget_type_status', '=', config('constants.status.Active'));
    if ($id) {
        $query->where('id', '=', $id);
    }
    $budgetTypeList = $query->get()->pluck('budget_type_names', 'id');
    $budgetTypeList = $budgetTypeList[$id];
    return $budgetTypeList;

//    $budgetTypeList = BudgetType::where('budget_type_status', '!=', config('constants.status.Deleted'))
//        ->whereNull('budget_type_code')
//        ->pluck('budget_type', 'id');
}

function getBudgetTypeById($id = null)
{
    $query = BudgetType::query();
    $query->where('budget_type_status', '=', config('constants.status.Active'));
    if ($id) {
        $query->where('id', '=', $id);
    }
    $budgetTypeList = $query->get()->toArray();
    $budgetTypeList = $budgetTypeList[0];
    return $budgetTypeList;

//    $budgetTypeList = BudgetType::where('budget_type_status', '!=', config('constants.status.Deleted'))
//        ->whereNull('budget_type_code')
//        ->pluck('budget_type', 'id');
}

function getBudgetSubTypeList($budgetType = null, $id = null)
{
    $query = BudgetSubType::query();
//    $query->select('id', 'budget_sub_type_code', 'budget_sub_type_name');
    $query->where('budget_sub_type_status', '=', config('constants.status.Active'));
    if (!empty($budgetType)) {
        $query->where('budget_type_id', '=', $budgetType);
    }
    if (!empty($id)) {
        $query->where('id', '=', $id);
    }
//    $budgetSubTypeList = $query->get()->pluck('budget_sub_type_name', 'id');
    $budgetSubTypeList = $query->get()->pluck('budgetSubTypeNames', 'id');
    return $budgetSubTypeList;
}

function getBudgetSubTypeCodeById($budgetSubTypeId = null)
{
    $query = BudgetSubType::query();
    $query->where('budget_sub_type_status', '=', config('constants.status.Active'));
    if (!empty($budgetSubTypeId)) {
        $query->where('id', '=', $budgetSubTypeId);
    }
    $budgetSubTypeCode = $query->get()->toArray();
//    dd($budgetSubTypeCode[0]['budget_sub_type_code']);
    $budgetSubTypeCode = $budgetSubTypeCode[0]['budget_sub_type_code'];
    return $budgetSubTypeCode;
}

function getBudgetTypeInfoList($budgetTypeId = null)
{
    $query = BudgetType::query();
    $query->select('id', 'budget_type_name', 'budget_type_code');
    $query->where('budget_type_status', '=', config('constants.status.Active'));
    if (!empty($budgetTypeId)) {
        $query->where('id', '=', $budgetTypeId);
    }
    $budgetTypeList = $query->get()->toArray();
    $budgetTypeInfoList = [];
    foreach ($budgetTypeList as $sr => $vals) {
        if (!isset($budgetTypeInfoList[$vals['id']])) {
            $budgetTypeInfoList[$vals['id']] = [];
        }
        $budgetTypeInfoList[$vals['id']] = $vals;
    }
    return $budgetTypeInfoList;
}

function getBudgetTypeCodeByOptions($id = null)
{
    $budgetTypeList = [];
    $query = BudgetType::query();
    $query->where('budget_type_status', '!=', config('constants.status.Deleted'));
    $query->whereNotNull('budget_type_code');
    if ($id) {
        $query->where('id', $id);
        $resData = $query->pluck('budget_type_code', 'id');
        $budgetTypeList = $resData[$id];
    } else {
        $budgetTypeList = $query->pluck('budget_type_code', 'id');
    }
    return $budgetTypeList;
}

function getExternalBudgetTypeList()
{
    $budgetTypeList = [];
    $query = BudgetType::query();
    $query->where('budget_type', '=', config('constants.budget_type.LocalUse'));
//    $query->whereNotNull('budget_type_code');
    $localUseBudgetTypeList = $query->pluck('budget_type_name', 'id');
//    dd($localUseBudgetTypeList);

    return $localUseBudgetTypeList;
}

function getFinancialYearList()
{
    $start = 2020;
    $curYer = date("Y");
    $yearListArr = [];
    for ($year = $start; $year <= $curYer; $year++) {
        $yearListArr[$year] = $year . " - " . ($year + 1);
    }
    return $yearListArr;
}

function updateOfficeBudgetBalance($budget_id, $budget_state = true)
{ // bidget_state determined the add or subtraction
    if (!empty($budget_id)) {
        DB::beginTransaction();
        try {
            $finalResData = false;
            if ($budget_state) {
//                1. Fetch Data From office_budgets Table
                $officeBudgetModel = new OfficeBudget();
                $officeBudgetBalanceModel = new OfficeBudgetBalance();

                $officeBudgetData = $officeBudgetModel::where('id', $budget_id)
                    ->first()
                    ->toArray();

                $getPreviousSavedData = $officeBudgetBalanceModel::where('financial_year', $officeBudgetData['financial_year'])
                    ->where('budget_type', $officeBudgetData['budget_type']) //  budget_type =  budget_types table 'id', office_budgets table 'budget_type';
                    ->where('institute_id', $officeBudgetData['institute_id'])
                    ->first();

//                2. Prepare JSON Data
                $codeWiseData = json_decode($officeBudgetData['budget_details']);

//            3. Prepare Result Data Array
                $total_balance_json = $usable_balances_json = [];
                $total_balance = $usable_balances = 0;
                $updateData = [];

                foreach ($codeWiseData as $codes => $values) {
                    $total_balance_json[$codes] = $values->amount;
                    $usable_balances_json[$codes] = $values->useable_amount;
                    $total_balance += $values->amount;
                    $usable_balances += $values->useable_amount;
                }

//            4. Add or Deduct The Budget by Code
                if (!empty($getPreviousSavedData)) { // if previous data exists in Office Budget Balance
//                Budget Add
                    $flag = 1;
                    if (!$budget_state) {
                        $flag = -1;
                    }

                    $decodedUpdatableBalancesData = json_decode($getPreviousSavedData->total_updatable_balances, true);

                    $decodedData = json_decode($getPreviousSavedData->total_balances, true);
                    $decodedUsableData = json_decode($getPreviousSavedData->usable_balances, true);

                    foreach ($total_balance_json as $code => $amount) {
                        if (array_key_exists($code, $decodedData)) { // $decodedData = office budget balance total_amount and if office budget code is in office budget balance data
                            $total_balance_json[$code] = $decodedData[$code] + ($amount * $flag);
                        } else { // if office budget code is not in office budget balance data
                            $total_balance_json[$code] = $amount * $flag;
                        }
                    }
                    foreach ($usable_balances_json as $ucode => $uamount) {
                        if (array_key_exists($ucode, $decodedUsableData)) { // $decodedUsableData = office budget
                            // balance usable_balance and if office budget code is in office budget balance data
                            $usable_balances_json[$ucode] = $decodedUsableData[$ucode] + ($uamount * $flag);
                        } else { // if office budget code is in not in office budget balance data
                            $usable_balances_json[$ucode] = $uamount * $flag;
                        }
                    }

                    $decodedUpdatableBalancesData = [
                        "total_balance" => $decodedUpdatableBalancesData['total_balance'] + $total_balance,
                        "usable_balance" => $decodedUpdatableBalancesData['usable_balance'] + $usable_balances,
                    ];
//                    $usable_balances = $decodedUpdatableBalancesData['usable_balance'] + $usable_balances;

                    $getPreviousSavedData->total_updatable_balances = json_encode($decodedUpdatableBalancesData);
                    $getPreviousSavedData->total_balances = json_encode($total_balance_json);
                    $getPreviousSavedData->usable_balances = json_encode($usable_balances_json);

                    /*  Update the Budget Balance Table Data    */
                    $getPreviousSavedData->save();

                } else {
                    $officeBudgetBalanceModel->financial_year = $officeBudgetData['financial_year'];
                    $officeBudgetBalanceModel->budget_type = $officeBudgetData['budget_type'];
//                    $officeBudgetBalanceModel->institute_id = auth()->user()->institute_id;
                    $officeBudgetBalanceModel->institute_id = $officeBudgetData['institute_id'];
                    $officeBudgetBalanceModel->created_by = auth()->user()->id;

                    $decodedUpdatableBalancesData = [
                        "total_balance" => $total_balance,
                        "usable_balance" => $usable_balances,
                    ];
//                    pr($decodedUpdatableBalancesData);
//                    dd($decodedUpdatableBalancesData);

                    $officeBudgetBalanceModel->total_updatable_balances = json_encode($decodedUpdatableBalancesData);

                    $officeBudgetBalanceModel->total_balances = json_encode($total_balance_json);
                    $officeBudgetBalanceModel->usable_balances = json_encode($usable_balances_json);

                    /*  Update the Budget Balance Table Data    */
                    $officeBudgetBalanceModel->save();
                }

//            5. Prepare Data For Internal Budget Balance Table
//                dd($officeBudgetData);
//                $resData = [
//                    'financial_year' => $officeBudgetData['financial_year'],
//                    'total_balances' => $decodedUpdatableBalancesData['usable_balance'],
//                    'institute_id' => $officeBudgetData['institute_id'],
//                ];

                $resData = [
                    'financial_year' => $officeBudgetData['financial_year'],
                    'total_balances' => $usable_balances,
                    'institute_id' => $officeBudgetData['institute_id'],
                ];

                if ($officeBudgetData['budget_type'] === config('constants.budget_type.LocalUse')) {
                }


                $finalResData = updateInternalBudgetBalance($resData, $officeBudgetData['financial_year'], $budget_state);

            } else {
                pr('else');
                $officeBudgetExpenseModel = new OfficeBudgetExpense();
                $officeBudgetBalanceModel = new OfficeBudgetBalance();

                $officeBudgetExpenseData = $officeBudgetExpenseModel::findOrFail($budget_id)->toArray();

                $getPreviousExpData = $officeBudgetExpenseModel::select(DB::raw('sum(prev_expense) as prevExp,sum(current_exp) as curRxp, sum(total_expense) as totExp'))
                    ->where('financial_year', $officeBudgetExpenseData['financial_year'])
                    ->where('budget_type', $officeBudgetExpenseData['budget_type'])
                    ->where('institute_id', $officeBudgetExpenseData['institute_id'])
                    ->where('status', config('constants.budget_status.Approved'))
                    ->first();


                dd($officeBudgetExpenseData);


                $getPreviousSavedData = $officeBudgetBalanceModel::where('financial_year', $officeBudgetExpenseData['financial_year'])
                    ->where('budget_type', $officeBudgetExpenseData['budget_type'])
                    ->where('institute_id', $officeBudgetExpenseData['institute_id'])
                    ->first();

                $total_balances = json_decode($getPreviousSavedData->total_balances, true);
                $usable_balances = json_decode($getPreviousSavedData->usable_balances, true);

                $budgetCodeArr = getBudgetCodeById($officeBudgetExpenseData['budget_code']);

                $budgetCode = $budgetCodeArr['budget_code'];

                /*  Update Office Budget Balance Data   */
                if (array_key_exists($budgetCode, $total_balances)) { // Update total balance
                    $total_balances[$budgetCode] = $total_balances[$budgetCode] - $officeBudgetExpenseData['total_expense'];
                    $total_balances = json_encode($total_balances);
                }
                if (array_key_exists($budgetCode, $usable_balances)) { // Update total usable balance
                    $usable_balances[$budgetCode] = $usable_balances[$budgetCode] - $officeBudgetExpenseData['total_expense'];
                    $usable_balances = json_encode($usable_balances);
                }

                $getPreviousSavedData->total_balances = $total_balances;
                $getPreviousSavedData->usable_balances = $usable_balances;

                $getPreviousSavedData->save();

                $resData = [
                    'financial_year' => $officeBudgetExpenseData['financial_year'],
                    'total_balances' => $officeBudgetExpenseData['total_expense'],
                    'institute_id' => auth()->user()->institute_id,
                    'created_by' => auth()->user()->id,
                ];

                $finalResData = updateInternalBudgetBalance($resData, $officeBudgetExpenseData['financial_year'], false);
            }

            if ($finalResData) {
                DB::commit();
                return true;
            } else {
                DB::rollback();
                return false;
            }
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 400);
//            return redirect()->back()->with('error', 'Operation failed: ' . $exception->getMessage());
//            dd($e);
//            return false;
        }
    }
}

function updateOfficeBudgetExpense($expanseId, $status)
{
    try {


        if ($status = config('constants.budget_status.Approved')) {
            $officeBudgetExpenseModel = new OfficeBudgetExpense();
            $officeBudgetExpenseData = $officeBudgetExpenseModel::findOrFail($expanseId)->toArray();

            $getPreviousExpData = $officeBudgetExpenseModel::select(DB::raw('sum(prev_expense) as prevExp,sum(current_exp) as curExp, sum(total_expense) as totExp'))
                ->where('financial_year', $officeBudgetExpenseData['financial_year'])
                ->where('budget_type', $officeBudgetExpenseData['budget_type'])
                ->where('institute_id', $officeBudgetExpenseData['institute_id'])
                ->where('status', config('constants.budget_status.Approved'))
                ->get()
                ->toArray();

            $getPreviousExpData = $getPreviousExpData[0];
            if (empty($getPreviousExpData['curExp'])) { // dont have previous approved data
                $getPreviousExpData['curExp'] = 0;
                $getPreviousExpData['prevExp'] = 0;
                $getPreviousExpData['totExp'] = 0;
                $updateDataArr = [
                    'total_expense' => $officeBudgetExpenseData['current_exp'],
                    'status' => $status,
                ];
            } else { // Have previous approved data
                $prevExp = $getPreviousExpData['curExp'];
                $totExp = $getPreviousExpData['curExp'] + $officeBudgetExpenseData['current_exp'];

                $updateDataArr = [
                    'total_expense' => $totExp,
                    'prev_expense' => $prevExp,
                    'status' => $status,
                ];
            }
        } else {
            $updateDataArr = [
                'status' => $status,
            ];
        }


//        pr($getPreviousExpData);
//        pr($updateDataArr);
//        dd($officeBudgetExpenseData);
        return $updateDataArr;
    } catch (Exception $exception) {
    }
}

function updateInternalBudgetBalance($dataArr, $financial_year, $budget_state = true)
{
    $updateData = [];
    try {
//        throw new \Exception('From the Internal budget balance function....');
        DB::beginTransaction();
        $internalBudgetBalanceModel = new InternalBudgetBalance();
        $getPreviousSavedData = $internalBudgetBalanceModel->where('financial_year', $financial_year)
            ->where('institute_id', $dataArr['institute_id'])
            ->first();

        if (!empty($getPreviousSavedData)) {
            $flag = 1;
            if (!$budget_state) {
                $flag = -1;
            }

//            Internal Budget Update
            $getPreviousSavedData->total_balances = $getPreviousSavedData->total_balances + ($dataArr['total_balances'] * $flag);
            $getPreviousSavedData->updated_by = auth()->user()->id;
            $getPreviousSavedData->save();
        } else {
//            Internal Budget Insert
            $internalBudgetBalanceModel->financial_year = $dataArr['financial_year'];
            $internalBudgetBalanceModel->total_balances = $dataArr['total_balances'];
            $internalBudgetBalanceModel->institute_id = $dataArr['institute_id'];
            $internalBudgetBalanceModel->created_by = auth()->user()->id;

            $internalBudgetBalanceModel->save();
        }

        DB::commit();
        return true;
    } catch (Exception $exception) {
        DB::rollback();
        return redirect()->back()->with('error', 'Operation failed: ' . $exception->getMessage());
//        $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
//        return response()->json(['status' => 'error0']);
//        dd($exception);
//        return false;
    }
}

function getInternalBalanceByOption($id = null, $status = null, $isPluck = false)
{
    $query = InternalBudget::query();
    $budgetTypeList = [];
    if ($status) {
        if ($status == config('constants.budget_status.Pending')) {
            $query->where('status', config('constants.budget_status.Pending'));
        } elseif ($status == config('constants.budget_status.Approved')) {
            $query->where('status', config('constants.budget_status.Approved'));
        } elseif ($status == config('constants.budget_status.Rejected')) {
            $query->where('status', config('constants.budget_status.Rejected'));
        }
    } else {
        $query->where('status', '!=', config('constants.budget_status.Rejected'));
    }
    if ($id) {
        $query->where('id', $id);
    }

    if ($isPluck) {
        $budgetTypeList = $query->pluck('total_amount', 'id');
    } else {
        $budgetTypeList = $query->get();
    }

    return $budgetTypeList;
}

function getInternalBalanceExpenseByOption($id = null, $status = null, $isPluck = false)
{
    $query = InternalBudgetExpense::query();
    $budgetTypeList = [];
    if ($status) {
        if ($status == config('constants.budget_status.Pending')) {
            $query->where('status', config('constants.budget_status.Pending'));
        } elseif ($status == config('constants.budget_status.Approved')) {
            $query->where('status', config('constants.budget_status.Approved'));
        } elseif ($status == config('constants.budget_status.Rejected')) {
            $query->where('status', config('constants.budget_status.Rejected'));
        }
    } else {
        $query->where('status', '!=', config('constants.budget_status.Rejected'));
    }
    if ($id) {
        $query->where('id', $id);
    }

    if ($isPluck) {
        $budgetTypeList = $query->pluck('total_price', 'id');
    } else {
        $budgetTypeList = $query->get();
    }

    return $budgetTypeList;
}

function getOfficeBudgetBalanceListByOptions($year = false, $type = false, $codeId = false, $instituteId = false)
{
//    pr($year);
//    pr($type);
//    pr($codeId);

    $officeBudgetBalanceModel = new OfficeBudgetBalance();
    $query = $officeBudgetBalanceModel::query();
    if ($year) {
        $query->where('financial_year', $year);
    }
    if ($type) {
        $query->where('budget_type', $type);
    }

    if ($instituteId) {
        $query->where('institute_id', $instituteId);
    }

//    $resData = $query->toSql();
    $resData = $query->get()->toArray();
//    dd($resData);
    try {
        if (!empty($resData)) {
            $resData = $resData[0];
//            \Log::channel('custLog')->info($resData);
            $total_balances = json_decode($resData['total_balances'], true);
            $usable_balances = json_decode($resData['usable_balances'], true);

//            pr($total_balances);
//            pr($usable_balances);

            if ($codeId) {
//                \Log::channel('custLog')->info($codeId);
                $getPreviousData = getPreviousOfficeExpanseByOptions($year, $type, $codeId, $instituteId);
                $codArr = getBudgetCodeById($codeId);
                $code = $codArr['full_budget_code'];
//                dd($getPreviousData);

                if (!empty($getPreviousData)) {
                    $dataArrPrev = $getPreviousData;
                    $getPreviousData = $getPreviousData[0];
                    $resData['total_balances'] = $total_balances[$code];
                    $resData['usable_balances'] = $usable_balances[$code] - $getPreviousData['totalExp'];
                    $resData['real_usable_balances'] = $usable_balances[$code];
                    $resData['total_exp'] = $getPreviousData['totalExp'];
                    $resData['total_prev_exp'] = $getPreviousData['realPrevExp'];
                    // Have to calculate Previous all data
//                    pr($dataArrPrev);
//                    getTotalPrevAllExp($dataArrPrev);
//                    dd("sssss");

                } else {
                    if (array_key_exists($code, $total_balances)) {
                        $resData['total_balances'] = $total_balances[$code];
                    }
                    if (array_key_exists($code, $usable_balances)) {
                        $resData['usable_balances'] = $usable_balances[$code];
                        $resData['real_usable_balances'] = $usable_balances[$code];
                    }
                    $resData['total_exp'] = 0;
                    $resData['prev_expense'] = 0;
                }
            }
        }
    } catch
    (Exception $exception) {
//        dd($exception->getMessage(), $exception->getCode());
    }
    return $resData;
}

function getPreviousOfficeExpanseByOptions($year, $type, $codeId, $instituteId)
{
    try {

        $officeExpenseModel = new OfficeBudgetExpense();
//        $codArr = getBudgetCodeById($codeId);
//        $code = $codArr['budget_code'];

//        $resData = $officeExpenseModel->select(DB::raw('sum(total_expense) as totalExp,sum(prev_expense) as totalPrevExp, useable_expense'))
        $resData = $officeExpenseModel->select(DB::raw('sum(total_expense) as totalExp,sum(current_exp) as realPrevExp,sum(prev_expense) as totalPrevExp, useable_expense'))
            ->where('status', config('constants.budget_status.Approved'))
            ->where('budget_type', $type)
            ->where('financial_year', $year)
            ->where('budget_code', $codeId)
            ->where('institute_id', $instituteId)
            ->groupBy('budget_code')
            ->get()
            ->toArray();
//        pr($type);
//        pr($year);
//        pr($code);
//        pr($codeId);
//        pr($instituteId);
//        dd($resData);

        return $resData;
    } catch (Exception $exception) {
//        dd($exception->getMessage());
    }
}

function getTotalPrevAllExp($dataArr = null)
{
    if (!empty($dataArr)) {
        $prevExpSum = $totalExpSum = 0;
        foreach ($dataArr as $key => $val) {
            pr($val);
            $prevExpSum += $val['totalExp'];
            $totalExpSum += $val['totalExp'];
        }
    }
}

function updateLedger($formData, $action, $isInclude = false, $previousBudgetDetailsData = false)
{
    $officeBudgetModel = new OfficeBudget();
    $officeLedgerModel = new Ledger();

//    $query = $officeBudgetModel::query();
//    $resData = $query->where('id', $officeBudgetId)->get()->toArray();
    $resData = $formData;
    $financial_year = $resData['financial_year'];
    $budget_type = $resData['budget_type'];
    $institute_id = $resData['institute_id'];
    $budget_details = json_decode($resData['budget'], true);
    if ($previousBudgetDetailsData) { // If Previous data passed for the same budget ID
        $previousBudgetDetailsData = json_decode($previousBudgetDetailsData, true);
    }

    $ledgerData = $officeLedgerModel->where('financial_year', $financial_year)
        ->where('budget_type', $budget_type)
        ->where('institute_id', $institute_id)
        ->get()->toArray();

    $newLedgerArray = [];
    if (!empty($ledgerData)) { // If ledger data is not empty. Means has previous entry
        foreach ($ledgerData as $item)
            $newLedgerArray[$item['code']] = $item;
    }

    try {
        foreach ($budget_details as $code => $budgetValues) {
            if (!empty($ledgerData)) { // If multiple budgets of the same type are submitted in the same financial year or any budget is revised in Edit.
                if (array_key_exists($code, $newLedgerArray)) { // If all budget codes are inserted in the ledger and no need to add extra code in the existing ledger data table
                    $updatedData = [];
                    $prvBgtDtlData = false;
                    if ($previousBudgetDetailsData) {
                        $prvBgtDtlData = $previousBudgetDetailsData[$code];
                    }

                    if ($action == config('constants.budget_add_remove.Add')) { // We know amount will be added
//                        $updatedData = addLedgerData($resData, $code, $budgetValues);
                        $updatedData = updateLedgerData($newLedgerArray[$code], $code, $budgetValues, $isInclude, $prvBgtDtlData);
                    }
                    if ($action == config('constants.budget_add_remove.Deduction')) { // We know amount will be Deducted
                    }
                    if ($action == config('constants.budget_add_remove.Edit')) { // No Idea. Have to calculate whether it will be added or deducted
//                    pr($previousBudgetDetailsData);
                        $updatedData = updateLedgerData($newLedgerArray[$code], $code, $budgetValues, $isInclude, $prvBgtDtlData);
                    }
                    if (!empty($updatedData)) {
                        $officeLedgerModel::where('code', $code)
                            ->where('financial_year', $financial_year)
                            ->where('budget_type', $budget_type)
                            ->where('institute_id', $institute_id)
                            ->update($updatedData);
                    }
                } else {
                    // If all budget codes are not inserted in the ledger and need to add extra code in the existing ledger data table
                    if ($action == config('constants.budget_add_remove.Add')) {
                        $updatedData = addLedgerData($resData, $code, $budgetValues); // Last inserted budget data array, budget code and budget details array values
                    }
                    $officeLedgerModel::create($updatedData);
                }
            } else {
                // Fully New Fresh Budget entry. No previous ledger data
                if ($action == config('constants.budget_add_remove.Add')) {
                    $updatedData = addLedgerData($resData, $code, $budgetValues);
                }
                $officeLedgerModel::create($updatedData);
            }
        }
        return true;
    } catch (Exception $exception) {
        dd($exception);
        return redirect('/office-budget')->with('error', $exception->getMessage());
//        return false;
    }
}

function addLedgerData($officeBudgetDataArr, $code, $budgetDetailsArr)
{
    $dataArr = [
        'financial_year' => $officeBudgetDataArr['financial_year'],
        'budget_type' => $officeBudgetDataArr['budget_type'],
        'code' => $code,
        'institute_id' => $officeBudgetDataArr['institute_id'],
        'vat' => $budgetDetailsArr['vat'],
        'tax' => $budgetDetailsArr['tax'],
        'amount' => $budgetDetailsArr['amount'],
        'usable_amount' => $budgetDetailsArr['useable_amount'],
        'total_previous_amount' => 0,
        'total_current_amount' => $budgetDetailsArr['useable_amount'],
        'status' => config('constants.ledger_status.Running')
    ];
    return $dataArr;
}

function updateLedgerData($ledgerDataArr, $code, $budgetDetailsArr, $isInclude = false, $prvBgtDtlData = false)
{
    $isPositive = true;

    /*  New Budget Data */
    $vat = $budgetDetailsArr['vat'];
    $tax = $budgetDetailsArr['tax'];
    $amount = $budgetDetailsArr['amount'];
    $usable_amount = $budgetDetailsArr['useable_amount'];

    /*  Ledger Budget Data */
    $lvat = $ledgerDataArr['vat'];
    $ltax = $ledgerDataArr['tax'];
    $lamount = $ledgerDataArr['amount'];
    $usable_lamount = $ledgerDataArr['usable_amount'];

    /*  Previous Budget Data */
    $pvat = ($prvBgtDtlData ? $prvBgtDtlData['vat'] : -1);
    $ptax = ($prvBgtDtlData ? $prvBgtDtlData['tax'] : -1);
    $pamount = ($prvBgtDtlData ? $prvBgtDtlData['amount'] : -1);
    $usable_pamount = ($prvBgtDtlData ? $prvBgtDtlData['useable_amount'] : -1);

    $total_previous_amount = $ledgerDataArr['total_previous_amount'];
    $total_current_amount = $ledgerDataArr['total_current_amount'];

    if (($vat == $pvat) && ($tax == $ptax) && ($pamount == $amount) && ($usable_pamount == $usable_amount)) {
        return array();
    } else {
        if (!empty($amount)) {
            $total_previous_amount = $total_current_amount;
            if ($isInclude) {
                /*  Updated Ledger Amount (Current Ledger Amount + New Amount)    */
                $lamount = $lamount + $amount; // ledger amount + new amount
                $usable_lamount = $usable_lamount + $usable_amount; // ledger usable amount + new usable amount
                $total_current_amount = $total_current_amount + $usable_amount; // New usable ledger amount = current useable ledger amount + new usable amount
            } else {
                /*  Updated Ledger Amount (Current Ledger Amount - Previous Amount + New Amount)    */
                $lamount = $lamount - $pamount + $amount; // ledger amount - previous amount + new amount
                $usable_lamount = $usable_lamount - $usable_pamount + $usable_amount; // ledger usable amount - previous usable amount + new usable amount
                $total_current_amount = $total_current_amount - $usable_pamount + $usable_amount; // New usable ledger amount = current useable ledger amount - previous usable amount + new usable amount
            }
        }
    }


    $dataArr = [
        'financial_year' => $ledgerDataArr['financial_year'],
        'budget_type' => $ledgerDataArr['budget_type'],
        'code' => $code,
        'institute_id' => $ledgerDataArr['institute_id'],
        'vat' => $vat,
        'tax' => $tax,
        'amount' => $lamount,
        'usable_amount' => $usable_lamount,
        'total_previous_amount' => $total_previous_amount,
        'total_current_amount' => $total_current_amount,
        'status' => config('constants.ledger_status.Running')
    ];
    return $dataArr;
}

function hasPreviousLedgerData()
{
}

function userDataAccess($query, $postData = null)
{

    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
        $query->where('created_by', '=', auth()->user()->id)
            ->where('institute_id', '=', auth()->user()->institute_id);
    }

//    $institute_id = auth()->user()->institute_id;
//    $userType = auth()->user()->user_type;
//
////    if()
//
//    if ($institute_id >= 1) {
//        if (!empty($postData)) {
//            if (!empty($postData['institute_id'])) {
//                $query->where('institute_id', $postData['institute_id']);
//            }
//        } else if (!empty($institute_id)) {
//            $query->where('institute_id', $institute_id);
//        }
//    }

    return $query;
}

function fileUpload($fileRequest, $path, $fileName)
{
    try {
        // Create the directory if it doesn't exist
        if (!File::exists(public_path($path))) {
            File::makeDirectory(public_path($path), 0755, true);
        }

        $originalFileExt = $fileRequest->getClientOriginalExtension();
        $newFilename = $fileName . '.' . $originalFileExt;

        if (File::exists(public_path($path . '/' . $newFilename))) {
            File::delete(public_path($path . '/' . $newFilename));
        }

        $fileRequest->storeAs($path, $newFilename);
        return $newFilename;
    } catch (Exception $exception) {
//        return $exception->getMessage();
    }
}

function numberToNumberArray($number)
{

// Convert the numeric number to a string
    $numberString = strval($number);

// Split the string into an array of characters
    $charArray = str_split($numberString);

    return $numberString;
}

function numberToBangla($number){
    $numto = new NumberToBangla();
    $text = $numto->bnMoney($number);
    return $text;
}

function getItemListArr($isNested = false){
    $query = Item::query();
    $query->where('item_status','!=',config('constants.status.Deleted'));
    $resData = $query->pluck('item_name', 'id');

    return $resData;
}

function getItemListByBudgetCodeId($id){
    $itemListArr = [];
    $query = Item::query();
    $query->where('item_status', '!=', config('constants.status.Deleted'));
    if ($id) {
        $query->where('item_type', $id);
        $resData = $query->pluck('item_name', 'id');
//        dd($resData);
        $budgetTypeList = $resData;
    } else {
        $budgetTypeList = $query->pluck('item_name', 'id');
    }
    return $budgetTypeList;
}
