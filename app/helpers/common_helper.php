<?php

use App\Permission;
use App\Role;
use App\User;
use App\Models\Applicant;
use App\Models\Land;
use App\Models\District;
use App\Models\Upazila;
use App\Models\Union;
use App\Models\FlatRate;
use App\Models\Tenant;
use Vinkla\Hashids\Facades\Hashids as Hashid;

function pr($array = array())
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}


function bn2en($number)
{
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    return str_replace($bn, $en, $number);
}

function en2bn($number)
{
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    return str_replace($en, $bn, $number);
}

function uniqueNid($nid, $id = null)
{
    try {
        if ($id) {
            Tenant::where('nid', '=', $nid)
                ->where('id', '!=', $id)
                ->firstOrFail();
        } else {
            Tenant::where('nid', '=', $nid)->firstOrFail();
        }
        return false;

    } catch (Exception $exception) {
        return true;
    }
}

function uniquePhone($phone)
{
    try {
        Applicant::where('phone', '=', $phone)->firstOrFail();
        return false;

    } catch (Exception $exception) {
        return true;
    }
}

function encodeId($id)
{
    $idToString = Hashid::encode($id);
    return $idToString[0];
}

function decodeId($string)
{
    $strToId = Hashid::decode($string);
    return $strToId[0];
}

function generateRandomString($length = 10)
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateBillId()
{
    $billId = "INV";
    $billId .= generateRandomString(10);
    $res = uniqueBillId($billId);
    if($res === 400){
        generateBillId();
    }else{
        return $billId;
    }
}



