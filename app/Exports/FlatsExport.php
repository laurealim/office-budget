<?php

namespace App\Exports;

use App\Models\Flat;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class FlatsExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromQuery, WithCustomStartCell
{
    use Exportable;

    public function query()
    {
        // TODO: Implement query() method.
        $flatModel = new Flat();

        $resData = $flatModel->query()
            ->select("flats.*", "users.name as owner", 'districts.bn_name as dist_name', 'upazilas.bn_name as upaz_name', 'tenants.name as tenant_name')
            ->orderBy('status', 'desc')
            ->leftJoin('users', 'flats.created_by', 'users.id')
            ->leftJoin('districts', 'districts.id', 'users.dist')
            ->leftJoin('upazilas', 'upazilas.id', 'users.upa')
            ->leftJoin('tenants', 'tenants.id', 'flats.tenant_id');
        return $resData;
    }

    public function map($flats): array
    {
        // which column have to display
        $facility = "গ্যাস: " . config('constants.flat_facility.' . $flats->gas) . "</br> লিফট: " . config('constants.flat_facility.' . $flats->lift) . "</br> জেনারেটর: " . config('constants.flat_facility.' . $flats->generator) . "</br> গ্যারেজঃ " . config('constants.flat_facility.' . $flats->parking) . "</br> ইন্টারকম: " . config('constants.flat_facility.' . $flats->intercom) . "</br> গিজার: " . config('constants.flat_facility.' . $flats->geyser);

        $desc = "ফ্ল্যাটের সাইজঃ " . $flats->size . "(sq. ft.) </br>" . "ফ্ল্যাটের অবস্থাঃ " . config('constants.flat_floor.' . $flats->floor) . "</br>" . "ফ্ল্যাটের দিকঃ " . config('constants.flat_face.' . $flats->face) . "</br>" . "রুম সংখ্যাঃ " . $flats->room . "</br>" . "বাথরুম সংখ্যাঃ " . $flats->bath . "</br>" . "বারান্দ সংখ্যাঃ " . $flats->veranda;
        $status = config('constants.flat_status.' . $flats->status);

        return [
            $flats->owner,
            $flats->tenant_name,
            $flats->flat_id,
            $flats->flat,
            $flats->address . ',' . $flats->upaz_name . ',' . $flats->dist_name,
            $desc,
            $facility,
            $flats->rent,
            $flats->service_charge,
            $flats->advance_month,
            $status
        ];
    }

    public function headings(): array
    {
        // For Table Heading
        return [
            "ফ্ল্যাটের মালিক",
            "ফ্ল্যাটের ভাড়াটিয়া",
            "ফ্ল্যাট আইডি",
            "ফ্ল্যাট নং",
            "ঠিকানা",
            "ফ্ল্যাটের বিবরণ",
            "ফ্ল্যাটের সুবিধা",
            "মাসিক ভাড়া",
            "সার্ভিস চার্জ",
            "এডভান্স মাস",
            "স্ট্যাটাস",
        ];
    }

    public function registerEvents(): array
    {
        // For table design
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle("A1:I1")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            }
        ];
    }

    public function startCell(): string
    {
        // For Starting Row
        return "A1";
    }

//    /**
//    * @return \Illuminate\Support\Collection
//    */
//    public function collection()
//    {
//        return Flat::all();
//    }
}
