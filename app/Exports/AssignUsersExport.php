<?php

namespace App\Exports;

use App\AssignUser;
use Maatwebsite\Excel\Concerns\FromCollection;

class AssignUsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AssignUser::all();
    }
}
