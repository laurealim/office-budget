<?php

namespace App\Exports;

use App\AssignFlat;
use Maatwebsite\Excel\Concerns\FromCollection;

class AssignFlatsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AssignFlat::all();
    }
}
