<?php

namespace App\Exports;

use App\Models\Tenant;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class TenantsExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromQuery, WithCustomStartCell
{
    use Exportable;

    public function query()
    {
        // TODO: Implement query() method.
        $tenantModel = new Tenant();

        $resData = $tenantModel->query()
            ->select("tenants.*", "unions.bn_name as unions_name", 'districts.bn_name as dist_name', 'upazilas.bn_name as upaz_name')
            ->orderBy('status', 'desc')
            ->leftJoin('districts', 'districts.id', 'tenants.dist')
            ->leftJoin('upazilas', 'upazilas.id', 'tenants.upaz')
            ->leftJoin('unions', 'unions.id', 'tenants.union');
        return $resData;
    }

    public function map($tenants): array
    {

        $union = 'পৌরসভা';
        $gender = 'অন্যান্ন';

        if ($tenants->union >= 1)
        $union = $tenants->unions_name;

        if ($tenants->gender == 1)
            $gender = 'পুরুষ';
        if ($tenants->gender == 2)
            $gender = 'নারী';

        $flatList = getAssignedFlatString($tenants->id);

        $status = config('constants.tenant_status.' . $tenants->status);

        return [
            $tenants->name,
            $tenants->g_name,
            $tenants->nid,
            $tenants->mobile,
            $gender,
            $tenants->dob,
            $tenants->dist_name,
            $tenants->upaz_name,
            $union,
            $tenants->address,
            $flatList['count'],
            $flatList['list'],
            $status
        ];
    }

    public function headings(): array
    {
        // For Table Heading
        return [
            "ভাড়াটিয়ার নাম",
            "অভিবাককের নাম",
            "এন,আই,ডি নং",
            "মোবাইল নং",
            "লিঙ্গ",
            "জন্ম তারিখ",
            "জেলা",
            "উপজেলা",
            "ইউনিয়ন / পৌরসভা",
            "ঠিকানা",
            "ভাড়া ফ্ল্যাটের সংখ্যা",
            "ভাড়া ফ্ল্যাট",
            "স্ট্যাটাস",
        ];
    }

    public function registerEvents(): array
    {
        // For table design
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle("A1:I1")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            }
        ];
    }

    public function startCell(): string
    {
        // For Starting Row
        return "A1";
    }



    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tenant::all();
    }
}
