<?php

namespace App\Exports;

use App\FlatRate;
use Maatwebsite\Excel\Concerns\FromCollection;

class FlatRatesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return FlatRate::all();
    }
}
