<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInternalVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'institute_id' => 'required',
            'year' => 'required',
            'v_date' => 'required',
            'total_cost' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'institute_id.required' => 'প্রতিষ্ঠান বাছাই করুন...',
            'year.required' => 'আর্থিক বছর বাছাই করুন...',
            'v_date.required' => 'তারিখ প্রদান করুন...',
            'total_cost.required' => 'আর্থিক তথ্য প্রদান করুন...',
        ];
    }
}
