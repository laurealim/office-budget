<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOfficeVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'budget_type' => 'required',
            'year' => 'required',
            'budget_code_id' => 'required',
            'institute_id' => 'required',
            'v_date' => 'required',
            'total_cost' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'budget_type.required' => 'বাজেটের ধরণ বাছাই করুন...',
            'year.required' => 'আর্থিক বছর বাছাই করুন...',
            'budget_code_id.required' => 'বাজেটের কোড  বাছাই করুন...',
            'institute_id.required' => 'প্রতিষ্ঠান বাছাই করুন...',
            'v_date.required' => 'তারিখ তথ্য প্রদান করুন...',
            'total_cost.required' => 'আর্থিক তথ্য প্রদান করুন...',
        ];
    }
}
