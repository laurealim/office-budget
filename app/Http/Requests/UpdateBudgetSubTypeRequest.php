<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBudgetSubTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'budget_type_id' => 'required',
            'budget_sub_type_name' => 'required',
            'budget_sub_type_code' => 'required',
            'budget_sub_type_status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'budget_type_id.required' => 'বাজেটের ধরণ বাছাই করুন...',
            'budget_sub_type_name.required' => 'বাজেটের উপ-ধরণ নাম প্রদান করুন...',
            'budget_sub_type_code.required' => 'বাজেটের উপ-ধরণ কোড প্রদান করুন...',
            'budget_sub_type_status.required' => 'অবস্থা বাছাই করুন...',
        ];
    }
}
