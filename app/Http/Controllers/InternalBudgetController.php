<?php

namespace App\Http\Controllers;

use App\Models\InternalBudget;
use App\Models\OfficeBudgetExpense;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class InternalBudgetController extends Controller
{

    public function index(Request $request)
    {
        if (!auth()->user()->can('internal-budget-list')) {
            abort(404);
        }

        $page_title = 'আভ্যন্তরীণ বাজেট তালিকা';

        $internalBudgetModel = new InternalBudget();

        $query = $internalBudgetModel::query();

        $query->orderBy('status', 'asc');
        $query->orderBy('created_at', 'desc');

        $data = $query->get();

        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getInstituteList();
        $financialYearList = getFinancialYearList();
        $budgetTypeCode = getBudgetTypeCodeByOptions();
//        $budgetTypeCodeId = getBudgetTypeCodeByOptions(4);

        if ($request->ajax()) {
//            dd($data);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';

                    if ($row->status != config('constants.budget_status.Rejected')) {
                        if ($row->status == config('constants.budget_status.Pending')) {
                            if (auth()->user()->can('edit-internal-budget')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-internal-budget" href= ' . route('internal-budget.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-internal-budget')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('internal-budget.destroy', $row->id) . ' class="btn btn-danger delete-internal-budget" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-internal-budget')) {
                                $val_app = config('constants.budget_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('internal-budget.internalBudgetApproveReject') . ' class="btn btn-success int_bgt_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                        }
                    }
                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == 0)
                        $color = '#9d1e15';
                    if ($row->status == 2)
                        $color = '#E97311';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.budget_status.' . $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('budget_type', function ($row) use ($budgetTypeListArr) {
                    return $budgetTypeListArr[$row->budget_type];
                })
                ->editColumn('institute_id', function ($row) use ($instituteListArr) {
                    return $instituteListArr[$row->institute_id];
                })
                ->editColumn('financial_year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->financial_year];
                })
                ->addColumn('file_name', function ($row) {
                    $icon = "";
                    if (!empty($row->file_name)) {
                        $path = asset("images/budget/internal_budget/" . $row->id . "/" . $row->file_name);
                        $icon = '<a target="_blank" title="Edit" class="btn btn-info" id="internal-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i></a>';
                    }
                    return $icon;
                })
                ->rawColumns(['action', 'status', 'file_name'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('internal_budget.index', compact('page_title'));
    }


    public function create()
    {
        if (!auth()->user()->can('create-internal-budget')) {
            abort(404);
        }
        $page_title = 'আভ্যন্তরীণ অফিস বাজেট তৈরি';
        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
        $instituteListArr = getInstituteList();
//        $parentItemListArr = getItemNameAttribute();
        return view('internal_budget.create', compact('page_title', 'financialList', 'budgetTypeList'));
    }


    public function store(Request $request)
    {
        if (!auth()->user()->can('create-internal-budget')) {
            abort(404);
        }

        $formData = $request->all();
        $budgetTypeCodeId = getBudgetTypeCodeByOptions($formData['budget_type']);
        $internalBudgetModel = new InternalBudget();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'title' => 'required',
                'financial_year' => 'required',
                'budget_type' => 'required',
                'total_amount' => 'required',
            ], [
                'title.required' => 'বাজেট শিওনাম প্রদান করুন।',
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'total_amount.required' => 'অর্থের পরিমান উল্ল্যেখ করুন।',
            ]);

            $request->request->add(['institute_id' => auth()->user()->institute_id]);

            $request->request->add(['budget_code' => $budgetTypeCodeId]);
//            $request->request->remove('bgt_code');

            $lastInsertedId = $internalBudgetModel->saveData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $budget_id = $lastInsertedId;
                $path = "images/budget/internal_budget/";
                $picName = '';

                // cache the file
                $file = $request->file('file_name');
//                foreach ($files as $key => $file) {
                $destinationPath = $path . $budget_id;
                $file_name = "budget." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                InternalBudget::where('id', $budget_id)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/internal-budget')->with('success', 'অফিস বাজেট খরচ যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/internal-budget/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        if (!auth()->user()->can('edit-internal-budget')) {
            abort(404);
        }
        $page_title = 'আভ্যন্তরীণ অফিস বাজেট সংশোধন';
        $internalBudgetModel = new InternalBudget();

        try {
            $query = $internalBudgetModel::query();
            $query->where('internal_budgets.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('internal_budgets.created_by', '=', auth()->user()->id)
                    ->where('internal_budgets.institute_id', '=', auth()->user()->institute_id);
            }

            $internalBudgetData = $query->firstOrFail();
            $financialList = getFinancialYearList();
            $budgetTypeList = getBudgetTypeList(false);

            return view('internal_budget.edit', compact('page_title', 'internalBudgetData', 'id', 'financialList', 'budgetTypeList'));
        } catch (\Exception $exception) {
            return redirect('/internal_budget')->with('error', $exception->getMessage());
//            return redirect("/office-budget")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }


    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-internal-budget')) {
            abort(404);
        }
        $formData = $request->all();
        $budgetTypeCodeId = getBudgetTypeCodeByOptions($formData['budget_type']);
        $internalBudgetModel = new InternalBudget();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'title' => 'required',
                'financial_year' => 'required',
                'budget_type' => 'required',
                'total_amount' => 'required',
            ], [
                'title.required' => 'বাজেট শিওনাম প্রদান করুন।',
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'total_amount.required' => 'অর্থের পরিমান উল্ল্যেখ করুন।',
            ]);
            $request->request->add(['budget_code' => $budgetTypeCodeId]);

//            $request->request->remove('bgt_code');
//            $request->request->remove('total_cur_balance');
            $lastInsertedId = $internalBudgetModel->updateData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "images/budget/internal_budget/";
                $picName = '';

                // cache the file
                $file = $request->file('file_name');
//                foreach ($files as $key => $file) {
                $destinationPath = $path . $id;
                $file_name = "budget." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                InternalBudget::where('id', $id)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/internal-budget/')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {

            DB::rollback();
            dd($exception);
            return redirect("/internal-budget/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি...");
        }
    }


    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-internal-budget')) {
            abort(404);
        }
        $internalBudgetModel = new InternalBudget();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.budget_status.Rejected'),
                ];
                $query = $internalBudgetModel::query();
                $query->where('id', '=', $id);
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $query->where('created_by', '=', auth()->user()->id)
                        ->where('institute_id', '=', auth()->user()->institute_id);
                }
                $query->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function internalBudgetApproveReject(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('id');
            $statusValue = $request->post('value');
            $internalBudgetModel = new InternalBudget();

            $intDataArr = getInternalBalanceByOption($id);
            if(empty($intDataArr)){
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error']);
            }

            $intDataArr = $intDataArr->toArray();
            $intDataArr = $intDataArr[0];
            $financial_year = $intDataArr['financial_year'];
            $total_balances = $intDataArr['total_amount'];

            $resData = [
                'financial_year' => $financial_year,
                'total_balances' => $total_balances,
                'institute_id' => auth()->user()->institute_id,
                'created_by' => auth()->user()->id,
            ];
            // only (id,null,null) = [0=>[data]]
            // only (null,status,null) = [0=>[data],1=>[data],2=>[data]]
            // only (null,null,true) = [1=>1000,3=>5000,4=>2000]
            // only (null,null,null) = [0=>[data],1=>[data],2=>[data]]


            DB::beginTransaction();
            try {
                $resultData = updateInternalBudgetBalance($resData, $financial_year);
                if ($resultData) {
                    $updateData = [
                        'status' => $statusValue,
                    ];

                    try {
                        $query = $internalBudgetModel::query();
                        $query->where('id', '=', $id);
                        $query->update($updateData);
                        DB::commit();

                        $request->session()->flash('success', 'সফলভাবে অনুমোদন করা হয়েছে...');
                        return response()->json(['status' => 'success']);
                    } catch (\Exception $exception) {
                        DB::rollback();
                        $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                        return response()->json(['status' => 'error0']);
                    }
                }else{
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error1']);
                }
            } catch (Exception $e) {
                DB::rollback();
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error2']);
            }
        }
    }
}
