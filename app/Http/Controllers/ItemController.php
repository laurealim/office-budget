<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('item-list')) {
            abort(404);
        }

        $page_title = 'আইটেম এর তালিকা';

        $itemModel = new Item();

        $query = $itemModel::query();

        $query->where('item_status', '!=', config('constants.status.Deleted'))
            ->orderBy('item_status', 'asc')
            ->orderBy('created_at', 'asc');

        $parentItemListArr = getParentItemList();

        $data = $query->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
//                    if ($row->status >= 1) {
                    if (auth()->user()->can('item-edit'))
                        $action = '<a title="Edit" class="btn btn-success" id="edit-item" href= ' . route('items.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                    if (auth()->user()->can('item-delete'))
                        $action = $action . '<a id=' . $row->id . ' href=' . route('items.destroy', $row->id) . ' class="btn btn-danger delete-items" title="Delete"><i class="fas fa-trash-alt"></i></a>';
//                    }
                    return $action;
                })
                ->editColumn('item_status', function ($row) {
                    return config('constants.status.' . $row->item_status);
                })
                ->editColumn('item_type', function ($row) use($parentItemListArr){
                    return $parentItemListArr[$row->item_type];
                })
                ->rawColumns(['action'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('items.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('item-create')) {
            abort(404);
        }
        $page_title = 'আইটেম তৈরি';
        $parentItemListArr = getParentItemList();
//        $parentItemListArr = getItemNameAttribute();
        return view('items.create', compact('page_title','parentItemListArr'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('item-create')) {
            abort(404);
        }
        $formData = $request->all();
        $itemModel = new Item();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'item_type' => 'required',
                'item_name' => 'required',
                'item_status' => 'required',
            ], [
                'item_type.required' => 'আইটেমের ধরন বাছাই করুন...',
                'item_name.required' => 'আইটেমের নাম প্রদান করুন...',
                'item_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $itemModel->saveData($request);

            DB::commit();
            return redirect('/items')->with('success', 'নতুন আইটেম যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/items/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('item-edit')) {
            abort(404);
        }
        $page_title = 'আইটেম এর তথ্য সংশোধন';
//        $countryData = Country::all();

        $itemModel = new Item();
        $parentItemListArr = getParentItemList();

        try {
            $query = $itemModel::query();
            $query->where('id', $id);
//            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                $query->where('flats.created_by', '=', auth()->user()->id);
//            }
            $itemData = $query->firstOrFail();

            return view('items.edit', compact('page_title', 'itemData', 'id','parentItemListArr'));
        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/items")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('item-edit')) {
            abort(404);
        }
        $formData = $request->all();
        $itemModel = new Item();
        $parentItemListArr = getParentItemList();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'item_type' => 'required',
                'item_name' => 'required',
                'item_status' => 'required',
            ], [
                'item_type.required' => 'আইটেমের ধরন বাছাই করুন...',
                'item_name.required' => 'আইটেমের নাম প্রদান করুন...',
                'item_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $itemModel->updateData($request);

            DB::commit();
            return redirect('/items')->with('success', 'সফলভাবে তথ্য সংশোধন করা হয়েছে...।');
        } catch (\Exception $exception) {

//            dd($exception);
            DB::rollback();
            return redirect("/items/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy(Request $request,$id)
    {
        if (!auth()->user()->can('item-delete')) {
            abort(404);
        }
        $itemModel = new Item();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'item_status' => config('constants.status.Deleted'),
                ];

                try {
                    $query = $itemModel::query();
                    $query->where('items.id', '=', $id);
//                    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                        $query->where('created_by', '=', auth()->user()->id);
//                    }
                    $query->update($updateData);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error1']);
                }

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error2']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error3']);
        }
    }
}
