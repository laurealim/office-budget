<?php

namespace App\Http\Controllers;

use App\Models\OfficeVoucher;
use App\Http\Requests\StoreOfficeVoucherRequest;
use App\Http\Requests\UpdateOfficeVoucherRequest;
use Dompdf\Exception;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class OfficeVoucherController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('office-voucher-list')) {
            abort(404);
        }

        $page_title = 'অফিস ভাউচার এর তালিকা';

        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getFullInstituteList();
        $financialYearList = getFinancialYearList();
        $budgetCodeListArr = getBudgetCodeList();
        $itemListArr = getItemListArr();
        $unitList = config('constants.unit_value.arr');

        $officeVoucherModel = new OfficeVoucher();
        $query = $officeVoucherModel::query();

        userDataAccess($query);

        $query->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc');

        $data = $query->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
                    if ($row->status != config('constants.voucher_status.Deleted')) {
                        if ($row->status == config('constants.voucher_status.Pending')) {
                            if (auth()->user()->can('edit-office-voucher')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-office-voucher" href= ' . route('office-voucher.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-office-voucher')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('office-voucher.destroy', $row->id) . ' class="btn btn-danger delete-budget-voucher" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-office-voucher')) {
                                $val_app = config('constants.voucher_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-voucher.budgetVoucherApproveReject') . ' class="btn btn-success bgt_voucher_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                            if (auth()->user()->can('can-reject-office-voucher')) {
                                $val_app = config('constants.voucher_status.Rejected');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-voucher.budgetVoucherApproveReject') . ' class="btn btn-warning bgt_voucher_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-times"></i></a>';
                            }
                        }
                    }
                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == config('constants.voucher_status.Pending'))
                        $color = '#9d1e15';
                    if ($row->status == config('constants.voucher_status.Rejected'))
                        $color = '#E97311';
                    if ($row->status == config('constants.voucher_status.Assigned'))
                        $color = '#E9118A';
                    if ($row->status == config('constants.voucher_status.Deleted'))
                        $color = '#FE0008';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.voucher_status.arr.' .
                            $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('budget_code_id', function ($row) use ($budgetCodeListArr) {
                    return $budgetCodeListArr[$row->budget_code_id];
                })
                ->editColumn('institute_id', function ($row) use ($instituteListArr) {
                    return $instituteListArr[$row->institute_id];
                })
                ->editColumn('year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->year];
                })
                ->editColumn('voucher_items', function ($row) use ($unitList, $itemListArr) {
                    $decodedData = json_decode($row->voucher_items, true);
                    $voucher_items = "<ul><span>";
                    foreach ($decodedData as $key => $vals) {
                        $item = $vals['items'];
                        $unit = $unitList[$vals['unit']];
                        $amount = $vals['amount'];
                        $price = $vals['price'];
                        $total_price = $vals['total_price'];

                        $voucher_items .= "<li><span style='font-weight: bold'>" . $itemListArr[$item] . "</span> মোট " . $total_price . " টাকা। " . "</li>";

                    }
                    $voucher_items .= "</span></ul>";

                    return $voucher_items;

                })
                ->rawColumns(['action', 'status','voucher_items'])
                ->make(true);
        }

        return view('office_voucher.index', compact('page_title'));

    }

    public function create()
    {
        if (!auth()->user()->can('create-office-voucher')) {
            abort(404);
        }

        $page_title = 'অফিস বাজেট ভাউচার';

        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
        $budgetInstituteList = getFullInstituteList();

//        dd($unitList);
//        $parentItemListArr = getItemNameAttribute();
        return view('office_voucher.create', compact('page_title', 'financialList', 'budgetTypeList', 'budgetInstituteList'));
    }

    public function store(StoreOfficeVoucherRequest $request)
    {
        if (!auth()->user()->can('create-office-expense-budget')) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $officeVoucherModel = new OfficeVoucher();
            $formData = $request->all();
            $voucherItemArr = [];

            $validated = $request->validated();
            $validated['created_by'] = auth()->user()->id;
//            pr($formData['voucher']);
            if (!empty($formData['voucher']['items'])) {
                foreach ($formData['voucher']['items'] as $items => $values) {
//                pr($values);
//                foreach ($values as $key => $value) {
//                    pr($key);
                    $voucherItemArr[$items]['items'] = $formData['voucher']['items'][$items];
                    $voucherItemArr[$items]['amount'] = $formData['voucher']['amount'][$items];
                    $voucherItemArr[$items]['unit'] = $formData['voucher']['unit'][$items];
                    $voucherItemArr[$items]['price'] = $formData['voucher']['price'][$items];
                    $voucherItemArr[$items]['total_price'] = $formData['voucher']['total_price'][$items];
//                }
                }
            }
//            dd($voucherItemArr);
            $voucherItemJson = json_encode($voucherItemArr, true);

            $validated['voucher_items'] = $voucherItemJson;
            $validated['status'] = config('constants.voucher_status.Pending');
//            dd($validated);

            $resData = $officeVoucherModel->create($validated);
            $lastInsertedId = $resData->id;

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "public/images/voucher/office_voucher/" . $lastInsertedId;
                $fileName = "office_voucher";
                $fileRequest = $request->file('file_name');

                $voucherName = fileUpload($fileRequest, $path, $fileName);

                OfficeVoucher::where('id', $lastInsertedId)->update(['attachment' => $voucherName]);
            }
            DB::commit();
            return redirect('/office-voucher')->with('success', 'নতুন অতিস ভাউচার যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/office-voucher/create')->with('error', $exception->getMessage());
//            dd($exception->getMessage());
        }
    }

    public function show(OfficeVoucher $officeVoucher)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-office-voucher')) {
            abort(404);
        }

        $page_title = 'অফিস ভাউচার তথ্য সংশোধন';
        $officeVoucherModel = new OfficeVoucher();
        $budgetTypeList = getBudgetTypeList(false);

        try {
            $query = $officeVoucherModel::query();
            $query->where('office_vouchers.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('office_budget_expense.institute_id', '=', auth()->user()->institute_id);
            }

            // get particular data from office_budget_expenses table.
            $officeVoucherData = $query->firstOrFail();
//            $itemArrData = json_decode($officeVoucherData->voucher_items, true);
//            pr($officeVoucherData);

            // get budget code list by budget type from budget_type_code table.
            $budgetCodeList = getBudgetCodeListByBudgetType($officeVoucherData->budget_type);
//            pr($budgetCodeList);

            // Financial Year List
            $financialList = getFinancialYearList();
//            pr($financialList);

            // get Budget Type List form budget_type_lists table.
            $budgetTypeList = getBudgetTypeList(false);
//            pr($budgetTypeList);

            // get full Institute name
            $budgetInstituteList = getFullInstituteList();
//            pr($budgetInstituteList);

            $itemArrarList = getItemListByBudgetCodeId($officeVoucherData->budget_code_id);
            $itemArrarList = $itemArrarList->toArray();


            return view('office_voucher.edit', compact('page_title', 'officeVoucherData', 'id', 'financialList', 'budgetTypeList', 'budgetCodeList', 'budgetInstituteList', 'itemArrarList'));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function update(UpdateOfficeVoucherRequest $request, OfficeVoucher $officeVoucher)
    {
        if (!auth()->user()->can('edit-office-voucher')) {
            abort(404);
        }

        $formData = $request->all();

        DB::beginTransaction();
//        pr($formData);
//        dd($officeVoucher->id);

        try {
            $validatedData = $request->validated();
            $id = $officeVoucher->id;
//            $validatedData['updated_by'] = auth()->user()->id;
//            $validatedData['id'] = $officeVoucher->id;
            $voucherItemArr = [];

            if (!empty($formData['voucher']['items'])) {
                foreach ($formData['voucher']['items'] as $items => $values) {
                    $voucherItemArr[$items]['items'] = $formData['voucher']['items'][$items];
                    $voucherItemArr[$items]['amount'] = $formData['voucher']['amount'][$items];
                    $voucherItemArr[$items]['unit'] = $formData['voucher']['unit'][$items];
                    $voucherItemArr[$items]['price'] = $formData['voucher']['price'][$items];
                    $voucherItemArr[$items]['total_price'] = $formData['voucher']['total_price'][$items];
                }
            }
            $voucherItemJson = json_encode($voucherItemArr, true);
            $validatedData['voucher_items'] = $voucherItemJson;

            $officeVoucher->updated_by = auth()->user()->id;
            $officeVoucher->budget_type = $formData['budget_type'];
            $officeVoucher->budget_code_id = $formData['budget_code_id'];
            $officeVoucher->year = $formData['year'];
            $officeVoucher->v_date = $formData['v_date'];
            $officeVoucher->institute_id = $formData['institute_id'];
            $officeVoucher->total_cost = $formData['total_cost'];
            $officeVoucher->voucher_items = $voucherItemJson;


//            dd($validatedData);
            $officeVoucher->save();

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "public/images/voucher/office_voucher/" . $id;
                $fileName = "office_voucher";
                $fileRequest = $request->file('file_name');

                $voucherName = fileUpload($fileRequest, $path, $fileName);

                OfficeVoucher::where('id', $id)->update(['attachment' => $voucherName]);
            }

            DB::commit();
            return redirect('/office-voucher')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            return redirect("/office-voucher/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি... " . $exception->getMessage());
        }
    }

    public function destroy(OfficeVoucher $officeVoucher)
    {

        if (!auth()->user()->can('delete-office-voucher')) {
            abort(404);
        }
//        pr($officeVoucher);
//        $officeBudgetExpenseModel = new OfficeBudgetExpense();
        DB::beginTransaction();
        try {
            if (!empty($officeVoucher->id)) {
                $updateData = [
                    'status' => config('constants.voucher_status.Deleted'),
                ];
//                $query = $officeBudgetExpenseModel::query();
//                $query->where('id', '=', $id);
//                userDataAccess($query);
//                $officeVoucher->status = config('constants.status.Deleted');
//                dd($officeVoucher);
//                $officeVoucher->save();

                $officeVoucher->update($updateData);
                DB::commit();
                session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function budgetVoucherApproveReject(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('id');
            $value = $request->post('value');
            $officeVoucher = new OfficeVoucher();

            DB::beginTransaction();
            try {
                $maxVal = DB::table('office_vouchers')
                    ->where('status', '!=', 4)
                    ->max('voucher_no');
//                echo $maxVal;

                $voucher_no = $maxVal + 1;

//                pr($voucher_no++);
//                dd($maxVal);
//                dd($maxVal);

                $stateStatus = 'অনুমোদন';

                if ($value == 1) {
                    $updateData = [
                        'status' => config('constants.voucher_status.Approved'),
                        'voucher_no' => $voucher_no,
                    ];
                } elseif ($value == 2) {
                    $stateStatus = 'বাতিল';
                    $updateData = [
                        'status' => config('constants.voucher_status.Rejected'),
                    ];
                }

                try {
                    $query = $officeVoucher::query();
                    $query->where('id', '=', $id);
                    $query->update($updateData);
                    DB::commit();

                    $request->session()->flash('success', 'সফলভাবে ' . $stateStatus . ' করা হয়েছে...');
                    return response()->json(['status' => 'success']);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error0']);
                }
            } catch (Exception $e) {
                DB::rollback();
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error2']);
            }
        }
    }
}
