<?php

namespace App\Http\Controllers;

use App\Exports\FlatsExport;
use App\Models\Applicant;
use App\Models\AssignFlat;
use App\Models\AssignUser;
use App\Models\Bill;
use App\Models\District;
use App\Models\Flat;
use App\Models\FlatRate;
use App\Models\Tenant;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Maatwebsite\Excel\Excel;

class FlatController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('flat-list')) {
            abort(404);
        }

        $page_title = 'ফ্ল্যাটের লিস্ট';
        $flatModel = new Flat();
//        dd(auth()->user()->id);


        $query = $flatModel::query();

        $query->select("flats.*", "users.name as created_by", "flat_rates.flat_rent", "flat_rates.flat_charge")
//            ->where('flats.status', '!=', config('constants.flat_status.Deleted'))
            ->where('flat_rates.status', '!=', config('constants.status.Inactive'))
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'asc')
            ->leftJoin('users', 'flats.created_by', 'users.id')
            ->leftJoin('flat_rates', 'flat_rates.flat_id', 'flats.id');

        if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
            $query->where('flats.created_by', '=', auth()->user()->id);
        }

        $data = $query->get();
//            ->toSql();
//        dd($data);

        $tenantList = Tenant::where('status', '=', config('constants.status.Active'))->pluck('name', 'id');
        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            $unionArr = Union::pluck('bn_name', 'id');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
                    if ($row->status >= 1) {
                        if (auth()->user()->can('edit-flat'))
                            $action = '<a title="Edit" class="btn btn-success" id="edit-user" href= ' . route('flats.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';

                        if ($row->tenant_id >= 1) {
                            if (auth()->user()->can('unassign-tenant'))
                                $action = $action . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-info unAssignTenant" title="Un-Assign Tenant"><i class="fas fa-user-alt-slash"></i></a>';
                        } else {
                            if (auth()->user()->can('delete-flat'))
                                $action = $action . '<a id=' . $row->id . ' href=' . route('flats.destroy', $row->id) . ' class="btn btn-danger delete-flat" title="Delete"><i class="fas fa-trash-alt"></i></a>';

                            if (auth()->user()->can('assign-tenant'))
                                $action = $action . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="btn btn-primary assignTenant" title="Assign Tenant"><i class="fas fa-user-check"></i></a>';
                        }
                    }
                    return $action;
                })
                ->addColumn('facilities', function ($row) {
                    $str = "গ্যাস: " . config('constants.flat_facility.' . $row->gas) . "</br> লিফট: " . config('constants.flat_facility.' . $row->lift) . "</br> জেনারেটর: " . config('constants.flat_facility.' . $row->generator) . "</br> গ্যারেজঃ " . config('constants.flat_facility.' . $row->parking) . "</br> ইন্টারকম: " . config('constants.flat_facility.' . $row->intercom) . "</br> গিজার: " . config('constants.flat_facility.' . $row->geyser);
                    return $str;
                })
                ->addColumn('full_address', function ($row) use ($districtListArr, $upazillaListArr, $unionArr) {
                    $union = 'পৌরসভা';
                    if ($row->union >= 1)
                        $union = $unionArr[$row->union];

                    return $row->address . ", </br>ইউনিয়ন/পৌরসভাঃ " . $union . ", </br> উপজেলাঃ " . $upazillaListArr[$row->upaz] . ", </br> জেলাঃ " . $districtListArr[$row->dist];
                })
                ->addColumn('flat_description', function ($row) {
                    return "ফ্ল্যাটের সাইজঃ " . $row->size . "(sq. ft.) </br>" . "ফ্ল্যাটের অবস্থাঃ " . config('constants.flat_floor.' . $row->floor) . "</br>" . "ফ্ল্যাটের দিকঃ " . config('constants.flat_face.' . $row->face) . "</br>" . "রুম সংখ্যাঃ " . $row->room . "</br>" . "বাথরুম সংখ্যাঃ " . $row->bath . "</br>" . "বারান্দ সংখ্যাঃ " . $row->veranda;
                })
                ->editColumn('status', function ($row) {
                    return config('constants.flat_status.' . $row->status);
                })
                ->rawColumns(['action', 'facilities', 'full_address', 'flat_description'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('flats.index', compact('page_title', 'tenantList'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-flat')) {
            abort(404);
        }

        $page_title = 'ফ্ল্যাটের তথ্য';
        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
        $unionArr = Union::pluck('bn_name', 'id');
        return view('flats.create', compact('page_title', 'districtListArr', 'upazillaListArr', 'unionArr'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-flat')) {
            abort(404);
        }
        $formData = $request->all();
        $flatModel = new Flat();
        $flatRateModel = new FlatRate();
//        pr($formData);
//        pr($formData['image']);
//        pr($request->file('image'));
//        dd();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'union' => 'required',
                'address' => 'required',
                'face' => 'required',
                'flat' => 'required',
                'floor' => 'required',
                'size' => 'required',
                'room' => 'required',
                'bath' => 'required',
                'veranda' => 'required',
                'rent' => 'required',
                'service_charge' => 'required',
                'advance_month' => 'required',
            ], [
                'dist.required' => 'Need',
                'upaz.required' => 'Need',
                'union.required' => 'Need',
                'address.required' => 'Need',
                'face.required' => 'Need',
                'flat.required' => 'Need',
                'floor.required' => 'Need',
                'size.required' => 'Need',
                'room.required' => 'Need',
                'bath.required' => 'Need',
                'veranda.required' => 'Need',
                'rent.required' => 'Need',
                'service_charge.required' => 'Need',
                'advance_month.required' => 'Need',
            ]);

            $request->request->add(['flat_id' => generateRandomString()]);
            $lastInsertedId = $flatModel->saveData($request);

            $flatRateModel->flat_id = $lastInsertedId;
            $flatRateModel->flat_rent = $formData['rent'];
            $flatRateModel->flat_charge = $formData['service_charge'];
            $flatRateModel->year = date("Y");
            $flatRateModel->month = date("n");
            $flatRateModel->created_by = auth()->user()->id;
            $flatRateModel->save();

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('image')) {
                $flat_id = $lastInsertedId;
                $path = "public/flats";
                $picName = '';

                // cache the file
                $files = $request->file('image');
                foreach ($files as $key => $file) {
                    $destinationPath = 'images/flats/' . $flat_id;;
                    $file_name = time() . "." . $file->getClientOriginalExtension();
                    $picName .= $file_name . ',';
                    $file->move($destinationPath, $file_name);
                }
                Flat::where('id', $lastInsertedId)->update(['pic' => $picName]);
            }

            DB::commit();
            return redirect('/flats')->with('success', 'New Flat added successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/flats/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-flat')) {
            abort(404);
        }
        $page_title = 'ফ্ল্যাট সংশোধন';
//        $countryData = Country::all();
        $flatModel = new Flat();
        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
        $unionArr = Union::pluck('bn_name', 'id');

        try {
            $query = $flatModel::query();
            $query->where('flats.id', $id);
            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('flats.created_by', '=', auth()->user()->id);
            }
            $flatData = $query->firstOrFail();
            $upazilaListSelect = Upazila::where('district_id', $flatData->dist)->pluck('bn_name', 'id')->all();
            $unionListSelect = Union::where('upazilla_id', $flatData->upaz)->pluck('bn_name', 'id')->all();

            return view('flats.edit', compact('page_title', 'flatData', 'id', 'districtListArr', 'upazillaListArr', 'unionArr', 'upazilaListSelect', 'unionListSelect'));
        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/flats")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-flat')) {
            abort(404);
        }
        $formData = $request->all();
        $flatModel = new Flat();
        $flatRateModel = new FlatRate();
//        updateFlateRate($id,$formData['rent'],$formData['service_charge']);
//        pr($formData);
//        pr($formData['image']);
//        pr($request->file('image'));
//        dd();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'union' => 'required',
                'address' => 'required',
                'face' => 'required',
                'flat' => 'required',
                'floor' => 'required',
                'size' => 'required',
                'room' => 'required',
                'bath' => 'required',
                'veranda' => 'required',
                'rent' => 'required',
                'service_charge' => 'required',
                'advance_month' => 'required',
            ], [
                'dist.required' => 'Need',
                'upaz.required' => 'Need',
                'union.required' => 'Need',
                'address.required' => 'Need',
                'face.required' => 'Need',
                'flat.required' => 'Need',
                'floor.required' => 'Need',
                'size.required' => 'Need',
                'room.required' => 'Need',
                'bath.required' => 'Need',
                'veranda.required' => 'Need',
                'rent.required' => 'Need',
                'service_charge.required' => 'Need',
                'advance_month.required' => 'Need',
            ]);
//            dd($id);
//            dd($request->all());

            $lastInsertedId = $flatModel->updateData($request);

            try {
                updateFlateRate($id, $formData['rent'], $formData['service_charge']);
            } catch (\Exception $rentExcp) {
                dd($request);
            }

//            if (!) {
//                throw new \Exception('Can\'t Update Flate Rate Data...');
//            }

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('image')) {
                $flat_id = $id;
                $path = "public/flats";
                $picName = '';

                // cache the file
                $files = $request->file('image');
                foreach ($files as $key => $file) {
                    $destinationPath = 'images/flats/' . $flat_id;;
                    $file_name = time() . "." . $file->getClientOriginalExtension();
                    $picName .= $file_name . ',';
                    $file->move($destinationPath, $file_name);
                }
                Flat::where('id', $id)->update(['pic' => $picName]);
            }

            DB::commit();
            return redirect('/flats')->with('success', 'New Flat Edited successfully');
        } catch (\Exception $exception) {

            dd($exception);
            DB::rollback();
            return redirect("/flats/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-flat')) {
            abort(404);
        }
        $flatModel = new Flat();
        $flatRateModel = new FlatRate();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.status.Deleted'),
                ];

                try {
                    $query = $flatRateModel::query();
                    $query->where('flat_id', '=', $id);
                    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                        $query->where('created_by', '=', auth()->user()->id);
                    }
                    $query->update($updateData);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error']);
                }

                $flatQuery = $flatModel::query();
                $flatQuery->where('id', '=', $id);
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $flatQuery->where('created_by', '=', auth()->user()->id);
                }
                $flatQuery->update($updateData);

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function removeFlatImage(Request $request)
    {
        $file_name = $request->post('imageId');
        $flat_id = $request->post('flatId');
        $file_path = "images/flats/" . $flat_id . '/' . $file_name;
//        dd($request->post());
        DB::beginTransaction();
        try {
            if (file_exists(public_path($file_path))) {

                $flatData = Flat::find($flat_id);
                $images = $flatData->pic;
                $new_image_list = str_replace($file_name . ',', "", $images);
                $flatData->pic = $new_image_list;
                $flatData->save();
//                dd($data->pic);
                unlink(public_path($file_path));
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
//            $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
//            return response()->json(['status' => 'error']);
        }
    }

    public function assignTenant(Request $request, $flat_id = null, $tenant_id = null)
    {

        if (!auth()->user()->can('assign-tenant')) {
            abort(404);
        }
        if ($request->ajax()) {
            DB::beginTransaction();
            try {
                $flatId = $request->get('flat_id');
                $tenantId = $request->get('tenant_id');

                $flatUpdateData = [
                    'tenant_id' => $tenantId,
                    'status' => config('constants.flat_status.Booked'),
                ];

                Flat::where('id', '=', $flatId)->update($flatUpdateData);

                try {
                    assignFlat($tenantId, $flatId);
                } catch (\Exception $exFlar) {
                    DB::rollback();
                    dd($exFlar);
                    return response()->json(['status' => 'error']);
                }
                try {
                    assignUser($tenantId, $flatId);
                } catch (\Exception $exUser) {
                    DB::rollback();
                    dd($exUser);
                    return response()->json(['status' => 'error']);
                }

                DB::commit();
                $request->session()->flash('success', 'ভাড়াটিয়া সফলভাবে এসাইন করা হয়েছে...');
                return response()->json(['status' => 'success']);
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception);
                return response()->json(['status' => 'error']);
            }
        } else {
            $page_title = 'ভাড়াটিয়া এসাইন';
            DB::beginTransaction();
            try {
                $unassignedFlatList = '';
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $unassignedFlatList = getUnassignedFlatList(auth()->user()->id);
                }else{
                    $unassignedFlatList = getUnassignedFlatList();
                }
                $tenantList = getTenantList();

//                pr($unassignedFlatList);
//                pr($tenantList);
            } catch (\Exception $exception) {
                dd($exception);
//            $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }

            return view('flats.assign-tenant', compact('page_title', 'unassignedFlatList', 'tenantList'));
        }
    }

    public function saveAssignTenant(Request $request)
    {

        DB::beginTransaction();
        try {

            $formData = $request->all();
            $flat_id = $formData['flat_id'];
            $tenant_id = $formData['tenant_id'];


            $data = $this->validate($request, [
                'flat_id' => 'required',
                'tenant_id' => 'required',
            ], [
                'flat_id.required' => 'Need',
                'tenant_id.required' => 'Need',
            ]);

            $flatUpdateData = [
                'tenant_id' => $tenant_id,
                'status' => config('constants.flat_status.Booked'),
            ];

            Flat::where('id', '=', $flat_id)->update($flatUpdateData);

            try {
                assignFlat($tenant_id, $flat_id);
            } catch (\Exception $exFlar) {
                DB::rollback();
                dd($exFlar);
                return response()->json(['status' => 'error']);
            }
            try {
                assignUser($tenant_id, $flat_id);
            } catch (\Exception $exUser) {
                DB::rollback();
                dd($exUser);
                return response()->json(['status' => 'error']);
            }
            DB::commit();
            return redirect('/flats/assign-tenant')->with('success', 'ভাড়াটিয়া সফলভাবে এসাইন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            return response()->json(['status' => 'error']);
        }
    }

    public function unAssignTenant(Request $request, $flat_id = null)
    {
        if (!auth()->user()->can('unassign-tenant')) {
            abort(404);
        }
        if ($request->ajax()) {
            $tenantId = 0;
            DB::beginTransaction();
            try {
                $flatId = $request->get('flat_id');

                $flatUpdateData = [
                    'tenant_id' => 0,
                    'status' => config('constants.flat_status.Free'),
                ];

                Flat::where('id', '=', $flatId)->update($flatUpdateData);

                try {
                    $tenantId = unAssignUser($flatId);
                    $tenantId = $tenantId[0];
                } catch (\Exception $exUser) {
                    DB::rollback();
                    dd($exUser);
                    return response()->json(['status' => 'error']);
                }

                try {
                    unAssignFlat($tenantId, $flatId);
                } catch (\Exception $exFlar) {
                    DB::rollback();
                    dd($exFlar);
                    return response()->json(['status' => 'error']);
                }
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception);
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function assignedFlatLists(Request $request)
    {
        if ($request->ajax()) {
//            DB::beginTransaction();
            try {
                $tenant_id = $request->get('tenant_id');

                $assignedFlatList = Flat::where('tenant_id', '=', $tenant_id)
                    ->where('status', '=', config('constants.flat_status.Booked'))
                    ->select('flat_id', 'address', 'flat', 'size', 'rent', 'service_charge')
                    ->get()
                    ->toArray();

//                pr($assignedFlatList);
                $assignedFlatList = json_encode($assignedFlatList, true);
//                dd();

//                DB::commit();
//                $request->session()->flash('success', 'ভাড়াটিয়া সফলভাবে এসাইন করা হয়েছে...');
                return response()->json(['status' => 'success', 'list' => $assignedFlatList]);
            } catch (\Exception $exception) {
//                DB::rollback();
//                dd($exception);
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function export(Excel $excel)
    {
//        return Excel::download(new EmployeeExport, 'Employee_List.xlsx');
//        return (new EmployeeExport)->download('Employee_List.xlsx');
        return $excel->download(new FlatsExport, 'ফ্ল্যাটের_লিস্ট.xlsx');

    }
}
