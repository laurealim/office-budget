<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\District;
use App\Models\Flat;
use App\Models\FlatRate;
use App\Models\Ledger;
use App\Models\Tenant;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
//use Barryvdh\DomPDF\PDF;

class BillController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('bill-list')) {
            abort(404);
        }

        $page_title = 'ইনভয়েসের তালিকা';
        $billModel = new Bill();
        $query = $billModel::query();

        $query->select("bills.*", "tenants.name", "flats.flat_id as flat_code", "flats.flat", "flats.address")
            ->where('flats.status', '=', config('constants.flat_status.Booked'))
            ->where('flats.tenant_id', '!=', config('constants.is_assigned.Unassigned'))
            ->orderBy('bills.status', 'desc')
            ->leftJoin('flats', 'bills.flat_id', 'flats.id')
            ->leftJoin('tenants', 'bills.tenant_id', 'tenants.id');

        if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
            $query->where('flats.created_by', '=', auth()->user()->id);
        }


        $data = $query->get();
//        dd($data);
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
//                    pr($row);
                    $action = '';
                    if ($row->status == config('constants.bill_status.Unpaid')) {
                        $action = '<a class="btn btn-success" title="Edit" id="edit-user" href= ' . route('bills.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
//<a id=' . $row->id . ' href=' . route('flats.assignedFlatLists', $row->id) . ' class="btn btn-info flatList"><i class="fas fa-house-user"></i></a>';
                    }
                    $action .= ' <a id=' . $row->id . ' href=' . route('bills.show', $row->id) . ' class="btn btn-info delete-tenants" title="View"><i class="far fa-eye"></i></a>';
                    if ($row->status != config('constants.bill_status.Full')) {
                        $action .= ' <a id=' . $row->id . ' href=' . route('payments.create', [$row->invoice_code, $row->flat_id, $row->tenant_id]) . ' class="btn bg-purple pay_rent" title="Pay Flat Rent"><i class="fab fa-amazon-pay"></i></a>';
                    }
                    return $action;
                })
                ->addColumn('billing_time', function ($row) {
                    return config('constants.month_list.arr.' . $row->month) . ", " . en2bn($row->year) . ".";
                })
                ->editColumn('bill_status', function ($row) {
                    $colorCode = "#1f2d3d";
                    switch ($row->status) {
                        case 0:
                            $colorCode = '#e83e8c';
                            break;
                        case 1:
                            $colorCode = '#ff851b';
                            break;
                        case 2:
                            $colorCode = '#3d9970';
                            break;
                        default:
                            $colorCode = '#1f2d3d';
                            break;
                    }
                    $status = '<span style="color:' . $colorCode . '">' . config('constants.bill_status.' . $row->status) . '</span>';
//                    return config('constants.bill_status.' . $row->status);
                    return $status;
                })
                ->rawColumns(['action', 'billing_time', 'bill_status'])
                ->make(true);
        }
        return view('bills.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-bill')) {
            abort(404);
        }

        $page_title = 'ইনভয়েস তৈরী';
//        pr(generateBillId());

//        $billableFlatList = Flat::select('flats.flat_id', 'flats.address', 'flats.flat', 'flats.id')
//            ->where('flats.status', '=', config('constants.flat_status.Booked'))
//            ->where('flats.tenant_id', '!=', config('constants.is_assigned.Unassigned'))
//            ->leftJoin('bills', function ($join) use ($currentYear, $currentMonth) {
//                $join->on('flats.id', '=', 'bills.flat_id')
//                    ->on('bills.year', '!=', DB::raw($currentYear))
//                    ->on('bills.month', '!=', DB::raw($currentMonth));
//            })
//            ->groupBy('flats.id','flats.flat_id','flats.flat','flats.address')
//            ->get();

        $userID = auth()->user()->id;
        $billableFlatList = getBillableFlatList($userID);

        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
        $unionArr = Union::pluck('bn_name', 'id');
        return view('bills.create', compact('page_title', 'districtListArr', 'upazillaListArr', 'unionArr', 'billableFlatList'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-bill')) {
            abort(404);
        }

        $billModel = new Bill();
        $formData = $request->all();
        DB::beginTransaction();
        try {
            /*************** Bill Table Input **************/
            $flat_list = $formData;

            $invoiceID = generateBillId();
            $advance_rent = $advance_pay = 0;
            if (!empty($formData['advance_bill'])) {
                $advance_rent = $formData['advance_bill'];
                $advance_pay = config('constants.advance_pay.Billed');
            }

            $billModel->invoice_code = $invoiceID;
            $billModel->flat_id = $formData['flat_list'];
            $billModel->tenant_id = $formData['tenant_id'];
            $billModel->year = $formData['year'];
            $billModel->month = $formData['month'];
            $billModel->rent = $formData['rent'];
            $billModel->s_charge = $formData['s_charge'];
            $billModel->adv_rent = $advance_rent;
            $billModel->other_charge = $formData['other_charge'];
            $billModel->prv_due = $formData['prv_due'];
            $billModel->total_bill = $formData['total_bill'];
            $billModel->cur_due = $formData['total_bill'];
            $billModel->status = config('constants.bill_status.Unpaid');
            $lastBillId = $billModel->save();
            /*************** Bill Table Input **************/

            /*************** Ledger Table Input **************/
            $ledgerModel = new Ledger();
            $ledgerModel->invoice_code = $invoiceID;
            $ledgerModel->flat_id = $flat_list['flat_list'];
            $ledgerModel->tenant_id = $flat_list['tenant_id'];
            $ledgerModel->year = $flat_list['year'];
            $ledgerModel->month = $flat_list['month'];
            $ledgerModel->f_rent = $flat_list['rent'];
            $ledgerModel->f_charge = $flat_list['s_charge'];
            $ledgerModel->adv_rent = $advance_rent;
            $ledgerModel->other_charge = $flat_list['other_charge'];
            $ledgerModel->prv_due = $flat_list['prv_due'];
            $ledgerModel->total_bill = $flat_list['total_bill'];
            $ledgerModel->cur_due_adv = $flat_list['total_bill'] * (-1);
            $ledgerModel->status = config('constants.ledger_status.Unpaid');
            $lastLedgerId = $ledgerModel->save();
            /*************** Ledger Table Input **************/

            /*************** Flats Table Input **************/
            $flatsModel = new Flat();
            $updateArr = [
                'prev_due' => $flat_list["prv_due"],
                'cur_due' => $flat_list["total_bill"] - $flat_list["prv_due"],
                'total_due' => $flat_list["total_bill"],
            ];
            if (!empty($formData['advance_bill'])) {
                $updateArr['advance_pay'] = $advance_pay;
            }

            $resData = $flatsModel->where('id', '=', $flat_list['flat_list'])
                ->where('tenant_id', '=', $flat_list['tenant_id'])
                ->update($updateArr);
            /*************** Flats Table Input **************/
            DB::commit();
            $msg = config('constants.month_list.arr.' . $formData['month']) . ', ' . en2bn($formData['year']) . ' এর বিল তৈরী সম্পন্ন হয়েছে।';
            return redirect('/bills')->with('success', " $msg ");
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/bills/create')->with('error', $exception->getMessage());
//            dd($exception);
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        if (!auth()->user()->can('edit-bill')) {
            abort(404);
        }

        $billModel = new Bill();
        $page_title = 'বিল ইনভয়েস';
        $invoice_title = 'বিল ইনভয়েস';
        try {
            $billData = $billModel->where('bills.id', $id)
                ->leftJoin('tenants', 'bills.tenant_id', 'tenants.id')
                ->leftJoin('flats', 'bills.flat_id', 'flats.id')
                ->leftJoin('users', 'users.id', 'flats.flat_owner')
                ->select('bills.*', 'tenants.name', 'tenants.mobile', 'flats.advance_month', 'flats.advance_bill', 'flats.advance_pay', 'flats.flat', 'flats.address', 'flats.word', 'flats.union', 'flats.upaz', 'flats.dist','users.name as owner_name','users.email','users.phone')
                ->firstOrFail();

//            dd($billData);

//            $billableFlatList = getBillableFlatList();
            return view('bills.show', compact('page_title', 'billData', 'id','invoice_title'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-bill')) {
            abort(404);
        }

        $billModel = new Bill();
        $page_title = 'বিল সংশোধন';
        try {
            $billData = $billModel->where('bills.id', $id)
                ->leftJoin('tenants', 'bills.tenant_id', 'tenants.id')
                ->leftJoin('flats', 'bills.flat_id', 'flats.id')
                ->select('bills.*', 'tenants.name', 'tenants.mobile', 'flats.advance_month', 'flats.advance_bill', 'flats.advance_pay')
                ->firstOrFail();

            $billableFlatList = getBillableFlatList();
            return view('bills.edit', compact('page_title', 'billData', 'id', 'billableFlatList'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-bill')) {
            abort(404);
        }

        $billModel = new Bill();
        $flatModel = new Flat();
        $ledgerModel = new Ledger();

        DB::beginTransaction();
        $formData = $request->all();
        try {
            $billData = $billModel->find($id);
            $flatId = $billData->flat_id;
            $flatData = $flatModel->select('rent', 'service_charge', 'advance_bill', 'monthly_bill')
                ->where('id', '=', $flatId)
                ->get()
                ->toArray();
            $flatData = $flatData[0];

            /*************  Bill Table Update   *************/
            $newTotalBill = $formData['other_charge'] + $flatData['monthly_bill'] + $flatData['advance_bill'];

            $billData->month = $formData['month'];
            $billData->year = $formData['year'];
            $billData->other_charge = $formData['other_charge'];
            $billData->cur_due = $newTotalBill;
            $billData->total_bill = $newTotalBill;
            $billData->save();
            /*************  Bill Table Update   *************/

            /*************  Ledger Table Update   *************/
            $ledgerUpdateArr = [
                'year' => $formData['year'],
                'month' => $formData['month'],
                'other_charge' => $formData['other_charge'],
                'total_bill' => $newTotalBill,
                'cur_due_adv' => $newTotalBill * (-1)
            ];
            $resData = $ledgerModel->where('flat_id', '=', $flatId)
                ->where('tenant_id', '=', $formData['tenant_id'])
                ->update($ledgerUpdateArr);
            /*************  Ledger Table Update   *************/

            /*************  Flat Table Update   *************/
            $updateArr = [
                'prev_due' => $billData["prv_due"],
                'cur_due' => $newTotalBill - $billData["prv_due"],
                'total_due' => $newTotalBill,
            ];
            $resData = $flatModel->where('id', '=', $flatId)
                ->where('tenant_id', '=', $formData['tenant_id'])
                ->update($updateArr);
            /*************  Flat Table Update   *************/

            DB::commit();
            return redirect('/bills')->with('success', 'বিল সংশোধন করা হয়েছে।');
        } catch (\Exception $exception) {

            DB::rollback();
            return redirect("/bills/$id/edit")->with('error', $exception->getMessage());
//            dd($exception);
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy($id)
    {
        //
    }

    public function ajaxGetFlatBillData(Request $request)
    {
        if ($request->ajax()) {
            $flatModel = new Flat();
            $flatId = $request->post('flatId');
            $flatBill = $flatModel->select('flats.id', 'flats.flat_id', 'flats.rent', 'flats.service_charge', 'flats.prev_due', 'flats.advance_month', 'flats.advance_bill', 'flats.advance_pay', 'tenants.name', 'tenants.mobile', 'tenants.id as tenantId')
                ->where('flats.id', '=', $flatId)
                ->where('flats.status', '=', config('constants.flat_status.Booked'))
                ->where('flats.tenant_id', '!=', config('constants.is_assigned.Unassigned'))
                ->leftJoin('tenants', 'flats.tenant_id', 'tenants.id')
                ->get()
                ->toArray();
            $flatBill = $flatBill[0];
            $previousDue = getPreviousDueByFlatAndTenantId($flatId, $flatBill['tenantId']);
            $flatBill['prev_due'] = $previousDue * (-1);
            return json_encode($flatBill);
        }
    }

    public function ajaxGetUnpaidBillDetails(Request $request)
    {
        if ($request->ajax()) {
            $billModel = new Bill();
            $billId = $request->post('billId');
            $unpaidBill = $billModel->select('bills.*', 'tenants.name', 'tenants.mobile', 'tenants.id as tenantId')
                ->where('bills.id', '=', $billId)
                ->where('bills.status', '!=', config('constants.bill_status.Full'))
                ->leftJoin('tenants', 'bills.tenant_id', 'tenants.id')
                ->get()
                ->toArray();
//            pr($unpaidBill);
//            dd();
            $unpaidBill = $unpaidBill[0];
            $unpaidBill['ban_month'] = config('constants.month_list.arr.' . $unpaidBill['month']);
            $unpaidBill['ban_year'] = en2bn($unpaidBill['year']);
            return json_encode($unpaidBill);
        }
    }

    public function ajaxCheckDuplicateBillMonth(Request $request)
    {
        if ($request->ajax()) {
            $month = $request->post('month');
            $year = $request->post('year');
            $flatId = $request->post('flatId');

            $flatName = getFlatNameById($flatId);
            $bnYear = en2bn($year);
            $monthName = config('constants.month_list.arr.' . $month);

            $billModel = new Bill();
            $messageCode = 404;
            $message = "{$flatName} ফ্ল্যাটির {$monthName}, {$bnYear} মাসের বিল পুর্বেই তৈরী করা হয়েছে। ";

            $resdata = $billModel->select('id')
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->where('flat_id', '=', $flatId)
                ->get()
                ->toArray();

            if (empty($resdata)) {
                $messageCode = 200;
                $message = "{$flatName} ফ্ল্যাটির {$monthName}, {$bnYear} মাসের বিল সফল ভাবে তৈরী করা হয়েছে। ";
            }

            return json_encode(['code' => $messageCode, 'message' => $message]);

        }
    }

    public function downloadPDF() {
        $show = [];
        $pdf = PDF::loadView('bills.show', compact('show'));

        return $pdf->download('disney.pdf');
    }


}
