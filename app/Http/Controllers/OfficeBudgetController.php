<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\OfficeBudget;
use App\Models\OfficeBudgetBalance;
use App\Models\Upazila;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\Facade\Pdf;
use Mccarlosen\LaravelMpdf\Facades\LaravelMpdf as MPDF;
//use LPDF;
//use MPDF;

class OfficeBudgetController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('office-budget-list')) {
            abort(404);
        }

        $page_title = 'অফিস বাজেটের তালিকা';

        $officeBudgetModel = new OfficeBudget();

        $query = $officeBudgetModel::query();

        $query->orderBy('status', 'asc')
            ->orderBy('financial_year', 'desc')
            ->orderBy('created_at', 'desc');

//        $parentItemListArr = getParentItemList();
        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getInstituteList();
        $financialYearList = getFinancialYearList();

        $data = $query->get();
//        dd($data);

        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';

                    if ($row->status != config('constants.budget_status.Rejected')) {
                        if ($row->status == config('constants.budget_status.Pending')) {
                            if (auth()->user()->can('edit-office-budget')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-office-budget" href= ' . route('office-budget.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-office-budget')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('office-budget.destroy', $row->id) . ' class="btn btn-danger delete-budget" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-office-budget')) {
                                $val_app = config('constants.budget_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-budge.budgetApproveReject') . ' class="btn btn-success bgt_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                            if (auth()->user()->can('can-reject-office-budget')) {
                                $val_app = config('constants.budget_status.Rejected');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-budge.budgetApproveReject') . ' class="btn btn-warning bgt_reject" value=' . $val_app . ' title="Reject"><i class="fas fa-times"></i></a>';
                            }
                        }
                    }
                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == 0)
                        $color = '#9d1e15';
                    if ($row->status == 2)
                        $color = '#E97311';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.budget_status.' . $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('budget_type', function ($row) use ($budgetTypeListArr) {
                    return $budgetTypeListArr[$row->budget_type];
                })
                ->editColumn('institute_id', function ($row) use ($instituteListArr, $districtListArr, $upazillaListArr) {
                    $instituteInfo = getInstituteNameById($row->institute_id);
                    $upazilla = "";
                    if ($instituteInfo->upaz == -1) {
                        $upazilla = "জেলা কার্যালয়";
                    } else {
                        $upazilla = $upazillaListArr[$instituteInfo->upaz];
                    }
                    return $instituteInfo->office_name . ", " . $upazilla . ", " . $districtListArr[$instituteInfo->dist];
//                    return $instituteInfo->office_name;
                })
                ->editColumn('financial_year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->financial_year];
                })
                ->addColumn('file_name', function ($row) {
                    $icon = "";
                    if (!empty($row->file_name)) {
                        $pathFromEnv = env('BUDGET_FILE_LOCATION');
                        $path = asset($pathFromEnv . "budget/office_budget/" . $row->id . "/" .
                            $row->file_name);
                        $icon = '<a target="_blank" title="Edit" class="btn btn-info" id="edit-office-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i></a>';
                    }
                    return $icon;
                })
                ->rawColumns(['action', 'status', 'file_name', 'institute_id', 'address'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('office_budget.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-office-budget')) {
            abort(404);
        }
        $page_title = 'অফিস বাজেট তৈরি';
        $parentItemListArr = getParentItemList();
        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
//        $budgetInstituteList = getInstituteList();
        $budgetInstituteList = getFullInstituteList();
//        $parentItemListArr = getItemNameAttribute();
        return view('office_budget.create', compact('page_title', 'financialList', 'budgetTypeList', 'budgetInstituteList'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-office-budget')) {
            abort(404);
        }
        $formData = $request->all();
        $officeBudgetModel = new OfficeBudget();
        $officeBudgetBalanceModel = new OfficeBudgetBalance();
        $officeBudgetBalanceData = [];

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'title' => 'required',
                'financial_year' => 'required',
                'budget_type' => 'required',
            ], [
                'title.required' => 'বাজেট শিরনাম প্রদান করুন।',
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
            ]);

            $budget_val = [];
            $budget_balance_val = [];
            foreach ($formData['budget']['id'] as $key => $vals) {
                $budget_code = $formData['budget']['full_budget_code'][$key];

//                $budget_val[$budget_code]['budget_code'] = $formData['budget']['budget_code'][$key];
                $budget_val[$budget_code]['amount'] = $formData['budget']['amount'][$key];
                $budget_val[$budget_code]['vat'] = $formData['budget']['vat'][$key];
                $budget_val[$budget_code]['tax'] = $formData['budget']['tax'][$key];
                $budget_val[$budget_code]['useable_amount'] = $formData['budget']['useable_amount'][$key];
                $budget_balance_val['total_balances'][$budget_code] = $formData['budget']['amount'][$key];
                $budget_balance_val['usable_balances'][$budget_code] = $formData['budget']['useable_amount'][$key];
            }

            $budget_details = json_encode($budget_val);
            $formData['budget'] = $budget_details;
            unset($formData['_token']);
            unset($formData['title']);

            $request->request->add(['institute_id' => $formData['institute_id']]);
            $request->request->add(['budget_details' => $budget_details]);

            /*  Office Budget Balance Table Data Entry  */
//            $officeBudgetBalanceModel->institute_id = $formData['institute_id'];
//            $officeBudgetBalanceModel->institute_id = $formData['institute_id'];
//            $officeBudgetBalanceModel->budget_type = $formData['budget_type'];
//            $officeBudgetBalanceModel->financial_year = $formData['financial_year'];
//            $officeBudgetBalanceModel->total_updatable_balances = json_encode([]);
//            $officeBudgetBalanceModel->total_balances = json_encode($budget_balance_val['total_balances']);
//            $officeBudgetBalanceModel->usable_balances = json_encode($budget_balance_val['usable_balances']);
//            $officeBudgetBalanceModel->created_by = auth()->user()->id;

            $request->request->remove('budget');

//            updateLedger($formData, config('constants.budget_add_remove.Add'), true);
//            $officeBudgetBalanceModel->save();
            $lastInsertedId = $officeBudgetModel->saveData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
//                $budget_id = $lastInsertedId;
//                $path = "images/budget/office_budget/". $lastInsertedId;
//                $picName = '';
//
//                // Create the directory if it doesn't exist
//                if (!File::exists(public_path($path))) {
//                    File::makeDirectory(public_path($path), 0755, true);
//                }
//
//                // cache the file
//                $file = $request->file('file_name');
////                foreach ($files as $key => $file) {
////                $destinationPath = $path .'/'. $lastInsertedId;
//                $file_name = "budget." . $file->getClientOriginalExtension();
//                $picName .= $file_name;
////                $file->move($destinationPath, $file_name);
//                $file->move(public_path($path), $file_name);
////                }

                $path = "public/images/budget/office_budget/" . $lastInsertedId;
                $fileName = "budget";
                $fileRequest = $request->file('file_name');

                $picName = fileUpload($fileRequest, $path, $fileName);

                OfficeBudget::where('id', $lastInsertedId)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/office-budget')->with('success', 'অফিস বাজেট যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/office-budget/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-office-budget')) {
            abort(404);
        }
        $page_title = 'বাজেট সংশোধন';
        $officeBudgetModel = new OfficeBudget();

        try {
            $parentItemListArr = getParentItemList();
            $financialList = getFinancialYearList();
            $budgetTypeList = getBudgetTypeList(false);
//            $budgetInstituteList = getInstituteList();
            $budgetInstituteList = getFullInstituteList();

            $query = $officeBudgetModel::query();
            $query->where('office_budgets.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('office_budgets.created_by', '=', auth()->user()->id)
                    ->where('office_budgets.institute_id', '=', auth()->user()->institute_id);
            }

            $officeBudgetData = $query->firstOrFail();
//            pr($officeBudgetData->toArray());
            $budgetType = $officeBudgetData->budget_type;

            $budget_details = $officeBudgetData->budget_details;
//            pr($budgetType);

            $budgetSubType = getBudgetSubTypeList($budgetType);
            $budgetCodeListArr = getBudgetCodeByBudgetType($budgetType);

            $newBudgetCodeListArr = [];
            foreach ($budgetCodeListArr as $key => $vals) {
                ;
                if (!isset($newBudgetCodeListArr[$vals['bgt_sub_type']])) {
                    $newBudgetCodeListArr[$vals['bgt_sub_type']] = [];
                }
                $newBudgetCodeListArr[$vals['bgt_sub_type']][] = $vals;
            };

//            pr($budget_details);
//            $budgetCodeLists = getBudgetCodeByBudgetType($officeBudgetData->budget_type);
//            pr(arsort($budgetCodeLists));

//            dd($newBudgetCodeListArr);

            return view('office_budget.edit', compact('page_title', 'officeBudgetData', 'id', 'budget_details', 'financialList', 'budgetTypeList', 'budgetInstituteList', 'newBudgetCodeListArr', 'budgetSubType'));
        } catch (\Exception $exception) {
//            DB::rollback();
            return redirect('/office-budget')->with('error', $exception->getMessage());
//            return redirect("/office-budget")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-office-budget')) {
            abort(404);
        }

        $formData = $request->all();
        $officeBudgetModel = new OfficeBudget();
        $previousData = $officeBudgetModel->find($id);
        $previousBudgetDetailsData = $previousData['budget_details'];
//        updateFlateRate($id,$formData['rent'],$formData['service_charge']);
//        pr($formData);
//        pr($formData['image']);
//        pr($request->file('image'));
//        dd();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'title' => 'required',
                'financial_year' => 'required',
                'budget_type' => 'required',
            ], [
                'title.required' => 'বাজেট শিরনাম প্রদান করুন।',
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
            ]);
//            dd($id);
//            dd($request->all());

            $budget_val = [];
            foreach ($formData['budget']['id'] as $key => $vals) {
                $budget_code = $formData['budget']['full_budget_code'][$key];

//                $budget_val[$budget_code]['budget_code'] = $formData['budget']['budget_code'][$key];
                $budget_val[$budget_code]['amount'] = $formData['budget']['amount'][$key];
                $budget_val[$budget_code]['vat'] = $formData['budget']['vat'][$key];
                $budget_val[$budget_code]['tax'] = $formData['budget']['tax'][$key];
                $budget_val[$budget_code]['useable_amount'] = $formData['budget']['useable_amount'][$key];
            }

            $budget_details = json_encode($budget_val);

            $formData['budget'] = $budget_details;
            unset($formData['_token']);
            unset($formData['title']);
            unset($formData['_method']);


//            $request->request->add(['institute_id' => auth()->user()->institute_id]);
            $request->request->add(['budget_details' => $budget_details]);
            $request->request->remove('budget');

//            $res = updateLedger($formData, config('constants.budget_add_remove.Edit'), false, $previousBudgetDetailsData);
//            updateOfficeBudgetBalance();
            $lastInsertedId = $officeBudgetModel->updateData($request);

//            if (!) {
//                throw new \Exception('Can\'t Update Flate Rate Data...');
//            }

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
//                $path = "images/budget/office_budget/";
//                $picName = '';
//
//                // cache the file
//                $file = $request->file('file_name');
////                foreach ($files as $key => $file) {
//                $destinationPath = $path . $id;
//                $file_name = "budget." . $file->getClientOriginalExtension();
//                $picName .= $file_name;
//                $file->move($destinationPath, $file_name);
////                }

                $path = "public/images/budget/office_budget/" . $id;
                $fileName = "budget";
                $fileRequest = $request->file('file_name');

                $picName = fileUpload($fileRequest, $path, $fileName);

                OfficeBudget::where('id', $id)->update(['file_name' => $picName]);
            }

            DB::commit();

//            if (!$res) {
//                return redirect("/office-budget/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি...");
//            }
            return redirect('/office-budget')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {

            dd($exception);
            DB::rollback();
//            return redirect("/office-budget/$id/edit")->with('error', $exception->getMessage());
            return redirect("/office-budget/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি...");
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-office-budget')) {
            abort(404);
        }
        $officeBudgetModel = new OfficeBudget();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.budget_status.Rejected'),
                ];
                $query = $officeBudgetModel::query();
                $query->where('id', '=', $id);
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $query->where('created_by', '=', auth()->user()->id)
                        ->where('institute_id', '=', auth()->user()->institute_id);
                }
                $query->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function getBudgetCodeByType(Request $request)
    {
        $budgetType = $request->post('budgetType');
        $budgetSubType = getBudgetSubTypeList($budgetType);
        $budgetCodeListArr = getBudgetCodeByBudgetType($budgetType);
        $newBudgetCodeListArr = [];
        foreach ($budgetCodeListArr as $key => $vals) {
            if (!isset($newBudgetCodeListArr[$vals['bgt_sub_type']])) {
                $newBudgetCodeListArr[$vals['bgt_sub_type']] = [];
            }
            $newBudgetCodeListArr[$vals['bgt_sub_type']][] = $vals;
        }
//        pr($budgetSubType);
//        pr($newBudgetCodeListArr);
//        dd($budgetCodeListArr);
        if ($request->ajax()) {
            try {
                return json_encode(['status' => 200, 'resData' => $newBudgetCodeListArr, 'budgetSubType' => $budgetSubType]);
            } catch (Exception $e) {
                dd($e->getMessage());
                return json_encode(['status' => 400]);
            }
        }
    }

    public function budgetApproveReject(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('id');
            $value = $request->post('value');
            $officeBudgetModel = new OfficeBudget();
//            pr($value);
//            dd($id);

            DB::beginTransaction();
            try {
                if ($value == config('constants.budget_status.Rejected')) {
                    $updateData = [
                        'status' => $value,
                    ];
                    try {
                        $query = $officeBudgetModel::query();
                        $query->where('id', '=', $id);
                        $query->update($updateData);
                        DB::commit();

                        $request->session()->flash('success', 'সফলভাবে অনুমোদন বাতিল করা হয়েছে...');
                        return response()->json(['status' => 'success']);
                    } catch (\Exception $exception) {
                        DB::rollback();
                        $request->session()->flash('errors', 'অনুমোদন বাতিল করা সম্ভব হয় নি...');
                        return response()->json(['status' => 'error0']);
                    }
                } elseif ($value == config('constants.budget_status.Approved')) {
                    $localUseBudgetList = getExternalBudgetTypeList();
                    $localUseBudgetList = $localUseBudgetList->toArray();

                    $officeBudgetData = $officeBudgetModel::where('id', $id)
                        ->first()
                        ->toArray();
//                    pr($externalBudgetList);
//                    pr($officeBudgetData);

                    $budget_type = $officeBudgetData['budget_type'];
//                    pr($budget_type);
//                    dd();

//                    updateOfficeBudgetBalance($id);
                    $resultData = true;
                    if (!empty($localUseBudgetList) && array_key_exists($budget_type, $localUseBudgetList)) {
                        $resultData = updateOfficeBudgetBalance($id);
//                        dd();
                        if (!$resultData) {
                            DB::rollback();
                            throw new \Exception('অনুমোদন করা সম্ভব হয় নি...');
                        } else {
                            /*  Update Ledger   */
                        }
                    }
//                    dd($resultData);
//                $res = updateLedger($formData, config('constants.budget_add_remove.Edit'), false, $previousBudgetDetailsData);
                    if ($resultData) {
                        $updateData = [
                            'status' => $value,
                        ];
                        try {
                            $query = $officeBudgetModel::query();
                            $query->where('id', '=', $id);
                            $query->update($updateData);
                            DB::commit();

                            $request->session()->flash('success', 'সফলভাবে অনুমোদন করা হয়েছে...');
                            return response()->json(['status' => 'success']);
                        } catch (\Exception $exception) {
                            DB::rollback();
                            $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                            return response()->json(['status' => 'error0']);
                        }
                    } else {
                        DB::rollback();
                        $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                        return response()->json(['status' => 'error1']);
                    }
                } else {
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error1']);
                }
            } catch (Exception $e) {
                DB::rollback();
//                dd($e);
//                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
//                return response()->json(['status' => 'error2']);
            }
        }
    }

    public function calculation()
    {
        $employee = OfficeBudget::all()->toArray();
        $file_path = [];
        $data = [
            "rdata" => $employee,
            "file_path" => $file_path,
            "name" => $employee[0]['title'],
            "count" => 137594.64
        ];
        return view('office_bill.office_bill_template', compact('data'));


//        $year = 2023;
//        $budgetType = 2;
//
//        $getOffficeBudgetData = OfficeBudget::where('financial_year', $year)
//            ->where('budget_type', $budgetType)
//            ->get()
//            ->toArray();
//
//        $offficeBudgetCalData = [];
//        $totalSumData = [
//            'amount' => 0,
//            'useable_amount' => 0
//        ];
//
//        $totalBalanceSumData = [
//            'amount' => 0,
//            'useable_amount' => 0
//        ];
//
//        foreach ($getOffficeBudgetData as $key => $val) {
//            $budget_details = json_decode($val['budget_details'], false);
////            pr($budget_details);
//            foreach ($budget_details as $code => $info) {
//                pr($info);
//
//                $totalSumData['amount'] = $totalSumData['amount'] + $info->amount;
//                $totalSumData['useable_amount'] = $totalSumData['useable_amount'] + $info->useable_amount;
//
//                if (!isset($offficeBudgetCalData[$code])) {
//                    $offficeBudgetCalData[$code] = [];
//
//                    $innerArr['amount'] = $info->amount;
//                    $innerArr['useable_amount'] = $info->useable_amount;
//                    $offficeBudgetCalData[$code] = $innerArr;
//                } elseif (isset($offficeBudgetCalData[$code])) {
//                    $amount = $offficeBudgetCalData[$code]['amount'];
//                    $useable_amount = $offficeBudgetCalData[$code]['useable_amount'];
//
//                    $offficeBudgetCalData[$code]['amount'] = $amount + $info->amount;
//                    $offficeBudgetCalData[$code]['useable_amount'] = $useable_amount + $info->useable_amount;
//                } else {
//                }
//            }
//        }
//        pr($offficeBudgetCalData);
//        pr($totalSumData);
//
//        $getOffficeBudgetBalanceData = OfficeBudgetBalance::where('financial_year', $year)
//            ->where('budget_type', $budgetType)
//            ->first()
//            ->toArray();
//
//        $resData = json_decode($getOffficeBudgetBalanceData['total_updatable_balances'],false);
//        pr($resData);
//
//
//        pr($getOffficeBudgetData);
//
//        dd($getOffficeBudgetBalanceData);
    }

    // Generate PDF
    public function createPDF()
    {
//        pr(base_path('resources/fonts/'));
//        dd(storage_path('fonts'));
        // retreive all records from db
        $rdata = OfficeBudget::all()->toArray();
//        dd(base_path('resource/fonts/Kalpurush.ttf'));


        $file_path = [
            "css" => [
                "bootstrap" => "public/css/bootstrap.min.css",
                "style" => "public/css/bill_template.css"
            ],
            "js" => [
                "js" => "public/css/bootstrap.min.js",
            ]
        ];
        $data = [
            "rdata" => $rdata,
            "file_path" => $file_path,
            "name" => $rdata[0]['title'],
            "count" => 137594.64
        ];
//        dd($resData['file_path']['css']['bootstrap']);

        // Dom PDF //
//         share data to view
//        view()->share('data', $data);

//        $pdf = Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Nikosh'])->loadView('office_bill.office_bill_template', compact('data'));
//        $pdf = Pdf::loadView('office_bill.office_bill_template', $data);
//        return $pdf->stream('invoice.pdf');
//        Pdf::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf');

//        $pdf = PDF::loadView('office_bill.office_bill_template', $data);
//        PDF::setOption(['dpi' => 150, 'defaultFont' => 'nikosh']);
//        PDF::setPaper('A4');
        // download PDF file with download method
//        return $pdf->download('pdf_file.pdf');

        // LPDF
//        $pdf = LPDF::Make();
//
//        $font_data = array(
//            'nikosh' => [
//                'R' => 'Li_Shamim_Robi_Unicode.ttf',      // regular font
//                'useOTL' => 0xFF,
//                'useKashida' => 75,
//            ]
//            // ...add as many as you want.
//        );
//
//        $pdf->addCustomFont($font_data, true);
//        // If your font file is unicode and "OpenType Layout" then set true. Default value is false.
//
//        $pdf->loadView('office_bill.office_bill_template', $data);
//        return $pdf->stream('document.pdf');

        // MPDF
//        dd($data);
        $pdf = MPDF::loadView('office_bill.office_bill_template', $data, [],
            [
                'default_font' => 'nikosh',
                "title" => 'Office Bill',
                'format' => 'A4',
            ]
        );

        return $pdf->stream('Office_Bill.pdf');
//        return $pdf->download('Office_Bill.pdf');
    }
}
