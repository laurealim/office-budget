<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Division;
use App\Models\Upazila;
use Illuminate\Http\Request;

class UpazilaController extends Controller
{

    public function list()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $upazilaModel = new Upazila();

        $upazilaLists = $upazilaModel->select('divisions.bn_name as division_name', 'districts.bn_name as district_name', 'upazilas.*')
            ->leftJoin('districts', 'districts.id', '=', 'upazilas.district_id')
            ->leftJoin('divisions', 'divisions.id', '=', 'districts.division_id')
            ->where('districts.name', 'like', '%' . $searchData . '%')
            ->orwhere('divisions.name', 'like', '%' . $searchData . '%')
            ->orwhere('upazilas.name', 'like', '%' . $searchData . '%')
            ->orderby('id','desc')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('upazila.ajax_list', compact('upazilaLists'));
        } else {
            return view('upazila.adminList', compact('upazilaLists'));
        }
    }

    public function form()
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        return view('upazila.adminForm', compact('divisionList', 'districtList'));
    }

    public function store(Request $request)
    {
        $upazilaModel = new Upazila();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'district_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'district_id.required' => 'Please Select a District',

            ]);

            $upazilaModel->saveData($request->except('division_id', '_token'));
            return redirect('admin/upazila/list')->with('success', 'New Upazila added successfully');
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'Data can not saved...');
            return redirect()->back();
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $request)
    {
        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();

        try {
            $upazilaData = Upazila::where('id', $id)->first();
            $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
//            dd($districtList);
            $districtData = District::where('id', $upazilaData->district_id)->first();
            $divisionID = $districtData->division_id;
            $districtList = $districtModel->where('division_id', $divisionID)->pluck('bn_name', 'id')->all();
//            dd($divisionID);
            return view('upazila.adminEdit', compact('divisionList', 'upazilaData', 'divisionID', 'districtList', 'id'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        $upazilaModel = new Upazila();
        try {
            $data = $this->validate($request, [
                'name' => 'required',
                'bn_name' => 'required',
                'district_id' => 'required',
            ], [
                'name.required' => 'District Name is required',
                'bn_name.required' => 'District Name (Bangla) is required',
                'district_id.required' => 'Please Select a District',

            ]);
            unset($request['division_id']);

            $upazilaModel->updateData($request);

            return redirect('admin/upazila/list')->with('success', 'Upazila edited successfully..');
        }catch (\Exception $exception){
            dd($exception);
            $request->session()->flash('error', 'Upazila can not edited successfully...');
            return redirect()->back();
        }

    }

    public function destroy($id, Request $request)
    {
        $upazilaInfo = Upazila::findOrFail($id);
        if (isset($request->id)) {
            $upazilaInfo->delete();
            $request->session()->flash('success', 'Upazila Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Upazila Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function upazilaSelectAjaxList(Request $request)
    {
//        var_dump($request->post('divisionId'));
        return $this->_upazilaSelectAjaxList($request);
    }

    private function _upazilaSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $upazilaModel = new Upazila();
            $districtID = $request->post('districtID');
            $upazilaList = $upazilaModel->where("district_id", $districtID)->pluck("bn_name", "id")->all();
            return json_encode($upazilaList);
        }
    }
}
