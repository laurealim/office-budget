<?php

namespace App\Http\Controllers;

use App\Models\OfficeBudget;
use App\Models\OfficeBudgetExpense;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class OfficeBudgetExpenseController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('office-budget-expense-list')) {
            abort(404);
        }

        $page_title = 'অফিস বাজেট খরচের তালিকা';

        $officeBudgetExpenseModel = new OfficeBudgetExpense();

        $query = $officeBudgetExpenseModel::query();

        $query->orderBy('status', 'asc')
//            ->orderBy('financial_year', 'asc')
            ->orderBy('created_at', 'desc');

        $data = $query->get();
//        pr($data);

//        $parentItemListArr = getParentItemList();
        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getFullInstituteList();
        $financialYearList = getFinancialYearList();
        $budgetCodeListArr = getBudgetCodeList();

//        pr($budgetTypeListArr);
//        pr($instituteListArr);
//        pr($financialYearList);
//        pr($budgetCodeListArr);


        if ($request->ajax()) {
//            dd($data);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';

                    if ($row->status != config('constants.budget_status.Rejected')) {
                        if ($row->status == config('constants.budget_status.Pending')) {
                            if (auth()->user()->can('edit-office-expense-budget')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-office-expense-budget" href= ' . route('office-budget-expense.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-office-expense-budget')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('office-budget-expense.destroy', $row->id) . ' class="btn btn-danger delete-budget-expense" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-office-budget-expense')) {
                                $val_app = config('constants.budget_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-budget-expense.budgetExpenseApproveReject') . ' class="btn btn-success bgt_expn_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                            if (auth()->user()->can('can-reject-office-budget-expense')) {
                                $val_app = config('constants.budget_status.Rejected');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('office-budget-expense.budgetExpenseApproveReject') . ' class="btn btn-warning bgt_expn_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-times"></i></a>';
                            }
                        }
                    }

                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == 0)
                        $color = '#9d1e15';
                    if ($row->status == 2)
                        $color = '#E97311';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.budget_status.arr.' .
                            $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('budget_type', function ($row) use ($budgetTypeListArr) {
                    return $budgetTypeListArr[$row->budget_type];
                })
                ->editColumn('budget_code', function ($row) use ($budgetCodeListArr) {
                    return $budgetCodeListArr[$row->budget_code];
                })
                ->editColumn('institute_id', function ($row) use ($instituteListArr) {
                    return $instituteListArr[$row->institute_id];
                })
                ->editColumn('financial_year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->financial_year];
                })
                ->addColumn('file_name', function ($row) {
                    $icon = "";
                    if (!empty($row->file_name)) {
                        $pathFromEnv = env('BUDGET_FILE_LOCATION');
                        $path = asset($pathFromEnv . "budget/office_budget_expense/" . $row->id . "/" .
                            $row->file_name);
//                        $path = asset("storage/images/budget/office_budget_expense/{$row->id}/{$row->file_name}");
//                        $path = Storage::url("app/public/images/budget/office_budget_expense/" . $row->id . "/" .$row->file_name);
                        $icon = '<a target="_blank" title="Edit" class="btn btn-info" id="edit-office-budget-expense" href= "' . $path . '" download ><i class="fas fa-file-downloads fa-download"></i></a>';
                    }
                    return $icon;
                })
                ->rawColumns(['action', 'status', 'file_name'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('office_budget_expense.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-office-expense-budget')) {
            abort(404);
        }
        $path = asset("images/budget/office_budget/" . 1 . "/" . 3);
//        dd($path);
        $page_title = 'কেন্দ্রীয় অফিস বাজেট খরচ';
        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
        $budgetInstituteList = getFullInstituteList();
//        $parentItemListArr = getItemNameAttribute();
        return view('office_budget_expense.create', compact('page_title', 'financialList', 'budgetTypeList', 'budgetInstituteList'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-office-expense-budget')) {
            abort(404);
        }
        $formData = $request->all();
        $officeBudgetExpenseModel = new OfficeBudgetExpense();

        DB::beginTransaction();
        try {
//            dd($formData);
            $data = $this->validate($request, [
                'financial_year' => 'required',
                'budget_type' => 'required',
                'budget_code' => 'required',
                'institute_id' => 'required',
                'current_exp' => 'required',
            ], [
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'budget_code.required' => 'বাজেটের কোড বাছাই করুন।',
                'institute_id.required' => 'প্রতিষ্ঠান বাছাই করুন।',
                'current_exp.required' => 'বর্তমান খরচ উল্ল্যেখ করুন।',
            ]);

//            $request->request->add(['institute_id' => auth()->user()->institute_id]);
            $request->request->add(['expn_date' => date('Y-m-d', strtotime(now()))]);
            $request->request->add(['status' => config('constants.budget_status.Pending')]);
//            $request->request->add(['budget_code' => $formData['bgt_code']]);
            $request->request->add(['budget_full_code' => $formData['bgt_code']]);
            $request->request->add(['total_expense' => 0]);
//            $request->request->add(['total_expense' => $formData['current_exp'] + $formData['prev_expense']]);
            $request->request->remove('total_cur_balance');
            $request->request->remove('bgt_code');

            $lastInsertedId = $officeBudgetExpenseModel->saveData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "public/images/budget/office_budget_expense/" . $lastInsertedId;
                $fileName = "budget_expense";
                $fileRequest = $request->file('file_name');

                $picName = fileUpload($fileRequest, $path, $fileName);

                OfficeBudgetExpense::where('id', $lastInsertedId)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/office-budget-expense')->with('success', 'অফিস বাজেট খরচ যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/office-budget-expense/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-office-expense-budget')) {
            abort(404);
        }

        $page_title = 'বাজেট খরচ সংশোধন';
        $officeBudgetExpenseModel = new OfficeBudgetExpense();

        try {
            $query = $officeBudgetExpenseModel::query();
            $query->where('office_budget_expenses.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('office_budget_expense.created_by', '=', auth()->user()->id)
                    ->where('office_budget_expense.institute_id', '=', auth()->user()->institute_id);
            }

            // get particular data from office_budget_expenses table.
            $officeBudgetExpenseData = $query->firstOrFail();
//            pr($officeBudgetExpenseData);

            // get budget code list by budget type from budget_type_code table.
            $budgetCodeList = getBudgetCodeListByBudgetType($officeBudgetExpenseData->budget_type);
//            pr($budgetCodeList);

            // get particular Office Budget Balance Data from office_budget_balance table.
            $budgetBalance = getOfficeBudgetBalanceListByOptions($officeBudgetExpenseData->financial_year,
                $officeBudgetExpenseData->budget_type, $officeBudgetExpenseData->budget_code, $officeBudgetExpenseData->institute_id);
//            pr($budgetBalance);

            // Financial Year List
            $financialList = getFinancialYearList();
//            pr($financialList);

            // get Budget Type List form budget_type_lists table.
            $budgetTypeList = getBudgetTypeList(false);
//            pr($budgetTypeList);

            // get full Institute name
            $budgetInstituteList = getFullInstituteList();
//            pr($budgetInstituteList);

            return view('office_budget_expense.edit', compact('page_title', 'officeBudgetExpenseData', 'id', 'budgetBalance', 'financialList', 'budgetTypeList', 'budgetCodeList', 'budgetInstituteList'));
        } catch (\Exception $exception) {
//            DB::rollback();
            return redirect('/office-budget-expense')->with('error', $exception->getMessage());
//            return redirect("/office-budget")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-office-expense-budget')) {
            abort(404);
        }
        $formData = $request->all();
        $query = OfficeBudgetExpense::query();
        $query->where('id', $id);
        $prevData = $query->get()->first()->toArray();
//        dd($prevData);
        $officeBudgetExpenseModel = new OfficeBudgetExpense();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'financial_year' => 'required',
                'budget_type' => 'required',
                'budget_code' => 'required',
                'total_expense' => 'required',
                'file_name' => 'mimes:pdf|max:2048',
            ], [
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'budget_code.required' => 'বাজেটের কোড বাছাই করুন।',
                'total_expense.required' => 'মোট খরচ উল্ল্যেখ করুন।',
                'file_name' => 'File Type or Size Missmatch',
            ]);

//            $request->request->add(['total_expense' => $formData['current_exp'] + $prevData['prev_expense']]);
            $request->request->remove('bgt_code');
            $request->request->remove('total_cur_balance');
            $lastInsertedId = $officeBudgetExpenseModel->updateData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {

                $path = "public/images/budget/office_budget_expense/" . $id;
                $fileName = "budget_expense";
                $fileRequest = $request->file('file_name');

                $picName = fileUpload($fileRequest, $path, $fileName);


//                $path = "images/budget/office_budget_expense/";
//                $picName = '';

                // cache the file
//                $file = $request->file('file_name');
////                foreach ($files as $key => $file) {
//                $destinationPath = $path . $id;
//                $file_name = "budget." . $file->getClientOriginalExtension();
//                $picName .= $file_name;
//                $file->move($destinationPath, $file_name);
//                }
                OfficeBudgetExpense::where('id', $id)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/office-budget-expense')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {

//            dd($exception);
            DB::rollback();
            return redirect("/office-budget-expense/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি... " . $exception->getMessage());
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-office-expense-budget')) {
            abort(404);
        }
        $officeBudgetExpenseModel = new OfficeBudgetExpense();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.budget_status.Rejected'),
                ];
                $query = $officeBudgetExpenseModel::query();
                $query->where('id', '=', $id);
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $query->where('created_by', '=', auth()->user()->id)
                        ->where('institute_id', '=', auth()->user()->institute_id);
                }
                $query->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function expenseDataAjax(Request $request)
    {
        if ($request->ajax()) {
            try {
                $budgetType = $request->post('budgetType');
                $budget_code_id = $request->post('budget_code_id');
                $financial_year = $request->post('financial_year');
                $institute_id = $request->post('institute_id');

                $resCodeData = getBudgetCodeById($budget_code_id);
//                pr($resCodeData);
                $resBalanceData = getOfficeBudgetBalanceListByOptions($financial_year, $budgetType, $budget_code_id, $institute_id);
//                dd($resBalanceData);

                $resCodeData['total_balances'] = isset($resBalanceData['total_balances']) ? $resBalanceData['total_balances'] : 0;
                $resCodeData['usable_balances'] = isset($resBalanceData['usable_balances']) ? $resBalanceData['usable_balances'] : 0;
                $resCodeData['real_usable_balances'] = isset($resBalanceData['real_usable_balances']) ? $resBalanceData['real_usable_balances'] : 0;
                $resCodeData['total_exp'] = isset($resBalanceData['total_exp']) ? $resBalanceData['total_exp'] : 0;
                $resCodeData['total_prev_exp'] = isset($resBalanceData['total_prev_exp']) ? $resBalanceData['total_prev_exp'] : 0;

//                $itemList = getItemListByBudgetCodeId($budget_code_id);
//                $unitList = config('constants.unit_value.arr');

                return response()->json(['status' => 200, 'resData' => $resCodeData]);
            } catch (\Exception $e) {
                dd($e->getMessage());
                return response()->json(['status' => 400]);
            }
        }
    }

    public function budgetExpenseApproveReject(Request $request)
    {
        if ($request->ajax()) {
//            dd($request->post());
            $id = $request->post('id');
            $value = $request->post('value');
            $officeBudgetExpenseModel = new OfficeBudgetExpense();

            DB::beginTransaction();
            try {
                $resultData = updateOfficeBudgetExpense($id, $value);
                if ($resultData) {
//                    $updateData = [
//                        'status' => $value,
//                    ];

                    try {
                        $query = $officeBudgetExpenseModel::query();
                        $query->where('id', '=', $id);
                        $query->update($resultData);
                        DB::commit();

                        $request->session()->flash('success', 'সফলভাবে অনুমোদন করা হয়েছে...');
                        return response()->json(['status' => 'success']);
                    } catch (\Exception $exception) {
                        DB::rollback();
                        $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                        return response()->json(['status' => 'error0']);
                    }
                } else {
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error1']);
                }
            } catch (Exception $e) {
                DB::rollback();
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error2']);
            }
        }
    }
}
