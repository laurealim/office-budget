<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ledger;
use App\Models\OfficeBudget;
use Illuminate\Http\Request;
use PhpParser\Lexer\TokenEmulator\FlexibleDocStringEmulator;


class MigrateController extends Controller
{
    public function budget()
    {
        $officeBudgetModel = new OfficeBudget();
        $officeLedgerModel = new Ledger();

        $query = $officeBudgetModel::query();
        $resData = $query->get()->toArray();

        $ledgerData = $officeLedgerModel->get()->toArray();
//        pr($resData);
//        pr($ledgerData);
//        dd();

        try {
            foreach ($resData as $key => $values) {
                $isEmptyLedger = true;
//            pr($values);
//            pr($values['budget_details']);
                $budget_details = json_decode($values['budget_details'], true);
//                pr($budget_details);

                if (!empty($ledgerData)) {
                    $isEmptyLedger = false;
                    $newArray = [];
                    foreach ($ledgerData as $item)
                        $newArray[$item['code']] = $item;
//                    pr($newArray);
                    $ledgerData = $newArray;
//                    dd($ledgerData);
                }
                foreach ($budget_details as $code => $vals) {
                    if (!$isEmptyLedger) {
                        if (array_key_exists($code, $ledgerData)) {
//                            pr($vals);
//                            dd($ledgerData[$code]);
                            echo "Key exists!";
//                            $dataArr = $this->insertUpdatedData($ledgerData[$code], $code, $vals, true);
                            $dataArr = updateLedgerData($ledgerData[$code], $code, $vals, true);
                            if (!empty($dataArr)) {
                                // Code Here
                            } else {
                                // Code Here
                            }
                        } else {
//                            $dataArr = $this->insertNewData($values, $code, $vals);
                            $dataArr = addLedgerData($values, $code, $vals);
                            $officeLedgerModel::create($dataArr);
                        }
                    } else {

//                        $dataArr = $this->insertNewData($values, $code, $vals);
                        $dataArr = addLedgerData($values, $code, $vals);
                    $officeLedgerModel::create($dataArr);
                    }
                }
            }
            dd("all done...");
        } catch (\Exception $exception) {
            dd($exception);
        }

        dd();
    }

    private function insertNewData($values, $code, $vals)
    {
        $dataArr = [
            'financial_year' => $values['financial_year'],
            'budget_type' => $values['budget_type'],
            'code' => $code,
            'institute_id' => $values['institute_id'],
            'vat' => $vals['vat'],
            'tax' => $vals['tax'],
            'amount' => $vals['amount'],
            'usable_amount' => $vals['useable_amount'],
            'total_previous_amount' => 0,
            'total_current_amount' => $vals['useable_amount'],
            'status' => config('constants.ledger_status.Running')
        ];
        return $dataArr;
    }

    private function insertUpdatedData($ledgerData, $code, $updatedData, $isInclude = false)
    {
        pr($ledgerData);
        pr($updatedData);
        $vat = $updatedData['vat'];
        $tax = $updatedData['tax'];
        $lvat = $ledgerData['vat'];
        $ltax = $ledgerData['tax'];

        $lamount = $ledgerData['amount'];
        $usable_lamount = $ledgerData['useable_amount'];
        $total_previous_amount = $ledgerData['total_previous_amount'];
        $total_current_amount = $ledgerData['total_current_amount'];

        if (($vat == $lvat) && ($tax == $ltax) && ($lamount == $updatedData['amount']) && ($usable_lamount == $updatedData['useable_amount'])) {
            return array();
        }

        if ($isInclude) {
            $lamount = $lamount + $updatedData['amount'];
            $useable_new_amount = $updatedData['amount'] - ((($updatedData['amount'] * $vat) / 100) + (($updatedData['amount'] * $tax) / 100));
            $usable_lamount = $usable_lamount + $useable_new_amount;

            $total_previous_amount = $total_current_amount;
            $total_current_amount = $total_current_amount + $useable_new_amount;
        } else {
            $lamount = $lamount - $updatedData['amount'];
            $useable_new_amount = $updatedData['amount'] - ((($updatedData['amount'] * $vat) / 100) + (($updatedData['amount'] * $tax) / 100));
            $usable_lamount = $usable_lamount + $useable_new_amount;

            $total_previous_amount = $total_current_amount;
            $total_current_amount = $total_current_amount - $useable_new_amount;
        }


//        dd();
        $dataArr = [
            'financial_year' => $ledgerData['financial_year'],
            'budget_type' => $ledgerData['budget_type'],
            'code' => $code,
            'institute_id' => $ledgerData['institute_id'],
            'vat' => $vat,
            'tax' => $tax,
            'amount' => $lamount,
            'usable_amount' => $usable_lamount,
            'total_previous_amount' => $total_previous_amount,
            'total_current_amount' => $total_current_amount,
            'status' => config('constants.ledger_status.Running')
        ];
        return $dataArr;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
