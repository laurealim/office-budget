<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function list()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $countries = new Country();

        $countries = $countries->where('name', 'like', '%' . $searchData . '%')
            ->orWhere('sortname', 'like', '%' . $searchData . '%')
            ->orWhere('phonecode', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'countries.created_by', 'users.id')
            ->select('countries.*', 'users.first_name')
            ->paginate($displayValue);

        if (request()->ajax()) {
            return view('country.ajax_list', compact('countries'));
        }
        else{
            return view('country.adminList', compact('countries'));
        }
    }

    public function form()
    {
        return view('country.adminForm');
    }

    public function store(Request $request)
    {
        $countryModel = new Country();
        $data = $this->validate($request,[
            'name' => 'required',
            'sortname' => 'required',
        ]);

        $countryModel->saveData($request);
        return redirect('admin/country/list')->with('success','New Country added successfully');
    }

    public function show(Country $country)
    {
        //
    }

    public function edit(Country $country, $id)
    {
        $countryData = Country::where('id', $id)->first();
        return view('country.adminEdit', compact('countryData', 'id'));
    }

    public function update(Request $request, Country $country, $id)
    {
        $countryModel = new Country();
        $data = $this->validate($request, [
            'name' => 'required',
            'sortname' => 'required',
        ]);

        $countryModel->updateData($request);

        return redirect('admin/country/list')->with('success','Country edited successfully');
    }

    public function destroy(Request $request, Country $country)
    {
        $eligibilityInfo = Country::findOrFail($request->id);
        if(isset($request->id)){
            $eligibilityInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }
}
