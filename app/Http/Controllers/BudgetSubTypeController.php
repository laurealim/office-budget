<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBudgetSubTypeRequest;
use App\Http\Requests\UpdateBudgetSubTypeRequest;
use App\Models\BudgetSubType;
use App\Models\BudgetType;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class BudgetSubTypeController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('budget-sub-type-list')) {
            abort(404);
        }

        $page_title = 'বাজেট উপ-ধরনের তালিকা';

        $bgtTypModel = new BudgetType();
        $budgetTypeList = getBudgetTypeInfoList();
//        dd($budgetTypeList);
        $bgtSubTypModel = new BudgetSubType();

        $query = $bgtSubTypModel::query();
//        $query = DB::table('budget_types');

        $query->where('budget_sub_type_status', '!=', config('constants.status.Deleted'))
            ->orderBy('budget_sub_type_status', 'asc')
            ->orderBy('created_at', 'asc');

        $data = $query->get();
//        dd($data);

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
//                    if ($row->status >= 1) {
                    if (auth()->user()->can('edit-budget-sub-type'))
                        $action = '<a title="Edit" class="btn btn-success" id="edit-budget-sub-type" href= ' . route('budget-sub-type.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                    if (auth()->user()->can('delete-budget-sub-type'))
                        $action = $action . '<a id=' . $row->id . ' href=' . route('budget-sub-type.destroy', $row->id) . ' class="btn btn-danger delete-budget-sub-type" title="Delete"><i class="fas fa-trash-alt"></i></a>';
//                    }
                    return $action;
                })
                ->editColumn('budget_sub_type_status', function ($row) {
                    return config('constants.status.' . $row->budget_sub_type_status);
                })
                ->editColumn('budget_type_name', function ($row) use ($budgetTypeList) {
                    return $budgetTypeList[$row->budget_type_id]['budget_type_name'];
                })
                ->editColumn('budget_type_code', function ($row) use ($budgetTypeList) {
                    return $budgetTypeList[$row->budget_type_id]['budget_type_code'];
                })
                ->rawColumns(['action', 'budget_type_name', 'budget_type_code'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('budget_sub_type.index', compact('page_title'));
    }


    public function create()
    {
        if (!auth()->user()->can('create-budget-sub-type')) {
            abort(404);
        }
        $page_title = 'বাজেট ধরন তৈরি';
        $budgetType = getBudgetTypeList(false);
//        dd($budgetType);
//        $config('constants.status.Deleted');
        return view('budget_sub_type.create', compact('page_title', 'budgetType'));
    }


    public function store(StoreBudgetSubTypeRequest $request)
    {
        if (!auth()->user()->can('create-budget-sub-type')) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $budgetSubTypeModel = new BudgetSubType();
            $validated = $request->validated();
            $validated['created_by'] = auth()->user()->id;

//            dd($validated);
            $budgetSubTypeModel->create($validated);

            DB::commit();
            return redirect('/budget-sub-type')->with('success', 'নতুন বাজেটের উপ-ধরন যুক্ত হয়েছে...');
        } catch (Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            return redirect('/budget-sub-type/create')->with('error', $exception->getMessage());
        }
    }


    public function show(BudgetSubType $budgetSubType)
    {
        //
    }


    public function edit($id)
    {
        if (!auth()->user()->can('edit-budget-sub-type')) {
            abort(404);
        }
        $page_title = 'বাজেট উপ ধরন তথ্য সংশোধন';
        $bgtSubTypModel = new BudgetSubType();
        $budgetTypeList = getBudgetTypeList(false);
//        dd($budgetTypeList);

        try {
            $query = $bgtSubTypModel::query();
            $query->where('id', $id);
            $bgtSubTypData = $query->firstOrFail();

            return view('budget_sub_type.edit', compact('page_title', 'bgtSubTypData', 'budgetTypeList', 'id'));
        } catch (\Exception $exception) {
//            dd($exception->getMessage());
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/budget-sub-type")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }


    public function update(UpdateBudgetSubTypeRequest $request, $id)
    {
        if (!auth()->user()->can('edit-budget-sub-type')) {
            abort(404);
        }
        $formData = $request->all();
//        $request->request->add(['budget_type' => $formData['budget_type_name']]);
        DB::beginTransaction();
        try {
            $bgtSubTypData = BudgetSubType::find($id);

            $validatedData = $request->validated();
            $validatedData['updated_by'] = auth()->user()->id;
            $validatedData['id'] = $id;

            foreach ($validatedData as $key => $vals) {
//                pr();
                $bgtSubTypData->$key = $vals;
            }
//            dd($validatedData);

//            $request->request->add(['id' => $id]);

            $lastInsertedId = $bgtSubTypData->save();

            DB::commit();
            return redirect('/budget-sub-type')->with('success', 'সফলভাবে বাজেট উপ-ধরনের তথ্য সংশোধন করা হয়েছে...।');
        } catch (\Exception $exception) {

            dd($exception->getMessage());
            DB::rollback();
            return redirect("/budget-sub-type/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function destroy(BudgetSubType $budgetSubType)
    {
        //
    }


    public function ajaxBgtSubTypList(Request $request)
    {
        $status = 200;
        try {
            if ($request->ajax()) {
                $bgt_type = $request->bgt_type;
                $budgetSubTypeList = getBudgetSubTypeList($bgt_type);
                $budgetTypeInfo = getBudgetTypeInfoList($bgt_type);
                $budgetTypeInfo = $budgetTypeInfo[$bgt_type];


//                pr($budgetSubTypeList);
//                pr($budgetTypeInfo);
//                dd("end");
                return response()->json(['status' => $status, 'budgetSubType' => $budgetSubTypeList, 'budgetType' => $budgetTypeInfo]);
            }
        } catch (Exception $exception) {
            $status = 400;
            return response()->json(['status' => $status, 'message' => $exception->getMessage()]);
        }
    }

    public function ajaxBgtSubTypCode(Request $request){
        $status = 200;
        try {
            if ($request->ajax()) {
                $bgt_sub_type = $request->bgt_sub_type;
                $budgetSubTypeCode = getBudgetSubTypeCodeById($bgt_sub_type);
//                $budgetTypeInfo = $budgetTypeInfo[$bgt_type];


//                pr($budgetSubTypeList);
//                pr($budgetTypeInfo);
//                dd("end");
                return response()->json(['status' => $status, 'budgetSubTypeCode' => $budgetSubTypeCode]);
            }
        } catch (Exception $exception) {
            $status = 400;
            return response()->json(['status' => $status, 'message' => $exception->getMessage()]);
        }
    }
}
