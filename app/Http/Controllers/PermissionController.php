<?php
namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use DataTables;

class PermissionController extends Controller
{
    public function index(Request $request){
        $page_title = 'সকল অনুমতির তালিকা';
        $data = Permission::orderby('id','asc')->get();
        $controller_list = config('constants.controller_list.list');
//        dd($controller_list);
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-permission" href= ' . route('permissions.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('permissions.destroy', $row->id) . ' class="btn btn-danger delete-permission">Delete</a>';
                    return $action;
                })
                ->editColumn('controller', function ($row) use($controller_list) {
                    return $controller_list[$row->controller];
                })
                ->rawColumns(['action'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('permissions.index', compact('page_title'));
    }

    public function create()
    {
        $page_title = 'অনুমতি তৈরী';
        return view('permissions.create', compact('page_title'));
    }

    public function store(Request $request)
    {
        $formData = $request->all();

        DB::beginTransaction();
        try {
            $permissionName = $formData['name'];
            $controller = $formData['controller'];
            $slug = Str::slug($permissionName);
            $request->request->add(['slug' => $slug]);

            $data = $this->validate($request, [
                'name' => 'required',
                'controller' => 'required',
            ], [
                'name.required' => 'অনুগ্রহ করে অনুমতি প্রদাল করুন',
                'controller.required' => 'অনুগ্রহ করে সেকশন প্রদাল করুন',
            ]);

            $permissionModel = new Permission();
            $permissionModel->slug = $slug;
            $permissionModel->name = $permissionName;
            $permissionModel->controller = $controller;
            $permissionModel->save();

//            $lastInsertedId = $landModel->saveData($request);

            DB::commit();
            return redirect('/permissions')->with('success', 'নতুন অনুমতি যুক্ত হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/permissions/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page_title = 'অনুমতি সংশোধন';
//        $countryData = Country::all();
        $permissionModel = new Permission();
        try {
            $permissionData = $permissionModel->where('permissions.id', $id)
                ->firstOrFail();

            return view('permissions.edit', compact('page_title', 'permissionData', 'id'));
        } catch (\Exception $exception) {
        }
    }

    public function update(Request $request, $id)
    {
        $formData = $request->all();
        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'name' => 'required',
                'controller' => 'required',
            ], [
                'name.required' => 'অনুগ্রহ করে অনুমতি প্রদাল করুন',
                'controller.required' => 'অনুগ্রহ করে সেকশন প্রদাল করুন',
            ]);

            $permissionName = $formData['name'];
            $controller = $formData['controller'];

            $data = Permission::find($id);
            $data->name = $permissionName;
            $data->controller = $controller;

            $data->save();
//            dd($request);
            DB::commit();

//            return redirect()->back()->with('success', 'data updated');
            return redirect('/permissions')->with('success', 'তথ্য সফলভাবে সংশোধন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            return redirect("/permissions/$id/edit")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ' . $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Permission::destroy($id);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function Permission()
    {
    	$dev_permission = Permission::where('slug','create-tasks')->first();
		$manager_permission = Permission::where('slug', 'edit-users')->first();

		//RoleTableSeeder.php
		$dev_role = new Role();
		$dev_role->slug = 'developer';
		$dev_role->name = 'Front-end Developer';
		$dev_role->save();
		$dev_role->permissions()->attach($dev_permission);

		$manager_role = new Role();
		$manager_role->slug = 'manager';
		$manager_role->name = 'Assistant Manager';
		$manager_role->save();
		$manager_role->permissions()->attach($manager_permission);

		$dev_role = Role::where('slug','developer')->first();
		$manager_role = Role::where('slug', 'manager')->first();

		$createTasks = new Permission();
		$createTasks->slug = 'create-tasks';
		$createTasks->name = 'Create Tasks';
		$createTasks->save();
		$createTasks->roles()->attach($dev_role);

		$editUsers = new Permission();
		$editUsers->slug = 'edit-users';
		$editUsers->name = 'Edit Users';
		$editUsers->save();
		$editUsers->roles()->attach($manager_role);

		$dev_role = Role::where('slug','developer')->first();
		$manager_role = Role::where('slug', 'manager')->first();
		$dev_perm = Permission::where('slug','create-tasks')->first();
		$manager_perm = Permission::where('slug','edit-users')->first();

		$developer = new User();
		$developer->name = 'Mahedi Hasan';
		$developer->email = 'mahedi@gmail.com';
		$developer->password = bcrypt('secrettt');
		$developer->save();
		$developer->roles()->attach($dev_role);
		$developer->permissions()->attach($dev_perm);

		$manager = new User();
		$manager->name = 'Hafizul Islam';
		$manager->email = 'hafiz@gmail.com';
		$manager->password = bcrypt('secrettt');
		$manager->save();
		$manager->roles()->attach($manager_role);
		$manager->permissions()->attach($manager_perm);


		return redirect()->back();
    }
}
