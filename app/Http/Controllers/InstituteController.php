<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Institute;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class InstituteController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('institute-list')) {
            abort(404);
        }
        $page_title = 'প্রতিষ্ঠানের তালিকা';
        $instituteModel = new Institute();
//        dd(auth()->user()->id);


        $query = $instituteModel::query();

        $query->where('status', '!=', config('constants.status.Deleted'))
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'asc');

        $data = $query->get();
//            ->toSql();
//        dd($data);


        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
                    if ($row->status >= 1) {
                        if (auth()->user()->can('edit-institute'))
                            $action = '<a title="Edit" class="btn btn-success" id="edit-institute" href= ' . route('institutes.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        if (auth()->user()->can('delete-institute'))
                            $action = $action . '<a id=' . $row->id . ' href=' . route('institutes.destroy', $row->id) . ' class="btn btn-danger delete-institute" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                    }
                    return $action;
                })
                ->editColumn('status', function ($row) {
                    return config('constants.status.' . $row->status);
                })
                ->editColumn('office_name', function ($row) use ($districtListArr, $upazillaListArr) {
                    $upazilla = "";
                    if($row->upaz == -1){
                        $upazilla = "জেলা কার্যালয়";
                    }else{
                        $upazilla = $upazillaListArr[$row->upaz];
                    }
                    return $row->office_name . ", " . $upazilla . ", " . $districtListArr[$row->dist];
//                    return config('constants.status.' . $row->status);
                })
                ->rawColumns(['action'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('institutes.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-institute')) {
            abort(404);
        }
        $page_title = 'নতুন প্রতিষ্ঠান';
        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
//        pr($districtListArr);
//        dd($upazillaListArr);
        return view('institutes.create', compact('page_title', 'districtListArr', 'upazillaListArr'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-institute')) {
            abort(404);
        }
        $formData = $request->all();
        $instituteModel = new Institute();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'office_name' => 'required',
                'office_code' => 'required',
                'status' => 'required',
            ], [
                'dist.required' => 'জেলা বাছাই করুন...',
                'upaz.required' => 'উপজেলা বাছাই করুন...',
                'office_name.required' => 'অফিসের নাম প্রদান করুন...',
                'office_code.required' => 'অফিসের কোড (শেষ ৬ ডিজিট) প্রদান করুন...',
                'status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $instituteModel->saveData($request);

            DB::commit();
            return redirect('/institutes')->with('success', 'নতুন প্রতিষ্ঠান সফলভাবে যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/institutes/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-institute')) {
            abort(404);
        }
        $page_title = 'প্রতিষ্ঠানের তথ্য সংশোধন';
//        $countryData = Country::all();
        $instituteModel = new Institute();
        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
        $upazillaListArr[-1] = 'জেলা কার্যালয়';

        try {
            $query = $instituteModel::query();
            $query->where('id', $id);
//            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                $query->where('flats.created_by', '=', auth()->user()->id);
//            }
            $instituteData = $query->firstOrFail();
            $upazilaListSelect = Upazila::where('district_id', $instituteData->dist)->pluck('bn_name', 'id')->all();
            $upazilaListSelect[-1] = 'জেলা কার্যালয়';

            return view('institutes.edit', compact('page_title', 'instituteData', 'id', 'districtListArr', 'upazillaListArr', 'upazilaListSelect'));
        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/institutes")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-institute')) {
            abort(404);
        }
        $formData = $request->all();
        $instituteModer = new Institute();
        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'office_name' => 'required',
                'office_code' => 'required',
                'status' => 'required',
            ], [
                'dist.required' => 'জেলা বাছাই করুন...',
                'upaz.required' => 'উপজেলা বাছাই করুন...',
                'office_name.required' => 'অফিসের নাম প্রদান করুন...',
                'office_code.required' => 'অফিসের কোড (শেষ ৬ ডিজিট) প্রদান করুন...',
                'status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $instituteModer->updateData($request);

            DB::commit();
            return redirect('/institutes')->with('success', 'সফলভাবে প্রতিষ্ঠানের তথ্য সংশোধন করা হয়েছে...।');
        } catch (\Exception $exception) {

//            dd($exception);
            DB::rollback();
            return redirect("/institutes/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can(' ')) {
            abort(404);
        }
        $instituteModel = new Institute();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.status.Deleted'),
                ];

                try {
                    $query = $instituteModel::query();
                    $query->where('institutes.id', '=', $id);
//                    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                        $query->where('created_by', '=', auth()->user()->id);
//                    }
                    $query->update($updateData);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error']);
                }

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
