<?php

namespace App\Http\Controllers;

use App\Exports\TenantsExport;
use App\Models\Applicant;
use App\Models\District;
use App\Models\Tenant;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->can('tenant-list')) {
            abort(404);
        }

        $page_title = 'ভাড়া গ্রহণকারীর তথ্য';
        $data = Tenant::latest()->where('status', '!=', config('constants.status.Deleted'))->get();
        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            $unionArr = Union::pluck('bn_name', 'id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
                    if ($row->status == 1) {
                        if ((!auth()->user()->hasRole(['admin', 'super-admin']) && (auth()->user()->id == $row->created_by)) || (auth()->user()->hasRole(['admin', 'super-admin']))) {
                            if (auth()->user()->can('edit-tenant'))
                                $action = ' <a class="btn btn-success" id="edit-user" href= ' . route('tenants.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            if (auth()->user()->can('delete-tenant'))
                                $action .= ' <a id=' . $row->id . ' href=' . route('tenants.destroy', $row->id) . ' class="btn btn-danger delete-tenant"><i class="fas fa-trash-alt"></i></a>';
//                            if (!auth()->user()->can('edit-tenant'))
//<a id=' . $row->id . ' href=' . route('flats.assignedFlatLists', $row->id) . ' class="btn btn-info flatList"><i class="fas fa-house-user"></i></a>';
                        }
                        $action .= ' <a data-toggle="tooltip" data-id=' . $row->id . ' href="javascript:void(0)" class="btn btn-info flatList"><i class="fas fa-house-user"></i></a>';
                    }
                    return $action;
                })
                ->addColumn('full_address', function ($row) use ($districtListArr, $upazillaListArr, $unionArr) {
                    $union = 'পৌরসভা';
                    if ($row->union >= 1)
                        $union = $unionArr[$row->union];

                    return $row->address . ", </br>ইউনিয়ন/পৌরসভাঃ " . $union . ", </br> উপজেলাঃ " . $upazillaListArr[$row->upaz] . ", </br> জেলাঃ " . $districtListArr[$row->dist];
                })
                ->rawColumns(['action'])
                ->editColumn('status', function ($row) {
                    return config('constants.status.' . $row->status);
                })
                ->editColumn('gender', function ($row) {
                    $gender = 'অন্যান্ন';
                    if ($row->gender == 1)
                        $gender = 'পুরুষ';
                    if ($row->gender == 2)
                        $gender = 'নারী';
                    return $gender;
                    return config('constants.status.' . $row->status);
                })
                ->rawColumns(['action', 'full_address'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('tenants.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->can('create-tenant')) {
            abort(404);
        }

        $page_title = 'ভাড়াটিয়ার তথ্য';
        $districtListArr = District::pluck('bn_name', 'id');
        $upazillaListArr = Upazila::pluck('bn_name', 'id');
        $unionArr = Union::pluck('bn_name', 'id');
        return view('tenants.create', compact('page_title', 'districtListArr', 'upazillaListArr', 'unionArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('create-tenant')) {
            abort(404);
        }

        $formData = $request->all();
        $tenantModel = new Tenant();
//        dd($formData);


        DB::beginTransaction();
        try {
            $mobile = bn2en($formData['mobile']);
            $nid = bn2en($formData['nid']);
//            if(!empty($formData['f_card'])){
//                $fcard = bn2en($formData['f_card']);
//                $request->request->add(['f_card' => $fcard]);
//            }

            $request->request->add(['nid' => $nid]);
            $request->request->add(['mobile' => $mobile]);

            if (!uniqueNid($nid)) {
                return redirect('/tenants/create')->with('error', 'Tenant with this NID already exists...');
            }
//            if(!uniquePhone($phone)){
//                return redirect('/applicants/create')->with('error', 'Duplicate Phone');
//            }


            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'union' => 'required',
                'address' => 'required',
                'nid' => 'required',
                'mobile' => 'required',
                'name' => 'required',
                'g_name' => 'required',
                'dob' => 'required',
            ], [
                'dist.required' => 'Need',
                'upaz.required' => 'Need',
                'union.required' => 'Need',
                'address.required' => 'Need',
                'nid.required' => 'Need',
                'mobile.required' => 'Need',
                'name.required' => 'Need',
                'g_name.required' => 'Need',
                'dob.required' => 'Need',
            ]);

            $lastInsertedId = $tenantModel->saveData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('image')) {
                $tenant_id = $lastInsertedId;
                $picName = '';

                // cache the file
                $file = $request->file('image');
//                foreach ($files as $key => $file) {
                $destinationPath = 'images/tenants/' . $tenant_id;;
                $file_name = $tenant_id . "." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                Tenant::where('id', $lastInsertedId)->update(['pic' => $picName]);
            }

            DB::commit();
            return redirect('/tenants')->with('success', 'New Tenant added successfully');
        } catch (\Exception $exception) {
            dd($exception);
            DB::rollback();
            return redirect('/tenants/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('edit-tenant')) {
            abort(404);
        }

        $tenantModel = new Tenant();
        $page_title = 'বন্দবস্ত গ্রহণকারীর তথ্য সংশোধন';
//        $countryData = Country::all();
        try {
            $tenamtData = $tenantModel->where('tenants.id', $id)
                ->firstOrFail();

            $districtListArr = District::pluck('bn_name', 'id');
            $upozillaListArr = Upazila::where('district_id', $tenamtData->dist)->pluck('bn_name', 'id')->all();
            $unionListArr = Union::where('upazilla_id', $tenamtData->upaz)->pluck('bn_name', 'id')->all();
//            $landTypeListArr = config('constants.landType');

//            dd($unionListArr);
            return view('tenants.edit', compact('districtListArr', 'upozillaListArr', 'page_title', 'unionListArr', 'tenamtData', 'id'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-tenant')) {
            abort(404);
        }

        $formData = $request->all();
        $tenantModel = new Tenant();
//        updateFlateRate($id,$formData['rent'],$formData['service_charge']);
//        pr($formData);
//        pr($formData['image']);
//        pr($request->file('image'));
//        dd();

        DB::beginTransaction();
        try {

            $mobile = bn2en($formData['mobile']);
            $nid = bn2en($formData['nid']);

            $request->request->add(['id' => $id]);
            $request->request->add(['nid' => $nid]);
            $request->request->add(['mobile' => $mobile]);

            $data = $this->validate($request, [
                'dist' => 'required',
                'upaz' => 'required',
                'union' => 'required',
                'address' => 'required',
                'nid' => 'required',
                'mobile' => 'required',
                'name' => 'required',
                'g_name' => 'required',
                'dob' => 'required',
            ], [
                'dist.required' => 'Need',
                'upaz.required' => 'Need',
                'union.required' => 'Need',
                'address.required' => 'Need',
                'nid.required' => 'Need',
                'mobile.required' => 'Need',
                'name.required' => 'Need',
                'g_name.required' => 'Need',
                'dob.required' => 'Need',
            ]);

            if (!uniqueNid($nid, $id)) {
//                return redirect('/tenants/create')->with('error', 'Tenant with this NID already exists...');
                return redirect("/tenants/$id/edit")->with('error', 'Tenant with this NID \'' . $nid . '\' already exists...');
            }
//            dd($id);
//            dd($request->all());

            $lastInsertedId = $tenantModel->updateData($request);

//            if (!) {
//                throw new \Exception('Can\'t Update Flate Rate Data...');
//            }

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('image')) {
                $tenent_id = $id;
//                $path = "public/tenants";
                $picName = '';

                // cache the file
                $file = $request->file('image');
//                foreach ($files as $key => $file) {
                $destinationPath = 'images/tenants/' . $tenent_id;;
                $file_name = $tenent_id . "." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                Tenant::where('id', $id)->update(['pic' => $picName]);
            }

            DB::commit();
            return redirect('/tenants')->with('success', 'New Tenant Edited successfully');
        } catch (\Exception $exception) {

            dd($exception);
            DB::rollback();
            return redirect("/tenants/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-tenant')) {
            abort(404);
        }

        $tenantModel = new Tenant();
//        $flatRateModel = new FlatRate();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.status.Inactive'),
                ];

                $tenantModel->where('id', '=', $id)->update($updateData);

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function removeTenantImage(Request $request)
    {
        $file_name = $request->post('imageId');
        $tenant_id = $request->post('tenantId');
        $file_path = "images/tenants/" . $tenant_id . '/' . $file_name;
//        dd($request->post());
        DB::beginTransaction();
        try {
            if (file_exists(public_path($file_path))) {

                $flatData = Flat::find($tenant_id);
                $images = $flatData->pic;
                $new_image_list = str_replace($file_name . ',', "", $images);
                $flatData->pic = $new_image_list;
                $flatData->save();
//                dd($data->pic);
                unlink(public_path($file_path));
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            dd($exception);
//            $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
//            return response()->json(['status' => 'error']);
        }
    }

    public function export(Excel $excel)
    {
//        return Excel::download(new EmployeeExport, 'Employee_List.xlsx');
//        return (new EmployeeExport)->download('Employee_List.xlsx');
        return $excel->download(new TenantsExport, 'ভাড়াটিয়ার_লিস্ট.xlsx');

    }
}
