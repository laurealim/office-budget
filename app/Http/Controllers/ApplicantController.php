<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\District;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $page_title = 'বন্দবস্ত গ্রহণকারীর তথ্য';
        $data = Applicant::latest()->where('is_active','!=',config('constants.status.Deleted'))->get();
        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            $unionListArr = Union::pluck('bn_name', 'id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-user" href= ' . route('applicants.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('applicants.destroy', $row->id) . ' class="btn btn-danger delete-user">Delete</a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->editColumn('union', function ($row) use($unionListArr) {
                    return $unionListArr[$row->union];
                })
                ->editColumn('dist', function ($row) use($districtListArr) {
                    return $districtListArr[$row->dist];
                })
                ->editColumn('upa', function ($row) use($upazillaListArr) {
                    return $upazillaListArr[$row->upa];
                })
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('applicants.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'বন্দবস্ত গ্রহণকারীর তথ্য পূরন';
//        $countryData = Country::all();
        $districtListArr = District::pluck('bn_name', 'id');
        $upozillaListArr = Upazila::pluck('bn_name', 'id');
        $landTypeListArr = config('constants.landType');
        return view('applicants.create', compact('districtListArr', 'upozillaListArr', 'page_title', 'landTypeListArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();
        $applicantModel = new Applicant();

        DB::beginTransaction();
        try {
            $phone = bn2en($formData['phone']);
            $nid = bn2en($formData['nid']);
            if(!empty($formData['f_card'])){
                $fcard = bn2en($formData['f_card']);
                $request->request->add(['f_card' => $fcard]);
            }

            $request->request->add(['nid' => $nid]);
            $request->request->add(['phone' => $phone]);

            if(!uniqueNid($nid)){
                return redirect('/applicants/create')->with('error', 'Duplicate NID');
            }
//            if(!uniquePhone($phone)){
//                return redirect('/applicants/create')->with('error', 'Duplicate Phone');
//            }


            $data = $this->validate($request, [
                'dist' => 'required',
                'upa' => 'required',
                'union' => 'required',
                'nid' => 'required',
                'phone' => 'required',
                'name' => 'required',
                'dob' => 'required',
                'f_name' => 'required',
            ], [
                'dist.required' => 'Need',
                'upa.required' => 'Need',
                'union.required' => 'Need',
                'nid.required' => 'Need',
                'phone.required' => 'Need',
                'name.required' => 'Need',
                'dob.required' => 'Need',
                'f_name.required' => 'Need',
            ]);

            $lastInsertedId = $applicantModel->saveData($request);

            DB::commit();
            return redirect('/applicants')->with('success', 'New Registration added successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/applicants/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicantModel = new Applicant();
        $page_title = 'বন্দবস্ত গ্রহণকারীর তথ্য সংশোধন';
//        $countryData = Country::all();
        try {
            $applicantData = $applicantModel->where('applicants.id', $id)
                ->firstOrFail();

            $districtListArr = District::pluck('bn_name', 'id');
            $upozillaListArr = Upazila::where('district_id', $applicantData->dist)->pluck('bn_name', 'id')->all();
            $unionListArr = Union::where('upazilla_id', $applicantData->upa)
                ->pluck('bn_name', 'id')->all();
//            $landTypeListArr = config('constants.landType');

//            dd($unionListArr);
            return view('applicants.edit', compact('districtListArr', 'upozillaListArr', 'page_title', 'unionListArr', 'applicantData', 'id'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();
        $applicantModel = new Applicant();

        DB::beginTransaction();
        try {
            $phone = bn2en($formData['phone']);
            $nid = bn2en($formData['nid']);
            if(!empty($formData['f_card'])){
                $fcard = bn2en($formData['f_card']);
                $request->request->add(['f_card' => $fcard]);
            }

            $request->request->add(['nid' => $nid]);
            $request->request->add(['phone' => $phone]);
            $request->request->add(['id' => $id]);

            if(!uniqueNid($nid,$id)){
                return redirect("/applicants/$id/edit")->with('error', 'Duplicate NID');
            }


            $data = $this->validate($request, [
                'dist' => 'required',
                'upa' => 'required',
                'union' => 'required',
                'nid' => 'required',
                'phone' => 'required',
                'name' => 'required',
                'dob' => 'required',
                'f_name' => 'required',
            ], [
                'dist.required' => 'Need',
                'upa.required' => 'Need',
                'union.required' => 'Need',
                'nid.required' => 'Need',
                'phone.required' => 'Need',
                'name.required' => 'Need',
                'dob.required' => 'Need',
                'f_name.required' => 'Need',
            ]);
//            dd($request->all());

            $lastInsertedId = $applicantModel->updateData($request);

            DB::commit();
            return redirect('/applicants')->with('success', 'তথ্য সফলভাবে সংশোধন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect("/applicants/$id/edit")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ' . $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $applicantModel = new Applicant();

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'is_active' => config('constants.status.Deleted'),
                ];

                $applicantModel->where('id', '=', $id)->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
