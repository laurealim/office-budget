<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Upazila;
use Illuminate\Http\Request;
use App\User;
use App\Models\Land;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;


class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $page_title = 'জমির তথ্য';
        $data = Land::latest()->where('is_active', '!=', config('constants.status.Deleted'))->get();
        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-user" href= ' . route('lands.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('lands.destroy', $row->id) . ' class="btn btn-danger delete-user">Delete</a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->editColumn('lnd_cat', function ($row) {
                    return config('constants.landType.' . $row->lnd_cat);
                })
                ->editColumn('dist', function ($row) use ($districtListArr) {
                    return $districtListArr[$row->dist];
                })
                ->editColumn('upa', function ($row) use ($upazillaListArr) {
                    return $upazillaListArr[$row->upa];
                })
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('lands.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'জমির তথ্য পূরন';
//        $countryData = Country::all();
        $districtListArr = District::pluck('bn_name', 'id');
        $upozillaListArr = Upazila::pluck('bn_name', 'id');
        $landTypeListArr = config('constants.landType');
        return view('lands.create', compact('districtListArr', 'upozillaListArr', 'page_title', 'landTypeListArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();

        $landModel = new Land();

        DB::beginTransaction();
        try {
            $land_size = $formData['lnd_size'];
            $land_cat = $formData['lnd_cat'];
            $request->request->add(['lnd_price' => ($land_size * $land_cat)]);

            $data = $this->validate($request, [
                'dist' => 'required',
                'upa' => 'required',
                'mouja' => 'required',
                'khotiyan' => 'required',
                'dag_no' => 'required',
                'lnd_size' => 'required',
                'lnd_cat' => 'required',
                'lnd_price' => 'required',
                'case_no' => 'required',
            ], [
                'dist.required' => 'Need',
                'upa.required' => 'Need',
                'mouja.required' => 'Need',
                'khotiyan.required' => 'Need',
                'dag_no.required' => 'Need',
                'lnd_size.required' => 'Need',
                'lnd_cat.required' => 'Need',
                'lnd_price.required' => 'Need',
                'case_no.required' => 'need',
            ]);

            $lastInsertedId = $landModel->saveData($request);

            DB::commit();
            return redirect('/lands')->with('success', 'New Registration added successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/lands/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $landModel = new Land();
        $page_title = 'জমির তথ্য সংশোধন';
//        $countryData = Country::all();
        try {
            $landData = $landModel->where('lands.id', $id)
                ->firstOrFail();

            $districtListArr = District::pluck('bn_name', 'id');
            $upozillaListArr = Upazila::where('district_id', $landData->dist)->pluck('bn_name', 'id')->all();
            $landTypeListArr = config('constants.landType');

            return view('lands.edit', compact('districtListArr', 'upozillaListArr', 'page_title', 'landTypeListArr', 'landData', 'id'));
        } catch (\Exception $exception) {
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();

        $landModel = new Land();

        DB::beginTransaction();
        try {

            $land_size = $formData['lnd_size'];
            $land_cat = $formData['lnd_cat'];
            $request->request->add(['lnd_price' => ($land_size * $land_cat)]);
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'dist' => 'required',
                'upa' => 'required',
                'mouja' => 'required',
                'khotiyan' => 'required',
                'dag_no' => 'required',
                'lnd_size' => 'required',
                'lnd_cat' => 'required',
                'lnd_price' => 'required',
                'case_no' => 'required',
            ], [
                'dist.required' => 'Need',
                'upa.required' => 'Need',
                'mouja.required' => 'Need',
                'khotiyan.required' => 'Need',
                'dag_no.required' => 'Need',
                'lnd_size.required' => 'Need',
                'lnd_cat.required' => 'Need',
                'lnd_price.required' => 'Need',
                'case_no.required' => 'need',
            ]);

//            dd($request);
            $landModel->updateData($request);
//            dd("1122");
            DB::commit();
            return redirect('/lands')->with('success', 'তথ্য সফলভাবে সংশোধন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            return redirect("/lands/$id/edit")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ' . $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $landModel = new Land();

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'is_active' => config('constants.status.Deleted'),
                ];

                $landModel->where('id', '=', $id)->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }


    }

    public function getLandInfoById(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('landId');
//            $val = getDistNameById($id);
//            dd($val);
            $resData = getLandById($id);
            return json_encode($resData);
        }
    }
}
