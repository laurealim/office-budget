<?php

namespace App\Http\Controllers;

use App\Models\BudgetCode;
use App\Models\BudgetType;
use Dompdf\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class BudgetCodeController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('budget-code-list')) {
            abort(404);
        }

        $page_title = 'বাজেট কোডের তালিকা';

        $bgtCodModel = new BudgetCode();

        $query = $bgtCodModel::query();

        $query->where('budget_status', '!=', config('constants.status.Deleted'))
            ->orderBy('budget_status', 'asc')
            ->orderBy('created_at', 'asc');

        $data = $query->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
//                    if ($row->status >= 1) {
                    if (auth()->user()->can('edit-budget-code'))
                        $action = '<a title="Edit" class="btn btn-success" id="edit-budget-code" href= ' . route('budget-code.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                    if (auth()->user()->can('delete-budget-code'))
                        $action = $action . '<a id=' . $row->id . ' href=' . route('budget-code.destroy', $row->id) . ' class="btn btn-danger delete-budget-code" title="Delete"><i class="fas fa-trash-alt"></i></a>';
//                    }
                    return $action;
                })
                ->editColumn('budget_status', function ($row) {
                    return config('constants.status.' . $row->budget_status);
                })
                ->editColumn('is_item_parent', function ($row) {
                    return config('constants.is_item_parent.' . $row->is_item_parent);
                })
                ->editColumn('budget_type', function ($row) {
                    return $budget_type = getBudgetTypeListById($row->bgt_type);
                })
                ->editColumn('budget_sub_type', function ($row) {
                    $budget_sub_type = getBudgetSubTypeList($row->bgt_type,$row->bgt_sub_type);
                    $budget_sub_type = $budget_sub_type[$row->bgt_sub_type];
                    return $budget_sub_type;
                })
                ->rawColumns(['action','budget_type','budget_sub_type'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('budget_code.index', compact('page_title'));
    }


    public function create()
    {
        if (!auth()->user()->can('create-budget-code')) {
            abort(404);
        }
        $page_title = 'বাজেট কোড তৈরি';
        $budget_type = getBudgetTypeList(false);
        return view('budget_code.create', compact('page_title', 'budget_type'));
    }


    public function store(Request $request)
    {
        if (!auth()->user()->can('create-budget-code')) {
            abort(404);
        }
        $formData = $request->all();
        $bgtCodModel = new BudgetCode();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'bgt_type' => 'required',
                'bgt_sub_type' => 'required',
                'bgt_type_code' => 'required',
                'bgt_sub_type_code' => 'required',
                'budget_code' => 'required',
                'budget_title' => 'required',
                'vat' => 'required',
                'tax' => 'required',
                'budget_status' => 'required',
            ], [
                'bgt_type.required' => 'বাজেটের ধরণ বাছাই করুন...',
                'bgt_sub_type.required' => 'বাজেটের উপ-ধরণ বাছাই করুন...',
                'bgt_type_code.required' => 'বাজেটের ধরণ এর কোড প্রদান করুন...',
                'bgt_sub_type_code.required' => 'বাজেটের উপ-ধরণ এর কোড প্রদান করুন...',
                'budget_code.required' => 'কোড প্রদান করুন...',
                'budget_title.required' => 'উপখাতের নাম প্রদাম করুন...',
                'vat.required' => 'ভ্যাট প্রদান করুন...',
                'tax.required' => 'ট্যাক্স প্রদান করুন...',
                'budget_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $bgtCodModel->saveData($request);

            DB::commit();
            return redirect('/budget-code')->with('success', 'নতুন বাজেট কোড যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/budget-code/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        if (!auth()->user()->can('edit-budget-code')) {
            abort(404);
        }
        $page_title = 'বাজেট কোড সংশোধন';
        $bgtCodModel = new BudgetCode();
        $budget_type = getBudgetTypeList(false);

        try {
            $query = $bgtCodModel::query();
            $query->where('id', $id);
//            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                $query->where('flats.created_by', '=', auth()->user()->id);
//            }
            $bgtCodData = $query->firstOrFail();
            $budget_Type_Id = $bgtCodData->bgt_type;
            $budget_sub_type = getBudgetSubTypeList($budget_Type_Id);
//            dd($budget_sub_type);

            return view('budget_code.edit', compact('page_title', 'bgtCodData', 'id', 'budget_type','budget_sub_type'));
        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/budget-code")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }


    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-budget-code')) {
            abort(404);
        }
        $formData = $request->all();
        $bgtCodModel = new BudgetCode();
        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'bgt_type' => 'required',
                'bgt_sub_type' => 'required',
                'bgt_type_code' => 'required',
                'bgt_sub_type_code' => 'required',
                'budget_code' => 'required',
                'budget_title' => 'required',
                'vat' => 'required',
                'tax' => 'required',
                'budget_status' => 'required',
            ], [
                'bgt_type.required' => 'বাজেটের ধরণ বাছাই করুন...',
                'bgt_sub_type.required' => 'বাজেটের উপ-ধরণ বাছাই করুন...',
                'bgt_type_code.required' => 'বাজেটের ধরণ এর কোড প্রদান করুন...',
                'bgt_sub_type_code.required' => 'বাজেটের উপ-ধরণ এর কোড প্রদান করুন...',
                'budget_code.required' => 'কোড প্রদান করুন...',
                'budget_title.required' => 'উপখাতের নাম প্রদাম করুন...',
                'vat.required' => 'ভ্যাট প্রদান করুন...',
                'tax.required' => 'ট্যাক্স প্রদান করুন...',
                'budget_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $bgtCodModel->updateData($request);

            DB::commit();
            return redirect('/budget-code')->with('success', 'সফলভাবে বাজেট কোড সংশোধন করা হয়েছে...।');
        } catch (\Exception $exception) {

            DB::rollback();
            return redirect("/budget-code/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-budget-code')) {
            abort(404);
        }
        $bgtCodModel = new BudgetCode();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'budget_status' => config('constants.status.Deleted'),
                ];

                try {
                    $query = $bgtCodModel::query();
                    $query->where('budget_codes.id', '=', $id);
//                    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                        $query->where('created_by', '=', auth()->user()->id);
//                    }
                    $query->update($updateData);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error']);
                }

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function getBudgetCodeList(Request $request)
    {
        if ($request->ajax()) {
            try {
                $budgetType = $request->post('budgetType');
                $resData = getBudgetCodeListByBudgetType($budgetType);
                return response()->json(['status' => 200, 'resData' => $resData]);
            } catch (\Exception $e) {
                return response()->json(['status' => 400]);
            }
        }
    }
}
