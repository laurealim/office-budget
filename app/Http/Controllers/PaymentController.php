<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Flat;
use App\Models\Ledger;
use App\Models\Payment;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $page_title = 'পরিশোধিত বিলের তালিকা';

        $paymentModel = new Payment();
        $query = $paymentModel::query();


        $query->select('payments.*', 'flats.flat_id as flatId', 'flats.flat', 'flats.address', 'flats.total_due')
            ->leftJoin('flats', 'payments.flat_id', 'flats.id');

        if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
            $query->where('flats.created_by', '=', auth()->user()->id);
        }
        $data = $query->get();
//            ->toArray();
//        pr($data);

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
//                    pr($row);
                    $action = '';
//                    if ($row->status == config('constants.payment_status.Unpaid')) {
//                        $action = '<a class="btn btn-success" title="Edit" id="edit-user" href= ' . route('payments.edit', $row->id) . '><i class="fas fa-edit"></i></a>
//<meta name="csrf-token" content="{{ csrf_token() }}">';
//<a id=' . $row->id . ' href=' . route('flats.assignedFlatLists', $row->id) . ' class="btn btn-info flatList"><i class="fas fa-house-user"></i></a>';
//                    }
                    $action .= ' <a id=' . $row->id . ' href=' . route('payments.show', $row->id) . ' class="btn btn-info delete-tenants" title="View"><i class="far fa-eye"></i></a>';
//                    $action .= ' <a id=' . $row->id . ' href=' . route('payments.create', [$row->invoice_code,$row->flat_id,$row->tenant_id]) . ' class="btn bg-purple pay_rent" title="Pay Flat Rent"><i class="fab fa-amazon-pay"></i></a>';
                    return $action;
                })
                ->addColumn('billing_time', function ($row) {
                    return config('constants.month_list.arr.' . $row->month) . ", " . en2bn($row->year) . ".";
                })
                ->addColumn('flat_name', function ($row) {
                    return $row->flatId . '-' . $row->flat . ' (' . $row->address . ')';
                })
                ->editColumn('bill_status', function ($row) {
                    $colorCode = "#1f2d3d";
                    $status = "";
                    switch ($row->status) {
                        case 0:
                            $colorCode = '#e83e8c';
                            break;
                        case 1:
                            $colorCode = '#ff851b';
                            break;
                        case 2:
                            $colorCode = '#3d9970';
                            break;
                        default:
                            $colorCode = '#1f2d3d';
                            break;
                    }
                    $status .= '<span style="color:' . $colorCode . '">' . config('constants.payment_status.' . $row->status) . '</span>';
//                    return config('constants.bill_status.' . $row->status);
                    return $status;
                })
                ->rawColumns(['action', 'billing_time', 'bill_status', 'flat_name'])
                ->make(true);
        }

        return view('payments.index', compact('page_title'));
    }

    public function create(Request $request)
    {
        $page_title = 'বিল পরিশোধ';

        $invoice_id = $request->invoice_id;
        $flat_id = $request->flat_id;
        $tenant_id = $request->tenant_id;

        if (empty($invoice_id) || empty($flat_id) || empty($tenant_id)) {
            $unpaidBillList = getUnpaidBillList();
        } else {
            $unpaidBillList = getUnpaidBillList($invoice_id);

        }

        return view('payments.create', compact('page_title', 'unpaidBillList'));
    }

    public function store(Request $request)
    {
        $paymentModel = new Payment();
        $billModel = new Bill();
        $ledgerModel = new Ledger();
        $flatModel = new Flat();

        $formData = $request->all();
//        pr($formData);
//        dd();
        $flatId = $formData['flat_id'];
        $tenantId = $formData['tenant_id'];
        $billId = $formData['bill_id'];
        $total_pay = $formData['total_pay'];
        $month = $formData['month'];
        $year = $formData['year'];
//        dd();
        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'total_pay' => 'required',
                'bill_id' => 'required',
            ], [
                'total_pay.required' => 'Need',
                'bill_id.required' => 'Need',
            ]);

            $billData = $billModel->where('id', '=', $billId)
                ->where('flat_id', '=', $flatId)
                ->where('tenant_id', '=', $tenantId)
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->get();
//                ->toArray();
            $billData = $billData[0];

            $ledgerData = $ledgerModel->where('invoice_code', '=', $billData['invoice_code'])
                ->where('flat_id', '=', $flatId)
                ->where('tenant_id', '=', $tenantId)
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->get();
//            dd($ledgerData);

            $ledgerData = $ledgerData[0];

            $flatData = $flatModel->where('id', '=', $flatId)
                ->where('tenant_id', '=', $tenantId)
                ->get();
//            dd($ledgerData);

            $flatData = $flatData[0];

//            pr($billData);
//            dd($flatData);

            $otherAmount = $billData->s_charge + $billData->adv_rent + $billData->other_charge + $billData->prv_due;
            $adv_due = $total_pay + $billData->total_pay - $billData->total_bill;
            $formData['invoice_code'] = $billData->invoice_code;

            /*************  Payment Table Insert *************/
            /*
             * 1=Partial (-ve)
             * 2=Full (0)
             * 3=Advance (+ve)
             * */
            $status = $adv_due >= 1 ? config('constants.payment_status.Advance') : ($adv_due <= -1 ? config('constants.payment_status.Partial') : config('constants.payment_status.Full'));

            $paymentModel->bill_id = $billData->id;
            $paymentModel->invoice_code = $billData->invoice_code;
            $paymentModel->flat_id = $billData->flat_id;
            $paymentModel->tenant_id = $billData->tenant_id;
            $paymentModel->rent_amount = $billData->rent;
            $paymentModel->other_amount = $otherAmount;
            $paymentModel->adv_amount = $adv_due;
            $paymentModel->total_amount = $total_pay + $billData->total_pay;
            $paymentModel->month = $billData->month;
            $paymentModel->year = $billData->year;
            $paymentModel->status = $status;
            $paymentModel->paid_by = auth()->user()->id;
            $paymentModel->save();
//            dd();
            /*************  Payment Table Insert *************/
            /*************  Ledger Table Update *************/
            $ledgerUpdateDataArr = ledgerUpdateAfterPayment($ledgerData, $formData);
            $resLedgerData = $ledgerModel->where('invoice_code', '=', $billData->invoice_code)
                ->where('flat_id', '=', $billData->flat_id)
                ->where('tenant_id', '=', $billData->tenant_id)
                ->where('year', '=', $billData->year)
                ->where('month', '=', $billData->month)
                ->update($ledgerUpdateDataArr);

//            $resData->wherer('invoice_code','=',$billData->invoice_code)
//                ->where('flat_id','=',$billData->flat_id)
//                ->where('tenant_id','=',$billData->tenant_id)
//                ->where('year','=',$billData->year)
//                ->where('month','=',$billData->month)
//                ->save();
//            pr($paymentModel);
            /*************  Ledger Table Update *************/
            /*************  Bill Table Update *************/

            $billUpdateDataArray = [
                'total_pay' => $total_pay + $billData->total_pay,
                'cur_due' => $billData->total_bill - $total_pay - $billData->total_pay,
                'status' => $ledgerUpdateDataArr['status'],
            ];
//            $billData->total_pay = $total_pay;
//            $billData->cur_due = $billData->total_bill - $total_pay;
//            $billData->status = $resData->status;
            $resBillData = $billModel->where('invoice_code', '=', $billData->invoice_code)
                ->where('flat_id', '=', $billData->flat_id)
                ->where('tenant_id', '=', $billData->tenant_id)
                ->where('year', '=', $billData->year)
                ->where('month', '=', $billData->month)
                ->update($billUpdateDataArray);
//            $billData->save();
//            pr($billData);
//            dd($billData);
            /*************  Bill Table Update *************/
            /*************  Flat Table Update *************/
            $flatCurDue = $billData->rent + $billData->s_charge + $billData->other_charge - $flatData->cur_due;

            $flatUpdateDataArray = [
                'total_due' => $flatData->total_due - $total_pay,
                'cur_due' => $flatCurDue,
            ];
            $resFlatData = $flatModel->where('id', '=', $billData->flat_id)
                ->where('tenant_id', '=', $billData->tenant_id)
                ->update($flatUpdateDataArray);
//            $flatData->save();
            /*************  Flat Table Update *************/

//            dd($billData);

            DB::commit();
            $msg = ' বিল পরিশোধ সম্পন্ন হয়েছে।';
            return redirect('/payments')->with('success', " $msg ");

        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            return redirect('/payments/create')->with('error', $exception->getMessage());
//            dd($exception);
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
