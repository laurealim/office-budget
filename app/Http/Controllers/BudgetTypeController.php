<?php

namespace App\Http\Controllers;

use App\Models\BudgetType;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class BudgetTypeController extends Controller
{

    public function index(Request $request)
    {
        if (!auth()->user()->can('budget-type-list')) {
            abort(404);
        }

        $page_title = 'বাজেট ধরনের তালিকা';

        $bgtTypModel = new BudgetType();

        $query = $bgtTypModel::query();
//        $query = DB::table('budget_types');

        $query->where('budget_type_status', '!=', config('constants.status.Deleted'))
            ->orderBy('budget_type_status', 'asc')
            ->orderBy('created_at', 'asc');

        $data = $query->get();
//        dd($data);

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
//                    if ($row->status >= 1) {
                        if (auth()->user()->can('edit-budget-type'))
                            $action = '<a title="Edit" class="btn btn-success" id="edit-budget-type" href= ' . route('budget-type.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                        if (auth()->user()->can('delete-budget-type'))
                            $action = $action . '<a id=' . $row->id . ' href=' . route('budget-type.destroy', $row->id) . ' class="btn btn-danger delete-budget-type" title="Delete"><i class="fas fa-trash-alt"></i></a>';
//                    }
                    return $action;
                })
                ->editColumn('budget_type', function ($row){
                    return config('constants.budget_type.arr.' . $row->budget_type);
                })
                ->editColumn('budget_type_status', function ($row) {
                    return config('constants.status.' . $row->budget_type_status);
                })
                ->rawColumns(['action'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('budget_type.index', compact('page_title'));
    }


    public function create()
    {
        if (!auth()->user()->can('create-budget-type')) {
            abort(404);
        }
        $page_title = 'বাজেট ধরন তৈরি';
//        $config('constants.status.Deleted');
        return view('budget_type.create', compact('page_title'));
    }


    public function store(Request $request)
    {
        if (!auth()->user()->can('create-budget-type')) {
            abort(404);
        }
        $formData = $request->all();
//        dd($formData['budget_type_name']);
        $request->request->add(['budget_type' => $formData['budget_type_name']]);
        $bgtTypModel = new BudgetType();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'budget_type_name' => 'required',
                'budget_type' => 'required',
                'budget_type_code' => 'required',
                'budget_type_status' => 'required',
            ], [
                'budget_type_name.required' => 'বাজেট ধরনের নাম প্রদাম করুন...',
                'budget_type.required' => 'ব্যবহারের ক্ষেত্র বাছাই করুন...',
                'budget_type_code.required' => 'কোড প্রদান করুন...',
                'budget_type_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $bgtTypModel->saveData($request);

            DB::commit();
            return redirect('/budget-type')->with('success', 'নতুন বাজেটের ধরন যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/budget-type/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        if (!auth()->user()->can('edit-budget-type')) {
            abort(404);
        }
        $page_title = 'বাজেট ধরন তথ্য সংশোধন';
        $bgtTypModel = new BudgetType();

        try {
            $query = $bgtTypModel::query();
            $query->where('id', $id);
//            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                $query->where('flats.created_by', '=', auth()->user()->id);
//            }
            $bgtTypData = $query->firstOrFail();

            return view('budget_type.edit', compact('page_title', 'bgtTypData', 'id'));
        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect('/flats/'.$id.'/create')->with('error', $exception->getMessage());
            return redirect("/budget-type")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }


    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-budget-type')) {
            abort(404);
        }
        $formData = $request->all();
        $request->request->add(['budget_type' => $formData['budget_type_name']]);
        $bgtTypModel = new BudgetType();
        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'budget_type_name' => 'required',
                'budget_type_code' => 'required',
                'budget_type_status' => 'required',
            ], [
                'budget_type_name.required' => 'বাজেট ধরনের নাম প্রদাম করুন...',
                'budget_type_code.required' => 'কোড প্রদান করুন...',
                'budget_type_status.required' => 'অবস্থা বাছাই করুন...',
            ]);

            $lastInsertedId = $bgtTypModel->updateData($request);

            DB::commit();
            return redirect('/budget-type')->with('success', 'সফলভাবে বাজেট ধরনের তথ্য সংশোধন করা হয়েছে...।');
        } catch (\Exception $exception) {

//            dd($exception);
            DB::rollback();
            return redirect("/budget-type/$id/edit")->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }


    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-budget-type')) {
            abort(404);
        }
        $bgtTypModel = new BudgetType();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'budget_type_status' => config('constants.status.Deleted'),
                ];

                try {
                    $query = $bgtTypModel::query();
                    $query->where('budget_types.id', '=', $id);
//                    if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
//                        $query->where('created_by', '=', auth()->user()->id);
//                    }
                    $query->update($updateData);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error']);
                }

                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
