<?php

namespace App\Http\Controllers;

use App\Models\InternalVoucher;
use App\Http\Requests\StoreInternalVoucherRequest;
use App\Http\Requests\UpdateInternalVoucherRequest;
use App\Models\OfficeVoucher;
use Dompdf\Exception;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class InternalVoucherController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('internal-voucher-list')) {
            abort(404);
        }

        $page_title = 'অভ্যন্তরিন ভাউচার এর তালিকা';

        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getFullInstituteList();
        $financialYearList = getFinancialYearList();
        $budgetCodeListArr = getBudgetCodeList();
        $unitList = config('constants.unit_value.arr');

        $internalVoucherModel = new InternalVoucher();
        $query = $internalVoucherModel::query();

        userDataAccess($query);

        $query->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc');

        $data = $query->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';
                    if ($row->status != config('constants.voucher_status.Deleted')) {
                        if ($row->status == config('constants.voucher_status.Pending')) {
                            if (auth()->user()->can('edit-internal-voucher')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-internal-voucher" href= ' . route('internal-voucher.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-internal-voucher')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('internal-voucher.destroy', $row->id) . ' class="btn btn-danger delete-budget-voucher" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-internal-voucher')) {
                                $val_app = config('constants.voucher_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('internal-voucher.budgetVoucherApproveReject') . ' class="btn btn-success bgt_voucher_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                            if (auth()->user()->can('can-reject-internal-voucher')) {
                                $val_app = config('constants.voucher_status.Rejected');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('internal-voucher.budgetVoucherApproveReject') . ' class="btn btn-warning bgt_voucher_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-times"></i></a>';
                            }
                        }
                    }
                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == config('constants.voucher_status.Pending'))
                        $color = '#9d1e15';
                    if ($row->status == config('constants.voucher_status.Rejected'))
                        $color = '#E97311';
                    if ($row->status == config('constants.voucher_status.Assigned'))
                        $color = '#E9118A';
                    if ($row->status == config('constants.voucher_status.Deleted'))
                        $color = '#FE0008';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.voucher_status.arr.' .
                            $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('institute_id', function ($row) use ($instituteListArr) {
                    return $instituteListArr[$row->institute_id];
                })
                ->editColumn('voucher_items', function ($row) use($unitList){
                    $decodedData = json_decode($row->voucher_items,true);
                    $voucher_items = "<ul><span>";
                    foreach ($decodedData as $key=>$vals) {
                        $item = $vals['items'];
                        $unit = $unitList[$vals['unit']];
                        $amount = $vals['amount'];
                        $price = $vals['price'];
                        $total_price = $vals['total_price'];

                        $voucher_items .= "<li><span style='font-weight: bold'>".$item."</span> মোট ".$total_price." টাকা। "."</li>";

                    }
                    $voucher_items .= "</span></ul>";

                    return $voucher_items;

                })
                ->editColumn('year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->year];
                })
                ->rawColumns(['action', 'status','voucher_items'])
                ->make(true);
        }

        return view('internal_voucher.index', compact('page_title'));

    }

    public function create()
    {
        if (!auth()->user()->can('create-internal-voucher')) {
            abort(404);
        }

        $page_title = 'অভ্যন্তরিন বাজেট ভাউচার';

        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
        $budgetInstituteList = getFullInstituteList();

//        dd($unitList);
//        $parentItemListArr = getItemNameAttribute();
        return view('internal_voucher.create', compact('page_title', 'financialList', 'budgetTypeList', 'budgetInstituteList'));
    }

    public function store(StoreInternalVoucherRequest $request)
    {
        if (!auth()->user()->can('create-internal-voucher')) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $internalVoucherModel = new InternalVoucher();
            $formData = $request->all();
            $voucherItemArr = [];

            $validated = $request->validated();
            $validated['created_by'] = auth()->user()->id;
//            pr($formData['voucher']);
            if (!empty($formData['voucher']['items'])) {
                foreach ($formData['voucher']['items'] as $items => $values) {
//                pr($values);
//                foreach ($values as $key => $value) {
//                    pr($key);
                    $voucherItemArr[$items]['items'] = $formData['voucher']['items'][$items];
                    $voucherItemArr[$items]['amount'] = $formData['voucher']['amount'][$items];
                    $voucherItemArr[$items]['unit'] = $formData['voucher']['unit'][$items];
                    $voucherItemArr[$items]['price'] = $formData['voucher']['price'][$items];
                    $voucherItemArr[$items]['total_price'] = $formData['voucher']['total_price'][$items];
//                }
                }
            }
//            dd($voucherItemArr);
            $voucherItemJson = json_encode($voucherItemArr, true);

            $validated['voucher_items'] = $voucherItemJson;
            $validated['status'] = config('constants.voucher_status.Pending');

            $resData = $internalVoucherModel->create($validated);
            $lastInsertedId = $resData->id;

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "public/images/voucher/internal_voucher/" . $lastInsertedId;
                $fileName = "internal_voucher";
                $fileRequest = $request->file('file_name');

                $voucherName = fileUpload($fileRequest, $path, $fileName);

                InternalVoucher::where('id', $lastInsertedId)->update(['attachment' => $voucherName]);
            }
            DB::commit();
            return redirect('/internal-voucher')->with('success', 'নতুন অতিস ভাউচার যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/internal-voucher/create')->with('error', $exception->getMessage());
//            dd($exception->getMessage());
        }
    }

    public function show(InternalVoucher $internalVoucher)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-internal-voucher')) {
            abort(404);
        }

        $page_title = 'অফিস ভাউচার তথ্য সংশোধন';
        $internalVoucherModel = new InternalVoucher();
        $budgetTypeList = getBudgetTypeList(false);

        try {
            $query = $internalVoucherModel::query();
            $query->where('internal_vouchers.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('internal_voucher.institute_id', '=', auth()->user()->institute_id);
            }

            // get particular data from office_budget_expenses table.
            $internalVoucherData = $query->firstOrFail();
//            $itemArrData = json_decode($officeVoucherData->voucher_items, true);
//            pr($officeVoucherData);

            // get budget code list by budget type from budget_type_code table.
            $budgetCodeList = getBudgetCodeListByBudgetType($internalVoucherData->budget_type);
//            pr($budgetCodeList);

            // Financial Year List
            $financialList = getFinancialYearList();
//            pr($financialList);

            // get Budget Type List form budget_type_lists table.
            $budgetTypeList = getBudgetTypeList(false);
//            pr($budgetTypeList);

            // get full Institute name
            $budgetInstituteList = getFullInstituteList();
//            pr($budgetInstituteList);

            $itemArrarList = getItemListByBudgetCodeId($internalVoucherData->budget_code_id);
            $itemArrarList = $itemArrarList->toArray();


            return view('internal_voucher.edit', compact('page_title', 'internalVoucherData', 'id', 'financialList', 'budgetTypeList', 'budgetCodeList', 'budgetInstituteList', 'itemArrarList'));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function update(UpdateInternalVoucherRequest $request, InternalVoucher $internalVoucher)
    {
        if (!auth()->user()->can('edit-internal-voucher')) {
            abort(404);
        }

        $formData = $request->all();

        DB::beginTransaction();

        try {
            $validatedData = $request->validated();
            $id = $internalVoucher->id;
            $voucherItemArr = [];

            if (!empty($formData['voucher']['items'])) {
                foreach ($formData['voucher']['items'] as $items => $values) {
                    $voucherItemArr[$items]['items'] = $formData['voucher']['items'][$items];
                    $voucherItemArr[$items]['amount'] = $formData['voucher']['amount'][$items];
                    $voucherItemArr[$items]['unit'] = $formData['voucher']['unit'][$items];
                    $voucherItemArr[$items]['price'] = $formData['voucher']['price'][$items];
                    $voucherItemArr[$items]['total_price'] = $formData['voucher']['total_price'][$items];
                }
            }
            $voucherItemJson = json_encode($voucherItemArr, true);
            $validatedData['voucher_items'] = $voucherItemJson;

            $internalVoucher->updated_by = auth()->user()->id;
            $internalVoucher->year = $formData['year'];
            $internalVoucher->v_date = $formData['v_date'];
            $internalVoucher->institute_id = $formData['institute_id'];
            $internalVoucher->total_cost = $formData['total_cost'];
            $internalVoucher->voucher_items = $voucherItemJson;


//            dd($validatedData);
            $internalVoucher->save();

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "public/images/voucher/internal_voucher/" . $id;
                $fileName = "internal_voucher";
                $fileRequest = $request->file('file_name');

                $voucherName = fileUpload($fileRequest, $path, $fileName);

                $internalVoucher::where('id', $id)->update(['attachment' => $voucherName]);
            }

            DB::commit();
            return redirect('/internal-voucher')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception->getMessage());
            return redirect("/internal-voucher/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি... " . $exception->getMessage());
        }
    }

    public function destroy(InternalVoucher $internalVoucher)
    {

        if (!auth()->user()->can('delete-internal-voucher')) {
            abort(404);
        }
        DB::beginTransaction();
        try {
            if (!empty($internalVoucher->id)) {
                $updateData = [
                    'status' => config('constants.voucher_status.Deleted'),
                ];

                $internalVoucher->update($updateData);
                DB::commit();
                session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function budgetVoucherApproveReject(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('id');
            $value = $request->post('value');
            $internalVoucher = new InternalVoucher();

            DB::beginTransaction();
            try {
                $maxVal = DB::table('internal_vouchers')
                    ->where('status', '!=', 4)
                    ->max('voucher_no');

                $voucher_no = $maxVal + 1;

                $stateStatus = 'অনুমোদন';

                if ($value == 1) {
                    $updateData = [
                        'status' => config('constants.voucher_status.Approved'),
                        'voucher_no' => $voucher_no,
                    ];
                } elseif ($value == 2) {
                    $stateStatus = 'বাতিল';
                    $updateData = [
                        'status' => config('constants.voucher_status.Rejected'),
                    ];
                }

                try {
                    $query = $internalVoucher::query();
                    $query->where('id', '=', $id);
                    $query->update($updateData);
                    DB::commit();

                    $request->session()->flash('success', 'সফলভাবে '.$stateStatus.' করা হয়েছে...');
                    return response()->json(['status' => 'success']);
                } catch (\Exception $exception) {
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error0']);
                }
            } catch (Exception $e) {
                DB::rollback();
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error2']);
            }
        }
    }
}
