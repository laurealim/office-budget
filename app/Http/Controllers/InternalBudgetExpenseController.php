<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InternalBudgetExpense;
use DataTables;
use Illuminate\Support\Facades\DB;

class InternalBudgetExpenseController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('internal-budget-expense-list')) {
            abort(404);
        }

        $page_title = 'আভ্যন্তরীণ বাজেট খরচ তালিকা';

        $internalBudgetExpenseModel = new InternalBudgetExpense();

        $query = $internalBudgetExpenseModel::query();

        $query->orderBy('status', 'asc');
        $query->orderBy('created_at', 'desc');

        $data = $query->get();

        $budgetTypeListArr = getBudgetTypeList(false);
        $instituteListArr = getInstituteList();
        $financialYearList = getFinancialYearList();
//        $budgetTypeCodeId = getBudgetTypeCodeByOptions(4);

        if ($request->ajax()) {
//            dd($data);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '';

                    if ($row->status != config('constants.budget_status.Rejected')) {
                        if ($row->status == config('constants.budget_status.Pending')) {
                            if (auth()->user()->can('edit-internal-budget-expense')) {
                                $action = '<a title="Edit" class="btn btn-primary" id="edit-internal-budget-expense" href= ' . route('internal-budget-expense.edit', $row->id) . '><i class="fas fa-edit"></i></a>
<meta name="csrf-token" content="{{ csrf_token() }}">';
                            }
                            if (auth()->user()->can('delete-internal-budget-expense')) {
                                $action = $action . '<a id=' . $row->id . ' href=' . route('internal-budget-expense.destroy', $row->id) . ' class="btn btn-danger delete-internal-budget-expense" title="Delete"><i class="fas fa-trash-alt"></i></a>';
                            }
                            if (auth()->user()->can('can-approve-internal-budget-expense')) {
                                $val_app = config('constants.budget_status.Approved');
                                $action = $action . ' <a id=' . $row->id . ' href=' . route('internal-budget-expense.internalBudgetExpenseApproveReject') . ' class="btn btn-success int_bgt_exp_approve" value=' . $val_app . ' title="Confirm"><i class="fas fa-check"></i></a>';
                            }
                        }
                    }
                    return $action;
                })
                ->addColumn('status', function ($row) {
                    $color = '#61A151';
                    if ($row->status == 0)
                        $color = '#9d1e15';
                    if ($row->status == 2)
                        $color = '#E97311';
                    $status = '<b><span style="color: ' . $color . '">' . config('constants.budget_status.' . $row->status) . '</span></b>';
                    return $status;
                })
                ->editColumn('budget_type', function ($row) use ($budgetTypeListArr) {
                    return $budgetTypeListArr[$row->budget_type];
                })
                ->editColumn('financial_year', function ($row) use ($financialYearList) {
                    return $financialYearList[$row->financial_year];
                })
                ->addColumn('file_name', function ($row) {
                    $icon = "";
                    if (!empty($row->file_name)) {
                        $path = asset("images/budget/internal_budget_expense/" . $row->id . "/" . $row->file_name);
                        $icon = '<a target="_blank" title="Attachment" class="btn btn-info" id="internal-budget-expense" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i></a>';
                    }
                    return $icon;
                })
                ->rawColumns(['action', 'status', 'file_name'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('internal_budget_expense.index', compact('page_title'));
    }

    public function create()
    {
        if (!auth()->user()->can('create-internal-budget-expense')) {
            abort(404);
        }
        $page_title = 'আভ্যন্তরীণ অফিস খরচ তৈরি';
        $financialList = getFinancialYearList();
        $budgetTypeList = getBudgetTypeList(false);
//        $parentItemListArr = getItemNameAttribute();
        return view('internal_budget_expense.create', compact('page_title', 'financialList', 'budgetTypeList'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('create-internal-budget-expense')) {
            abort(404);
        }

        $formData = $request->all();
//        dd($formData);

//        $budgetTypeCodeId = getBudgetTypeCodeByOptions($formData['budget_type']);
        $internalBudgetExpenseModel = new InternalBudgetExpense();

        DB::beginTransaction();
        try {
            $data = $this->validate($request, [
                'financial_year' => 'required',
                'budget_type' => 'required',
                'expn_date' => 'required',
            ], [
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'expn_date.required' => 'তারিখ উল্ল্যেখ করুন।',
            ]);

            $serialArr = array_values($formData['details']);
            $details = json_encode($serialArr);
            $request->request->add(['details' => $details]);
            $request->request->add(['institute_id' => auth()->user()->institute_id]);

//            $request->request->remove('bgt_code');

            $lastInsertedId = $internalBudgetExpenseModel->saveData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $budget_id = $lastInsertedId;
                $path = "images/budget/internal_budget_expense/";
                $picName = '';

                // cache the file
                $file = $request->file('file_name');
//                foreach ($files as $key => $file) {
                $destinationPath = $path . $budget_id;
                $file_name = "budget." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                InternalBudgetExpense::where('id', $budget_id)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/internal-budget-expense')->with('success', 'অফিস বাজেট খরচ যুক্ত হয়েছে...');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/internal-budget-expense/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (!auth()->user()->can('edit-internal-budget-expense')) {
            abort(404);
        }
        $page_title = 'আভ্যন্তরীণ অফিস খরচ সংশোধন';
        $internalBudgetExpenseModel = new InternalBudgetExpense();

        try {
            $query = $internalBudgetExpenseModel::query();
            $query->where('internal_budget_expenses.id', $id);

            if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                $query->where('internal_budget_expenses.created_by', '=', auth()->user()->id)
                    ->where('internal_budget_expenses.institute_id', '=', auth()->user()->institute_id);
            }

            $internalBudgetExpenseData = $query->firstOrFail();
            $detailsDecodedData = json_decode($internalBudgetExpenseData->details, true);
            $financialList = getFinancialYearList();
            $budgetTypeList = getBudgetTypeList(false);

            return view('internal_budget_expense.edit', compact('page_title', 'internalBudgetExpenseData', 'id', 'financialList', 'budgetTypeList', 'detailsDecodedData'));
        } catch (\Exception $exception) {
            return redirect('/internal_budget_expense')->with('error', $exception->getMessage());
//            return redirect("/office-budget")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ');
        }
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-internal-budget')) {
            abort(404);
        }
        $formData = $request->all();
//        dd($formData);
//        $budgetTypeCodeId = getBudgetTypeCodeByOptions($formData['budget_type']);
        $internalBudgetExpenseModel = new InternalBudgetExpense();

        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'financial_year' => 'required',
                'budget_type' => 'required',
                'expn_date' => 'required',
            ], [
                'financial_year.required' => 'আর্থিক বছর নির্বাচন করুন।',
                'budget_type.required' => 'বাজেটের ধরন বাছাই করুন।',
                'expn_date.required' => 'তারিখ উল্ল্যেখ করুন।',
            ]);

            $serialArr = array_values($formData['details']);
            $details = json_encode($serialArr);
            $request->request->add(['details' => $details]);

//            $request->request->remove('total_cur_balance');
            $lastInsertedId = $internalBudgetExpenseModel->updateData($request);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('file_name')) {
                $path = "images/budget/internal_budget_expense/";
                $picName = '';

                // cache the file
                $file = $request->file('file_name');
//                foreach ($files as $key => $file) {
                $destinationPath = $path . $id;
                $file_name = "budget." . $file->getClientOriginalExtension();
                $picName .= $file_name;
                $file->move($destinationPath, $file_name);
//                }
                InternalBudgetExpense::where('id', $id)->update(['file_name' => $picName]);
            }

            DB::commit();
            return redirect('/internal-budget-expense/')->with('success', 'তথ্য সফলভাবে সংশোধন করা হয়েছে...');
        } catch (\Exception $exception) {

            DB::rollback();
            dd($exception);
            return redirect("/internal-budget-expense/$id/edit")->with('error', "তথ্য সংশোধন করা সম্ভব হয় নি...");
        }
    }

    public function destroy(Request $request, $id)
    {
        if (!auth()->user()->can('delete-internal-budget-expense')) {
            abort(404);
        }
        $internalBudgetExpenseModel = new InternalBudgetExpense();
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.budget_status.Rejected'),
                ];
                $query = $internalBudgetExpenseModel::query();
                $query->where('id', '=', $id);
                if (!auth()->user()->hasRole(['admin', 'super-admin'])) {
                    $query->where('created_by', '=', auth()->user()->id)
                        ->where('institute_id', '=', auth()->user()->institute_id);
                }
                $query->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function internalBudgetExpenseApproveReject(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->post('id');
            $statusValue = $request->post('value');
            $internalBudgetExpenseModel = new InternalBudgetExpense();

            $intDataArr = getInternalBalanceExpenseByOption($id);
            if (empty($intDataArr)) {
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error']);
            }

            $intDataArr = $intDataArr->toArray();
            $intDataArr = $intDataArr[0];
            $financial_year = $intDataArr['financial_year'];
            $total_balances = $intDataArr['total_price'];

            $resData = [
                'financial_year' => $financial_year,
                'total_balances' => $total_balances,
                'institute_id' => auth()->user()->institute_id,
                'created_by' => auth()->user()->id,
            ];
            // only (id,null,null) = [0=>[data]]
            // only (null,status,null) = [0=>[data],1=>[data],2=>[data]]
            // only (null,null,true) = [1=>1000,3=>5000,4=>2000]
            // only (null,null,null) = [0=>[data],1=>[data],2=>[data]]


            DB::beginTransaction();
            try {
                $resultData = updateInternalBudgetBalance($resData, $financial_year,false);
                if ($resultData) {
                    $updateData = [
                        'status' => $statusValue,
                    ];

                    try {
                        $query = $internalBudgetExpenseModel::query();
                        $query->where('id', '=', $id);
                        $query->update($updateData);
                        DB::commit();

                        $request->session()->flash('success', 'সফলভাবে অনুমোদন করা হয়েছে...');
                        return response()->json(['status' => 'success']);
                    } catch (\Exception $exception) {
                        DB::rollback();
                        $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                        return response()->json(['status' => 'error0']);
                    }
                } else {
                    DB::rollback();
                    $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                    return response()->json(['status' => 'error1']);
                }
            } catch (Exception $e) {
                DB::rollback();
                $request->session()->flash('errors', 'অনুমোদন করা সম্ভব হয় নি...');
                return response()->json(['status' => 'error2']);
            }
        }
    }
}
