<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AllAjaxHelperController extends Controller
{
    public function getUnitList(Request $request){
        if ($request->ajax()) {
            try {
                $unitList = config('constants.unit_value.arr');
                return response()->json(['status' => 200, 'unitList' => $unitList]);
            }catch (\Exception $exception){
                dd($exception);
            }
        }
    }
    public function getItemList(Request $request){
        if ($request->ajax()) {
            try {
                $budget_code_id = null;
                if(isset($request->budget_code_id)){
                    $budget_code_id = $request->budget_code_id;
                }
//                dd($budget_code_id);
                $itemList = getItemListByBudgetCodeId($budget_code_id);
                return response()->json(['status' => 200, 'itemList' => $itemList]);
            }catch (\Exception $exception){
                dd($exception);
                return response()->json(['status' => 400]);
            }
        }
    }
}
