<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficeBudgetBalance extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'status', 'institute_id'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != 'file_name') {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.budget_status.Pending');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "file_name" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

    public function getFlatNameAttribute()
    {
        return $this->flat_id . ' (' . $this->flat.' - '.$this->address.')';
    }
}
