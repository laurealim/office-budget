<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    protected $fillable = [
        'financial_year','budget_type','code','institute_id','vat','tax','amount','usable_amount','total_previous_amount','total_current_amount','status'
    ];

//    public function saveData($data)
//    {
//        foreach ($data->request as $key => $value) {
//            if ($key != "_token" && $key != 'image') {
//                $this->$key = $value;
//            }
//        }
//        $this->created_by = auth()->user()->id;
//        $this->total_due = $data->monthly_bill;
//        $this->status = config('constants.flat_status.Free');
//        $this->save();
//        return $this->id;
//    }
//
//    public function updateData($data)
//    {
//        $ticket = $this->find($data['id']);
//        foreach ($data->request as $key => $value) {
//            if ($key != "_token" && $key != "image" && $key != "_method") {
//                $ticket->$key = $value;
//            }
//        }
//        $ticket->updated_by = auth()->user()->id;
//        $ticket->total_due = $data->monthly_bill;
//        $ticket->save();
//        return 1;
//    }
}
