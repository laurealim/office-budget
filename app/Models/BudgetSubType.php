<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetSubType extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_by', 'updated_by', 'budget_type_id', 'budget_sub_type_status', 'budget_sub_type_code', 'budget_sub_type_name'
    ];

    public function getBudgetSubTypeNamesAttribute()
    {
//        if(!empty($this->budget_sub_type_code))
            return $this->budget_sub_type_name . ' (' .$this->budget_sub_type_code.')';
//        else
//            return $this->budget_sub_type_name;
    }

//    public function budgetSubTypeNames(): BudgetSubType
//    {
//        return BudgetSubType::get(fn() => $this->budget_sub_type_name . ' (' .$this->budget_sub_type_code.')');
//    }
}
