<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetType extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'budget_type_status', 'budget_type_code', 'budget_type_name','budget_type'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
//        $this->status = config('constants.flat_status.Free');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

    public function getBudgetTypeNamesAttribute()
    {
        if(!empty($this->budget_type_code))
            return $this->budget_type_name . ' (' .$this->budget_type_code.')';
        else
            return $this->budget_type_name;
    }
}
