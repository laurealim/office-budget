<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternalVoucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_by', 'updated_by', 'status', 'voucher_items', 'budget_code_id', 'year', 'total_cost', 'institute_id', 'voucher_no', 'is_office_exp','v_date'
    ];
}
