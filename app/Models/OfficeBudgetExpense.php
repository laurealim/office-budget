<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficeBudgetExpense extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'status', 'institute_id','cur_exp','budget_full_code'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != 'file_name') {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.budget_status.Pending');
//        $this->expn_date = date('Y-m-d', strtotime(now));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "file_name" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }
}
