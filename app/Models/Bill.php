<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
         'status'
    ];

    public function     saveData($data)
    {
        foreach ($data->request as $key => $value) {
//            pr($key);
            if ($key != "_token" && $key != 'image') {
                $this->$key = $value;
            }
        }
        $this->status = config('constants.bill_status.Unpaid');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "image" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->total_due = $data->monthly_bill;
        $ticket->save();
        return 1;
    }
}
