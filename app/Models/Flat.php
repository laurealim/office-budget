<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'status','total_due'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != 'image') {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->total_due = $data->monthly_bill + $data->advance_bill - $data->prev_due;
        $this->cur_due = $data->monthly_bill + $data->advance_bill;
        $this->status = config('constants.flat_status.Free');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "image" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->total_due = $data->monthly_bill;
        $ticket->save();
        return 1;
    }

    public function getFlatNameAttribute()
    {
        return $this->flat_id . ' (' . $this->flat.' - '.$this->address.')';
    }
}
