<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'name','bn_name','division_id, url',
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }

        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $ticket->$key = $value;
            }
        }

        $ticket->save();
        return 1;
    }

    public function divisions()
    {
        return $this->hasOne('App\Models\Division',"id", "division_id");
    }

    public function upazilas()
    {
        return $this->hasMany('App\Models\Upazila')->orderBy('name', 'ASC');
    }

    public function relief()
    {
        return $this->hasMany('App\Models\Relief')->orderBy('name', 'ASC');
    }
}
