<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\City;

class Country extends Model
{
    protected $fillable = [
        'name', 'created_by', 'sortname', 'phonecode',
    ];

    public function saveData($data)
    {
        $this->created_by = auth()->user()->id;
        $this->name = $data->name;
        $this->sortname = $data->sortname;
        $this->phonecode = $data->phonecode;
        $this->status = $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data->name;
        $ticket->sortname = $data->sortname;
        $ticket->phonecode = $data->phonecode;
        $ticket->status = $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function cities()
    {
        return $this->hasMany('App\Models\City')->orderBy('name', 'ASC');
    }

    public function divisions()
    {
        return $this->hasMany('App\Models\Division')->orderBy('name', 'ASC');
    }
}
