<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetCode extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'budget_status'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;

        $bgt_type_code = $data->bgt_type_code;
        $bgt_sub_type_code = $data->bgt_sub_type_code;
        $budget_code = $data->budget_code;

        $full_budget_code = (($bgt_type_code * 100000) + ($bgt_sub_type_code * 1000) + $budget_code);
        $this->full_budget_code = $full_budget_code;
//        $this->status = config('constants.flat_status.Free');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;

        $bgt_type_code = $data['bgt_type_code'];
        $bgt_sub_type_code = $data['bgt_sub_type_code'];
        $budget_code = $data['budget_code'];

        $full_budget_code = (($bgt_type_code * 100000) + ($bgt_sub_type_code * 1000) + $budget_code);
        $ticket->full_budget_code = $full_budget_code;

        $ticket->save();
        return 1;
    }

    public function getBudgetCodeNameAttribute()
    {
        return $this->budget_title . ' (' . $this->budget_code . ')';
    }
}
