<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficeVoucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_by', 'updated_by', 'status', 'voucher_items', 'budget_code_id', 'year', 'total_cost', 'institute_id', 'voucher_no','v_date','budget_type'
    ];
}
