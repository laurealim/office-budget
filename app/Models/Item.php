<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'item_status'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
//        $this->status = config('constants.flat_status.Free');
//        $this->dob = date('Y-m-d', strtotime($data->dob));

        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

    public function budgte_codes()
    {
        return $this->hasOne('App\BudgetCode', 'id','item_type');
    }

//    public function getItemNameAttribute()
//    {
//        return $this->item_name . ' (' . getBudgetCodeById($this->dist).' - '.getUpazilaNameById($this->upaz).')';
//    }
}
