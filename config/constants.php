<?php
return [
    'status' => [
        0 => 'মুছে ফেলা হয়েছে',
        1 => 'সক্রিয়',
        2 => 'নিষ্ক্রিয়',
        'Deleted' => 0,
        'Active' => 1,
        'Inactive' => 2,
        'arr' => [
            0 => 'মুছে ফেলা হয়েছে',
            1 => 'সক্রিয়',
            2 => 'নিষ্ক্রিয়',
        ]
    ],

    'budget_status' => [
        0 => 'অনিষ্পাদিত',
        1 => 'অনুমোদিত',
        2 => 'বাতিল',
        'Pending' => 0,
        'Approved' => 1,
        'Rejected' => 2,
        'arr' => [
            0 => 'অনিষ্পাদিত',
            1 => 'অনুমোদিত',
            2 => 'বাতিল',
        ]
    ],

    'voucher_status' => [
        0 => 'অনিষ্পাদিত',
        1 => 'অনুমোদিত',
        2 => 'বাতিল',
        3 => 'ব্যবহৃত',
        4 => 'মুছে ফেলা হয়েছে',
        'Pending' => 0,
        'Approved' => 1,
        'Rejected' => 2,
        'Assigned' => 3,
        'Deleted' => 4,
        'arr' => [
            0 => 'অনিষ্পাদিত',
            1 => 'অনুমোদিত',
            2 => 'বাতিল',
            3 => 'ব্যবহৃত',
            4 => 'মুছে ফেলা হয়েছে',
        ]
    ],

    'ledger_status' => [
        0 => 'সম্পন্ন',
        1 => 'চলমান',
        'Finished' => 0,
        'Running' => 1,
        'arr' => [
            0 => 'সম্পন্ন',
            1 => 'চলমান',
        ]
    ],

//    'budget_type' => [
//        'Salary' => 0,
//        'Contingency' => 1,
//        'Training' => 2,
//        'Seminar' => 3,
//        'Contest' => 4,
//        'arr' => [
//            0 => 'নগদ মঞ্জুরী ও বেতন',
//            1 => 'পণ্য ও সেবার ব্যবহার',
//            2 => 'প্রশিক্ষণ',
//            3 => 'সেমিনার',
//            4 => 'দিবস ও প্রতিযোগিতা',
//        ],
//        'code' => [
//            0 => 3111,
//            1 => 32,
//            2 => 3231301,
//            3 => 3211111,
//            4 => 32,
//        ]
//    ],

    'budget_type' => [
        'CentralUse' => 1,
        'LocalUse' => 2,
        'arr' => [
            1 => 'কেন্দ্র থেকে নিয়ন্ত্রিত',
            2 => 'নিজ দপ্তর হতে নিয়ন্ত্রনযোগ্য',
        ]
    ],

    'userType' => [
        1 => 'Super User',
        2 => 'Owner',
        3 => 'Tenant',
        'SuperUser' => 1,
        'Owner' => 2,
        'Tenant' => 3,
    ],

    'month_list' => [
        'arr' => [
            1 => 'জানুয়ারি',
            2 => 'ফেভ্রুয়ারি',
            3 => 'মার্চ',
            4 => 'এপ্রিল',
            5 => 'মে',
            6 => 'জুন',
            7 => 'জুলাই',
            8 => 'আগস্ট',
            9 => 'সেপ্টেম্বর',
            10 => 'অক্টোবর',
            11 => 'নভেম্বর',
            12 => 'ডিসেম্বর',
        ]
    ],

    'controller_list' => [
        'arr' => [
            'UserController' => 'User',
            'PermissionController' => 'Permission',
            'RoleController' => 'Role',
            'AssignLandController' => 'Land Assign',
            'ApplicantController' => 'Applicant',
            'LandController' => 'Land',
            'HomeController' => 'Dashboard',
            'CountryController' => 'Country',
            'DivisionController' => 'Division',
            'DistrictController' => 'District',
            'UpazilaController' => 'Upazila',
            'UnionController' => 'Union',
            'FlatController' => 'Flat',
            'TenantController' => 'Tenant',
            'BillController' => 'Bill',
            'PaymentController' => 'Payment',
            'InstituteController' => 'Institute',
            'BudgetCodeController' => 'Budget Code',
            'ItemController' => 'Item',
            'BudgetTypeController' => 'Budget Type',
            'OfficeBudgetController' => 'Office Budget',
            'OfficeBudgetExpenseController' => 'Office Budget Expense',
            'OfficeBalanceController' => 'Office Balance',
            'InternalBudgetController' => 'Internal Budget',
            'InternalBudgetExpenseController' => 'Internal Budget Expense',
            'InternalBalanceController' => 'Internal Balance',
            'BudgetSubTypeController' => 'Budget Sub Type',
            'OfficeVoucherController' => 'Office Voucher',
            'InternalVoucherController' => 'Internal Voucher',
        ],
        'list' => [
            'User',
            'Permission',
            'Role',
            'Assign Land',
            'Applicant',
            'Land',
            'Home',
            'Country',
            'Division',
            'District',
            'Upazila',
            'Union',
            'Flat',
            'Tenant',
            'Bill',
            'Payment',
            'Institute',
            'Budget Code',
            'Item',
            'Budget Type',
            'Office Budget',
            'Office Budget Expense',
            'Office Balance',
            'Internal Budget',
            'Internal Budget Expense',
            'Internal Balance',
            'Budget Sub Type',
            'Office Voucher',
            'Internal Voucher',
        ],
    ],

    'gender' => [
        'arr' => [
            1 => 'পুরুষ',
            2 => 'মহিলা',
            3 => 'অন্যান্য',
        ],
        'Male' => 1,
        'Female' => 2,
        'Others' => 3,
    ],

    'is_item_parent' => [
        'arr' => [
            0 => 'না',
            1 => 'হ্যাঁ',
        ],
        0 => 'না',
        1 => 'হ্যাঁ',
        'No' => 0,
        'Yes' => 1,
    ],

    'budget_add_remove' => [
        'Add' => 1,
        'Deduction' => 2,
        'Edit' => 3,
        'arr' => [
            1 => 'Add',
            2 => 'Deduction',
            3 => 'Edit',
        ]
    ],

    'unit_value' =>[
        'arr' => [
            1=>'টি',
            2=>'বক্স',
            3=>'প্যাকেট',
            4=>'কেজি',
            5=>'ডজন',
            6=>'বোতল',
            7=>'সেট',
        ],
        1=>'টি',
        2=>'বক্স',
        3=>'প্যাকেট',
        4=>'কেজি',
        5=>'ডজন',
        6=>'বোতল',
        7=>'সেট',
    ],

//
//    'landStatus' => [
//        1 => 'Unassigned',
//        2 => 'Assigned',
//        'Unassigned' => 1,
//        'Assigned' => 2,
//    ],
//
//    'state' => [
//        1 => 'Pending',
//        2 => 'Approved',
//        3 => 'Rejected',
//        'Pending' => 1,
//        'Approved' => 2,
//        'Rejected' => 3,
//    ],
//
//    'is_assigned' => [
//        0 => 'Unassigned',
//        1 => 'Assigned',
//        2 => 'Confirmed',
//        'Unassigned' => 0,
//        'Assigned' => 1,
//        'Confirmed' => 2,
//    ],
//
//    'training_status' => [
//        1 => 'অনিষ্পাদিত',
//        2 => 'সম্পন্ন',
//        0 => 'বাতিল',
//        'Pending' => 1,
//        'Complete' => 2,
//        'Postponed' => 0,
//    ],
//
//    'is_deleted' => [
//        1 => 'Deleted',
//        0 => 'Active',
//        'Deleted' => 1,
//        'Active' => 0,
//    ],
//
//    'flat_status' => [
//        1 => 'সংরক্ষিত',
//        2 => 'হস্তনাতর যোগ্য',
//        0 => 'বাতিল',
//        'Booked' => 1,
//        'Free' => 2,
//        'Deleted' => 0,
//    ],
//
//    'tenant_status' => [
//        1 => 'সক্রিয়',
//        2 => 'নিষ্ক্রিয়',
//        0 => 'বাতিল',
//        'Active' => 1,
//        'Inactive' => 2,
//        'Deleted' => 0,
//    ],
//
//    'flat_facility' => [
//        0 => 'না',
//        1 => 'হ্যাঁ',
//    ],
//
//    'location_code' => [
//        'dist' => [
//            51 => 35
//        ],
//        'upo' => [
//            385 => 32,
//            386 => 43,
//            387 => 91,
//            388 => 51,
//            389 => 38,
//        ],
//        'uni' => [
//            3502 => 1,
//            3503 => 3,
//            3504 => 4,
//            3505 => 5,
//            3506 => 6,
//            3507 => 7,
//            3508 => 8,
//            3509 => 9,
//            3510 => 10,
//            3511 => 11,
//            3512 => 12,
//            3513 => 13,
//            3514 => 14,
//            3515 => 15,
//            3516 => 16,
//            3517 => 18,
//            3518 => 19,
//            3519 => 20,
//            3520 => 21,
//            3521 => 17,
//            3522 => 2,
//            3523 => 5,
//            3524 => 10,
//            3525 => 7,
//            3526 => 9,
//            3527 => 12,
//            3528 => 14,
//            3529 => 4,
//            3530 => 3,
//            3531 => 1,
//            3532 => 8,
//            3533 => 2,
//            3534 => 6,
//            3535 => 13,
//            3536 => 11,
//            3537 => 1,
//            3538 => 3,
//            3539 => 4,
//            3540 => 2,
//            3541 => 6,
//            3542 => 2,
//            3543 => 3,
//            3544 => 4,
//            3545 => 1,
//            3546 => 6,
//            3547 => 9,
//            3548 => 11,
//            3549 => 0,
//            3550 => 5,
//            3551 => 7,
//            3552 => 12,
//        ]
//    ],

//
//    'flat_face' => [
//        'দক্ষিনমুখী',
//        'উত্তরমুখী',
//        'পূর্বমুখী',
//        'পশ্চিমমুখী'
//    ],
//
//    'flat_floor' => [
//        '১ম(নিচ) তলা',
//        '২য় তলা',
//        '৩য় তলা',
//        '৪র্থ তলা',
//        '৫ম তলা',
//        '৬ষ্ট তলা',
//        '৭ম তলা',
//        '৮ম তলা',
//        '৯ম তলা',
//        '১০ম তলা'
//    ],
//
//    'bill_status' => [
//        0 => 'বকেয়া',
//        1 => "আংশিক পরিশোধিত",
//        2 => "পরিশোধিত",
//        'Unpaid' => 0,
//        "Partial" => 1,
//        "Full" => 2,
//    ],
//
//    'payment_status' => [
//        0 => 'বকেয়া',
//        1 => "আংশিক পরিশোধিত",
//        2 => "পরিশোধিত",
//        3 => "অগ্রিম পরিশোধিত",
//        'Unpaid' => 0,
//        "Partial" => 1,
//        "Full" => 2,
//        "Advance" => 3,
//    ],
//
//    'ledger_status' => [
//        0 => 'বকেয়া',
//        1 => "আংশিক পরিশোধিত",
//        2 => "পরিশোধিত",
//        'Unpaid' => 0,
//        "Partial" => 1,
//        "Full" => 2,
//    ],
//
//    'advance_pay' => [
//        0 => 'Unpaid',
//        1 => 'Billed',
//        2 => 'Paid',
//        'Unpaid' => 0,
//        "Billed" => 1,
//        "Paid" => 2,
//    ],


];
?>
