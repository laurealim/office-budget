<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
//    return view('welcome');
    return view('auth.login');
});

//    Cache Clear Route
Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "Cache cleared";
});


Auth::routes();
Route::group(['middleware' => ['role:admin|user|super-admin|flat-owner']], function () {

    /*  Office Budget Calculation Test Route   */
    Route::get('office-budget/test', 'OfficeBudgetController@calculation')->name('office-budget.test');   // index
    Route::get('office-budget/pdf', 'OfficeBudgetController@createPDF')->name('office-budget.pdf');   // index


    /*  DB Migration Script Route   */
    Route::get('migrate/budget', 'MigrateController@budget')->name('migrate.budget');   // index

    //*  Country Route Section */
    Route::get('country/list', 'CountryController@list')->name('country.list');   // index
    Route::get('country/add', 'CountryController@form')->name('country.form');    // create
    Route::post('country/store', 'CountryController@store')->name('country.store');   // store
    Route::get('country/{id}', 'CountryController@edit')->name('country.edit');   // edit
    Route::post('country/{id}', 'CountryController@update')->name('country.update');    // update
    Route::delete('country/{id}', 'CountryController@destroy')->name('country.delete');    // update

    //*  Division Route Section */
    Route::get('division/list', 'DivisionController@list')->name('division.list');   // index
    Route::get('division/add', 'DivisionController@form')->name('division.form');    // create
    Route::post('division/store', 'DivisionController@store')->name('division.store');   // store
    Route::post('division/divisionSelectAjaxList', 'DivisionController@divisionSelectAjaxList')->name('division.divisionSelectAjaxList');    // Ajax Division List
    Route::get('division/{id}', 'DivisionController@edit')->name('division.edit');   // edit
    Route::post('division/{id}', 'DivisionController@update')->name('division.update');    // update
    Route::delete('division/{id}', 'DivisionController@destroy')->name('division.delete');    // delete

    //*  District Route Section */
    Route::get('district/list', 'DistrictController@list')->name('district.list');   // index
    Route::get('district/add', 'DistrictController@form')->name('district.form');    // create
    Route::post('district/store', 'DistrictController@store')->name('district.store');   // store
    Route::post('district/districtSelectAjaxList', 'DistrictController@districtSelectAjaxList')->name('district.districtSelectAjaxList');    // Ajax District List
    Route::get('district/{id}', 'DistrictController@edit')->name('district.edit');   // edit
    Route::post('district/{id}', 'DistrictController@update')->name('district.update');    // update
    Route::delete('district/{id}', 'DistrictController@destroy')->name('district.delete');    // delete

    //*  Upazila-- Route Section */
    Route::get('upazila/list', 'UpazilaController@list')->name('upazila.list');   // index
    Route::get('upazila/add', 'UpazilaController@form')->name('upazila.form');    // create
    Route::post('upazila/store', 'UpazilaController@store')->name('upazila.store');   // store
    Route::post('upazila/upazilaSelectAjaxList', 'UpazilaController@upazilaSelectAjaxList')->name('upazila.upazilaSelectAjaxList');    // Ajax Upazila List
    Route::get('upazila/{id}', 'UpazilaController@edit')->name('upazila.edit');   // edit
    Route::post('upazila/{id}', 'UpazilaController@update')->name('upazila.update');    // update
    Route::delete('upazila/{id}', 'UpazilaController@destroy')->name('upazila.delete');    // delete

    //*  Union Route Section */
    Route::get('union/list', 'UnionController@list')->name('union.list');   // index
    Route::get('union/add', 'UnionController@form')->name('union.form');    // create
    Route::post('union/store', 'UnionController@Store')->name('union.store');   // store
    Route::post('union/unionSelectAjaxList', 'UnionController@unionSelectAjaxList')->name('union.unionSelectAjaxList');    // Ajax District List
    Route::get('union/{id}', 'UnionController@edit')->name('union.edit');   // edit
    Route::post('union/{id}', 'UnionController@update')->name('union.update');    // update
    Route::delete('union/{id}', 'UnionController@destroy')->name('union.delete');    // delete

    Route::get('/home', 'HomeController@index')->name('dashboard');

    /**********  Office Budget Project Routes    **********/

//    Institute Routes
    Route::Resource('/institutes', 'InstituteController');

//    Budget Code Routes
    Route::Resource('/budget-code', 'BudgetCodeController');
    Route::post('/budget-code/get-code-list', 'BudgetCodeController@getBudgetCodeList')->name('budget-code.getBudgetCodeList');
    Route::post('/budget-code/get-code-by-id', 'BudgetCodeController@getBudgetCodeById')->name('budget-code.getBudgetCodeById');

//    Budget Type Routes
    Route::Resource('/budget-type', 'BudgetTypeController');

//    Budget Sub Type Routes
    Route::post('/budget-sub-type/get-ajax-list', 'BudgetSubTypeController@ajaxBgtSubTypList')->name('budget-sub-type.ajaxBgtSubTypList');
    Route::post('/budget-sub-type/get-ajax-code', 'BudgetSubTypeController@ajaxBgtSubTypCode')->name('budget-sub-type.ajaxBgtSubTypCode');
    Route::Resource('/budget-sub-type', 'BudgetSubTypeController');

//    Items Routes
    Route::Resource('/items', 'ItemController');

//    Official Budget Routes
//    Office Budget
    Route::post('/office-budget/get-budget-code', 'OfficeBudgetController@getBudgetCodeByType')->name('office-budge.getBudgetCodeByType');
    Route::post('/office-budget/budget-approve', 'OfficeBudgetController@budgetApproveReject')->name('office-budge.budgetApproveReject');
    Route::Resource('/office-budget', "OfficeBudgetController");

//    Office Budget Expense
    Route::post('/office-budget-expense/budget-expense-approve', 'OfficeBudgetController@budgetExpenseApproveReject')->name('office-budge.budgetExpenseApproveReject');
    Route::post('/office-budget-expense/expense-data-ajax', 'OfficeBudgetExpenseController@expenseDataAjax')->name('office-budget-expense.expenseDataAjax');
    Route::post('/office-budget-expense/budget-approve', 'OfficeBudgetExpenseController@budgetExpenseApproveReject')->name('office-budget-expense.budgetExpenseApproveReject');
    Route::Resource('/office-budget-expense', "OfficeBudgetExpenseController");

//    Office Budget Balance
    Route::Resource('/office-balance', "OfficeBalanceController");

    //    All Ajax Helper Controller
    Route::post('/ajax-helper/unit-list', 'AllAjaxHelperController@getUnitList')->name('ajax-helper.getUnitList');
    Route::post('/ajax-helper/item-list', 'AllAjaxHelperController@getItemList')->name('ajax-helper.getItemList');

    //    Office Voucher
    Route::post('/office-voucher/voucher-approve', 'OfficeVoucherController@budgetVoucherApproveReject')->name('office-voucher.budgetVoucherApproveReject');
    Route::Resource('/office-voucher', "OfficeVoucherController");

    //    Internal Voucher
    Route::post('/internal-voucher/voucher-approve', 'InternalVoucherController@budgetVoucherApproveReject')->name('internal-voucher.budgetVoucherApproveReject');
    Route::Resource('/internal-voucher', "InternalVoucherController");

//    Internal Budget Routes
    Route::Resource('/internal-budget', "InternalBudgetController");
    Route::post('/internal-budget/budget-approve', 'InternalBudgetController@budgetExpenseApproveReject')->name('internal-budget.budgetExpenseApproveReject');
    Route::post('/internal-budget/budget-approve', 'InternalBudgetController@internalBudgetApproveReject')->name('internal-budget.internalBudgetApproveReject');

//    Internal Budget Expense
    Route::Resource('/internal-budget-expense', "InternalBudgetExpenseController");
    Route::post('/internal-budget-expense/budget-approve', 'InternalBudgetExpenseController@internalBudgetExpenseApproveReject')->name('internal-budget-expense.internalBudgetExpenseApproveReject');

//    Internal Budget Balance
    Route::Resource('/internal-balance', "InternalBalanceController");


//    Land Route
    Route::post('/lands/getLandInfoById', 'LandController@getLandInfoById')->name('lands.getLandInfoById');
    Route::resource('/lands', 'LandController');

//    Applicant Route
    Route::resource('/applicants', 'ApplicantController');

//    Land Assign Route
    Route::resource('/assign_lands', 'AssignLandController');

//    Role Routs
    Route::Resource('/roles', 'RoleController');
    Route::get('/role-setting', 'RoleController@roleSetting')->name('roles.roleSetting');
    Route::post('/role-setting', 'RoleController@roleSetting')->name('roles.roleSetting');
    Route::post('/update-role-setting', 'RoleController@updateRoleSetting')->name('roles.updateRoleSetting');

//    Permission Routs
    Route::resource('/permissions', 'PermissionController');

//    User Route
//    Route::get("/users/datatable", 'UserController@datatable')->name("users.datatable"); // for Data Table Export
//    Route::get("/users/datatable/getData", 'UserController@getData')->name("users.getData"); // for datatable Export
    Route::get("/users/export", 'UserController@export')->name("users.export"); // for datatable Export
    Route::Resource('/users', 'UserController');
    Route::get("/change-password", 'UserController@changepass')->name("users.changepass"); // for pass change
    Route::post("/change-password", 'UserController@changepass')->name("users.changepass"); // for pass change
    Route::post("/reset-password", 'UserController@resetpass')->name("users.resetpass"); // for pass reset

//    Flat Routes
    Route::get("/flats/export", 'FlatController@export')->name("flats.export"); // for datatable Export
    Route::get('/flats/assign-tenant/{flat_id?}/{tenant_id?}', 'FlatController@assignTenant')->name('flats.assignTenant'); // for Assign a Tenant to a Flat
    Route::get('/flats/unassign-tenant/{flat_id?}', 'FlatController@unAssignTenant')->name('flats.unAssignTenant'); // for Unassign a Tenant to a Flat
    Route::get('/assigned-flat-lists/{tenant_id?}', 'FlatController@assignedFlatLists')->name('flats.assignedFlatLists'); // for Assigned Flat List
    Route::Resource('/flats', 'FlatController');
    Route::post('/flats/assign-tenant', 'FlatController@saveAssignTenant')->name('flats.saveAssignTenant'); // for Save Assign Tenant to a Flat
    Route::post("/remove-flat-image", 'FlatController@removeFlatImage')->name("flats.removeFlatImage"); // for Flat Image Ajax Remove

//    Tenant Routes
    Route::get("/tenants/export", 'TenantController@export')->name("tenants.export"); // for datatable Export
    Route::Resource('/tenants', 'TenantController');
    Route::post("/remove-tenant-image", 'TenantController@removeTenantImage')->name("tenants.removeTenantImage"); // for Tenant Image Ajax Remove

//    Bill Router
    Route::Resource('/bills', 'BillController');
    Route::post("/ajax-get-flat-bill-data", 'BillController@ajaxGetFlatBillData')->name("bills.ajaxGetFlatBillData"); // for Get Flat Bill Data Ajax
    Route::post("/ajax-check-duplicate-bill-month", 'BillController@ajaxCheckDuplicateBillMonth')->name("bills.ajaxCheckDuplicateBillMonth"); // for Check Duplicate Bill Month Ajax
    Route::post("/ajax_get_unpaid_bill_details", 'BillController@ajaxGetUnpaidBillDetails')->name("bills.ajaxGetUnpaidBillDetails"); // for Get Unpaid Bill Data Ajax
    Route::get("/download_pdf", 'BillController@downloadPDF')->name("bills.downloadPDF"); // for Get Unpaid Bill Data Ajax

//    Payment Router
    Route::Resource('/payments', 'PaymentController');
    Route::get('/payments/create/{invoice_id?}/{flat_id?}/{tenant_id?}', 'PaymentController@create')->name('payments.create'); // for Flat Rent Pay from Bill List


});


//Route::group(['middleware' => ['role:developer|manager|superadmin']], function () {
//});

/*
Verb            URI                         Action          Route Name
GET             /photos                     index           photos.index
GET             /photos/create              create          photos.create
POST            /photos                     store           photos.store
GET             /photos/{photo}             show            photos.show
GET             /photos/{photo}/edit        edit            photos.edit
PUT/PATCH       /photos/{photo}             update          photos.update
DELETE          /photos/{photo}             destroy         photos.destroy
*/


//php artisan cache:clear
//php artisan route:clear
//php artisan config:clear
//php artisan view:clear
