/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : office_budget

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 02/02/2023 16:15:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assign_flats
-- ----------------------------
DROP TABLE IF EXISTS `assign_flats`;
CREATE TABLE `assign_flats`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_id` int NOT NULL,
  `flat_ids` json NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assign_flats
-- ----------------------------

-- ----------------------------
-- Table structure for assign_users
-- ----------------------------
DROP TABLE IF EXISTS `assign_users`;
CREATE TABLE `assign_users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `flat_id` int NOT NULL,
  `tenant_ids` json NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assign_users
-- ----------------------------

-- ----------------------------
-- Table structure for bills
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `flat_id` int NOT NULL,
  `year` int NOT NULL,
  `month` int NOT NULL,
  `rent` int NOT NULL,
  `s_charge` int NOT NULL,
  `other_charge` int NOT NULL,
  `prv_due` int NOT NULL,
  `total_bill` int NOT NULL,
  `total_pay` int NOT NULL,
  `cur_due` int NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of bills
-- ----------------------------

-- ----------------------------
-- Table structure for budget_codes
-- ----------------------------
DROP TABLE IF EXISTS `budget_codes`;
CREATE TABLE `budget_codes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `bgt_type` int NOT NULL,
  `budget_code` int NOT NULL,
  `budget_title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vat` double(16, 2) NOT NULL DEFAULT 0.00,
  `tax` double(16, 2) NOT NULL DEFAULT 0.00,
  `is_item_parent` tinyint NOT NULL DEFAULT 0,
  `budget_status` tinyint NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of budget_codes
-- ----------------------------
INSERT INTO `budget_codes` VALUES (1, 1, 3111101, 'মূল বেতন (কর্মকর্তা)', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:49:48', '2022-12-31 15:49:48');
INSERT INTO `budget_codes` VALUES (2, 1, 3111201, 'মূল বেতন (কর্মচারী)', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:52:50', '2022-12-31 15:52:50');
INSERT INTO `budget_codes` VALUES (3, 1, 3111301, 'দায়িত্ব ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:54:00', '2022-12-31 15:54:00');
INSERT INTO `budget_codes` VALUES (4, 1, 3111302, 'যাতায়াত ভাড়া', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:55:54', '2022-12-31 15:55:54');
INSERT INTO `budget_codes` VALUES (5, 1, 3111314, 'টিফিন ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:57:15', '2022-12-31 15:57:15');
INSERT INTO `budget_codes` VALUES (6, 1, 3111306, 'শিক্ষা ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:57:50', '2022-12-31 15:57:50');
INSERT INTO `budget_codes` VALUES (7, 1, 3111311, 'চিকিৎসা ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:58:31', '2022-12-31 15:58:31');
INSERT INTO `budget_codes` VALUES (8, 1, 3111310, 'বাড়ি ভাড়া', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 15:59:49', '2022-12-31 15:59:49');
INSERT INTO `budget_codes` VALUES (9, 1, 3111325, 'উৎসব ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 16:02:41', '2022-12-31 16:02:41');
INSERT INTO `budget_codes` VALUES (10, 1, 3111335, 'বাংলা নববর্ষ ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 16:03:33', '2022-12-31 16:03:33');
INSERT INTO `budget_codes` VALUES (11, 1, 3111309, 'পাহাড়ি ভাতা', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 16:05:05', '2022-12-31 16:05:05');
INSERT INTO `budget_codes` VALUES (12, 6, 3244101, 'ভ্রমন ব্যয়', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 16:07:58', '2022-12-31 16:07:58');
INSERT INTO `budget_codes` VALUES (13, 6, 3244102, 'বদলী ব্যয়', 0.00, 0.00, 0, 1, 1, NULL, '2022-12-31 16:08:44', '2022-12-31 16:08:44');
INSERT INTO `budget_codes` VALUES (14, 2, 3211106, 'অপ্যায়ন ব্যয়', 3.00, 7.50, 1, 1, 1, 1, '2022-12-31 16:25:24', '2022-12-31 16:27:49');
INSERT INTO `budget_codes` VALUES (15, 2, 3211117, 'ইন্টারনেট/ ফ্যাক্স/ টেলেক্স', 3.00, 7.50, 1, 1, 1, 1, '2022-12-31 16:26:33', '2022-12-31 16:27:58');
INSERT INTO `budget_codes` VALUES (16, 2, 3211119, 'ডাক', 3.00, 5.00, 1, 1, 1, NULL, '2022-12-31 16:29:43', '2022-12-31 16:29:43');
INSERT INTO `budget_codes` VALUES (17, 2, 3211120, 'Telephone', 3.00, 5.00, 1, 1, 1, NULL, '2022-12-31 16:30:24', '2022-12-31 16:30:24');
INSERT INTO `budget_codes` VALUES (18, 2, 3255102, 'মুদ্রন ও বাঁধাই', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 16:31:25', '2022-12-31 16:31:25');
INSERT INTO `budget_codes` VALUES (19, 2, 3255104, 'সিল ও ষ্ট্যাম্প', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 16:32:12', '2022-12-31 16:32:12');
INSERT INTO `budget_codes` VALUES (20, 2, 3255105, 'অন্যান্য মনিহারী', 3.00, 5.00, 1, 1, 1, NULL, '2022-12-31 16:33:09', '2022-12-31 16:33:09');
INSERT INTO `budget_codes` VALUES (21, 2, 3258102, 'আসবাবপত্র', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 16:33:46', '2022-12-31 16:33:46');
INSERT INTO `budget_codes` VALUES (22, 2, 3258103, 'কম্পিউটার', 3.00, 12.00, 1, 1, 1, NULL, '2022-12-31 16:34:29', '2022-12-31 16:34:29');
INSERT INTO `budget_codes` VALUES (23, 2, 3258104, 'অফিস সরঞ্জামাদি', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 16:36:17', '2022-12-31 16:36:17');
INSERT INTO `budget_codes` VALUES (24, 2, 3258105, 'অন্যান্য যন্ত্রপাতি ও সরঞ্জমাদি', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 16:38:50', '2022-12-31 16:38:50');
INSERT INTO `budget_codes` VALUES (25, 2, 3258107, 'অনাবাসিক ভবন', 3.00, 5.00, 1, 1, 1, NULL, '2022-12-31 17:05:24', '2022-12-31 17:05:24');
INSERT INTO `budget_codes` VALUES (26, 2, 4112303, 'বৈদ্যুতিক সরঞ্জামাদি', 5.00, 7.50, 1, 1, 1, NULL, '2022-12-31 17:07:07', '2022-12-31 17:07:07');
INSERT INTO `budget_codes` VALUES (27, 2, 4112310, 'অফিস সরঞ্জামাদি', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 17:08:14', '2022-12-31 17:08:14');
INSERT INTO `budget_codes` VALUES (28, 2, 4112314, 'আসবাবপত্র', 3.00, 7.50, 1, 1, 1, NULL, '2022-12-31 17:09:28', '2022-12-31 17:09:28');
INSERT INTO `budget_codes` VALUES (29, 2, 3211129, 'অফিস ভবন ভাড়া', 15.00, 5.00, 0, 1, 1, NULL, '2023-01-24 06:38:08', '2023-01-24 06:38:08');

-- ----------------------------
-- Table structure for budget_types
-- ----------------------------
DROP TABLE IF EXISTS `budget_types`;
CREATE TABLE `budget_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `budget_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '1=Salary, 2=Contingency, 3=Training, 4=Seminar',
  `budget_type_code` int NULL DEFAULT 0,
  `budget_type_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `budget_type_status` tinyint NOT NULL DEFAULT 0,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of budget_types
-- ----------------------------
INSERT INTO `budget_types` VALUES (1, 'নগদ মঞ্জুরী ও বেতন', NULL, NULL, 1, 1, 1, '2022-12-31 09:25:06', '2022-12-31 15:45:41');
INSERT INTO `budget_types` VALUES (2, 'পণ্য ও সেবার ব্যবহার', NULL, NULL, 1, 1, 1, '2022-12-31 09:26:28', '2022-12-31 15:45:47');
INSERT INTO `budget_types` VALUES (3, 'প্রশিক্ষণ', 3231301, NULL, 1, 1, 1, '2022-12-31 09:28:58', '2022-12-31 15:45:33');
INSERT INTO `budget_types` VALUES (4, 'সেমিনার', 3211111, NULL, 1, 1, NULL, '2022-12-31 09:29:24', '2022-12-31 09:29:24');
INSERT INTO `budget_types` VALUES (5, 'দিবস ও প্রতিযোগিতা', 32, NULL, 1, 1, NULL, '2022-12-31 09:30:07', '2022-12-31 09:30:07');
INSERT INTO `budget_types` VALUES (6, 'ভ্রমণ ও বদলী', NULL, NULL, 1, 1, NULL, '2022-12-31 16:06:58', '2022-12-31 16:06:58');

-- ----------------------------
-- Table structure for districts
-- ----------------------------
DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `division_id` int NOT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bn_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_code` int NOT NULL DEFAULT 0,
  `lat` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lon` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `division_id`(`division_id` ASC) USING BTREE,
  CONSTRAINT `districts_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of districts
-- ----------------------------
INSERT INTO `districts` VALUES (1, 1, 'Comillaaa', 'কুমিল্লা', 0, '23.4682747', '91.1788135', 'www.comilla.gov.bd', NULL, '2020-02-17 11:23:13');
INSERT INTO `districts` VALUES (2, 1, 'Feni', 'ফেনী', 0, '23.023231', '91.3840844', 'www.feni.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (3, 1, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 0, '23.9570904', '91.1119286', 'www.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (4, 1, 'Rangamati', 'রাঙ্গামাটি', 0, NULL, NULL, 'www.rangamati.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (5, 1, 'Noakhali', 'নোয়াখালী', 0, '22.869563', '91.099398', 'www.noakhali.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (6, 1, 'Chandpur', 'চাঁদপুর', 0, '23.2332585', '90.6712912', 'www.chandpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (7, 1, 'Lakshmipur', 'লক্ষ্মীপুর', 0, '22.942477', '90.841184', 'www.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (8, 1, 'Chattogram', 'চট্টগ্রাম', 0, '22.335109', '91.834073', 'www.chittagong.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (9, 1, 'Coxsbazar', 'কক্সবাজার', 0, NULL, NULL, 'www.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (10, 1, 'Khagrachhari', 'খাগড়াছড়ি', 0, '23.119285', '91.984663', 'www.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (11, 1, 'Bandarban', 'বান্দরবান', 0, '22.1953275', '92.2183773', 'www.bandarban.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (12, 2, 'Sirajganj', 'সিরাজগঞ্জ', 0, '24.4533978', '89.7006815', 'www.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (13, 2, 'Pabna', 'পাবনা', 0, '23.998524', '89.233645', 'www.pabna.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (14, 2, 'Bogura', 'বগুড়া', 0, '24.8465228', '89.377755', 'www.bogra.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (15, 2, 'Rajshahi', 'রাজশাহী', 0, NULL, NULL, 'www.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (16, 2, 'Natore', 'নাটোর', 0, '24.420556', '89.000282', 'www.natore.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (17, 2, 'Joypurhat', 'জয়পুরহাট', 0, NULL, NULL, 'www.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (18, 2, 'Chapainawabganj', 'চাঁপাইনবাবগঞ্জ', 0, '24.5965034', '88.2775122', 'www.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (19, 2, 'Naogaon', 'নওগাঁ', 0, NULL, NULL, 'www.naogaon.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (20, 3, 'Jashore', 'যশোর', 0, '23.16643', '89.2081126', 'www.jessore.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (21, 3, 'Satkhira', 'সাতক্ষীরা', 0, NULL, NULL, 'www.satkhira.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (22, 3, 'Meherpur', 'মেহেরপুর', 0, '23.762213', '88.631821', 'www.meherpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (23, 3, 'Narail', 'নড়াইল', 0, '23.172534', '89.512672', 'www.narail.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (24, 3, 'Chuadanga', 'চুয়াডাঙ্গা', 0, '23.6401961', '88.841841', 'www.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (25, 3, 'Kushtia', 'কুষ্টিয়া', 0, '23.901258', '89.120482', 'www.kushtia.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (26, 3, 'Magura', 'মাগুরা', 0, '23.487337', '89.419956', 'www.magura.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (27, 3, 'Khulna', 'খুলনা', 0, '22.815774', '89.568679', 'www.khulna.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (28, 3, 'Bagerhat', 'বাগেরহাট', 0, '22.651568', '89.785938', 'www.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (29, 3, 'Jhenaidah', 'ঝিনাইদহ', 0, '23.5448176', '89.1539213', 'www.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (30, 4, 'Jhalakathi', 'ঝালকাঠি', 0, NULL, NULL, 'www.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (31, 4, 'Patuakhali', 'পটুয়াখালী', 0, '22.3596316', '90.3298712', 'www.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (32, 4, 'Pirojpur', 'পিরোজপুর', 0, NULL, NULL, 'www.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (33, 4, 'Barisal', 'বরিশাল', 0, NULL, NULL, 'www.barisal.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (34, 4, 'Bhola', 'ভোলা', 0, '22.685923', '90.648179', 'www.bhola.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (35, 4, 'Barguna', 'বরগুনা', 0, NULL, NULL, 'www.barguna.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (36, 5, 'Sylhet', 'সিলেট', 0, '24.8897956', '91.8697894', 'www.sylhet.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (37, 5, 'Moulvibazar', 'মৌলভীবাজার', 0, '24.482934', '91.777417', 'www.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (38, 5, 'Habiganj', 'হবিগঞ্জ', 0, '24.374945', '91.41553', 'www.habiganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (39, 5, 'Sunamganj', 'সুনামগঞ্জ', 0, '25.0658042', '91.3950115', 'www.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (40, 6, 'Narsingdi', 'নরসিংদী', 0, '23.932233', '90.71541', 'www.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (41, 6, 'Gazipur', 'গাজীপুর', 0, '24.0022858', '90.4264283', 'www.gazipur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (42, 6, 'Shariatpur', 'শরীয়তপুর', 0, NULL, NULL, 'www.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (43, 6, 'Narayanganj', 'নারায়ণগঞ্জ', 0, '23.63366', '90.496482', 'www.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (44, 6, 'Tangail', 'টাঙ্গাইল', 0, NULL, NULL, 'www.tangail.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (45, 6, 'Kishoreganj', 'কিশোরগঞ্জ', 0, '24.444937', '90.776575', 'www.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (46, 6, 'Manikganj', 'মানিকগঞ্জ', 0, NULL, NULL, 'www.manikganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (47, 6, 'Dhaka', 'ঢাকা', 0, '23.7115253', '90.4111451', 'www.dhaka.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (48, 6, 'Munshiganj', 'মুন্সিগঞ্জ', 0, NULL, NULL, 'www.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (49, 6, 'Rajbari', 'রাজবাড়ী', 0, '23.7574305', '89.6444665', 'www.rajbari.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (50, 6, 'Madaripur', 'মাদারীপুর', 0, '23.164102', '90.1896805', 'www.madaripur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (51, 6, 'Gopalganj', 'গোপালগঞ্জ', 35, '23.0050857', '89.8266059', 'www.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (52, 6, 'Faridpur', 'ফরিদপুর', 0, '23.6070822', '89.8429406', 'www.faridpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (53, 7, 'Panchagarh', 'পঞ্চগড়', 0, '26.3411', '88.5541606', 'www.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (54, 7, 'Dinajpur', 'দিনাজপুর', 0, '25.6217061', '88.6354504', 'www.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (55, 7, 'Lalmonirhat', 'লালমনিরহাট', 0, NULL, NULL, 'www.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (56, 7, 'Nilphamari', 'নীলফামারী', 0, '25.931794', '88.856006', 'www.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (57, 7, 'Gaibandha', 'গাইবান্ধা', 0, '25.328751', '89.528088', 'www.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (58, 7, 'Thakurgaon', 'ঠাকুরগাঁও', 0, '26.0336945', '88.4616834', 'www.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (59, 7, 'Rangpur', 'রংপুর', 0, '25.7558096', '89.244462', 'www.rangpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (60, 7, 'Kurigram', 'কুড়িগ্রাম', 0, '25.805445', '89.636174', 'www.kurigram.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (61, 8, 'Sherpur', 'শেরপুর', 0, '25.0204933', '90.0152966', 'www.sherpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (62, 8, 'Mymensingh', 'ময়মনসিংহ', 0, NULL, NULL, 'www.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (63, 8, 'Jamalpur', 'জামালপুর', 0, '24.937533', '89.937775', 'www.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `districts` VALUES (64, 8, 'Netrokona', 'নেত্রকোণা', 0, '24.870955', '90.727887', 'www.netrokona.gov.bd', NULL, NULL);

-- ----------------------------
-- Table structure for divisions
-- ----------------------------
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE `divisions`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bn_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `div_code` int NOT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of divisions
-- ----------------------------
INSERT INTO `divisions` VALUES (1, 'Chattagram', 'চট্টগ্রাম', 20, 'www.chittagongdiv.gov.bd', NULL, '2020-02-17 05:45:29');
INSERT INTO `divisions` VALUES (2, 'Rajshahi', 'রাজশাহী', 50, 'www.rajshahidiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (3, 'Khulna', 'খুলনা', 40, 'www.khulnadiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (4, 'Barisal', 'বরিশাল', 10, 'www.barisaldiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (5, 'Sylhet', 'সিলেট', 60, 'www.sylhetdiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (6, 'Dhaka', 'ঢাকা', 30, 'www.dhakadiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (7, 'Rangpur', 'রংপুর', 55, 'www.rangpurdiv.gov.bd', NULL, NULL);
INSERT INTO `divisions` VALUES (8, 'Mymensingh', 'ময়মনসিংহ', 98, 'www.mymensinghdiv.gov.bd', NULL, '2022-06-17 06:57:03');
INSERT INTO `divisions` VALUES (9, 'Padma', 'পদ্মা', 99, NULL, '2022-06-17 06:40:35', '2022-06-17 06:56:50');

-- ----------------------------
-- Table structure for flat_rates
-- ----------------------------
DROP TABLE IF EXISTS `flat_rates`;
CREATE TABLE `flat_rates`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `flat_id` int NOT NULL,
  `flat_rent` int NOT NULL,
  `flat_charge` int NOT NULL,
  `year` int NOT NULL,
  `month` int NOT NULL,
  `status` tinyint NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of flat_rates
-- ----------------------------

-- ----------------------------
-- Table structure for flats
-- ----------------------------
DROP TABLE IF EXISTS `flats`;
CREATE TABLE `flats`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `dist` int NOT NULL,
  `upaz` int NOT NULL,
  `union` int NOT NULL,
  `word` int NOT NULL,
  `address` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `face` int NOT NULL,
  `flat` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor` int NOT NULL,
  `size` int NOT NULL,
  `room` int NOT NULL,
  `bath` int NOT NULL,
  `veranda` int NOT NULL,
  `gas` tinyint NOT NULL,
  `lift` tinyint NOT NULL,
  `generator` tinyint NOT NULL,
  `parking` tinyint NOT NULL,
  `intercom` tinyint NOT NULL,
  `garbage` tinyint NOT NULL,
  `comn_elec` tinyint NOT NULL,
  `comn_clean` tinyint NOT NULL,
  `comn_water` tinyint NOT NULL,
  `rent` int NOT NULL,
  `service_charge` int NOT NULL,
  `monthly_bill` int NOT NULL,
  `total_due` int NOT NULL,
  `advance_month` int NOT NULL,
  `advance_bill` int NOT NULL,
  `advance_pay` int NOT NULL,
  `status` tinyint NOT NULL,
  `pic` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of flats
-- ----------------------------

-- ----------------------------
-- Table structure for institutes
-- ----------------------------
DROP TABLE IF EXISTS `institutes`;
CREATE TABLE `institutes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `dist` int NOT NULL,
  `upaz` int NOT NULL,
  `office_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of institutes
-- ----------------------------
INSERT INTO `institutes` VALUES (1, 51, -1, 'তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর', 1, 1, NULL, '2023-01-01 04:37:13', '2023-01-01 04:37:13');
INSERT INTO `institutes` VALUES (2, 51, 385, 'তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর, উপজেলা কার্যালয়', 1, 1, 1, '2023-01-22 05:33:31', '2023-01-22 09:25:29');
INSERT INTO `institutes` VALUES (3, 51, 386, 'তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর, উপজেলা কার্যালয়', 1, 1, 1, '2023-01-22 05:34:02', '2023-01-22 09:25:53');

-- ----------------------------
-- Table structure for internal_budget_balances
-- ----------------------------
DROP TABLE IF EXISTS `internal_budget_balances`;
CREATE TABLE `internal_budget_balances`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_balances` double(15, 2) NOT NULL COMMENT 'Total Updated Amount',
  `institute_id` int NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of internal_budget_balances
-- ----------------------------

-- ----------------------------
-- Table structure for internal_budget_expenses
-- ----------------------------
DROP TABLE IF EXISTS `internal_budget_expenses`;
CREATE TABLE `internal_budget_expenses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `expn_date` date NOT NULL,
  `details` json NOT NULL COMMENT 'title,unit_price,quantity,price',
  `total_price` int NOT NULL,
  `institute_id` int NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject',
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of internal_budget_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for internal_budgets
-- ----------------------------
DROP TABLE IF EXISTS `internal_budgets`;
CREATE TABLE `internal_budgets`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `budget_code` int NOT NULL DEFAULT 0,
  `total_amount` double(15, 2) NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `institute_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject',
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of internal_budgets
-- ----------------------------

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_type` int NOT NULL COMMENT '1=Products, 2=Services',
  `item_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `item_status` tinyint NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (1, 28, 'টেবিল', 1, 1, NULL, '2022-12-31 17:11:07', '2022-12-31 17:11:07');
INSERT INTO `items` VALUES (2, 28, 'চেয়ার', 1, 1, NULL, '2022-12-31 17:11:26', '2022-12-31 17:11:26');
INSERT INTO `items` VALUES (3, 28, 'আলমারি', 1, 1, NULL, '2022-12-31 17:11:46', '2022-12-31 17:11:46');
INSERT INTO `items` VALUES (4, 27, 'স্টেপ্লার', 1, 1, NULL, '2022-12-31 17:28:09', '2022-12-31 17:28:09');
INSERT INTO `items` VALUES (5, 27, 'পিন', 1, 1, NULL, '2022-12-31 17:28:32', '2022-12-31 17:28:32');
INSERT INTO `items` VALUES (6, 27, 'স্কেল', 1, 1, NULL, '2022-12-31 17:29:04', '2022-12-31 17:29:04');
INSERT INTO `items` VALUES (7, 27, 'প্লেট', 1, 1, NULL, '2022-12-31 17:31:02', '2022-12-31 17:31:02');
INSERT INTO `items` VALUES (8, 26, 'মাল্টিপ্লাগ', 1, 1, NULL, '2022-12-31 17:31:27', '2022-12-31 17:31:27');
INSERT INTO `items` VALUES (9, 26, 'ইউ,পি,এস', 1, 1, NULL, '2022-12-31 17:31:52', '2022-12-31 17:31:52');
INSERT INTO `items` VALUES (10, 22, 'মনিটর', 1, 1, NULL, '2022-12-31 17:32:18', '2022-12-31 17:32:18');

-- ----------------------------
-- Table structure for ledgers
-- ----------------------------
DROP TABLE IF EXISTS `ledgers`;
CREATE TABLE `ledgers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `financial_year` int NOT NULL,
  `budget_type` int NOT NULL,
  `code` int NOT NULL,
  `institute_id` int NOT NULL,
  `vat` double NOT NULL DEFAULT 0,
  `tax` double NOT NULL DEFAULT 0,
  `amount` double NOT NULL DEFAULT 0,
  `usable_amount` double NOT NULL DEFAULT 0,
  `total_current_amount` double NOT NULL DEFAULT 0,
  `total_previous_amount` double NOT NULL DEFAULT 0,
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '0 = Finished, 1=Running',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ledgers
-- ----------------------------
INSERT INTO `ledgers` VALUES (1, 2022, 1, 3111101, 1, 0, 0, 742500, 742500, 742500, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (2, 2022, 1, 3111201, 1, 0, 0, 154600, 154600, 154600, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (3, 2022, 1, 3111301, 1, 0, 0, 0, 0, 0, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (4, 2022, 1, 3111302, 1, 0, 0, 0, 0, 0, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (5, 2022, 1, 3111306, 1, 0, 0, 5500, 5500, 5500, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (6, 2022, 1, 3111309, 1, 0, 0, 0, 0, 0, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (7, 2022, 1, 3111310, 1, 0, 0, 343200, 343200, 343200, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (8, 2022, 1, 3111311, 1, 0, 0, 49500, 49500, 49500, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (9, 2022, 1, 3111314, 1, 0, 0, 2200, 2200, 2200, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (10, 2022, 1, 3111325, 1, 0, 0, 163100, 163100, 163100, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (11, 2022, 1, 3111335, 1, 0, 0, 16400, 16400, 16400, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (12, 2022, 6, 3244101, 1, 0, 0, 25000, 25000, 25000, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (13, 2022, 6, 3244102, 1, 0, 0, 0, 0, 0, 0, 1, '2023-01-26 07:36:28', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (14, 2022, 2, 3211106, 1, 3, 7.5, 11010, 9909, 9909, 9900, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (15, 2022, 2, 3211117, 1, 3, 7.5, 12010, 10809, 10809, 10800, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (16, 2022, 2, 3211119, 1, 3, 5, 725, 667, 667, 644, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (17, 2022, 2, 3211120, 1, 3, 5, 8025, 7383, 7383, 7360, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (18, 2022, 2, 3211129, 1, 15, 5, 220000, 176000, 176000, 160008, 1, '2023-02-02 10:01:09', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (19, 2022, 2, 3255102, 1, 3, 7.5, 5010, 4509, 4509, 4500, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (20, 2022, 2, 3255104, 1, 3, 7.5, 1510, 1359, 1359, 1350, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (21, 2022, 2, 3255105, 1, 3, 5, 6025, 5543, 5543, 5520, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (22, 2022, 2, 3258102, 1, 3, 7.5, 5010, 4509, 4509, 4500, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (23, 2022, 2, 3258103, 1, 3, 12, 5020, 4267, 4267, 4250, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (24, 2022, 2, 3258104, 1, 3, 7.5, 5010, 4509, 4509, 4500, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (25, 2022, 2, 3258105, 1, 3, 7.5, 5010, 4509, 4509, 4500, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (26, 2022, 2, 3258107, 1, 3, 5, 5025, 4623, 4623, 4600, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (27, 2022, 2, 4112303, 1, 5, 7.5, 2525, 2222, 2222, 2200, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (28, 2022, 2, 4112310, 1, 3, 7.5, 5010, 4509, 4509, 4500, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (29, 2022, 2, 4112314, 1, 3, 7.5, 2510, 2259, 2259, 2250, 1, '2023-02-02 09:51:14', '2023-01-26 07:36:28');
INSERT INTO `ledgers` VALUES (30, 2021, 2, 3211106, 1, 3, 7.5, 5000, 4500, 4500, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (31, 2021, 2, 3211117, 1, 3, 7.5, 3000, 2700, 2700, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (32, 2021, 2, 3211119, 1, 3, 5, 1000, 920, 920, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (33, 2021, 2, 3211120, 1, 3, 5, 5000, 4600, 4600, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (34, 2021, 2, 3211129, 1, 15, 5, 0, 0, 0, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (35, 2021, 2, 3255102, 1, 3, 7.5, 5000, 4500, 4500, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (36, 2021, 2, 3255104, 1, 3, 7.5, 2500, 2250, 2250, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (37, 2021, 2, 3255105, 1, 3, 5, 8000, 7360, 7360, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (38, 2021, 2, 3258102, 1, 3, 7.5, 25000, 22500, 22500, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (39, 2021, 2, 3258103, 1, 3, 12, 20000, 17000, 17000, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (40, 2021, 2, 3258104, 1, 3, 7.5, 18000, 16200, 16200, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (41, 2021, 2, 3258105, 1, 3, 7.5, 10000, 9000, 9000, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (42, 2021, 2, 3258107, 1, 3, 5, 5000, 4600, 4600, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (43, 2021, 2, 4112303, 1, 5, 7.5, 12000, 10560, 10560, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (44, 2021, 2, 4112310, 1, 3, 7.5, 5000, 4500, 4500, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');
INSERT INTO `ledgers` VALUES (45, 2021, 2, 4112314, 1, 3, 7.5, 15000, 13500, 13500, 0, 1, '2023-02-02 10:04:22', '2023-02-02 10:04:22');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2020_10_09_182317_create_permissions_table', 1);
INSERT INTO `migrations` VALUES (4, '2020_10_09_182347_create_roles_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_10_09_182728_create_users_permissions_table', 1);
INSERT INTO `migrations` VALUES (6, '2020_10_09_182828_create_users_roles_table', 1);
INSERT INTO `migrations` VALUES (7, '2020_10_09_182902_create_roles_permissions_table', 1);
INSERT INTO `migrations` VALUES (8, '2020_12_31_070137_create_flats_table', 1);
INSERT INTO `migrations` VALUES (9, '2020_12_31_070212_create_tenants_table', 1);
INSERT INTO `migrations` VALUES (10, '2020_12_31_070232_create_bills_table', 1);
INSERT INTO `migrations` VALUES (11, '2020_12_31_070244_create_payments_table', 1);
INSERT INTO `migrations` VALUES (12, '2020_12_31_070516_create_ledgers_table', 1);
INSERT INTO `migrations` VALUES (13, '2020_12_31_082622_create_flat_rates_table', 1);
INSERT INTO `migrations` VALUES (14, '2020_12_31_090456_create_assign_flats_table', 1);
INSERT INTO `migrations` VALUES (15, '2020_12_31_090500_create_assign_users_table', 1);
INSERT INTO `migrations` VALUES (16, '2021_07_03_182809_create_institutes_table', 1);
INSERT INTO `migrations` VALUES (17, '2021_07_03_182914_create_budget_codes_table', 1);
INSERT INTO `migrations` VALUES (18, '2021_07_03_182923_create_budget_types_table', 1);
INSERT INTO `migrations` VALUES (19, '2021_07_03_182940_create_items_table', 1);
INSERT INTO `migrations` VALUES (20, '2021_07_08_083410_create_office_budgets_table', 1);
INSERT INTO `migrations` VALUES (21, '2021_07_08_083411_create_internal_budget_expenses_table', 1);
INSERT INTO `migrations` VALUES (22, '2021_07_08_083411_create_internal_budgets_table', 1);
INSERT INTO `migrations` VALUES (23, '2021_07_08_083411_create_office_budget_expenses_table', 1);
INSERT INTO `migrations` VALUES (24, '2021_07_08_083412_create_internal_budget_balances_table', 1);
INSERT INTO `migrations` VALUES (25, '2021_07_08_083412_create_office_budget_balances_table', 2);

-- ----------------------------
-- Table structure for office_budget_balances
-- ----------------------------
DROP TABLE IF EXISTS `office_budget_balances`;
CREATE TABLE `office_budget_balances`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `total_updatable_balances` json NOT NULL COMMENT 'Json formatted updatable data',
  `total_balances` json NOT NULL COMMENT 'Including vat tax',
  `usable_balances` json NOT NULL COMMENT 'Without vat tax',
  `institute_id` int NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of office_budget_balances
-- ----------------------------

-- ----------------------------
-- Table structure for office_budget_expenses
-- ----------------------------
DROP TABLE IF EXISTS `office_budget_expenses`;
CREATE TABLE `office_budget_expenses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `budget_code` int NOT NULL,
  `vat` int NOT NULL,
  `tax` int NOT NULL,
  `expn_date` date NOT NULL,
  `useable_expense` int NOT NULL DEFAULT 0,
  `details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT 'item,desc,unit_price,quantity,amount',
  `total_expense` int NOT NULL,
  `institute_id` int NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Rejected',
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of office_budget_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for office_budgets
-- ----------------------------
DROP TABLE IF EXISTS `office_budgets`;
CREATE TABLE `office_budgets`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `budget_details` json NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `institute_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject',
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of office_budgets
-- ----------------------------
INSERT INTO `office_budgets` VALUES (1, 'জেলা কর্মকর্তা ও কর্মচারীদের নগদ মঞ্জুরি ও বেতন ২২-২৩', '2022', 1, '{\"3111101\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"742500\", \"useable_amount\": \"742500\"}, \"3111201\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"154600\", \"useable_amount\": \"154600\"}, \"3111301\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111302\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111306\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"5500\", \"useable_amount\": \"5500\"}, \"3111309\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111310\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"343200\", \"useable_amount\": \"343200\"}, \"3111311\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"49500\", \"useable_amount\": \"49500\"}, \"3111314\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"2200\", \"useable_amount\": \"2200\"}, \"3111325\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"163100\", \"useable_amount\": \"163100\"}, \"3111335\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"16400\", \"useable_amount\": \"16400\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-01 03:49:49', '2023-01-01 05:59:43');
INSERT INTO `office_budgets` VALUES (2, 'ভ্রমণ ব্যয় ও বদলী ব্যয়', '2022', 6, '{\"3244101\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"25000\", \"useable_amount\": \"25000\"}, \"3244102\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-22 09:45:49', '2023-01-22 10:23:20');
INSERT INTO `office_budgets` VALUES (3, 'পণ্য ও সেবার ব্যবহার', '2022', 2, '{\"3211106\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"11000\", \"useable_amount\": \"9900\"}, \"3211117\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"12000\", \"useable_amount\": \"10800\"}, \"3211119\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"700\", \"useable_amount\": \"644\"}, \"3211120\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"8000\", \"useable_amount\": \"7360\"}, \"3211129\": {\"tax\": \"5\", \"vat\": \"15\", \"amount\": \"200000\", \"useable_amount\": \"160000\"}, \"3255102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3255104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"1500\", \"useable_amount\": \"1350\"}, \"3255105\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"6000\", \"useable_amount\": \"5520\"}, \"3258102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258103\": {\"tax\": \"12\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4250\"}, \"3258104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258105\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258107\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4600\"}, \"4112303\": {\"tax\": \"7.5\", \"vat\": \"5\", \"amount\": \"2500\", \"useable_amount\": \"2200\"}, \"4112310\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"4112314\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"2500\", \"useable_amount\": \"2250\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-22 10:28:22', '2023-02-01 11:21:50');
INSERT INTO `office_budgets` VALUES (4, 'পণ্য ও সেবার ব্যবহার -2', '2022', 2, '{\"3211106\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3211117\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3211119\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3211120\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3211129\": {\"tax\": \"5\", \"vat\": \"15\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3255102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3255104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3255105\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3258102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3258103\": {\"tax\": \"12\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3258104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3258105\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3258107\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"4112303\": {\"tax\": \"7.5\", \"vat\": \"5\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"4112310\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"4112314\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"0\", \"useable_amount\": \"0\"}}', NULL, 1, 0, 1, NULL, '2023-02-02 09:37:02', '2023-02-02 09:37:02');
INSERT INTO `office_budgets` VALUES (5, 'পণ্য ও সেবার ব্যবহার -3', '2022', 2, '{\"3211106\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3211117\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3211119\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"25\", \"useable_amount\": \"23\"}, \"3211120\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"25\", \"useable_amount\": \"23\"}, \"3211129\": {\"tax\": \"5\", \"vat\": \"15\", \"amount\": \"20000\", \"useable_amount\": \"16000\"}, \"3255102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3255104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3255105\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"25\", \"useable_amount\": \"23\"}, \"3258102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3258103\": {\"tax\": \"12\", \"vat\": \"3\", \"amount\": \"20\", \"useable_amount\": \"17\"}, \"3258104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3258105\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"3258107\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"25\", \"useable_amount\": \"23\"}, \"4112303\": {\"tax\": \"7.5\", \"vat\": \"5\", \"amount\": \"25\", \"useable_amount\": \"22\"}, \"4112310\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}, \"4112314\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10\", \"useable_amount\": \"9\"}}', NULL, 1, 0, 1, 1, '2023-02-02 09:51:14', '2023-02-02 10:01:09');
INSERT INTO `office_budgets` VALUES (6, 'পণ্য ও সেবার ব্যবহার -1', '2021', 2, '{\"3211106\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3211117\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"3000\", \"useable_amount\": \"2700\"}, \"3211119\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"1000\", \"useable_amount\": \"920\"}, \"3211120\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4600\"}, \"3211129\": {\"tax\": \"5\", \"vat\": \"15\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3255102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3255104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"2500\", \"useable_amount\": \"2250\"}, \"3255105\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"8000\", \"useable_amount\": \"7360\"}, \"3258102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"25000\", \"useable_amount\": \"22500\"}, \"3258103\": {\"tax\": \"12\", \"vat\": \"3\", \"amount\": \"20000\", \"useable_amount\": \"17000\"}, \"3258104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"18000\", \"useable_amount\": \"16200\"}, \"3258105\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"10000\", \"useable_amount\": \"9000\"}, \"3258107\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4600\"}, \"4112303\": {\"tax\": \"7.5\", \"vat\": \"5\", \"amount\": \"12000\", \"useable_amount\": \"10560\"}, \"4112310\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"4112314\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"15000\", \"useable_amount\": \"13500\"}}', NULL, 1, 0, 1, NULL, '2023-02-02 10:04:22', '2023-02-02 10:04:22');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_id` int NOT NULL,
  `rent_amount` int NOT NULL,
  `other_amount` int NOT NULL,
  `adv_amount` int NOT NULL,
  `total_amount` int NOT NULL,
  `status` int NOT NULL,
  `paid_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'create-role', 'create-role', '2', NULL, NULL);
INSERT INTO `permissions` VALUES (2, 'create-permission', 'create-permission', '1', NULL, NULL);
INSERT INTO `permissions` VALUES (3, 'role-setting', 'role-setting', '1', NULL, NULL);
INSERT INTO `permissions` VALUES (4, 'user-list', 'user-list', '0', NULL, NULL);
INSERT INTO `permissions` VALUES (5, 'office-budget-list', 'office-budget-list', '20', '2022-12-30 15:49:41', '2022-12-30 15:49:41');
INSERT INTO `permissions` VALUES (6, 'create-office-budget', 'create-office-budget', '20', '2022-12-30 16:00:21', '2022-12-30 16:00:21');
INSERT INTO `permissions` VALUES (7, 'edit-office-budget', 'edit-office-budget', '20', '2022-12-30 16:01:05', '2022-12-30 16:01:05');
INSERT INTO `permissions` VALUES (8, 'delete-office-budget', 'delete-office-budget', '20', '2022-12-30 16:01:29', '2022-12-30 16:01:29');
INSERT INTO `permissions` VALUES (9, 'budget-type-list', 'budget-type-list', '19', '2022-12-30 16:03:41', '2022-12-30 16:03:41');
INSERT INTO `permissions` VALUES (10, 'create-budget-type', 'create-budget-type', '19', '2022-12-30 16:04:16', '2022-12-30 16:04:16');
INSERT INTO `permissions` VALUES (11, 'edit-budget-type', 'edit-budget-type', '19', '2022-12-30 16:04:35', '2022-12-30 16:04:35');
INSERT INTO `permissions` VALUES (12, 'delete-budget-type', 'delete-budget-type', '19', '2022-12-30 16:04:56', '2022-12-30 16:04:56');
INSERT INTO `permissions` VALUES (13, 'budget-code-list', 'budget-code-list', '17', '2022-12-31 06:35:26', '2022-12-31 06:35:26');
INSERT INTO `permissions` VALUES (14, 'create-budget-code', 'create-budget-code', '17', '2022-12-31 06:35:45', '2022-12-31 06:35:45');
INSERT INTO `permissions` VALUES (15, 'edit-budget-code', 'edit-budget-code', '17', '2022-12-31 06:36:10', '2022-12-31 06:36:10');
INSERT INTO `permissions` VALUES (16, 'delete-budget-code', 'delete-budget-code', '17', '2022-12-31 06:36:31', '2022-12-31 06:36:31');
INSERT INTO `permissions` VALUES (17, 'item-list', 'item-list', '18', '2022-12-31 06:37:23', '2022-12-31 06:37:23');
INSERT INTO `permissions` VALUES (18, 'item-create', 'item-create', '18', '2022-12-31 06:37:56', '2022-12-31 06:37:56');
INSERT INTO `permissions` VALUES (19, 'item-edit', 'item-edit', '18', '2022-12-31 06:38:19', '2022-12-31 06:38:19');
INSERT INTO `permissions` VALUES (20, 'item-delete', 'item-delete', '18', '2022-12-31 06:38:34', '2022-12-31 06:38:34');
INSERT INTO `permissions` VALUES (21, 'institute-list', 'institute-list', '16', '2023-01-01 04:10:05', '2023-01-01 04:10:05');
INSERT INTO `permissions` VALUES (22, 'create-institute', 'create-institute', '16', '2023-01-01 04:10:35', '2023-01-01 04:10:35');
INSERT INTO `permissions` VALUES (23, 'create-institute', 'create-institute', '16', '2023-01-01 04:10:35', '2023-01-01 04:10:35');
INSERT INTO `permissions` VALUES (24, 'edit-institute', 'edit-institute', '16', '2023-01-01 04:11:13', '2023-01-01 04:11:13');
INSERT INTO `permissions` VALUES (25, 'delete-institute', 'delete-institute', '16', '2023-01-01 04:11:39', '2023-01-01 04:11:39');
INSERT INTO `permissions` VALUES (26, 'office-budget-expense-list', 'office-budget-expense-list', '21', '2023-01-24 08:34:50', '2023-01-24 08:34:50');
INSERT INTO `permissions` VALUES (27, 'create-office-expense-budget', 'create-office-expense-budget', '21', '2023-01-24 08:35:11', '2023-01-24 08:35:11');
INSERT INTO `permissions` VALUES (28, 'edit-office-expense-budget', 'edit-office-expense-budget', '21', '2023-01-24 08:35:32', '2023-01-24 08:35:32');
INSERT INTO `permissions` VALUES (29, 'delete-office-expense-budget', 'delete-office-expense-budget', '21', '2023-01-24 08:35:51', '2023-01-24 08:35:51');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'super-admin', 'super-admin', NULL, NULL);
INSERT INTO `roles` VALUES (2, 'admin', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `roles_permissions`;
CREATE TABLE `roles_permissions`  (
  `role_id` int UNSIGNED NOT NULL,
  `permission_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE,
  INDEX `roles_permissions_permission_id_foreign`(`permission_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of roles_permissions
-- ----------------------------
INSERT INTO `roles_permissions` VALUES (1, 1);
INSERT INTO `roles_permissions` VALUES (1, 2);
INSERT INTO `roles_permissions` VALUES (1, 3);
INSERT INTO `roles_permissions` VALUES (1, 4);
INSERT INTO `roles_permissions` VALUES (1, 5);
INSERT INTO `roles_permissions` VALUES (1, 6);
INSERT INTO `roles_permissions` VALUES (1, 7);
INSERT INTO `roles_permissions` VALUES (1, 8);
INSERT INTO `roles_permissions` VALUES (1, 9);
INSERT INTO `roles_permissions` VALUES (1, 10);
INSERT INTO `roles_permissions` VALUES (1, 11);
INSERT INTO `roles_permissions` VALUES (1, 12);
INSERT INTO `roles_permissions` VALUES (1, 13);
INSERT INTO `roles_permissions` VALUES (1, 14);
INSERT INTO `roles_permissions` VALUES (1, 15);
INSERT INTO `roles_permissions` VALUES (1, 16);
INSERT INTO `roles_permissions` VALUES (1, 17);
INSERT INTO `roles_permissions` VALUES (1, 18);
INSERT INTO `roles_permissions` VALUES (1, 19);
INSERT INTO `roles_permissions` VALUES (1, 20);
INSERT INTO `roles_permissions` VALUES (1, 21);
INSERT INTO `roles_permissions` VALUES (1, 22);
INSERT INTO `roles_permissions` VALUES (1, 23);
INSERT INTO `roles_permissions` VALUES (1, 24);
INSERT INTO `roles_permissions` VALUES (1, 25);
INSERT INTO `roles_permissions` VALUES (1, 26);
INSERT INTO `roles_permissions` VALUES (1, 27);
INSERT INTO `roles_permissions` VALUES (1, 28);
INSERT INTO `roles_permissions` VALUES (1, 29);

-- ----------------------------
-- Table structure for tenants
-- ----------------------------
DROP TABLE IF EXISTS `tenants`;
CREATE TABLE `tenants`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `g_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nid` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dist` int NOT NULL,
  `upaz` int NOT NULL,
  `address` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tenants
-- ----------------------------

-- ----------------------------
-- Table structure for unions
-- ----------------------------
DROP TABLE IF EXISTS `unions`;
CREATE TABLE `unions`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `upazilla_id` int NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bn_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uni_code` int NOT NULL DEFAULT 0,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `upazilla_id`(`upazilla_id` ASC) USING BTREE,
  CONSTRAINT `unions_ibfk_2` FOREIGN KEY (`upazilla_id`) REFERENCES `upazilas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4541 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of unions
-- ----------------------------
INSERT INTO `unions` VALUES (1, 1, 'Subil', 'সুবিল', 0, 'subilup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2, 1, 'North Gunaighor', 'উত্তর গুনাইঘর', 0, 'gunaighornorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3, 1, 'South Gunaighor', 'দক্ষিণ গুনাইঘর', 0, 'gunaighorsouth.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4, 1, 'Boroshalghor', 'বড়শালঘর', 0, 'boroshalghorup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (5, 1, 'Rajameher', 'রাজামেহার', 0, 'rajameherup.comila.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (6, 1, 'Yousufpur', 'ইউসুফপুর', 0, 'yousufpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (7, 1, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (8, 1, 'Fatehabad', 'ফতেহাবাদ', 0, 'fatehabadup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (9, 1, 'Elahabad', 'এলাহাবাদ', 0, 'elahabadup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (10, 1, 'Jafargonj', 'জাফরগঞ্জ', 0, 'jafargonjup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (11, 1, 'Dhampti', 'ধামতী', 0, 'dhamptiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (12, 1, 'Mohanpur', 'মোহনপুর', 0, 'mohanpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (13, 1, 'Vani', 'ভানী', 0, 'vaniup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (14, 1, 'Barkamta', 'বরকামতা', 0, 'barkamtaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (15, 1, 'Sultanpur', 'সুলতানপুর', 0, 'sultanpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (16, 2, 'Aganagar', 'আগানগর', 0, 'aganagarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (17, 2, 'Bhabanipur', 'ভবানীপুর', 0, 'bhabanipurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (18, 2, 'North Khoshbas', 'উত্তর খোশবাস', 0, 'khoshbasnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (19, 2, 'South Khoshbas', 'দক্ষিন খোশবাস', 0, 'khoshbassouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (20, 2, 'Jhalam', 'ঝলম', 0, 'jhalamup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (21, 2, 'Chitodda', 'চিতড্ডা', 0, 'chitoddaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (22, 2, 'North Shilmuri', 'উত্তর শিলমুড়ি', 0, 'shilmurinorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (23, 2, 'South Shilmuri', 'দক্ষিন শিলমুড়ি', 0, 'shilmurisouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (24, 2, 'Galimpur', 'গালিমপুর', 0, 'galimpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (25, 2, 'Shakpur', 'শাকপুর', 0, 'shakpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (26, 2, 'Bhaukshar', 'ভাউকসার', 0, 'bhauksharup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (27, 2, 'Adda', 'আড্ডা', 0, 'addaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (28, 2, 'Adra', 'আদ্রা', 0, 'adraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (29, 2, 'Payalgacha', 'পয়ালগাছা', 0, 'payalgachaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (30, 2, 'Laxmipur', 'লক্ষীপুর', 0, 'laxmipurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (31, 3, 'Shidli', 'শিদলাই', 0, 'shidliup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (32, 3, 'Chandla', 'চান্দলা', 0, 'chandlaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (33, 3, 'Shashidal', 'শশীদল', 0, 'shashidalup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (34, 3, 'Dulalpur', 'দুলালপুর', 0, 'dulalpurup2.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (35, 3, 'Brahmanpara Sadar', 'ব্রাহ্মনপাড়া সদর', 0, 'brahmanparasadarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (36, 3, 'Shahebabad', 'সাহেবাবাদ', 0, 'shahebabadup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (37, 3, 'Malapara', 'মালাপাড়া', 0, 'malaparaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (38, 3, 'Madhabpur', 'মাধবপুর', 0, 'madhabpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (39, 4, 'Shuhilpur', 'সুহিলপুর', 0, 'shuhilpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (40, 4, 'Bataghashi', 'বাতাঘাসি', 0, 'bataghashiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (41, 4, 'Joag', 'জোয়াগ', 0, 'joagup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (42, 4, 'Borcarai', 'বরকরই', 0, 'borcaraiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (43, 4, 'Madhaiya', 'মাধাইয়া', 0, 'madhaiyaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (44, 4, 'Dollai Nowabpur', 'দোল্লাই নবাবপুর', 0, 'dollainowabpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (45, 4, 'Mohichial', 'মহিচাইল', 0, 'mohichialup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (46, 4, 'Gollai', 'গল্লাই', 0, 'gollaiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (47, 4, 'Keronkhal', 'কেরণখাল', 0, 'keronkhalup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (48, 4, 'Maijkhar', 'মাইজখার', 0, 'maijkharup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (49, 4, 'Etberpur', 'এতবারপুর', 0, 'etberpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (50, 4, 'Barera', 'বাড়েরা', 0, 'bareraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (51, 4, 'Borcoit', 'বরকইট', 0, 'borcoitup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (52, 5, 'Sreepur', 'শ্রীপুর', 0, 'sreepurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (53, 5, 'Kashinagar', 'কাশিনগর', 0, 'kashinagarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (54, 5, 'Kalikapur', 'কালিকাপুর', 0, 'kalikapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (55, 5, 'Shuvapur', 'শুভপুর', 0, 'shuvapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (56, 5, 'Ghulpasha', 'ঘোলপাশা', 0, 'ghulpashaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (57, 5, 'Moonshirhat', 'মুন্সীরহাট', 0, 'moonshirhatup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (58, 5, 'Batisha', 'বাতিসা', 0, 'batishaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (59, 5, 'Kankapait', 'কনকাপৈত', 0, 'kankapaitup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (60, 5, 'Cheora', 'চিওড়া', 0, 'cheoraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (61, 5, 'Jagannatdighi', 'জগন্নাথদিঘী', 0, 'jagannatdighiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (62, 5, 'Goonabati', 'গুনবতী', 0, 'goonabatiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (63, 5, 'Alkara', 'আলকরা', 0, 'alkaraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (64, 6, 'Doulotpur', 'দৌলতপুর', 0, 'doulotpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (65, 6, 'Daudkandi', 'দাউদকান্দি', 0, 'daudkandinorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (66, 6, 'North Eliotgonj', 'উত্তর ইলিয়টগঞ্জ', 0, 'eliotgonjnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (67, 6, 'South Eliotgonj', 'দক্ষিন ইলিয়টগঞ্জ', 0, 'eliotgonjsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (68, 6, 'Zinglatoli', 'জিংলাতলী', 0, 'zinglatoliup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (69, 6, 'Sundolpur', 'সুন্দলপুর', 0, 'sundolpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (70, 6, 'Gouripur', 'গৌরীপুর', 0, 'gouripurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (71, 6, 'East Mohammadpur', 'পুর্ব মোহাম্মদপুর', 0, 'mohammadpureastup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (72, 6, 'West Mohammadpur', 'পশ্চিম মোহাম্মদপুর', 0, 'mohammadpurwestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (73, 6, 'Goalmari', 'গোয়ালমারী', 0, 'goalmariup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (74, 6, 'Maruka', 'মারুকা', 0, 'marukaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (75, 6, 'Betessor', 'বিটেশ্বর', 0, 'betessorup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (76, 6, 'Podua', 'পদুয়া', 0, 'poduaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (77, 6, 'West Passgacia', 'পশ্চিম পাচঁগাছিয়া', 0, 'passgaciawestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (78, 6, 'Baropara', 'বারপাড়া', 0, 'baroparaup2.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (79, 7, 'Mathabanga', 'মাথাভাঙ্গা', 0, 'mathabangaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (80, 7, 'Gagutiea', 'ঘাগুটিয়া', 0, 'gagutieaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (81, 7, 'Asadpur', 'আছাদপুর', 0, 'asadpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (82, 7, 'Chanderchor', 'চান্দেরচর', 0, 'chanderchorup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (83, 7, 'Vashania', 'ভাষানিয়া', 0, 'vashaniaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (84, 7, 'Nilokhi', 'নিলখী', 0, 'nilokhiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (85, 7, 'Garmora', 'ঘারমোড়া', 0, 'garmoraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (86, 7, 'Joypur', 'জয়পুর', 0, 'joypurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (87, 7, 'Dulalpur', 'দুলালপুর', 0, 'dulalpurup1.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (88, 8, 'Bakoi', 'বাকই', 0, 'bakoiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (89, 8, 'Mudafargonj', 'মুদাফফর গঞ্জ', 0, 'mudafargonjup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (90, 8, 'Kandirpar', 'কান্দিরপাড়', 0, 'kandirparup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (91, 8, 'Gobindapur', 'গোবিন্দপুর', 0, 'gobindapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (92, 8, 'Uttarda', 'উত্তরদা', 0, 'uttardaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (93, 8, 'Laksam Purba', 'লাকসাম পুর্ব', 0, 'laksampurbaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (94, 8, 'Azgora', 'আজগরা', 0, 'azgoraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (95, 9, 'Sreekil', 'শ্রীকাইল', 0, 'sreekilup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (96, 9, 'Akubpur', 'আকুবপুর', 0, 'akubpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (97, 9, 'Andicot', 'আন্দিকোট', 0, 'andicotup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (98, 9, 'Purbadair (East)', 'পুর্বধৈইর (পুর্ব)', 0, 'purbadaireastup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (99, 9, 'Purbadair (West)', 'পুর্বধৈইর (পশ্চিম)', 0, 'purbadairwestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (100, 9, 'Bangara (East)', 'বাঙ্গরা (পূর্ব)', 0, 'bangaraeastup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (101, 9, 'Bangara (West)', 'বাঙ্গরা (পশ্চিম)', 0, 'bangarawestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (102, 9, 'Chapitala', 'চাপিতলা', 0, 'chapitalaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (103, 9, 'Camalla', 'কামাল্লা', 0, 'camallaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (104, 9, 'Jatrapur', 'যাত্রাপুর', 0, 'jatrapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (105, 9, 'Ramachandrapur (North)', 'রামচন্দ্রপুর (উত্তর)', 0, 'ramachandrapurnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (106, 9, 'Ramachandrapur (South)', 'রামচন্দ্রপুর (দক্ষিন)', 0, 'ramachandrapursouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (107, 9, 'Muradnagar Sadar', 'মুরাদনগর সদর', 0, 'muradnagarsadarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (108, 9, 'Nobipur (East)', 'নবীপুর (পুর্ব)', 0, 'nobipureastup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (109, 9, 'Nobipur (West)', 'নবীপুর (পশ্চিম)', 0, 'nobipurwestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (110, 9, 'Damgar', 'ধামঘর', 0, 'damgarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (111, 9, 'Jahapur', 'জাহাপুর', 0, 'jahapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (112, 9, 'Salikandi', 'ছালিয়াকান্দি', 0, 'salikandiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (113, 9, 'Darura', 'দারোরা', 0, 'daruraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (114, 9, 'Paharpur', 'পাহাড়পুর', 0, 'paharpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (115, 9, 'Babutipara', 'বাবুটিপাড়া', 0, 'babutiparaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (116, 9, 'Tanki', 'টনকী', 0, 'tankiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (117, 10, 'Bangadda', 'বাঙ্গড্ডা', 0, 'bangadda.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (118, 10, 'Paria', 'পেরিয়া', 0, 'pariaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (119, 10, 'Raykot', 'রায়কোট', 0, 'raykotup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (120, 10, 'Mokara', 'মোকরা', 0, 'mokaraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (121, 10, 'Makrabpur', 'মক্রবপুর', 0, 'makrabpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (122, 10, 'Heshakhal', 'হেসাখাল', 0, 'heshakhalup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (123, 10, 'Adra', 'আদ্রা', 0, 'adraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (124, 10, 'Judda', 'জোড্ডা', 0, 'juddaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (125, 10, 'Dhalua', 'ঢালুয়া', 0, 'dhaluaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (126, 10, 'Doulkha', 'দৌলখাঁড়', 0, 'doulkhaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (127, 10, 'Boxgonj', 'বক্সগঞ্জ', 0, 'boxgonjup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (128, 10, 'Satbaria', 'সাতবাড়ীয়া', 0, 'satbariaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (129, 11, 'Kalirbazer', 'কালীর বাজার', 0, 'kalirbazerup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (130, 11, 'North Durgapur', 'উত্তর দুর্গাপুর', 0, 'durgapurnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (131, 11, 'South Durgapur', 'দক্ষিন দুর্গাপুর', 0, 'durgapursouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (132, 11, 'Amratoli', 'আমড়াতলী', 0, 'amratoliup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (133, 11, 'Panchthubi', 'পাঁচথুবী', 0, 'panchthubiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (134, 11, 'Jagannatpur', 'জগন্নাথপুর', 0, 'jagannatpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (135, 12, 'Chandanpur', 'চন্দনপুর', 0, 'chandanpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (136, 12, 'Chalibanga', 'চালিভাঙ্গা', 0, 'chalibangaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (137, 12, 'Radanagar', 'রাধানগর', 0, 'radanagarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (138, 12, 'Manikarchar', 'মানিকারচর', 0, 'manikarcharup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (139, 12, 'Barakanda', 'বড়কান্দা', 0, 'barakandaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (140, 12, 'Govindapur', 'গোবিন্দপুর', 0, 'govindapurup1.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (141, 12, 'Luterchar', 'লুটেরচর', 0, 'lutercharup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (142, 12, 'Vaorkhola', 'ভাওরখোলা', 0, 'vaorkholaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (143, 13, 'Baishgaon', 'বাইশগাঁও', 0, 'baishgaonup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (144, 13, 'Shoroshpur', 'সরসপুর', 0, 'shoroshpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (145, 13, 'Hasnabad', 'হাসনাবাদ', 0, 'hasnabadup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (146, 13, 'Jholam (North)', 'ঝলম (উত্তর)', 0, 'jholamnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (147, 13, 'Jholam (South)', 'ঝলম (দক্ষিন)', 0, 'jholamsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (148, 13, 'Moishatua', 'মৈশাতুয়া', 0, 'moishatuaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (149, 13, 'Lokkhanpur', 'লক্ষনপুর', 0, 'lokkhanpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (150, 13, 'Khela', 'খিলা', 0, 'khelaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (151, 13, 'Uttarhowla', 'উত্তর হাওলা', 0, 'uttarhowlaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (152, 13, 'Natherpetua', 'নাথেরপেটুয়া', 0, 'natherpetuaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (153, 13, 'Bipulashar', 'বিপুলাসার', 0, 'bipulasharup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (154, 14, 'Chuwara', 'চৌয়ারা', 0, 'chuwaraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (155, 14, 'Baropara', 'বারপাড়া', 0, 'baroparaup1.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (156, 14, 'Jorkanoneast', 'জোড়কানন (পুর্ব)', 0, 'jorkanoneastup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (157, 14, 'Goliara', 'গলিয়ারা', 0, 'goliaraup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (158, 14, 'Jorkanonwest', 'জোড়কানন (পশ্চিম)', 0, 'jorkanonwestup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (159, 14, 'Bagmara (North)', 'বাগমারা (উত্তর)', 0, 'bagmaranorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (160, 14, 'Bagmara (South)', 'বাগমারা (দক্ষিন)', 0, 'bagmarasouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (161, 14, 'Bhuloin (North)', 'ভূলইন (উত্তর)', 0, 'bhuloinnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (162, 14, 'Bhuloin (South)', 'ভূলইন (দক্ষিন)', 0, 'bhuloinsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (163, 14, 'Belgor (North)', 'বেলঘর (উত্তর)', 0, 'belgornorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (164, 14, 'Belgor (South)', 'বেলঘর (দক্ষিন)', 0, 'belgorsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (165, 14, 'Perul (North)', 'পেরুল (উত্তর)', 0, 'perulnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (166, 14, 'Perul (South)', 'পেরুল (দক্ষিন)', 0, 'perulsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (167, 14, 'Bijoypur', 'বিজয়পুর', 0, 'bijoypurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (168, 15, 'Satani', 'সাতানী', 0, 'sataniup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (169, 15, 'Jagatpur', 'জগতপুর', 0, 'jagatpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (170, 15, 'Balorampur', 'বলরামপুর', 0, 'balorampurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (171, 15, 'Karikandi', 'কড়িকান্দি', 0, 'karikandiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (172, 15, 'Kalakandi', 'কলাকান্দি', 0, 'kalakandiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (173, 15, 'Vitikandi', 'ভিটিকান্দি', 0, 'vitikandiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (174, 15, 'Narayandia', 'নারান্দিয়া', 0, 'narayandiaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (175, 15, 'Zearkandi', 'জিয়ারকান্দি', 0, 'zearkandiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (176, 15, 'Majidpur', 'মজিদপুর', 0, 'majidpurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (177, 16, 'Moynamoti', 'ময়নামতি', 0, 'moynamotiup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (178, 16, 'Varella', 'ভারেল্লা', 0, 'varellaup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (179, 16, 'Mokam', 'মোকাম', 0, 'mokamup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (180, 16, 'Burichang Sadar', 'বুড়িচং সদর', 0, 'burichangsadarup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (181, 16, 'Bakshimul', 'বাকশীমূল', 0, 'bakshimulup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (182, 16, 'Pirjatrapur', 'পীরযাত্রাপুর', 0, 'pirjatrapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (183, 16, 'Sholonal', 'ষোলনল', 0, 'sholonalup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (184, 16, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (185, 17, 'Bagmara (North)', 'বাগমারা (উত্তর)', 0, 'bagmaranorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (186, 17, 'Bagmara (South)', 'বাগমারা (দক্ষিন)', 0, 'bagmarasouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (187, 17, 'Bhuloin (North)', 'ভূলইন (উত্তর)', 0, 'bhuloinnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (188, 17, 'Bhuloin (South)', 'ভূলইন (দক্ষিন)', 0, 'bhuloinsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (189, 17, 'Belgor (North)', 'বেলঘর (উত্তর)', 0, 'belgornorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (190, 17, 'Belgor (South)', 'বেলঘর (দক্ষিন)', 0, 'belgorsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (191, 17, 'Perul (North)', 'পেরুল (উত্তর)', 0, 'perulnorthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (192, 17, 'Perul (South)', 'পেরুল (দক্ষিন)', 0, 'perulsouthup.comilla.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (193, 18, 'Mohamaya', 'মহামায়া', 0, 'mohamayaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (194, 18, 'Pathannagar', 'পাঠাননগর', 0, 'pathannagarup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (195, 18, 'Subhapur', 'শুভপুর', 0, 'subhapurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (196, 18, 'Radhanagar', 'রাধানগর', 0, 'radhanagarup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (197, 18, 'Gopal', 'ঘোপাল', 0, 'gopalup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (198, 19, 'Sarishadi', 'শর্শদি', 0, 'sarishadiup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (199, 19, 'Panchgachia', 'পাঁচগাছিয়া', 0, 'panchgachiaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (200, 19, 'Dhormapur', 'ধর্মপুর', 0, 'dhormapurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (201, 19, 'Kazirbag', 'কাজিরবাগ', 0, 'kazirbagup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (202, 19, 'Kalidah', 'কালিদহ', 0, 'kalidahup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (203, 19, 'Baligaon', 'বালিগাঁও', 0, 'baligaonup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (204, 19, 'Dholia', 'ধলিয়া', 0, 'dholiaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (205, 19, 'Lemua', 'লেমুয়া', 0, 'lemuaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (206, 19, 'Chonua', 'ছনুয়া', 0, 'chonuaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (207, 19, 'Motobi', 'মোটবী', 0, 'motobiup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (208, 19, 'Fazilpur', 'ফাজিলপুর', 0, 'fazilpurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (209, 19, 'Forhadnogor', 'ফরহাদনগর', 0, 'forhadnogorup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (210, 20, 'Charmozlishpur', 'চরমজলিশপুর', 0, 'charmozlishpurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (211, 20, 'Bogadana', 'বগাদানা', 0, 'bogadanaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (212, 20, 'Motigonj', 'মতিগঞ্জ', 0, 'motigonjup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (213, 20, 'Mongolkandi', 'মঙ্গলকান্দি', 0, 'mongolkandiup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (214, 20, 'Chardorbesh', 'চরদরবেশ', 0, 'chardorbeshup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (215, 20, 'Chorchandia', 'চরচান্দিয়া', 0, 'chorchandiaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (216, 20, 'Sonagazi', 'সোনাগাজী', 0, 'sonagaziup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (217, 20, 'Amirabad', 'আমিরাবাদ', 0, 'amirabadup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (218, 20, 'Nababpur', 'নবাবপুর', 0, 'nababpurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (219, 21, 'Fulgazi', 'ফুলগাজী', 0, 'fulgaziup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (220, 21, 'Munshirhat', 'মুন্সিরহাট', 0, 'munshirhatup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (221, 21, 'Dorbarpur', 'দরবারপুর', 0, 'dorbarpurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (222, 21, 'Anandopur', 'আনন্দপুর', 0, 'anandopurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (223, 21, 'Amzadhat', 'আমজাদহাট', 0, 'amzadhatup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (224, 21, 'Gmhat', 'জি,এম, হাট', 0, 'gmhatup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (225, 22, 'Mizanagar', 'মির্জানগর', 0, 'mizanagarup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (226, 22, 'Ctholia', 'চিথলিয়া', 0, 'ctholiaup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (227, 22, 'Boxmahmmud', 'বক্সমাহমুদ', 0, 'boxmahmmudup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (228, 23, 'Sindurpur', 'সিন্দুরপুর', 0, 'sindurpurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (229, 23, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (230, 23, 'Purbachandrapur', 'পূর্বচন্দ্রপুর', 0, 'purbachandrapurup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (231, 23, 'Ramnagar', 'রামনগর', 0, 'ramnagarup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (232, 23, 'Yeakubpur', 'ইয়াকুবপুর', 0, 'yeakubpur.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (233, 23, 'Daganbhuiyan', 'দাগনভূঞা', 0, 'daganbhuiyanup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (234, 23, 'Matubhuiyan', 'মাতুভূঞা', 0, 'matubhuiyanup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (235, 23, 'Jayloskor', 'জায়লস্কর', 0, 'jayloskorup.feni.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (236, 24, 'Basudeb', 'বাসুদেব', 0, 'basudeb.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (237, 24, 'Machihata', 'মাছিহাতা', 0, 'machihata.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (238, 24, 'Sultanpur', 'সুলতানপুর', 0, 'sultanpur.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (239, 24, 'Ramrail', 'রামরাইল', 0, 'ramrail.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (240, 24, 'Sadekpur', 'সাদেকপুর', 0, 'sadekpur.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (241, 24, 'Talsahar', 'তালশহর', 0, 'talsahar.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (242, 24, 'Natai', 'নাটাই (দক্ষিন)', 0, 'natais.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (243, 24, 'Natai', 'নাটাই (উত্তর)', 0, 'natain.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (244, 24, 'Shuhilpur', 'সুহিলপুর', 0, 'shuhilpur.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (245, 24, 'Bodhal', 'বুধল', 0, 'bodhal.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (246, 24, 'Majlishpur', 'মজলিশপুর', 0, 'majlishpur.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (247, 25, 'Mulagram', 'মূলগ্রাম', 0, 'mulagramup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (248, 25, 'Mehari', 'মেহারী', 0, 'mehariup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (249, 25, 'Badair', 'বাদৈর', 0, 'badairup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (250, 25, 'Kharera', 'খাড়েরা', 0, 'khareraup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (251, 25, 'Benauty', 'বিনাউটি', 0, 'benautyup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (252, 25, 'Gopinathpur', 'গোপীনাথপুর', 0, 'gopinathpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (253, 25, 'Kasbaw', 'কসবা', 0, 'kasbawup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (254, 25, 'Kuti', 'কুটি', 0, 'kutiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (255, 25, 'Kayempur', 'কাইমপুর', 0, 'kayempurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (256, 25, 'Bayek', 'বায়েক', 0, 'bayekup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (257, 26, 'Chatalpar', 'চাতলপাড়', 0, 'chatalparup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (258, 26, 'Bhalakut', 'ভলাকুট', 0, 'bhalakutup.brahmanbaria.gov.bd ', NULL, NULL);
INSERT INTO `unions` VALUES (259, 26, 'Kunda', 'কুন্ডা', 0, 'kundaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (260, 26, 'Goalnagar', 'গোয়ালনগর', 0, 'goalnagarup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (261, 26, 'Nasirnagar', 'নাসিরনগর', 0, 'nasirnagarup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (262, 26, 'Burishwar', 'বুড়িশ্বর', 0, 'burishwarup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (263, 26, 'Fandauk', 'ফান্দাউক', 0, 'fandaukup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (264, 26, 'Goniauk', 'গুনিয়াউক', 0, 'goniaukup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (265, 26, 'Chapartala', 'চাপৈরতলা', 0, 'chapartalaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (266, 26, 'Dharnondol', 'ধরমন্ডল', 0, 'dharnondolup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (267, 26, 'Haripur', 'হরিপুর', 0, 'haripurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (268, 26, 'Purbabhag', 'পূর্বভাগ', 0, 'purbabhagup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (269, 26, 'Gokarna', 'গোকর্ণ', 0, 'gokarnaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (270, 27, 'Auraol', 'অরুয়াইল', 0, 'auraolup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (271, 27, 'Pakshimuul', 'পাকশিমুল', 0, 'pakshimuulup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (272, 27, 'Chunta', 'চুন্টা', 0, 'chuntaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (273, 27, 'Kalikaccha', 'কালীকচ্ছ', 0, 'kalikacchaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (274, 27, 'Panishor', 'পানিশ্বর', 0, 'panishorup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (275, 27, 'Sarail', 'সরাইল সদর', 0, 'sarailup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (276, 27, 'Noagoun', 'নোয়াগাঁও', 0, 'noagounup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (277, 27, 'Shahajadapur', 'শাহজাদাপুর', 0, 'shahajadapurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (278, 27, 'Shahbazpur', 'শাহবাজপুর', 0, 'shahbazpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (279, 28, 'Ashuganj', 'আশুগঞ্জ সদর', 0, 'ashuganjup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (280, 28, 'Charchartala', 'চরচারতলা', 0, 'charchartalaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (281, 28, 'Durgapur', 'দুর্গাপুর', 0, 'durgapurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (282, 28, 'Araishidha', 'আড়াইসিধা', 0, 'araishidhaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (283, 28, 'Talshaharw', 'তালশহর(পঃ)', 0, 'talshaharwup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (284, 28, 'Sarifpur', 'শরীফপুর', 0, 'sarifpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (285, 28, 'Lalpur', 'লালপুর', 0, 'lalpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (286, 28, 'Tarua', 'তারুয়া', 0, 'taruaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (287, 29, 'Monionda', 'মনিয়ন্দ', 0, 'moniondaup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (288, 29, 'Dharkhar', 'ধরখার', 0, 'dharkharup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (289, 29, 'Mogra', 'মোগড়া', 0, 'mograup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (290, 29, 'Akhauran', 'আখাউড়া (উঃ)', 0, 'akhauranup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (291, 29, 'Akhauras', 'আখাউড়া (দঃ)', 0, 'akhaurasup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (292, 30, 'Barail', 'বড়াইল', 0, 'barailup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (293, 30, 'Birgaon', 'বীরগাঁও', 0, 'birgaonup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (294, 30, 'Krishnanagar', 'কৃষ্ণনগর', 0, 'krishnanagarup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (295, 30, 'Nathghar', 'নাটঘর', 0, 'nathgharup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (296, 30, 'Biddayakut', 'বিদ্যাকুট', 0, 'biddayakutup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (297, 30, 'Nabinagare', 'নবীনগর (পূর্ব)', 0, 'nabinagareup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (298, 30, 'Nabinagarw', 'নবীনগর(পশ্চিম)', 0, 'nabinagarwup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (299, 30, 'Bitghar', 'বিটঘর', 0, 'bitgharup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (300, 30, 'Shibpur', 'শিবপুর', 0, 'shibpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (301, 30, 'Sreerampur', 'শ্রীরামপুর', 0, 'sreerampurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (302, 30, 'Jinudpur', 'জিনোদপুর', 0, 'jinudpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (303, 30, 'Laurfatehpur', 'লাউরফতেপুর', 0, 'laurfatehpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (304, 30, 'Ibrahimpur', 'ইব্রাহিমপুর', 0, 'ibrahimpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (305, 30, 'Satmura', 'সাতমোড়া', 0, 'satmuraup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (306, 30, 'Shamogram', 'শ্যামগ্রাম', 0, 'shamogramup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (307, 30, 'Rasullabad', 'রসুল্লাবাদ', 0, 'rasullabadup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (308, 30, 'Barikandi', 'বড়িকান্দি', 0, 'barikandiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (309, 30, 'Salimganj', 'ছলিমগঞ্জ', 0, 'salimganjup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (310, 30, 'Ratanpur', 'রতনপুর', 0, 'ratanpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (311, 30, 'Kaitala (North)', 'কাইতলা (উত্তর)', 0, 'kaitalanup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (312, 30, 'Kaitala (South)', 'কাইতলা (দক্ষিন)', 0, 'kaitalasup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (313, 31, 'Tazkhali', 'তেজখালী', 0, 'tazkhaliup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (314, 31, 'Pahariya Kandi', 'পাহাড়িয়া কান্দি', 0, 'pahariyakandiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (315, 31, 'Dariadulat', 'দরিয়াদৌলত', 0, 'dariadulatup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (316, 31, 'Sonarampur', 'সোনারামপুর', 0, 'sonarampurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (317, 31, 'Darikandi', 'দড়িকান্দি', 0, 'darikandiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (318, 31, 'Saifullyakandi', 'ছয়ফুল্লাকান্দি', 0, 'saifullyakandiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (319, 31, 'Bancharampur', 'বাঞ্ছারামপুর', 0, 'bancharampurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (320, 31, 'Ayabpur', 'আইয়ুবপুর', 0, 'ayabpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (321, 31, 'Fardabad', 'ফরদাবাদ', 0, 'fardabadup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (322, 31, 'Rupushdi', 'রুপসদী পশ্চিম', 0, 'rupushdiup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (323, 31, 'Salimabad', 'ছলিমাবাদ', 0, 'salimabadup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (324, 31, 'Ujanchar', 'উজানচর পূর্ব', 0, 'ujancharup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (325, 31, 'Manikpur', 'মানিকপুর', 0, 'manikpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (326, 32, 'Bhudanty', 'বুধন্তি', 0, 'bhudantyup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (327, 32, 'Chandura', 'চান্দুরা', 0, 'chanduraup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (328, 32, 'Ichapura', 'ইছাপুরা', 0, 'ichapuraup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (329, 32, 'Champaknagar', 'চম্পকনগর', 0, 'champaknagarup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (330, 32, 'Harashpur', 'হরষপুর', 0, 'harashpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (331, 32, 'Pattan', 'পত্তন', 0, 'pattanup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (332, 32, 'Singerbil', 'সিংগারবিল', 0, 'singerbilup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (333, 32, 'Bishupor', 'বিষ্ণুপুর', 0, 'bishuporup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (334, 32, 'Charislampur', 'চর-ইসলামপুর', 0, 'charislampurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (335, 32, 'Paharpur', 'পাহাড়পুর', 0, 'paharpurup.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (336, 33, 'Jibtali', 'জীবতলি', 0, 'jibtaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (337, 33, 'Sapchari', 'সাপছড়ি', 0, 'sapchariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (338, 33, 'Kutukchari', 'কুতুকছড়ি', 0, 'kutukchariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (339, 33, 'Bandukbhanga', 'বন্দুকভাঙ্গা', 0, 'bandukbhangaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (340, 33, 'Balukhali', 'বালুখালী', 0, 'balukhaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (341, 33, 'Mogban', 'মগবান', 0, 'mogbanup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (342, 34, 'Raikhali', 'রাইখালী', 0, 'raikhaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (343, 34, 'Kaptai', 'কাপ্তাই', 0, 'kaptaiup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (344, 34, 'Wagga', 'ওয়াজ্ঞা', 0, 'waggaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (345, 34, 'Chandraghona', 'চন্দ্রঘোনা', 0, 'chandraghonaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (346, 34, 'Chitmorom', 'চিৎমরম', 0, 'chitmoromup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (347, 35, 'Ghagra', 'ঘাগড়া', 0, 'ghagraup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (348, 35, 'Fatikchari', 'ফটিকছড়ি', 0, 'fatikchariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (349, 35, 'Betbunia', 'বেতবুনিয়া', 0, 'betbuniaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (350, 35, 'Kalampati', 'কলমপতি', 0, 'kalampatiup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (351, 36, 'Sajek', 'সাজেক', 0, 'sajekup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (352, 36, 'Amtali', 'আমতলী', 0, 'amtaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (353, 36, 'Bongoltali', 'বঙ্গলতলী', 0, 'bongoltaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (354, 36, 'Rupokari', 'রুপকারী', 0, 'rupokariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (355, 36, 'Marisha', 'মারিশ্যা', 0, 'marishaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (356, 36, 'Khedarmara', 'খেদারমারা', 0, 'khedarmaraup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (357, 36, 'Sharoyatali', 'সারোয়াতলী', 0, 'sharoyataliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (358, 36, 'Baghaichari', 'বাঘাইছড়ি', 0, 'baghaichariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (359, 37, 'Subalong', 'সুবলং', 0, 'subalongup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (360, 37, 'Barkal', 'বরকল', 0, 'barkalup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (361, 37, 'Bushanchara', 'ভূষনছড়া', 0, 'bushancharaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (362, 37, 'Aimachara', 'আইমাছড়া', 0, 'aimacharaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (363, 37, 'Borohorina', 'বড় হরিণা', 0, 'borohorinaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (364, 38, 'Langad', 'লংগদু', 0, 'langaduup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (365, 38, 'Maeinimukh', 'মাইনীমুখ', 0, 'maeinimukhup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (366, 38, 'Vasannadam', 'ভাসান্যাদম', 0, 'vasannadamup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (367, 38, 'Bogachattar', 'বগাচতর', 0, 'bogachattarup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (368, 38, 'Gulshakhali', 'গুলশাখালী', 0, 'gulshakhaliup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (369, 38, 'Kalapakujja', 'কালাপাকুজ্যা', 0, 'kalapakujjaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (370, 38, 'Atarakchara', 'আটারকছড়া', 0, 'atarakcharaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (371, 39, 'Ghilachari', 'ঘিলাছড়ি', 0, 'ghilachariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (372, 39, 'Gaindya', 'গাইন্দ্যা', 0, 'gaindyaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (373, 39, 'Bangalhalia', 'বাঙ্গালহালিয়া', 0, 'bangalhaliaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (374, 40, 'Kengrachari', 'কেংড়াছড়ি', 0, 'kengrachariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (375, 40, 'Belaichari', 'বিলাইছড়ি', 0, 'belaichariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (376, 40, 'Farua', 'ফারুয়া', 0, 'faruaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (377, 41, 'Juraichari', 'জুরাছড়ি', 0, 'juraichariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (378, 41, 'Banajogichara', 'বনযোগীছড়া', 0, 'banajogicharaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (379, 41, 'Moidong', 'মৈদং', 0, 'moidongup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (380, 41, 'Dumdumya', 'দুমদুম্যা', 0, 'dumdumyaup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (381, 42, 'Sabekkhong', 'সাবেক্ষ্যং', 0, 'sabekkhongup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (382, 42, 'Naniarchar', 'নানিয়ারচর', 0, 'naniarcharup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (383, 42, 'Burighat', 'বুড়িঘাট', 0, 'burighatup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (384, 42, 'Ghilachhari', 'ঘিলাছড়ি', 0, 'ghilachhariup.rangamati.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (385, 43, 'Charmatua', 'চরমটুয়া', 0, 'charmatuaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (386, 43, 'Dadpur', 'দাদপুর', 0, 'dadpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (387, 43, 'Noannoi', 'নোয়ান্নই', 0, 'noannoiup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (388, 43, 'Kadirhanif', 'কাদির হানিফ', 0, 'kadirhanifup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (389, 43, 'Binodpur', 'বিনোদপুর', 0, 'binodpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (390, 43, 'Dharmapur', 'ধর্মপুর', 0, 'dharmapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (391, 43, 'Aujbalia', 'এওজবালিয়া', 0, 'aujbaliaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (392, 43, 'Kaladara', 'কালাদরপ', 0, 'kaladarapup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (393, 43, 'Ashwadia', 'অশ্বদিয়া', 0, 'ashwadiaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (394, 43, 'Newajpur', 'নিয়াজপুর', 0, 'newajpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (395, 43, 'East Charmatua', 'পূর্ব চরমটুয়া', 0, 'eastcharmatuap.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (396, 43, 'Andarchar', 'আন্ডারচর', 0, 'andarcharup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (397, 43, 'Noakhali', 'নোয়াখালী', 0, 'noakhaliup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (398, 44, 'Sirajpur', 'সিরাজপুর', 0, 'sirajpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (399, 44, 'Charparboti', 'চরপার্বতী', 0, 'charparbotiup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (400, 44, 'Charhazari', 'চরহাজারী', 0, 'charhazariup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (401, 44, 'Charkakra', 'চরকাঁকড়া', 0, 'charkakraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (402, 44, 'Charfakira', 'চরফকিরা', 0, 'charfakiraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (403, 44, 'Musapur', 'মুসাপুর', 0, 'musapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (404, 44, 'Charelahi', 'চরএলাহী', 0, 'charelahiup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (405, 44, 'Rampur', 'রামপুর', 0, 'rampurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (406, 45, 'Amanullapur', 'আমানউল্ল্যাপুর', 0, 'amanullapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (407, 45, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (408, 45, 'Jirtali', 'জিরতলী', 0, 'jirtaliup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (409, 45, 'Kutubpur', 'কুতবপুর', 0, 'kutubpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (410, 45, 'Alyearpur', 'আলাইয়ারপুর', 0, 'alyearpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (411, 45, 'Chayani', 'ছয়ানী', 0, 'chayaniup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (412, 45, 'Rajganj', 'রাজগঞ্জ', 0, 'rajganjup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (413, 45, 'Eklashpur', 'একলাশপুর', 0, 'eklashpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (414, 45, 'Begumganj', 'বেগমগঞ্জ', 0, 'begumganjup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (415, 45, 'Mirwarishpur', 'মিরওয়ারিশপুর', 0, 'mirwarishpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (416, 45, 'Narottampur', 'নরোত্তমপুর', 0, 'narottampurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (417, 45, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (418, 45, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (419, 45, 'Hajipur', 'হাজীপুর', 0, 'hajipurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (420, 45, 'Sharifpur', 'শরীফপুর', 0, 'sharifpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (421, 45, 'Kadirpur', 'কাদিরপুর', 0, 'kadirpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (422, 46, 'Sukhchar', 'সুখচর', 0, 'sukhcharup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (423, 46, 'Nolchira', 'নলচিরা', 0, 'nolchiraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (424, 46, 'Charishwar', 'চরঈশ্বর', 0, 'charishwarup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (425, 46, 'Charking', 'চরকিং', 0, 'charkingup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (426, 46, 'Tomoroddi', 'তমরদ্দি', 0, 'tomoroddiup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (427, 46, 'Sonadiya', 'সোনাদিয়া', 0, 'sonadiyaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (428, 46, 'Burirchar', 'বুড়িরচর', 0, 'burircharup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (429, 46, 'Jahajmara', 'জাহাজমারা', 0, 'jahajmaraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (430, 46, 'Nijhumdwi', 'নিঝুমদ্বীপ', 0, 'nijhumdwipup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (431, 47, 'Charjabbar', 'চরজাব্বার', 0, 'charjabbarup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (432, 47, 'Charbata', 'চরবাটা', 0, 'charbataup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (433, 47, 'Charclerk', 'চরক্লার্ক', 0, 'charclerkup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (434, 47, 'Charwapda', 'চরওয়াপদা', 0, 'charwapdaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (435, 47, 'Charjubilee', 'চরজুবলী', 0, 'charjubileeup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (436, 47, 'Charaman Ullah', 'চরআমান উল্যা', 0, 'charamanullahup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (437, 47, 'East Charbata', 'পূর্ব চরবাটা', 0, 'eastcharbataup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (438, 47, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (439, 48, 'Narottampur', 'নরোত্তমপুর', 0, 'narottampurup1.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (440, 48, 'Dhanshiri', 'ধানসিঁড়ি', 0, 'dhanshiriup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (441, 48, 'Sundalpur', 'সুন্দলপুর', 0, 'sundalpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (442, 48, 'Ghoshbag', 'ঘোষবাগ', 0, 'ghoshbagup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (443, 48, 'Chaprashirhat', 'চাপরাশিরহাট', 0, 'chaprashirhatup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (444, 48, 'Dhanshalik', 'ধানশালিক', 0, 'dhanshalikup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (445, 48, 'Batoiya', 'বাটইয়া', 0, 'batoiyaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (446, 49, 'Chhatarpaia', 'ছাতারপাইয়া', 0, 'chhatarpaiaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (447, 49, 'Kesharpar', 'কেশরপাড়া', 0, 'kesharparup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (448, 49, 'Dumurua', 'ডুমুরুয়া', 0, 'dumuruaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (449, 49, 'Kadra', 'কাদরা', 0, 'kadraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (450, 49, 'Arjuntala', 'অর্জুনতলা', 0, 'arjuntalaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (451, 49, 'Kabilpur', 'কাবিলপুর', 0, 'kabilpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (452, 49, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpurup7.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (453, 49, 'Nabipur', 'নবীপুর', 0, 'nabipurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (454, 49, 'Bejbagh', 'বিজবাগ', 0, 'bejbaghup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (455, 50, 'Sahapur', 'সাহাপুর', 0, 'sahapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (456, 50, 'Ramnarayanpur', 'রামনারায়নপুর', 0, 'ramnarayanpurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (457, 50, 'Porokote', 'পরকোট', 0, 'porokoteup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (458, 50, 'Badalkot', 'বাদলকোট', 0, 'badalkotup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (459, 50, 'Panchgaon', 'পাঁচগাঁও', 0, 'panchgaonup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (460, 50, 'Hat-Pukuria Ghatlabag', 'হাট-পুকুরিয়া ঘাটলাবাগ', 0, 'hatpukuriaghatlabagup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (461, 50, 'Noakhala', 'নোয়াখলা', 0, 'noakhalaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (462, 50, 'Khilpara', 'খিলপাড়া', 0, 'khilparaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (463, 50, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpuru5p.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (464, 51, 'Joyag', 'জয়াগ', 0, 'joyagup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (465, 51, 'Nodona', 'নদনা', 0, 'nodonaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (466, 51, 'Chashirhat', 'চাষীরহাট', 0, 'chashirhatup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (467, 51, 'Barogaon', 'বারগাঁও', 0, 'barogaonup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (468, 51, 'Ambarnagor', 'অম্বরনগর', 0, 'ambarnagorup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (469, 51, 'Nateshwar', 'নাটেশ্বর', 0, 'nateshwarup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (470, 51, 'Bajra', 'বজরা', 0, 'bajraup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (471, 51, 'Sonapur', 'সোনাপুর', 0, 'sonapurup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (472, 51, 'Deoti', 'দেওটি', 0, 'deotiup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (473, 51, 'Amishapara', 'আমিশাপাড়া', 0, 'amishaparaup.noakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (474, 52, 'Gazipur', 'গাজীপুর', 0, 'gazipurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (475, 52, 'Algidurgapur (North)', 'আলগী দুর্গাপুর (উত্তর)', 0, 'algidurgapurnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (476, 52, 'Algidurgapur (South)', 'আলগী দুর্গাপুর (দক্ষিণ)', 0, 'algidurgapursouth.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (477, 52, 'Nilkamal', 'নীলকমল', 0, 'nilkamalup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (478, 52, 'Haimchar', 'হাইমচর', 0, 'haimcharup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (479, 52, 'Charbhairabi', 'চরভৈরবী', 0, 'charbhairabiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (480, 53, 'Pathair', 'পাথৈর', 0, 'pathairup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (481, 53, 'Bitara', 'বিতারা', 0, 'bitaraup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (482, 53, 'Shohodebpur (East)', 'সহদেবপুর (পূর্ব)', 0, 'shohodebpureastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (483, 53, 'Shohodebpur (West)', 'সহদেবপুর (পশ্চিম)', 0, 'shohodebpurwestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (484, 53, 'Kachua (North)', 'কচুয়া (উত্তর)', 0, 'kachuanorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (485, 53, 'Kachua (South)', 'কচুয়া (দক্ষিণ)', 0, 'kachuasouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (486, 53, 'Gohat (North)', 'গোহাট (উত্তর)', 0, 'gohatnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (487, 53, 'Kadla', 'কাদলা', 0, 'kadlaup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (488, 53, 'Ashrafpur', 'আসরাফপুর', 0, 'ashrafpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (489, 53, 'Gohat (South)', 'গোহাট (দক্ষিণ)', 0, 'gohatsouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (490, 53, 'Sachar', 'সাচার', 0, 'sacharup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (491, 53, 'Koroia', 'কড়ইয়া', 0, 'koroiaup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (492, 54, 'Tamta (South)', 'টামটা (দক্ষিণ)', 0, 'tamtasouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (493, 54, 'Tamta (North)', 'টামটা (উত্তর)', 0, 'tamtanorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (494, 54, 'Meher (North)', 'মেহের (উত্তর)', 0, 'mehernorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (495, 54, 'Meher (South)', 'মেহের (দক্ষিণ)', 0, 'mehersouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (496, 54, 'Suchipara (North)', 'সুচিপাড়া (উত্তর)', 0, 'suchiparanorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (497, 54, 'Suchipara (South)', 'সুচিপাড়া (দক্ষিণ)', 0, 'suchiparasouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (498, 54, 'Chitoshi (East)', 'চিতষী (পূর্ব)', 0, 'chitoshieastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (499, 54, 'Raysree (South)', 'রায়শ্রী (দক্ষিন)', 0, 'raysreesouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (500, 54, 'Raysree (North)', 'রায়শ্রী (উত্তর)', 0, 'raysreenorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (501, 54, 'Chitoshiwest', 'চিতষী (পশ্চিম)', 0, 'chitoshiwestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (502, 55, 'Bishnapur', 'বিষ্ণপুর', 0, 'bishnapurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (503, 55, 'Ashikati', 'আশিকাটি', 0, 'ashikatiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (504, 55, 'Shahmahmudpur', 'শাহ্‌ মাহমুদপুর', 0, 'shahmahmudpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (505, 55, 'Kalyanpur', 'কল্যাণপুর', 0, 'kalyanpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (506, 55, 'Rampur', 'রামপুর', 0, 'rampurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (507, 55, 'Maishadi', 'মৈশাদী', 0, 'maishadiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (508, 55, 'Tarpurchandi', 'তরপুচন্ডী', 0, 'tarpurchandiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (509, 55, 'Baghadi', 'বাগাদী', 0, 'baghadiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (510, 55, 'Laxmipur Model', 'লক্ষীপুর মডেল', 0, 'laxmipurmodelup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (511, 55, 'Hanarchar', 'হানারচর', 0, 'hanarcharup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (512, 55, 'Chandra', 'চান্দ্রা', 0, 'chandraup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (513, 55, 'Rajrajeshwar', 'রাজরাজেশ্বর', 0, 'rajrajeshwarup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (514, 55, 'Ibrahimpur', 'ইব্রাহীমপুর', 0, 'ibrahimpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (515, 55, 'Balia', 'বালিয়া', 0, 'baliaup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (516, 56, 'Nayergaon (North)', 'নায়েরগাঁও (উত্তর)', 0, 'nayergaonnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (517, 56, 'Nayergaon (South)', 'নায়েরগাঁও (দক্ষিন)', 0, 'nayergaonsouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (518, 56, 'Khadergaon', 'খাদেরগাঁও', 0, 'khadergaonup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (519, 56, 'Narayanpur', 'নারায়নপুর', 0, 'narayanpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (520, 56, 'Upadi (South)', 'উপাদী (দক্ষিণ)', 0, 'upadisouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (521, 56, 'Upadi (North)', 'উপাদী (উত্তর)', 0, 'upadinorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (522, 57, 'Rajargaon (North)', 'রাজারগাঁও (উত্তর)', 0, 'rajargaonnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (523, 57, 'Bakila', 'বাকিলা', 0, 'bakilaup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (524, 57, 'Kalocho (North)', 'কালচোঁ (উত্তর)', 0, 'kalochonorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (525, 57, 'Hajiganj Sadar', 'হাজীগঞ্জ সদর', 0, 'hajiganjsadarup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (526, 57, 'Kalocho (South)', 'কালচোঁ (দক্ষিণ)', 0, 'kalochosouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (527, 57, 'Barkul (East)', 'বড়কুল (পূর্ব)', 0, 'barkuleastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (528, 57, 'Barkul (West)', 'বড়কুল (পশ্চিম)', 0, 'barkulwestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (529, 57, 'Hatila (East)', 'হাটিলা (পূর্ব)', 0, 'hatilaeastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (530, 57, 'Hatila (West)', 'হাটিলা (পশ্চিম)', 0, 'hatilawestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (531, 57, 'Gandharbapur (North)', 'গন্ধর্ব্যপুর (উত্তর)', 0, 'gandharbapurnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (532, 57, 'Gandharbapur (South)', 'গন্ধর্ব্যপুর (দক্ষিণ)', 0, 'gandharbapursouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (533, 58, 'Satnal', 'ষাটনল', 0, 'satnalup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (534, 58, 'Banganbari', 'বাগানবাড়ী', 0, 'banganbariup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (535, 58, 'Sadullapur', 'সাদুল্ল্যাপুর', 0, 'sadullapurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (536, 58, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (537, 58, 'Kalakanda', 'কালাকান্দা', 0, 'kalakandaup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (538, 58, 'Mohanpur', 'মোহনপুর', 0, 'mohanpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (539, 58, 'Eklaspur', 'এখলাছপুর', 0, 'eklaspurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (540, 58, 'Jahirabad', 'জহিরাবাদ', 0, 'jahirabadup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (541, 58, 'Fatehpur (East)', 'ফতেহপুর (পূর্ব)', 0, 'eastfatehpur.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (542, 58, 'Fatehpur (West)', 'ফতেহপুর (পশ্চিম)', 0, 'westfatehpurup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (543, 58, 'Farajikandi', 'ফরাজীকান্দি', 0, 'farajikandiup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (544, 58, 'Islamabad', 'ইসলামাবাদ', 0, 'islamabadup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (545, 58, 'Sultanabad', 'সুলতানাবাদ', 0, 'sultanabadup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (546, 58, 'Gazra', 'গজরা', 0, 'gazraup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (547, 59, 'Balithuba (West)', 'বালিথুবা (পশ্চিম)', 0, 'balithubawestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (548, 59, 'Balithuba (East)', 'বালিথুবা (পূর্ব)', 0, 'balithubaeastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (549, 59, 'Subidpur (East)', 'সুবিদপুর (পূর্ব)', 0, 'subidpureastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (550, 59, 'Subidpur (West)', 'সুবিদপুর (পশ্চিম)', 0, 'subidpurwestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (551, 59, 'Gupti (West)', 'গুপ্তি (পশ্চিম)', 0, 'guptiwestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (552, 59, 'Gupti (East)', 'গুপ্তি (পূর্ব)', 0, 'guptieastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (553, 59, 'Paikpara (North)', 'পাইকপাড়া (উত্তর)', 0, 'paikparanorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (554, 59, 'Paikpara (South)', 'পাইকপাড়া (দক্ষিণ)', 0, 'paikparasouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (555, 59, 'Gobindapur (North)', 'গবিন্দপুর (উত্তর)', 0, 'gobindapurnorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (556, 59, 'Gobindapur (South)', 'গবিন্দপুর (দক্ষিণ)', 0, 'gobindapursouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (557, 59, 'Chardukhia (East)', 'চরদুখিয়া (পূর্ব)', 0, 'chardukhiaeastup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (558, 59, 'Chardukhia (West)', 'চরদুঃখিয়া (পশ্চিম)', 0, 'chardukhiawestup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (559, 59, 'Faridgonj (South)', 'ফরিদ্গঞ্জ (দক্ষিণ)', 0, 'faridgonjsouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (560, 59, 'Rupsha (South)', 'রুপসা (দক্ষিণ)', 0, 'rupshasouthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (561, 59, 'Rupsha (North)', 'রুপসা (উত্তর)', 0, 'rupshanorthup.chandpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (562, 60, 'Hamsadi (North)', 'হামছাদী (উত্তর)', 0, 'northhamsadiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (563, 60, 'Hamsadi (South)', 'হামছাদী (দক্ষিন)', 0, 'southhamsadiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (564, 60, 'Dalalbazar', 'দালাল বাজার', 0, 'dalalbazarup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (565, 60, 'Charruhita', 'চররুহিতা', 0, 'charruhitaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (566, 60, 'Parbotinagar', 'পার্বতীনগর', 0, 'parbotinagarup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (567, 60, 'Bangakha', 'বাঙ্গাখাঁ', 0, 'bangakhaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (568, 60, 'Dattapara', 'দত্তপাড়া', 0, 'dattaparaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (569, 60, 'Basikpur', 'বশিকপুর', 0, 'basikpurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (570, 60, 'Chandrogonj', 'চন্দ্রগঞ্জ', 0, 'chandrogonjup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (571, 60, 'Nourthjoypur', 'উত্তর জয়পুর', 0, 'nourthjoypurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (572, 60, 'Hazirpara', 'হাজিরপাড়া', 0, 'hazirparaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (573, 60, 'Charshahi', 'চরশাহী', 0, 'charshahiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (574, 60, 'Digli', 'দিঘলী', 0, 'digliup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (575, 60, 'Laharkandi', 'লাহারকান্দি', 0, 'laharkandiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (576, 60, 'Vobanigonj', 'ভবানীগঞ্জ', 0, 'vobanigonjup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (577, 60, 'Kusakhali', 'কুশাখালী', 0, 'kusakhaliup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (578, 60, 'Sakchor', 'শাকচর', 0, 'sakchorup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (579, 60, 'Tearigonj', 'তেয়ারীগঞ্জ', 0, 'tearigonjup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (580, 60, 'Tumchor', 'টুমচর', 0, 'tumchorup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (581, 60, 'Charramoni Mohon', 'চররমনী মোহন', 0, 'charramonimohonup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (582, 61, 'Charkalkini', 'চর কালকিনি', 0, 'charkalkiniup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (583, 61, 'Shaheberhat', 'সাহেবেরহাট', 0, 'shaheberhatup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (584, 61, 'Char Martin', 'চর মার্টিন', 0, 'charmartinup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (585, 61, 'Char Folcon', 'চর ফলকন', 0, 'charfolconup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (586, 61, 'Patarirhat', 'পাটারীরহাট', 0, 'patarirhatup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (587, 61, 'Hajirhat', 'হাজিরহাট', 0, 'hajirhatup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (588, 61, 'Char Kadira', 'চর কাদিরা', 0, 'charkadiraup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (589, 61, 'Torabgonj', 'তোরাবগঞ্জ', 0, 'torabgonjup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (590, 61, 'Charlorench', 'চর লরেঞ্চ', 0, 'charlorenchup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (591, 62, 'North Char Ababil', 'উত্তর চর আবাবিল', 0, 'northcharababilup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (592, 62, 'North Char Bangshi', 'উত্তর চর বংশী', 0, 'northcharbangshiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (593, 62, 'Char Mohana', 'চর মোহনা', 0, 'charmohanaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (594, 62, 'Sonapur', 'সোনাপুর', 0, 'sonapurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (595, 62, 'Charpata', 'চর পাতা', 0, 'charpataup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (596, 62, 'Bamni', 'বামনী', 0, 'bamniup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (597, 62, 'South Char Bangshi', 'দক্ষিন চর বংশী', 0, 'southcharbangshiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (598, 62, 'South Char Ababil', 'দক্ষিন চর আবাবিল', 0, 'southcharababilup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (599, 62, 'Raipur', 'রায়পুর', 0, 'raipurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (600, 62, 'Keora', 'কেরোয়া', 0, 'keoraup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (601, 63, 'Char Poragacha', 'চর পোড়াগাছা', 0, 'charporagachaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (602, 63, 'Charbadam', 'চর বাদাম', 0, 'charbadamup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (603, 63, 'Char Abdullah', 'চর আবদুল্যাহ', 0, 'charabdullahup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (604, 63, 'Alxendar', 'আলেকজান্ডার', 0, 'alxendarup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (605, 63, 'Char Algi', 'চর আলগী', 0, 'charalgiup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (606, 63, 'Char Ramiz', 'চর রমিজ', 0, 'charramizup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (607, 63, 'Borokheri', 'বড়খেড়ী', 0, 'borokheriup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (608, 63, 'Chargazi', 'চরগাজী', 0, 'chargaziup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (609, 64, 'Kanchanpur', 'কাঞ্চনপুর', 0, 'kanchanpurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (610, 64, 'Noagaon', 'নোয়াগাঁও', 0, 'noagaonup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (611, 64, 'Bhadur', 'ভাদুর', 0, 'bhadurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (612, 64, 'Ichhapur', 'ইছাপুর', 0, 'ichhapurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (613, 64, 'Chandipur', 'চন্ডিপুর', 0, 'chandipurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (614, 64, 'Lamchar', 'লামচর', 0, 'lamcharup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (615, 64, 'Darbeshpur', 'দরবেশপুর', 0, 'darbeshpurup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (616, 64, 'Karpara', 'করপাড়া', 0, 'karparaup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (617, 64, 'Bholakot', 'ভোলাকোট', 0, 'bholakotup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (618, 64, 'Bhatra', 'ভাটরা', 0, 'bhatraup.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (619, 65, 'Rajanagar', 'রাজানগর', 0, 'rajanagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (620, 65, 'Hosnabad', 'হোছনাবাদ', 0, 'hosnabadup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (621, 65, 'Swanirbor Rangunia', 'স্বনির্ভর রাঙ্গুনিয়া', 0, 'swanirborranguniaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (622, 65, 'Mariumnagar', 'মরিয়মনগর', 0, 'mariumnagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (623, 65, 'Parua', 'পারুয়া', 0, 'paruaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (624, 65, 'Pomra', 'পোমরা', 0, 'pomraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (625, 65, 'Betagi', 'বেতাগী', 0, 'betagiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (626, 65, 'Sharafbhata', 'সরফভাটা', 0, 'sharafbhataup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (627, 65, 'Shilok', 'শিলক', 0, 'shilokup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (628, 65, 'Chandraghona', 'চন্দ্রঘোনা', 0, 'chandraghonaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (629, 65, 'Kodala', 'কোদালা', 0, 'kodalaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (630, 65, 'Islampur', 'ইসলামপুর', 0, 'islampurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (631, 65, 'South Rajanagar', 'দক্ষিণ রাজানগর', 0, 'southrajanagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (632, 65, 'Lalanagar', 'লালানগর', 0, 'lalanagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (633, 66, 'Kumira', 'কুমিরা', 0, 'kumiraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (634, 66, 'Banshbaria', 'বাঁশবারীয়া', 0, 'banshbariaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (635, 66, 'Barabkunda', 'বারবকুন্ড', 0, 'barabkundaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (636, 66, 'Bariadyala', 'বাড়িয়াডিয়ালা', 0, 'bariadyalaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (637, 66, 'Muradpur', 'মুরাদপুর', 0, 'muradpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (638, 66, 'Saidpur', 'সাঈদপুর', 0, 'saidpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (639, 66, 'Salimpur', 'সালিমপুর', 0, 'salimpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (640, 66, 'Sonaichhari', 'সোনাইছড়ি', 0, 'sonaichhariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (641, 66, 'Bhatiari', 'ভাটিয়ারী', 0, 'bhatiariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (642, 67, 'Korerhat', 'করেরহাট', 0, 'korerhatup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (643, 67, 'Hinguli', 'হিংগুলি', 0, 'hinguliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (644, 67, 'Jorarganj', 'জোরারগঞ্জ', 0, 'jorarganjup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (645, 67, 'Dhoom', 'ধুম', 0, 'dhoomup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (646, 67, 'Osmanpur', 'ওসমানপুর', 0, 'osmanpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (647, 67, 'Ichakhali', 'ইছাখালী', 0, 'ichakhaliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (648, 67, 'Katachhara', 'কাটাছরা', 0, 'katachharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (649, 67, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (650, 67, 'Mirsharai', 'মীরসরাই', 0, 'mirsharaiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (651, 67, 'Mithanala', 'মিঠানালা', 0, 'mithanalaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (652, 67, 'Maghadia', 'মঘাদিয়া', 0, 'maghadiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (653, 67, 'Khaiyachhara', 'খৈয়াছরা', 0, 'khaiyachharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (654, 67, 'Mayani', 'মায়ানী', 0, 'mayaniup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (655, 67, 'Haitkandi', 'হাইতকান্দি', 0, 'haitkandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (656, 67, 'Wahedpur', 'ওয়াহেদপুর', 0, 'wahedpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (657, 67, 'Saherkhali', 'সাহেরখালী', 0, 'saherkhaliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (658, 68, 'Asia', 'আশিয়া', 0, 'asiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (659, 68, 'Kachuai', 'কাচুয়াই', 0, 'kachuaiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (660, 68, 'Kasiais', 'কাশিয়াইশ', 0, 'kasiaisup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (661, 68, 'Kusumpura', 'কুসুমপুরা', 0, 'kusumpuraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (662, 68, 'Kelishahar', 'কেলিশহর', 0, 'kelishaharup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (663, 68, 'Kolagaon', 'কোলাগাঁও', 0, 'kolagaonup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (664, 68, 'Kharana', 'খরনা', 0, 'kharanaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (665, 68, 'Char Patharghata', 'চর পাথরঘাটা', 0, 'charpatharghataup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (666, 68, 'Char Lakshya', 'চর লক্ষ্যা', 0, 'charlakshyaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (667, 68, 'Chanhara', 'ছনহরা', 0, 'chanharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (668, 68, 'Janglukhain', 'জঙ্গলখাইন', 0, 'janglukhainup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (669, 68, 'Jiri', 'জিরি', 0, 'jiriup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (670, 68, 'Juldha', 'জুলধা', 0, 'juldhaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (671, 68, 'Dakkhin Bhurshi', 'দক্ষিণ ভূর্ষি', 0, 'dakhinbhurshiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (672, 68, 'Dhalghat', 'ধলঘাট', 0, 'dhalghatup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (673, 68, 'Bara Uthan', 'বড় উঠান', 0, 'barauthanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (674, 68, 'Baralia', 'বরলিয়া', 0, 'baraliaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (675, 68, 'Bhatikhain', 'ভাটিখাইন', 0, 'bhatikhainup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (676, 68, 'Sikalbaha', 'শিকলবাহা', 0, 'sikalbahaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (677, 68, 'Sobhandandi', 'শোভনদন্ডী', 0, 'sobhandandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (678, 68, 'Habilasdwi', 'হাবিলাসদ্বীপ', 0, 'habilasdwipup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (679, 68, 'Haidgaon', 'হাইদগাঁও', 0, 'haidgaonup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (680, 69, 'Rahmatpur', 'রহমতপুর', 0, 'rahmatpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (681, 69, 'Harispur', 'হরিশপুর', 0, 'harispurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (682, 69, 'Kalapania', 'কালাপানিয়া', 0, 'kalapaniaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (683, 69, 'Amanullah', 'আমানউল্যা', 0, 'amanullahup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (684, 69, 'Santoshpur', 'সন্তোষপুর', 0, 'santoshpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (685, 69, 'Gachhua', 'গাছুয়া', 0, 'gachhuaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (686, 69, 'Bauria', 'বাউরিয়া', 0, 'bauriaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (687, 69, 'Haramia', 'হারামিয়া', 0, 'haramiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (688, 69, 'Magdhara', 'মগধরা', 0, 'magdharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (689, 69, 'Maitbhanga', 'মাইটভাঙ্গা', 0, 'maitbhangaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (690, 69, 'Sarikait', 'সারিকাইত', 0, 'sarikaitup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (691, 69, 'Musapur', 'মুছাপুর', 0, 'musapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (692, 69, 'Azimpur', 'আজিমপুর', 0, 'azimpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (693, 69, 'Urirchar', 'উড়িরচর', 0, 'urircharup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (694, 70, 'Pukuria', 'পুকুরিয়া', 0, 'pukuriaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (695, 70, 'Sadhanpur', 'সাধনপুর', 0, 'sadhanpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (696, 70, 'Khankhanabad', 'খানখানাবাদ', 0, 'khankhanabadup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (697, 70, 'Baharchhara', 'বাহারছড়া', 0, 'baharchharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (698, 70, 'Kalipur', 'কালীপুর', 0, 'kalipurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (699, 70, 'Bailchhari', 'বৈলছড়ি', 0, 'bailchhariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (700, 70, 'Katharia', 'কাথরিয়া', 0, 'kathariaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (701, 70, 'Saral', 'সরল', 0, 'saralup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (702, 70, 'Silk', 'শীলকুপ', 0, 'silkupup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (703, 70, 'Chambal', 'চাম্বল', 0, 'chambalup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (704, 70, 'Gandamara', 'গন্ডামারা', 0, 'gandamaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (705, 70, 'Sekherkhil', 'শেখেরখীল', 0, 'sekherkhilup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (706, 70, 'Puichhari', 'পুঁইছড়ি', 0, 'puichhariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (707, 70, 'Chhanua', 'ছনুয়া', 0, 'chhanuaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (708, 71, 'Kandhurkhil', 'কধুরখীল', 0, 'kandhurkhilup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (709, 71, 'Pashchim Gamdandi', 'পশ্চিম গোমদন্ডী', 0, 'pashchimgamdandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (710, 71, 'Purba Gomdandi', 'পুর্ব গোমদন্ডী', 0, 'purbagomdandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (711, 71, 'Sakpura', 'শাকপুরা', 0, 'sakpuraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (712, 71, 'Saroatali', 'সারোয়াতলী', 0, 'saroataliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (713, 71, 'Popadia', 'পোপাদিয়া', 0, 'popadiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (714, 71, 'Charandwi', 'চরনদ্বীপ', 0, 'charandwipup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (715, 71, 'Sreepur-Kharandwi', 'শ্রীপুর-খরন্দীপ', 0, 'sreepurkharandwipup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (716, 71, 'Amuchia', 'আমুচিয়া', 0, 'amuchiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (717, 71, 'Ahla Karaldenga', 'আহল্লা করলডেঙ্গা', 0, 'ahlakaraldengaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (718, 72, 'Boirag', 'বৈরাগ', 0, 'boiragup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (719, 72, 'Barasat', 'বারশত', 0, 'barasatup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (720, 72, 'Raipur', 'রায়পুর', 0, 'raipurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (721, 72, 'Battali', 'বটতলী', 0, 'battaliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (722, 72, 'Barumchara', 'বরম্নমচড়া', 0, 'barumcharaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (723, 72, 'Baroakhan', 'বারখাইন', 0, 'baroakhanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (724, 72, 'Anwara', 'আনোয়ারা', 0, 'anwaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (725, 72, 'Chatari', 'চাতরী', 0, 'chatariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (726, 72, 'Paraikora', 'পরৈকোড়া', 0, 'paraikoraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (727, 72, 'Haildhar', 'হাইলধর', 0, 'haildharup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (728, 72, 'Juidandi', 'জুঁইদন্ডী', 0, 'juidandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (729, 73, 'Kanchanabad', 'কাঞ্চনাবাদ', 0, 'kanchanabadup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (730, 73, 'Joara', 'জোয়ারা', 0, 'joaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (731, 73, 'Barkal', 'বরকল', 0, 'barkalup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (732, 73, 'Barama', 'বরমা', 0, 'baramaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (733, 73, 'Bailtali', 'বৈলতলী', 0, 'bailtaliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (734, 73, 'Satbaria', 'সাতবাড়িয়া', 0, 'satbariaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (735, 73, 'Hashimpur', 'হাশিমপুর', 0, 'hashimpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (736, 73, 'Dohazari', 'দোহাজারী', 0, 'dohazariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (737, 73, 'Dhopachhari', 'ধোপাছড়ী', 0, 'dhopachhariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (738, 74, 'Charati', 'চরতী', 0, 'charatiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (739, 74, 'Khagaria', 'খাগরিয়া', 0, 'khagariaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (740, 74, 'Nalua', 'নলুয়া', 0, 'naluaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (741, 74, 'Kanchana', 'কাঞ্চনা', 0, 'kanchanaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (742, 74, 'Amilaisi', 'আমিলাইশ', 0, 'amilaisiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (743, 74, 'Eochiai', 'এওচিয়া', 0, 'eochiaiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (744, 74, 'Madarsa', 'মাদার্শা', 0, 'madarsaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (745, 74, 'Dhemsa', 'ঢেমশা', 0, 'dhemsaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (746, 74, 'Paschim Dhemsa', 'পশ্চিম ঢেমশা', 0, 'paschimdhemsaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (747, 74, 'Keochia', 'কেঁওচিয়া', 0, 'keochiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (748, 74, 'Kaliais', 'কালিয়াইশ', 0, 'kaliaisup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (749, 74, 'Bazalia', 'বাজালিয়া', 0, 'bazaliaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (750, 74, 'Puranagar', 'পুরানগড়', 0, 'puranagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (751, 74, 'Sadaha', 'ছদাহা', 0, 'sadahaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (752, 74, 'Satkania', 'সাতকানিয়া', 0, 'satkaniaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (753, 74, 'Sonakania', 'সোনাকানিয়া', 0, 'sonakaniaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (754, 75, 'Padua', 'পদুয়া', 0, 'paduaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (755, 75, 'Barahatia', 'বড়হাতিয়া', 0, 'barahatiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (756, 75, 'Amirabad', 'আমিরাবাদ', 0, 'amirabadup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (757, 75, 'Charamba', 'চরম্বা', 0, 'charambaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (758, 75, 'Kalauzan', 'কলাউজান', 0, 'kalauzanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (759, 75, 'Lohagara', 'লোহাগাড়া', 0, 'lohagaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (760, 75, 'Putibila', 'পুটিবিলা', 0, 'putibilaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (761, 75, 'Chunati', 'চুনতি', 0, 'chunatiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (762, 75, 'Adhunagar', 'আধুনগর', 0, 'adhunagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (763, 76, 'Farhadabad', 'ফরহাদাবাদ', 0, 'farhadabadup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (764, 76, 'Dhalai', 'ধলই', 0, 'dhalaiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (765, 76, 'Mirjapur', 'মির্জাপুর', 0, 'mirjapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (766, 76, 'Nangolmora', 'নাঙ্গলমোরা', 0, 'nangolmoraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (767, 76, 'Gomanmordan', 'গুমানমর্দ্দন', 0, 'gomanmordanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (768, 76, 'Chipatali', 'ছিপাতলী', 0, 'chipataliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (769, 76, 'Mekhal', 'মেখল', 0, 'mekhalup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (770, 76, 'Garduara', 'গড়দুয়ারা', 0, 'garduaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (771, 76, 'Fathepur', 'ফতেপুর', 0, 'fathepurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (772, 76, 'Chikondandi', 'চিকনদন্ডী', 0, 'chikondandiup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (773, 76, 'Uttar Madrasha', 'উত্তর মাদার্শা', 0, 'uttarmadrashaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (774, 76, 'Dakkin Madrasha', 'দক্ষিন মাদার্শা', 0, 'dakkinmadrashaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (775, 76, 'Sikarpur', 'শিকারপুর', 0, 'sikarpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (776, 76, 'Budirchar', 'বুডিরশ্চর', 0, 'budircharup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (777, 76, 'Hathazari', 'হাটহাজারী', 0, 'hathazariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (778, 77, 'Dharmapur', 'ধর্মপুর', 0, 'dharmapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (779, 77, 'Baganbazar', 'বাগান বাজার', 0, 'baganbazarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (780, 77, 'Dantmara', 'দাঁতমারা', 0, 'dantmaraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (781, 77, 'Narayanhat', 'নারায়নহাট', 0, 'narayanhatup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (782, 77, 'Bhujpur', 'ভূজপুর', 0, 'bhujpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (783, 77, 'Harualchari', 'হারুয়ালছড়ি', 0, 'harualchariup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (784, 77, 'Paindong', 'পাইনদং', 0, 'paindongup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (785, 77, 'Kanchannagor', 'কাঞ্চনগর', 0, 'kanchannagorup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (786, 77, 'Sunderpur', 'সুনদরপুর', 0, 'sunderpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (787, 77, 'Suabil', 'সুয়াবিল', 0, 'Suabilup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (788, 77, 'Abdullapur', 'আবদুল্লাপুর', 0, 'abdullapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (789, 77, 'Samitirhat', 'সমিতির হাট', 0, 'samitirhatup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (790, 77, 'Jafathagar', 'জাফতনগর', 0, 'jafathagarup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (791, 77, 'Bokhtapur', 'বক্তপুর', 0, 'bokhtapurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (792, 77, 'Roshangiri', 'রোসাংগিরী', 0, 'roshangiriup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (793, 77, 'Nanupur', 'নানুপুর', 0, 'nanupurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (794, 77, 'Lelang', 'লেলাং', 0, 'lelangup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (795, 77, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (796, 78, 'Raozan', 'রাউজান', 0, 'raozanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (797, 78, 'Bagoan', 'বাগোয়ান', 0, 'bagoanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (798, 78, 'Binajuri', 'বিনাজুরী', 0, 'binajuriup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (799, 78, 'Chikdair', 'চিকদাইর', 0, 'chikdairup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (800, 78, 'Dabua', 'ডাবুয়া', 0, 'dabuaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (801, 78, 'Purbagujra', 'পূর্ব গুজরা', 0, 'purbagujraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (802, 78, 'Paschim Gujra', 'পশ্চিম গুজরা', 0, 'paschimgujraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (803, 78, 'Gohira', 'গহিরা', 0, 'gohiraup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (804, 78, 'Holdia', 'হলদিয়া', 0, 'holdiaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (805, 78, 'Kodolpur', 'কদলপূর', 0, 'kodolpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (806, 78, 'Noapara', 'নোয়াপাড়া', 0, 'noaparaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (807, 78, 'Pahartali', 'পাহাড়তলী', 0, 'pahartaliup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (808, 78, 'Urkirchar', 'উড়কিরচর', 0, 'urkircharup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (809, 78, 'Nowajushpur', 'নওয়াজিশপুর', 0, 'nowajushpurup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (810, 79, 'Char Patharghata', 'চর পাথরঘাটা', 0, 'charpatharghataup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (811, 79, 'Char Lakshya', 'চর লক্ষ্যা', 0, 'charlakshyaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (812, 79, 'Juldha', 'জুলধা', 0, 'juldhaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (813, 79, 'Barauthan', 'বড় উঠান', 0, 'barauthanup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (814, 79, 'Sikalbaha', 'শিকলবাহা', 0, 'sikalbahaup.chittagong.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (815, 80, 'Islamabad', 'ইসলামাবাদ', 0, 'islamabadup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (816, 80, 'Islampur', 'ইসলামপুর', 0, 'islampurup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (817, 80, 'Pokkhali', 'পোকখালী', 0, 'pokkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (818, 80, 'Eidgaon', 'ঈদগাঁও', 0, 'eidgaonup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (819, 80, 'Jalalabad', 'জালালাবাদ', 0, 'jalalabadup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (820, 80, 'Chowfaldandi', 'চৌফলদন্ডী', 0, 'chowfaldandi.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (821, 80, 'Varuakhali', 'ভারুয়াখালী', 0, 'varuakhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (822, 80, 'Pmkhali', 'পিএমখালী', 0, 'pmkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (823, 80, 'Khurushkhul', 'খুরুশকুল', 0, 'khurushkhulup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (824, 80, 'Jhilongjha', 'ঝিলংঝা', 0, 'jhilongjhaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (825, 81, 'Kakhara', 'কাকারা', 0, 'Kakharaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (826, 81, 'Kaiar Bil', 'কাইয়ার বিল', 0, 'kaiarbilup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (827, 81, 'Konakhali', 'কোনাখালী', 0, 'konakhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (828, 81, 'Khuntakhali', 'খুটাখালী', 0, 'khuntakhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (829, 81, 'Chiringa', 'চিরিঙ্গা', 0, 'chiringaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (830, 81, 'Demusia', 'ঢেমুশিয়া', 0, 'demusiaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (831, 81, 'Dulahazara', 'ডুলাহাজারা', 0, 'dulahazaraup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (832, 81, 'Paschim Bara Bheola', 'পশ্চিম বড় ভেওলা', 0, 'paschimbarabheolaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (833, 81, 'Badarkhali', 'বদরখালী', 0, 'badarkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (834, 81, 'Bamobil Chari', 'বামু বিলছড়ি', 0, 'bamobilchariup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (835, 81, 'Baraitali', 'বড়ইতলী', 0, 'baraitaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (836, 81, 'Bheola Manik Char', 'ভেওলা মানিক চর', 0, 'bheolamanikcharup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (837, 81, 'Saharbil', 'শাহারবিল', 0, 'saharbilup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (838, 81, 'Surajpur Manikpur', 'সুরজপুর মানিকপুর', 0, 'surajpurmanikpurup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (839, 81, 'Harbang', 'হারবাঙ্গ', 0, 'harbangup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (840, 81, 'Fashiakhali', 'ফাঁসিয়াখালী', 0, 'fashiakhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (841, 82, 'Ali Akbar Deil', 'আলি আকবর ডেইল', 0, 'aliakbardeilup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (842, 82, 'Uttar Dhurung', 'উত্তর ধুরুং', 0, 'uttardhurungup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (843, 82, 'Kaiyarbil', 'কৈয়ারবিল', 0, 'kaiyarbilup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (844, 82, 'Dakshi Dhurung', 'দক্ষিণ ধুরুং', 0, 'dakshidhurungup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (845, 82, 'Baragho', 'বড়ঘোপ', 0, 'baraghopup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (846, 82, 'Lemsikhali', 'লেমসিখালী', 0, 'lemsikhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (847, 83, 'Rajapalong', 'রাজাপালং', 0, 'rajapalongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (848, 83, 'Jaliapalong', 'জালিয়াপালং', 0, 'jaliapalongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (849, 83, 'Holdiapalong', 'হলদিয়াপালং', 0, 'holdiapalongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (850, 83, 'Ratnapalong', 'রত্নাপালং', 0, 'ratnapalongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (851, 83, 'Palongkhali', 'পালংখালী', 0, 'palongkhali.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (852, 84, 'Boro Moheshkhali', 'বড় মহেশখালী', 0, 'boramoheshkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (853, 84, 'Choto Moheshkhali', 'ছোট মহেশখালী', 0, 'chotamoheshkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (854, 84, 'Shaplapur', 'শাপলাপুর', 0, 'shaplapurup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (855, 84, 'Kutubjum', 'কুতুবজোম', 0, 'kutubjumup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (856, 84, 'Hoanak', 'হোয়ানক', 0, 'hoanakup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (857, 84, 'Kalarmarchhara', 'কালারমারছড়া', 0, 'kalarmarchharaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (858, 84, 'Matarbari', 'মাতারবাড়ী', 0, 'matarbariup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (859, 84, 'Dhalghata', 'ধলঘাটা', 0, 'dhalghataup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (860, 85, 'Ujantia', 'উজানটিয়া', 0, 'ujantiaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (861, 85, 'Taitong', 'টাইটং', 0, 'taitongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (862, 85, 'Pekua', 'পেকুয়া', 0, 'pekuaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (863, 85, 'Barabakia', 'বড় বাকিয়া', 0, 'barabakiaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (864, 85, 'Magnama', 'মগনামা', 0, 'magnamaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (865, 85, 'Rajakhali', 'রাজাখালী', 0, 'rajakhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (866, 85, 'Shilkhali', 'শীলখালী', 0, 'shilkhaliup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (867, 86, 'Fotekharkul', 'ফতেখাঁরকুল', 0, 'fotekharkulup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (868, 86, 'Rajarkul', 'রাজারকুল', 0, 'rajarkulup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (869, 86, 'Rashidnagar', 'রশীদনগর', 0, 'rashidnagarup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (870, 86, 'Khuniapalong', 'খুনিয়াপালং', 0, 'khuniapalongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (871, 86, 'Eidghar', 'ঈদগড়', 0, 'eidgharup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (872, 86, 'Chakmarkul', 'চাকমারকুল', 0, 'chakmarkulup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (873, 86, 'Kacchapia', 'কচ্ছপিয়া', 0, 'kacchapiaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (874, 86, 'Kauwarkho', 'কাউয়ারখোপ', 0, 'kauwarkhopup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (875, 86, 'Dakkhin Mithachhari', 'দক্ষিণ মিঠাছড়ি', 0, 'dakkhinmithachhariup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (876, 86, 'Jouarianala', 'জোয়ারিয়া নালা', 0, 'jouarianalaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (877, 86, 'Garjoniya', 'গর্জনিয়া', 0, 'garjoniyaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (878, 87, 'Subrang', 'সাবরাং', 0, 'subrangup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (879, 87, 'Baharchara', 'বাহারছড়া', 0, 'baharcharaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (880, 87, 'Hnila', 'হ্নীলা', 0, 'hnilaup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (881, 87, 'Whykong', 'হোয়াইক্যং', 0, 'whykongup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (882, 87, 'Saintmartin', 'সেন্ট মার্টিন', 0, 'saintmartinup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (883, 87, 'Teknaf Sadar', 'টেকনাফ সদর', 0, 'teknafsadarup.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (884, 88, 'Khagrachhari Sadar', 'খাগরাছড়ি সদর', 0, 'sadarup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (885, 88, 'Golabari', 'গোলাবাড়ী', 0, 'golabariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (886, 88, 'Parachara', 'পেরাছড়া', 0, 'paracharaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (887, 88, 'Kamalchari', 'কমলছড়ি', 0, 'kamalchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (888, 89, 'Merung', 'মেরুং', 0, 'merungup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (889, 89, 'Boalkhali', 'বোয়ালখালী', 0, 'boalkhaliup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (890, 89, 'Kabakhali', 'কবাখালী', 0, 'kabakhaliup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (891, 89, 'Dighinala', 'দিঘীনালা', 0, 'dighinalaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (892, 89, 'Babuchara', 'বাবুছড়া', 0, 'babucharaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (893, 90, 'Logang', 'লোগাং', 0, 'logangup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (894, 90, 'Changi', 'চেংগী', 0, 'changiup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (895, 90, 'Panchari', 'পানছড়ি', 0, 'panchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (896, 90, 'Latiban', 'লতিবান', 0, 'latibanup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (897, 91, 'Dullyatali', 'দুল্যাতলী', 0, 'dullyataliup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (898, 91, 'Barmachari', 'বর্মাছড়ি', 0, 'barmachariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (899, 91, 'Laxmichhari', 'লক্ষীছড়ি', 0, 'laxmichhariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (900, 92, 'Bhaibonchara', 'ভাইবোনছড়া', 0, 'bhaiboncharaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (901, 92, 'Mahalchari', 'মহালছড়ি', 0, 'mahalchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (902, 92, 'Mobachari', 'মুবাছড়ি', 0, 'mobachariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (903, 92, 'Kayanghat', 'ক্যায়াংঘাট', 0, 'kayanghatup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (904, 92, 'Maischari', 'মাইসছড়ি', 0, 'maischariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (905, 93, 'Manikchari', 'মানিকছড়ি', 0, 'manikchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (906, 93, 'Batnatali', 'বাটনাতলী', 0, 'batnataliup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (907, 93, 'Jogyachola', 'যোগ্যছোলা', 0, 'jogyacholaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (908, 93, 'Tintahari', 'তিনটহরী', 0, 'tintahariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (909, 94, 'Ramgarh', 'রামগড়', 0, 'ramgarhup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (910, 94, 'Patachara', 'পাতাছড়া', 0, 'patacharaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (911, 94, 'Hafchari', 'হাফছড়ি', 0, 'hafchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (912, 95, 'Taindong', 'তাইন্দং', 0, 'taindongup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (913, 95, 'Tabalchari', 'তবলছড়ি', 0, 'tabalchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (914, 95, 'Barnal', 'বর্ণাল', 0, 'barnalup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (915, 95, 'Gomti', 'গোমতি', 0, 'gomtiup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (916, 95, 'Balchari', 'বেলছড়ি', 0, 'balchariup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (917, 95, 'Matiranga', 'মাটিরাঙ্গা', 0, 'matirangaup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (918, 95, 'Guimara', 'গুইমারা', 0, 'guimaraup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (919, 95, 'Amtali', 'আমতলি', 0, 'amtaliup.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (920, 97, 'Rajbila', 'রাজবিলা', 0, 'rajbilaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (921, 97, 'Tongkaboty', 'টংকাবতী', 0, 'tongkabotyup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (922, 97, 'Suwalok', 'সুয়ালক', 0, 'suwalokup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (923, 97, 'Bandarban Sadar', 'বান্দরবান সদর', 0, 'bandarbansadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (924, 97, 'Kuhalong', 'কুহালং', 0, 'kuhalongup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (925, 98, 'Alikadam Sadar', 'আলীকদম সদর', 0, 'alikadamsadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (926, 98, 'Chwekhyong', 'চৈক্ষ্যং', 0, 'chwekhyongup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (927, 99, 'Naikhyongchari Sadar', 'নাইক্ষ্যংছড়ি সদর', 0, 'naikhyongcharisadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (928, 99, 'Gumdhum', 'ঘুমধুম', 0, 'gumdhumup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (929, 99, 'Baishari', 'বাইশারী', 0, 'baishariup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (930, 99, 'Sonaychari', 'সোনাইছড়ি', 0, 'sonaychariup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (931, 99, 'Duwchari', 'দোছড়ি', 0, 'duwchariup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (932, 100, 'Rowangchari Sadar', 'রোয়াংছড়ি সদর', 0, 'rowangcharisadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (933, 100, 'Taracha', 'তারাছা', 0, 'tarachaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (934, 100, 'Alekyong', 'আলেক্ষ্যং', 0, 'alekyongup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (935, 100, 'Nawapotong', 'নোয়াপতং', 0, 'nawapotongup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (936, 101, 'Gajalia', 'গজালিয়া', 0, 'gajaliaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (937, 101, 'Lama Sadar', 'লামা সদর', 0, 'lamasadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (938, 101, 'Fasiakhali', 'ফাসিয়াখালী', 0, 'fasiakhaliup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (939, 101, 'Fythong', 'ফাইতং', 0, 'fythongup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (940, 101, 'Rupushipara', 'রূপসীপাড়া', 0, 'rupushiparaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (941, 101, 'Sarai', 'সরই', 0, 'saraiup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (942, 101, 'Aziznagar', 'আজিজনগর', 0, 'aziznagarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (943, 102, 'Paind', 'পাইন্দু', 0, 'painduup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (944, 102, 'Ruma Sadar', 'রুমা সদর', 0, 'rumasadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (945, 102, 'Ramakreprangsa', 'রেমাক্রীপ্রাংসা', 0, 'ramakreprangsaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (946, 102, 'Galanggya', 'গ্যালেংগ্যা', 0, 'galanggyaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (947, 103, 'Remakre', 'রেমাক্রী', 0, 'remakreup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (948, 103, 'Tind', 'তিন্দু', 0, 'tinduup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (949, 103, 'Thanchi Sadar', 'থানচি সদর', 0, 'thanchisadarup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (950, 103, 'Balipara', 'বলিপাড়া', 0, 'baliparaup.bandarban.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (951, 104, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (952, 104, 'Baradhul', 'বড়ধুল', 0, 'baradhulup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (953, 104, 'Belkuchi Sadar', 'বেলকুচি সদর', 0, 'belkuchisadarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (954, 104, 'Dhukuriabera', 'ধুকুরিয়া বেড়া', 0, 'dhukuriaberaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (955, 104, 'Doulatpur', 'দৌলতপুর', 0, 'doulatpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (956, 104, 'Bhangabari', 'ভাঙ্গাবাড়ী', 0, 'bhangabariup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (957, 105, 'Baghutia', 'বাঘুটিয়া', 0, 'baghutiaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (958, 105, 'Gharjan', 'ঘোরজান', 0, 'gharjanup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (959, 105, 'Khaskaulia', 'খাসকাউলিয়া', 0, 'khaskauliaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (960, 105, 'Khaspukuria', 'খাসপুকুরিয়া', 0, 'khaspukuriaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (961, 105, 'Omarpur', 'উমারপুর', 0, 'omarpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (962, 105, 'Sadia Chandpur', 'সদিয়া চাঁদপুর', 0, 'sadiachandpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (963, 105, 'Sthal', 'স্থল', 0, 'sthalup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (964, 106, 'Bhadraghat', 'ভদ্রঘাট', 0, 'bhadraghatup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (965, 106, 'Jamtail', 'জামতৈল', 0, 'jamtailup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (966, 106, 'Jhawail', 'ঝাঐল', 0, 'jhawailup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (967, 106, 'Roydaulatpur', 'রায়দৌলতপুর', 0, 'roydaulatpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (968, 107, 'Chalitadangha', 'চালিতাডাঙ্গা', 0, 'chalitadanghaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (969, 107, 'Chargirish', 'চরগিরিশ', 0, 'chargirishup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (970, 107, 'Gandail', 'গান্ধাইল', 0, 'gandailup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (971, 107, 'Kazipur Sadar', 'কাজিপুর সদর', 0, 'kazipursadarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (972, 107, 'Khasrajbari', 'খাসরাজবাড়ী', 0, 'khasrajbariup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (973, 107, 'Maijbari', 'মাইজবাড়ী', 0, 'maijbariup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (974, 107, 'Monsur Nagar', 'মনসুর নগর', 0, 'monsurnagarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (975, 107, 'Natuarpara', 'নাটুয়ারপাড়া', 0, 'natuarparaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (976, 107, 'Nishchintapur', 'নিশ্চিন্তপুর', 0, 'nishchintapurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (977, 107, 'Sonamukhi', 'সোনামুখী', 0, 'sonamukhiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (978, 107, 'Subhagacha', 'শুভগাছা', 0, 'subhagachaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (979, 107, 'Tekani', 'তেকানী', 0, 'tekaniup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (980, 108, 'Brommogacha', 'ব্রহ্মগাছা', 0, 'brommogachaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (981, 108, 'Chandaikona', 'চান্দাইকোনা', 0, 'chandaikonaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (982, 108, 'Dhamainagar', 'ধামাইনগর', 0, 'dhamainagarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (983, 108, 'Dhangora', 'ধানগড়া', 0, 'dhangoraup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (984, 108, 'Dhubil', 'ধুবিল', 0, 'dhubilup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (985, 108, 'Ghurka', 'ঘুড়কা', 0, 'ghurkaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (986, 108, 'Nalka', 'নলকা', 0, 'nalkaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (987, 108, 'Pangashi', 'পাঙ্গাসী', 0, 'pangashiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (988, 108, 'Sonakhara', 'সোনাখাড়া', 0, 'sonakharaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (989, 109, 'Beltail', 'বেলতৈল', 0, 'beltailup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (990, 109, 'Jalalpur', 'জালালপুর', 0, 'jalalpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (991, 109, 'Kayempure', 'কায়েমপুর', 0, 'kayempureup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (992, 109, 'Garadah', 'গাড়াদহ', 0, 'garadahup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (993, 109, 'Potazia', 'পোতাজিয়া', 0, 'potaziaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (994, 109, 'Rupbati', 'রূপবাটি', 0, 'rupbatiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (995, 109, 'Gala', 'গালা', 0, 'galaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (996, 109, 'Porzona', 'পোরজনা', 0, 'porzonaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (997, 109, 'Habibullah Nagar', 'হাবিবুল্লাহ নগর', 0, 'habibullahnagarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (998, 109, 'Khukni', 'খুকনী', 0, 'khukniup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (999, 109, 'Koizuri', 'কৈজুরী', 0, 'koizuriup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1000, 109, 'Sonatoni', 'সোনাতনী', 0, 'sonatoniup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1001, 109, 'Narina', 'নরিনা', 0, 'narinaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1002, 110, 'Bagbati', 'বাগবাটি', 0, 'bagbatiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1003, 110, 'Ratankandi', 'রতনকান্দি', 0, 'ratankandiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1004, 110, 'Bohuli', 'বহুলী', 0, 'bohuliup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1005, 110, 'Sheyalkol', 'শিয়ালকোল', 0, 'sheyalkolup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1006, 110, 'Khokshabari', 'খোকশাবাড়ী', 0, 'khokshabariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1007, 110, 'Songacha', 'ছোনগাছা', 0, 'songachaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1008, 110, 'Mesra', 'মেছড়া', 0, 'mesraup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1009, 110, 'Kowakhola', 'কাওয়াখোলা', 0, 'kowakholaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1010, 110, 'Kaliahoripur', 'কালিয়াহরিপুর', 0, 'kaliahoripurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1011, 110, 'Soydabad', 'সয়দাবাদ', 0, 'soydabadup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1012, 111, 'Baruhas', 'বারুহাস', 0, 'baruhasup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1013, 111, 'Talam', 'তালম', 0, 'talamup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1014, 111, 'Soguna', 'সগুনা', 0, 'sogunaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1015, 111, 'Magura Binod', 'মাগুড়া বিনোদ', 0, 'magurabinodup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1016, 111, 'Naogaon', 'নওগাঁ', 0, 'naogaonup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1017, 111, 'Tarash Sadar', 'তাড়াশ সদর', 0, 'tarashsadarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1018, 111, 'Madhainagar', 'মাধাইনগর', 0, 'madhainagarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1019, 111, 'Deshigram', 'দেশীগ্রাম', 0, 'deshigramup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1020, 112, 'Ullapara Sadar', 'উল্লাপাড়া সদর', 0, 'ullaparasadarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1021, 112, 'Ramkrisnopur', 'রামকৃষ্ণপুর', 0, 'ramkrisnopurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1022, 112, 'Bangala', 'বাঙ্গালা', 0, 'bangalaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1023, 112, 'Udhunia', 'উধুনিয়া', 0, 'udhuniaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1024, 112, 'Boropangashi', 'বড়পাঙ্গাসী', 0, 'boropangashiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1025, 112, 'Durga Nagar', 'দুর্গা নগর', 0, 'durganagarup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1026, 112, 'Purnimagati', 'পূর্ণিমাগাতী', 0, 'purnimagatiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1027, 112, 'Salanga', 'সলঙ্গা', 0, 'salangaup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1028, 112, 'Hatikumrul', 'হটিকুমরুল', 0, 'hatikumrulup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1029, 112, 'Borohor', 'বড়হর', 0, 'borohorup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1030, 112, 'Ponchocroshi', 'পঞ্চক্রোশী', 0, 'ponchocroshiup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1031, 112, 'Salo', 'সলপ', 0, 'salopup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1032, 112, 'Mohonpur', 'মোহনপুর', 0, 'mohonpurup.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1033, 113, 'Vaina', 'ভায়না', 0, 'vainaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1034, 113, 'Tantibonda', 'তাঁতিবন্দ', 0, 'tantibondaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1035, 113, 'Manikhat', 'মানিকহাট', 0, 'manikhatup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1036, 113, 'Dulai', 'দুলাই', 0, 'dulaiup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1037, 113, 'Ahammadpur', 'আহম্মদপুর', 0, 'ahammadpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1038, 113, 'Raninagar', 'রাণীনগর', 0, 'raninagarup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1039, 113, 'Satbaria', 'সাতবাড়ীয়া', 0, 'satbariaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1040, 113, 'Hatkhali', 'হাটখালী', 0, 'hatkhaliup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1041, 113, 'Nazirganj', 'নাজিরগঞ্জ', 0, 'nazirganjup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1042, 113, 'Sagorkandi', 'সাগরকান্দি', 0, 'sagorkandiup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1043, 114, 'Sara', 'সাঁড়া', 0, 'saraup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1044, 114, 'Pakshi', 'পাকশী', 0, 'pakshiup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1045, 114, 'Muladuli', 'মুলাডুলি', 0, 'muladuliup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1046, 114, 'Dashuria', 'দাশুরিয়া', 0, 'dashuriaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1047, 114, 'Silimpur', 'ছলিমপুর', 0, 'silimpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1048, 114, 'Sahapur', 'সাহাপুর', 0, 'sahapurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1049, 114, 'Luxmikunda', 'লক্ষীকুন্ডা', 0, 'luxmikundaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1050, 115, 'Bhangura', 'ভাঙ্গুড়া', 0, 'bhanguraup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1051, 115, 'Khanmarich', 'খানমরিচ', 0, 'khanmarichup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1052, 115, 'Ashtamanisha', 'অষ্টমণিষা', 0, 'ashtamanishaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1053, 115, 'Dilpasar', 'দিলপাশার', 0, 'dilpasarup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1054, 115, 'Parbhangura', 'পারভাঙ্গুড়া', 0, 'parbhanguraup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1055, 116, 'Maligachha', 'মালিগাছা', 0, 'maligachhaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1056, 116, 'Malanchi', 'মালঞ্চি', 0, 'malanchiup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1057, 116, 'Gayeshpur', 'গয়েশপুর', 0, 'gayeshpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1058, 116, 'Ataikula', 'আতাইকুলা', 0, 'ataikulaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1059, 116, 'Chartarapur', 'চরতারাপুর', 0, 'chartarapurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1060, 116, 'Sadullahpur', 'সাদুল্লাপুর', 0, 'sadullahpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1061, 116, 'Bharara', 'ভাঁড়ারা', 0, 'bhararaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1062, 116, 'Dogachi', 'দোগাছী', 0, 'dogachiup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1063, 116, 'Hemayetpur', 'হেমায়েতপুর', 0, 'hemayetpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1064, 116, 'Dapunia', 'দাপুনিয়া', 0, 'dapuniaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1065, 117, 'Haturia Nakalia', 'হাটুরিয়া নাকালিয়া', 0, 'haturianakaliaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1066, 117, 'Notun Varenga', 'নতুন ভারেঙ্গা', 0, 'notunvarengaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1067, 117, 'Koitola', 'কৈটোলা', 0, 'koitolaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1068, 117, 'Chakla', 'চাকলা', 0, 'chaklaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1069, 117, 'Jatsakhini', 'জাতসাখিনি', 0, 'jatsakhiniup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1070, 117, 'Puran Varenga', 'পুরান ভারেঙ্গা', 0, 'puranvarengaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1071, 117, 'Ruppur', 'রূপপুর', 0, 'ruppurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1072, 117, 'Masumdia', 'মাসুমদিয়া', 0, 'masumdiaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1073, 117, 'Dhalar Char', 'ঢালার চর', 0, 'dhalarcharup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1074, 118, 'Majhpara', 'মাজপাড়া', 0, 'majhparaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1075, 118, 'Chandba', 'চাঁদভা', 0, 'chandbaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1076, 118, 'Debottar', 'দেবোত্তর', 0, 'debottarup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1077, 118, 'Ekdanta', 'একদন্ত', 0, 'ekdantaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1078, 118, 'Laxshmipur', 'লক্ষীপুর', 0, 'laxshmipurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1079, 119, 'Handial', 'হান্ডিয়াল', 0, 'handialup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1080, 119, 'Chhaikola', 'ছাইকোলা', 0, 'chhaikolaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1081, 119, 'Nimaichara', 'নিমাইচড়া', 0, 'nimaicharaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1082, 119, 'Gunaigachha', 'গুনাইগাছা', 0, 'gunaigachhaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1083, 119, 'Parshadanga', 'পার্শ্বডাঙ্গা', 0, 'parshadangaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1084, 119, 'Failjana', 'ফৈলজানা', 0, 'failjanaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1085, 119, 'Mulgram', 'মুলগ্রাম', 0, 'mulgramup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1086, 119, 'Haripur', 'হরিপুর', 0, 'haripurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1087, 119, 'Mothurapur', 'মথুরাপুর', 0, 'mothurapurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1088, 119, 'Bilchalan', 'বিলচলন', 0, 'bilchalanup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1089, 119, 'Danthia Bamangram', 'দাতিয়া বামনগ্রাম', 0, 'danthiabamangramup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1090, 120, 'Nagdemra', 'নাগডেমড়া', 0, 'nagdemraup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1091, 120, 'Dhulauri', 'ধুলাউড়ি', 0, 'dhulauriup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1092, 120, 'Bhulbaria', 'ভুলবাড়ীয়া', 0, 'bhulbariaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1093, 120, 'Dhopadaha', 'ধোপাদহ', 0, 'dhopadahaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1094, 120, 'Karamja', 'করমজা', 0, 'karamjaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1095, 120, 'Kashinathpur', 'কাশিনাথপুর', 0, 'kashinathpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1096, 120, 'Gaurigram', 'গৌরীগ্রাম', 0, 'gaurigramup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1097, 120, 'Nandanpur', 'নন্দনপুর', 0, 'nandanpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1098, 120, 'Khetupara', 'ক্ষেতুপাড়া', 0, 'khetuparaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1099, 120, 'Ar-Ataikula', 'আর-আতাইকুলা', 0, 'rataiqulaup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1100, 121, 'Brilahiribari', 'বৃলাহিড়ীবাড়ী', 0, 'brilahiribariup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1101, 121, 'Pungali', 'পুঙ্গুলি', 0, 'pungaliup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1102, 121, 'Faridpur', 'ফরিদপুর', 0, 'faridpurup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1103, 121, 'Hadal', 'হাদল', 0, 'hadalup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1104, 121, 'Banwarinagar', 'বনওয়ারীনগর', 0, 'banwarinagarup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1105, 121, 'Demra', 'ডেমড়া', 0, 'demraup.pabna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1106, 122, 'Birkedar', 'বীরকেদার', 0, 'birkedarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1107, 122, 'Kalai', 'কালাই', 0, 'kalaiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1108, 122, 'Paikar', 'পাইকড়', 0, 'paikarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1109, 122, 'Narhatta', 'নারহট্ট', 0, 'narhattaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1110, 122, 'Murail', 'মুরইল', 0, 'murailup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1111, 122, 'Kahaloo', 'কাহালু', 0, 'kahalooup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1112, 122, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1113, 122, 'Jamgaon', 'জামগ্রাম', 0, 'jamgaonup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1114, 122, 'Malancha', 'মালঞ্চা', 0, 'malanchaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1115, 123, 'Fapore', 'ফাঁপোর', 0, 'faporeup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1116, 123, 'Shabgram', 'সাবগ্রাম', 0, 'shabgramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1117, 123, 'Nishindara', 'নিশিন্দারা', 0, 'nishindaraup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1118, 123, 'Erulia', 'এরুলিয়া', 0, 'eruliaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1119, 123, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1120, 123, 'Shakharia', 'শাখারিয়া', 0, 'shakhariaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1121, 123, 'Sekherkola', 'শেখেরকোলা', 0, 'sekherkolaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1122, 123, 'Gokul', 'গোকুল', 0, 'gokulup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1123, 123, 'Noongola', 'নুনগোলা', 0, 'noongolaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1124, 123, 'Lahiripara', 'লাহিড়ীপাড়া', 0, 'lahiriparaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1125, 123, 'Namuja', 'নামুজা', 0, 'namujaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1126, 124, 'Sariakandi Sadar', 'সারিয়াকান্দি সদর', 0, 'sariakandisadarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1127, 124, 'Narchi', 'নারচী', 0, 'narchiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1128, 124, 'Bohail', 'বোহাইল', 0, 'bohailup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1129, 124, 'Chaluabari', 'চালুয়াবাড়ী', 0, 'chaluabariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1130, 124, 'Chandanbaisha', 'চন্দনবাইশা', 0, 'chandanbaishaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1131, 124, 'Hatfulbari', 'হাটফুলবাড়ী', 0, 'hatfulbariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1132, 124, 'Hatsherpur', 'হাটশেরপুর', 0, 'hatsherpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1133, 124, 'Karnibari', 'কর্ণিবাড়ী', 0, 'karnibariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1134, 124, 'Kazla', 'কাজলা', 0, 'kazlaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1135, 124, 'Kutubpur', 'কুতুবপুর', 0, 'kutubpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1136, 124, 'Kamalpur', 'কামালপুর', 0, 'kamalpur.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1137, 124, 'Bhelabari', 'ভেলাবাড়ী', 0, 'bhelabari.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1138, 125, 'Asekpur', 'আশেকপুর', 0, 'asekpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1139, 125, 'Madla', 'মাদলা', 0, 'madlaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1140, 125, 'Majhira', 'মাঝিড়া', 0, 'majhiraup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1141, 125, 'Aria', 'আড়িয়া', 0, 'ariaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1142, 125, 'Kharna', 'খরনা', 0, 'kharnaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1143, 125, 'Khottapara', 'খোট্টাপাড়া', 0, 'Khottaparaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1144, 125, 'Chopinagar', 'চোপিনগর', 0, 'chopinagarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1145, 125, 'Amrul', 'আমরুল', 0, 'amrulup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1146, 125, 'Gohail', 'গোহাইল', 0, 'gohailup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1147, 126, 'Zianagar', 'জিয়ানগর', 0, 'zianagarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1148, 126, 'Chamrul', 'চামরুল', 0, 'chamrulup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1149, 126, 'Dupchanchia', 'দুপচাঁচিয়া', 0, 'dupchanchiaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1150, 126, 'Gunahar', 'গুনাহার', 0, 'gunaharup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1151, 126, 'Gobindapur', 'গোবিন্দপুর', 0, 'gobindapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1152, 126, 'Talora', 'তালোড়া', 0, 'taloraup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1153, 127, 'Chhatiangram', 'ছাতিয়ানগ্রাম', 0, 'chhatiangramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1154, 127, 'Nasaratpur', 'নশরতপুর', 0, 'nasaratpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1155, 127, 'Adamdighi', 'আদমদিঘি', 0, 'adamdighiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1156, 127, 'Kundagram', 'কুন্দগ্রাম', 0, 'kundagramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1157, 127, 'Chapapur', 'চাঁপাপুর', 0, 'chapapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1158, 127, 'Shantahar', 'সান্তাহার', 0, 'shantaharup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1159, 128, 'Burail', 'বুড়ইল', 0, 'burailup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1160, 128, 'Nandigram', 'নন্দিগ্রাম', 0, 'nandigramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1161, 128, 'Bhatra', 'ভাটরা', 0, 'bhatraup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1162, 128, 'Thalta Majhgram', 'থালতা মাঝগ্রাম', 0, 'thaltamajhgramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1163, 128, 'Bhatgram', 'ভাটগ্রাম', 0, 'bhatgramup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1164, 129, 'Sonatala', 'সোনাতলা', 0, 'sonatalaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1165, 129, 'Balua', 'বালুয়া', 0, 'baluaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1166, 129, 'Zorgacha', 'জোড়গাছা', 0, 'zorgachaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1167, 129, 'Digdair', 'দিগদাইড়', 0, 'digdairup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1168, 129, 'Madhupur', 'মধুপুর', 0, 'madhupurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1169, 129, 'Pakulla', 'পাকুল্ল্যা', 0, 'pakullaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1170, 129, 'Tekani Chukinagar', 'তেকানী চুকাইনগর', 0, 'tekanichukinagarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1171, 130, 'Nimgachi', 'নিমগাছি', 0, 'nimgachiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1172, 130, 'Kalerpara', 'কালেরপাড়া', 0, 'kalerparaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1173, 130, 'Chikashi', 'চিকাশী', 0, 'chikashiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1174, 130, 'Gossainbari', 'গোসাইবাড়ী', 0, 'gossainbariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1175, 130, 'Bhandarbari', 'ভান্ডারবাড়ী', 0, 'bhandarbariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1176, 130, 'Gopalnagar', '১গোপালনগর', 0, 'gopalnagarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1177, 130, 'Mothurapur', 'মথুরাপুর', 0, 'mothurapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1178, 130, 'Chowkibari', 'চৌকিবাড়ী', 0, 'chowkibariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1179, 130, 'Elangi', 'এলাঙ্গী', 0, 'elangiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1180, 130, 'Dhunat Sadar', 'ধুনট সদর', 0, 'dhunatsadarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1181, 131, 'Baliadighi', 'বালিয়া দিঘী', 0, 'baliadighiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1182, 131, 'Dakshinpara', 'দক্ষিণপাড়া', 0, 'dakshinparaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1183, 131, 'Durgahata', 'দুর্গাহাটা', 0, 'durgahataup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1184, 131, 'Kagail', 'কাগইল', 0, 'kagailup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1185, 131, 'Sonarai', 'সোনারায়', 0, 'sonaraiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1186, 131, 'Rameshwarpur', 'রামেশ্বরপুর', 0, 'rameshwarpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1187, 131, 'Naruamala', 'নাড়ুয়ামালা', 0, 'naruamalaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1188, 131, 'Nepaltali', 'নেপালতলী', 0, 'nepaltaliup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1189, 131, 'Gabtali', 'গাবতলি', 0, 'gabtaliup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1190, 131, 'Mahishaban', 'মহিষাবান', 0, 'mahishabanup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1191, 131, 'Nasipur', 'নশিপুর', 0, 'nasipurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1192, 132, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1193, 132, 'Khamarkandi', 'খামারকান্দি', 0, 'khamarkandiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1194, 132, 'Garidaha', 'গাড়িদহ', 0, 'garidahaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1195, 132, 'Kusumbi', 'কুসুম্বী', 0, 'kusumbiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1196, 132, 'Bishalpur', 'বিশালপুর', 0, 'bishalpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1197, 132, 'Shimabari', 'সীমাবাড়ি', 0, 'shimabariup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1198, 132, 'Shahbondegi', 'শাহবন্দেগী', 0, 'shahbondegiup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1199, 132, 'Sughat', 'সুঘাট', 0, 'sughatup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1200, 132, 'Khanpur', 'খানপুর', 0, 'khanpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1201, 132, 'Bhabanipur', 'ভবানীপুর', 0, 'bhabanipurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1202, 133, 'Moidanhatta', 'ময়দানহাট্টা', 0, 'moidanhattaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1203, 133, 'Kichok', 'কিচক', 0, 'kichokup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1204, 133, 'Atmul', 'আটমূল', 0, 'atmulup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1205, 133, 'Pirob', 'পিরব', 0, 'pirobup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1206, 133, 'Majhihatta', 'মাঝিহট্ট', 0, 'majhihattaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1207, 133, 'Buriganj', 'বুড়িগঞ্জ', 0, 'buriganjup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1208, 133, 'Bihar', 'বিহার', 0, 'biharup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1209, 133, 'Shibganj', 'শিবগঞ্জ', 0, 'shibganjup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1210, 133, 'Deuly', 'দেউলি', 0, 'deulyup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1211, 133, 'Sayedpur', 'সৈয়দপুর', 0, 'sayedpurup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1212, 133, 'Mokamtala', 'মোকামতলা', 0, 'mokamtalaup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1213, 133, 'Raynagar', 'রায়নগর', 0, 'raynagarup.bogra.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1214, 134, 'Darsanpara', 'দর্শনপাড়া', 0, 'darsanparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1215, 134, 'Hujuripara', 'হুজুরী পাড়া', 0, 'hujuriparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1216, 134, 'Damkura', 'দামকুড়া', 0, 'damkuraup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1217, 134, 'Horipur', 'হরিপুর', 0, 'horipurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1218, 134, 'Horogram', 'হড়গ্রাম', 0, 'horogramup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1219, 134, 'Harian', 'হরিয়ান', 0, 'harianup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1220, 134, 'Borgachi', 'বড়্গাছি', 0, 'borgachiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1221, 134, 'Parila', 'পারিলা', 0, 'parilaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1222, 135, 'Naopara', 'নওপাড়া', 0, 'naoparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1223, 135, 'Kismatgankoir', 'কিসমতগণকৈড়', 0, 'kismatgankoirup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1224, 135, 'Pananagar', 'পানানগর', 0, 'pananagarup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1225, 135, 'Deluabari', 'দেলুয়াবাড়ী', 0, 'deluabariup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1226, 135, 'Jhaluka', 'ঝালুকা', 0, 'jhalukaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1227, 135, 'Maria', 'মাড়িয়া', 0, 'mariaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1228, 135, 'Joynogor', 'জয়নগর', 0, 'joynogorup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1229, 136, 'Dhuroil', 'ধুরইল', 0, 'dhuroilup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1230, 136, 'Ghasigram', 'ঘষিগ্রাম', 0, 'ghasigramup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1231, 136, 'Raighati', 'রায়ঘাটি', 0, 'raighatiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1232, 136, 'Mougachi', 'মৌগাছি', 0, 'mougachiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1233, 136, 'Baksimoil', 'বাকশিমইল', 0, 'baksimoilup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1234, 136, 'Jahanabad', 'জাহানাবাদ', 0, 'jahanabadup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1235, 137, 'Yousufpur', 'ইউসুফপুর', 0, 'yousufpurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1236, 137, 'Solua', 'শলুয়া', 0, 'soluaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1237, 137, 'Sardah', 'সরদহ', 0, 'sardahup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1238, 137, 'Nimpara', 'নিমপাড়া', 0, 'nimparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1239, 137, 'Charghat', 'চারঘাট', 0, 'charghatup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1240, 137, 'Vialuxmipur', 'ভায়ালক্ষ্মীপুর', 0, 'vialuxmipurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1241, 138, 'Puthia', 'পুঠিয়া', 0, 'puthiaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1242, 138, 'Belpukuria', 'বেলপুকুরিয়া', 0, 'belpukuriaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1243, 138, 'Baneswar', 'বানেশ্বর', 0, 'baneswarup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1244, 138, 'Valukgachi', 'ভালুক গাছি', 0, 'valukgachiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1245, 138, 'Shilmaria', 'শিলমাড়িয়া', 0, 'shilmariaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1246, 138, 'Jewpara', 'জিউপাড়া', 0, 'jewparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1247, 139, 'Bajubagha', 'বাজুবাঘা', 0, 'bajubaghaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1248, 139, 'Gorgori', 'গড়গড়ি', 0, 'gorgoriup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1249, 139, 'Pakuria', 'পাকুড়িয়া', 0, 'pakuriaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1250, 139, 'Monigram', 'মনিগ্রাম', 0, 'monigramup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1251, 139, 'Bausa', 'বাউসা', 0, 'bausaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1252, 139, 'Arani', 'আড়ানী', 0, 'araniup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1253, 140, 'Godagari', 'গোদাগাড়ী', 0, 'godagariup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1254, 140, 'Mohonpur', 'মোহনপুর', 0, 'mohonpurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1255, 140, 'Pakri', 'পাকড়ী', 0, 'pakriup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1256, 140, 'Risikul', 'রিশিকুল', 0, 'risikulup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1257, 140, 'Gogram', 'গোগ্রাম', 0, 'gogramup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1258, 140, 'Matikata', 'মাটিকাটা', 0, 'matikataup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1259, 140, 'Dewpara', 'দেওপাড়া', 0, 'dewparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1260, 140, 'Basudebpur', 'বাসুদেবপুর', 0, 'basudebpurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1261, 140, 'Asariadaha', 'আষাড়িয়াদহ', 0, 'asariadahaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1262, 141, 'Kalma', 'কলমা', 0, 'kalmaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1263, 141, 'Badhair', 'বাধাইড়', 0, 'badhairup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1264, 141, 'Panchandar', 'পাঁচন্দর', 0, 'panchandarup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1265, 141, 'Saranjai', 'সরঞ্জাই', 0, 'saranjaiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1266, 141, 'Talondo', 'তালন্দ', 0, 'talondoup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1267, 141, 'Kamargaon', 'কামারগাঁ', 0, 'kamargaonup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1268, 141, 'Chanduria', 'চান্দুড়িয়া', 0, 'chanduriaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1269, 142, 'Gobindopara', 'গোবিন্দপাড়া', 0, 'gobindoparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1270, 142, 'Nordas', 'নরদাস', 0, 'nordasup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1271, 142, 'Dippur', 'দ্বীপপুর', 0, 'dippurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1272, 142, 'Borobihanoli', 'বড়বিহানলী', 0, 'borobihanoliup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1273, 142, 'Auchpara', 'আউচপাড়া', 0, 'auchparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1274, 142, 'Sreepur', 'শ্রীপুর', 0, 'sreepurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1275, 142, 'Basupara', 'বাসুপাড়া', 0, 'basuparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1276, 142, 'Kacharikoalipara', 'কাচাড়ী কোয়লিপাড়া', 0, 'kacharikoaliparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1277, 142, 'Suvodanga', 'শুভডাঙ্গা', 0, 'suvodangaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1278, 142, 'Mariaup', 'মাড়িয়া', 0, 'mariaup10.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1279, 142, 'Ganipur', 'গণিপুর', 0, 'ganipurup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1280, 142, 'Zhikara', 'ঝিকড়া', 0, 'zhikaraup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1281, 142, 'Gualkandi', 'গোয়ালকান্দি', 0, 'gualkandiup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1282, 142, 'Hamirkutsa', 'হামিরকুৎসা', 0, 'hamirkutsaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1283, 142, 'Jogipara', 'যোগিপাড়া', 0, 'jogiparaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1284, 142, 'Sonadanga', 'সোনাডাঙ্গা', 0, 'sonadangaup.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1285, 143, 'Brahmapur', 'ব্রহ্মপুর', 0, 'brahmapurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1286, 143, 'Madhnagar', 'মাধনগর', 0, 'madhnagar.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1287, 143, 'Khajura', 'খাজুরা', 0, 'khajura.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1288, 143, 'Piprul', 'পিপরুল', 0, 'piprulup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1289, 143, 'Biprobelghoria', 'বিপ্রবেলঘড়িয়া', 0, 'biprobelghoria.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1290, 143, 'Chhatni', 'ছাতনী', 0, 'chhatni.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1291, 143, 'Tebaria', 'তেবাড়িয়া', 0, 'tebariaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1292, 143, 'Dighapatia', 'দিঘাপতিয়া', 0, 'dighapatiaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1293, 143, 'Luxmipurkholabaria', 'লক্ষীপুর খোলাবাড়িয়া', 0, 'luxmipurkholabariaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1294, 143, 'Barahorispur', 'বড়হরিশপুর', 0, 'barahorispur.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1295, 143, 'Kaphuria', 'কাফুরিয়া', 0, 'kaphuria.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1296, 143, 'Halsa', 'হালসা', 0, 'halsa.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1297, 144, 'Sukash', 'শুকাশ', 0, 'sukashup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1298, 144, 'Dahia', 'ডাহিয়া', 0, 'dahiaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1299, 144, 'Italy', 'ইটালী', 0, 'italyup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1300, 144, 'Kalam', 'কলম', 0, 'kalamup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1301, 144, 'Chamari', 'চামারী', 0, 'chamariup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1302, 144, 'Hatiandaha', 'হাতিয়ানদহ', 0, 'hatiandahaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1303, 144, 'Lalore', 'লালোর', 0, 'laloreup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1304, 144, 'Sherkole', 'শেরকোল', 0, 'sherkoleup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1305, 144, 'Tajpur', 'তাজপুর', 0, 'tajpurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1306, 144, 'Chaugram', 'চৌগ্রাম', 0, 'chaugramup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1307, 144, 'Chhatardighi', 'ছাতারদিঘী', 0, 'chhatardighiup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1308, 144, 'Ramanandakhajura', 'রামান্দখাজুরা', 0, 'ramanandakhajuraup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1309, 145, 'Joari', 'জোয়াড়ী', 0, 'joariup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1310, 145, 'Baraigram', 'বড়াইগ্রাম', 0, 'baraigramup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1311, 145, 'Zonail', 'জোনাইল', 0, 'zonailup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1312, 145, 'Nagor', 'নগর', 0, 'nagorup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1313, 145, 'Majgoan', 'মাঝগাও', 0, 'majgoanup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1314, 145, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1315, 145, 'Chandai', 'চান্দাই', 0, 'chandai.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1316, 146, 'Panka', 'পাঁকা', 0, 'pankaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1317, 146, 'Jamnagor', 'জামনগর', 0, 'jamnagorup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1318, 146, 'Bagatipara', 'বাগাতিপাড়া', 0, 'bagatiparaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1319, 146, 'Dayarampur', 'দয়ারামপুর', 0, 'dayarampurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1320, 146, 'Faguardiar', 'ফাগুয়ারদিয়াড়', 0, 'faguardiarup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1321, 147, 'Lalpur', 'লালপুর', 0, 'lalpurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1322, 147, 'Iswardi', 'ঈশ্বরদী', 0, 'iswardiup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1323, 147, 'Chongdhupoil', 'চংধুপইল', 0, 'chongdhupoilup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1324, 147, 'Arbab', 'আড়বাব', 0, 'arbabup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1325, 147, 'Bilmaria', 'বিলমাড়িয়া', 0, 'bilmariaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1326, 147, 'Duaria', 'দুয়ারিয়া', 0, 'duariaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1327, 147, 'Oalia', 'ওয়ালিয়া', 0, 'oaliaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1328, 147, 'Durduria', 'দুড়দুরিয়া', 0, 'durduriaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1329, 147, 'Arjunpur', 'অর্জুনপুর বরমহাটী', 0, 'arjunpurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1330, 147, 'Kadimchilan', 'কদিমচিলান', 0, 'kadimchilanup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1331, 148, 'Nazirpur', 'নাজিরপুর', 0, 'nazirpurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1332, 148, 'Biaghat', 'বিয়াঘাট', 0, 'biaghatup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1333, 148, 'Khubjipur', 'খুবজীপুর', 0, 'khubjipurup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1334, 148, 'Dharabarisha', 'ধারাবারিষা', 0, 'dharabarishaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1335, 148, 'Moshindha', 'মসিন্দা', 0, 'moshindhaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1336, 148, 'Chapila', 'চাপিলা', 0, 'chapilaup.natore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1337, 150, 'Rukindipur', 'রুকিন্দীপুর', 0, 'rukindipurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1338, 150, 'Sonamukhi', 'সোনামূখী', 0, 'sonamukhiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1339, 150, 'Tilakpur', 'তিলকপুর', 0, 'tilakpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1340, 150, 'Raikali', 'রায়কালী', 0, 'raikaliup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1341, 150, 'Gopinathpur', 'গোপীনাথপুর', 0, 'gopinathpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1342, 151, 'Matrai', 'মাত্রাই', 0, 'matraiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1343, 151, 'Ahammedabad', 'আহম্মেদাবাদ', 0, 'ahammedabadup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1344, 151, 'Punot', 'পুনট', 0, 'punotup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1345, 151, 'Zindarpur', 'জিন্দারপুর', 0, 'zindarpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1346, 151, 'Udaipur', 'উদয়পুর', 0, 'udaipurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1347, 152, 'Alampur', 'আলমপুর', 0, 'alampurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1348, 152, 'Borail', 'বড়াইল', 0, 'borailup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1349, 152, 'Tulshiganga', ' তুলশীগংগা', 0, 'tulshigangaup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1350, 152, 'Mamudpur', 'মামুদপুর', 0, 'mamudpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1351, 152, 'Boratara', 'বড়তারা', 0, 'borataraup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1352, 153, 'Bagjana', 'বাগজানা', 0, 'bagjanaup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1353, 153, 'Dharanji', 'ধরঞ্জি', 0, 'dharanjiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1354, 153, 'Aymarasulpur', 'আয়মারসুলপুর', 0, 'aymarasulpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1355, 153, 'Balighata', 'বালিঘাটা', 0, 'balighataup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1356, 153, 'Atapur', 'আটাপুর', 0, 'atapurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1357, 153, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1358, 153, 'Aolai', 'আওলাই', 0, 'aolaiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1359, 153, 'Kusumba', 'কুসুম্বা', 0, 'kusumbaup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1360, 154, 'Amdai', 'আমদই', 0, 'amdaiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1361, 154, 'Bamb', 'বম্বু', 0, 'bambuup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1362, 154, 'Dogachi', 'দোগাছি', 0, 'dogachiup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1363, 154, 'Puranapail', 'পুরানাপৈল', 0, 'puranapailup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1364, 154, 'Jamalpur', 'জামালপুর', 0, 'jamalpurup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1365, 154, 'Chakborkat', 'চকবরকত', 0, 'chakborkatup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1366, 154, 'Mohammadabad', 'মোহাম্মদাবাদ', 0, 'mohammadabadup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1367, 154, 'Dhalahar', 'ধলাহার', 0, 'dhalaharup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1368, 154, 'Bhadsha', 'ভাদসা', 0, 'bhadshaup.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1369, 155, 'Alatuli', 'আলাতুলী', 0, 'alatuliup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1370, 155, 'Baroghoria', 'বারঘরিয়া', 0, 'baroghoriaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1371, 155, 'Moharajpur', 'মহারাজপুর', 0, 'moharajpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1372, 155, 'Ranihati', 'রানীহাটি', 0, 'ranihatiup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1373, 155, 'Baliadanga', 'বালিয়াডাঙ্গা', 0, 'baliadangaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1374, 155, 'Gobratola', 'গোবরাতলা', 0, 'gobratolaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1375, 155, 'Jhilim', 'ঝিলিম', 0, 'jhilimup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1376, 155, 'Char Anupnagar', 'চর অনুপনগর', 0, 'charaunupnagarup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1377, 155, 'Debinagar', 'দেবীনগর', 0, 'debinagarup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1378, 155, 'Shahjahanpur', 'শাহজাহানপুর', 0, 'shahjahanpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1379, 155, 'Islampur', 'ইসলামপুর', 0, 'islampurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1380, 155, 'Charbagdanga', 'চরবাগডাঙ্গা', 0, 'charbagdangaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1381, 155, 'Narayanpur', 'নারায়নপুর', 0, 'narayanpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1382, 155, 'Sundarpur', 'সুন্দরপুর', 0, 'sundarpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1383, 156, 'Radhanagar', 'রাধানগর', 0, 'radhanagarup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1384, 156, 'Rahanpur', 'রহনপুর', 0, 'rahanpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1385, 156, 'Boalia', 'বোয়ালিয়া', 0, 'boaliaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1386, 156, 'Bangabari', 'বাঙ্গাবাড়ী', 0, 'bangabariup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1387, 156, 'Parbotipur', 'পার্বতীপুর', 0, 'parbotipurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1388, 156, 'Chowdala', 'চৌডালা', 0, 'chowdalaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1389, 156, 'Gomostapur', 'গোমস্তাপুর', 0, 'gomostapurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1390, 156, 'Alinagar', 'আলীনগর', 0, 'alinagarup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1391, 157, 'Fhotepur', 'ফতেপুর', 0, 'fhotepurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1392, 157, 'Kosba', 'কসবা', 0, 'kosbaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1393, 157, 'Nezampur', 'নেজামপুর', 0, 'nezampurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1394, 157, 'Nachol', 'নাচোল', 0, 'nacholup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1395, 158, 'Bholahat', 'ভোলাহাট', 0, 'bholahatup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1396, 158, 'Jambaria', 'জামবাড়িয়া', 0, 'jambariaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1397, 158, 'Gohalbari', 'গোহালবাড়ী', 0, 'gohalbariup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1398, 158, 'Daldoli', 'দলদলী', 0, 'daldoliup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1399, 159, 'Binodpur', 'বিনোদপুর', 0, 'binodpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1400, 159, 'Chakkirti', 'চককির্তী', 0, 'chakkirtiup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1401, 159, 'Daipukuria', 'দাইপুকুরিয়া', 0, 'daipukuriaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1402, 159, 'Dhainagar', 'ধাইনগর', 0, 'dhainagarup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1403, 159, 'Durlovpur', 'দুর্লভপুর', 0, 'durlovpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1404, 159, 'Ghorapakhia', 'ঘোড়াপাখিয়া', 0, 'ghorapakhiaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1405, 159, 'Mobarakpur', 'মোবারকপুর', 0, 'mobarakpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1406, 159, 'Monakasha', 'মনাকষা', 0, 'monakashaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1407, 159, 'Noyalavanga', 'নয়ালাভাঙ্গা', 0, 'noyalavangaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1408, 159, 'Panka', 'পাঁকা', 0, 'pankaup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1409, 159, 'Chatrajitpur', 'ছত্রাজিতপুর', 0, 'chhatrajitpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1410, 159, 'Shahabajpur', 'শাহাবাজপুর', 0, 'shahabajpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1411, 159, 'Shyampur', 'শ্যামপুর', 0, 'shyampurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1412, 159, 'Kansat', 'কানসাট', 0, 'kansatup.bdgovportal.com', NULL, NULL);
INSERT INTO `unions` VALUES (1413, 159, 'Ujirpur', 'উজিরপুর', 0, 'ujirpurup.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1414, 160, '1nomohadevpur', 'মহাদেবপুর', 0, '1nomohadevpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1415, 160, 'Hatur', 'হাতুড়', 0, '2nohaturup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1416, 160, 'Khajur', 'খাজুর', 0, '3nokhajurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1417, 160, 'Chandas', 'চাঁন্দাশ', 0, '4nochandasup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1418, 160, 'Enayetpur', 'এনায়েতপুর', 0, '6noenayetpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1419, 160, 'Sofapur', 'সফাপুর', 0, '7nosofapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1420, 160, 'Uttargram', 'উত্তরগ্রাম', 0, '8nouttargramup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1421, 160, 'Cheragpur', 'চেরাগপুর', 0, '9nocheragpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1422, 160, 'Vimpur', 'ভীমপুর', 0, '10novimpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1423, 160, 'Roygon', 'রাইগাঁ', 0, 'roygonup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1424, 161, 'Badalgachi', 'বদলগাছী', 0, '1nobadalgachiup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1425, 161, 'Mothurapur', 'মথুরাপুর', 0, '2nomothurapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1426, 161, 'Paharpur', 'পাহারপুর', 0, '3nopaharpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1427, 161, 'Mithapur', 'মিঠাপুর', 0, '4nomithapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1428, 161, 'Kola', 'কোলা', 0, '5nokolaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1429, 161, 'Bilashbari', 'বিলাশবাড়ী', 0, '6nobilashbariup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1430, 161, 'Adhaipur', 'আধাইপুর', 0, '7noadhaipurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1431, 161, 'Balubhara', 'বালুভরা', 0, '8nobalubharaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1432, 162, 'Patnitala', 'পত্নীতলা', 0, '1nopatnitalaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1433, 162, 'Nirmail', 'নিমইল', 0, '2nonirmailup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1434, 162, 'Dibar', 'দিবর', 0, '3nodibarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1435, 162, 'Akbarpur', 'আকবরপুর', 0, '4noakbarpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1436, 162, 'Matindar', 'মাটিন্দর', 0, '5nomatindarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1437, 162, 'Krishnapur', 'কৃষ্ণপুর', 0, '6nokrishnapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1438, 162, 'Patichrara', 'পাটিচড়া', 0, '7nopatichraraup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1439, 162, 'Nazipur', 'নজিপুর', 0, '8nonazipurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1440, 162, 'Ghasnagar', 'ঘষনগর', 0, '9noghasnagarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1441, 162, 'Amair', 'আমাইড়', 0, '10noamairup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1442, 162, 'Shihara', 'শিহারা', 0, '11noahiharaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1443, 163, 'Dhamoirhat', 'ধামইরহাট', 0, '1nodhamoirhatup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1444, 163, 'Alampur', 'আলমপুর', 0, '3noalampurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1445, 163, 'Umar', 'উমার', 0, '4noumarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1446, 163, 'Aranagar', 'আড়ানগর', 0, '5noaranagarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1447, 163, 'Jahanpur', 'জাহানপুর', 0, '6nojahanpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1448, 163, 'Isabpur', 'ইসবপুর', 0, '7noisabpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1449, 163, 'Khelna', 'খেলনা', 0, '8nokhelnaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1450, 163, 'Agradigun', 'আগ্রাদ্বিগুন', 0, '2noagradigunup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1451, 164, 'Hajinagar', 'হাজীনগর', 0, '1nohajinagarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1452, 164, 'Chandannagar', 'চন্দননগর', 0, '2nochandannagarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1453, 164, 'Bhabicha', 'ভাবিচা', 0, '3nobhabichaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1454, 164, 'Niamatpur', 'নিয়ামতপুর', 0, '4noniamatpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1455, 164, 'Rasulpur', 'রসুলপুর', 0, '5norasulpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1456, 164, 'Paroil', 'পাড়ইল', 0, '6noparoilup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1457, 164, 'Sremantapur', 'শ্রীমন্তপুর', 0, '7nosremantapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1458, 164, 'Bahadurpur', 'বাহাদুরপুর', 0, '8nobahadurpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1459, 165, 'Varsho', 'ভারশো', 0, '1novarshoup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1460, 165, 'Valain', 'ভালাইন', 0, '2novalainup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1461, 165, 'Paranpur', 'পরানপুর', 0, '3noparanpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1462, 165, 'Manda', 'মান্দা', 0, '4nomandaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1463, 165, 'Goneshpur', 'গনেশপুর', 0, '5nogoneshpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1464, 165, 'Moinom', 'মৈনম', 0, '6nomoinomup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1465, 165, 'Proshadpur', 'প্রসাদপুর', 0, '7noproshadpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1466, 165, 'Kosomba', 'কুসুম্বা', 0, '8nokosombaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1467, 165, 'Tetulia', 'তেঁতুলিয়া', 0, '9notetuliaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1468, 165, 'Nurullabad', 'নূরুল্যাবাদ', 0, '10nonurullabadup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1469, 165, 'Kalikapur', 'কালিকাপুর', 0, '11nokalikapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1470, 165, 'Kashopara', 'কাঁশোকাপুর', 0, '12nokashoparaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1471, 165, 'Koshob', 'কশব', 0, '13nokoshobup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1472, 165, 'Bisnopur', 'বিষ্ণপুর', 0, '14nobisnopurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1473, 166, 'Shahagola', 'শাহাগোলা', 0, '1noshahagolaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1474, 166, 'Bhonpara', 'ভোঁপড়া', 0, '2nobhonparaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1475, 166, 'Ahsanganj', 'আহসানগঞ্জ', 0, '3noahsanganjup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1476, 166, 'Panchupur', 'পাঁচুপুর', 0, '4nopanchupurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1477, 166, 'Bisha', 'বিশা', 0, '5nobishaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1478, 166, 'Maniary', 'মনিয়ারী', 0, '6nomaniaryup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1479, 166, 'Kalikapur', 'কালিকাপুর', 0, '7nokalikapurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1480, 166, 'Hatkalupara', 'হাটকালুপাড়া', 0, '8nohatkaluparaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1481, 167, 'Khatteshawr', 'খট্টেশ্বর রাণীনগর', 0, '1nokhatteshawrup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1482, 167, 'Kashimpur', 'কাশিমপুর', 0, '2nokashimpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1483, 167, 'Gona', 'গোনা', 0, '3nogonaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1484, 167, 'Paroil', 'পারইল', 0, '4noparoilup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1485, 167, 'Borgoca', 'বরগাছা', 0, '5noborgocaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1486, 167, 'Kaligram', 'কালিগ্রাম', 0, '6nokaligramup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1487, 167, 'Ekdala', 'একডালা', 0, '7noekdalaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1488, 167, 'Mirat', 'মিরাট', 0, '8nomiratup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1489, 168, 'Barshail', 'বর্ষাইল', 0, '1nobarshailup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1490, 168, 'Kritipur', 'কির্ত্তিপুর', 0, '2nokritipurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1491, 168, 'Baktiarpur', 'বক্তারপুর', 0, '3nobaktiarpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1492, 168, 'Tilakpur', 'তিলোকপুর', 0, '4notilakpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1493, 168, 'Hapaniya', 'হাপানিয়া', 0, '5nohapaniyaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1494, 168, 'Dubalhati', 'দুবলহাটী', 0, '6nodubalhatiup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1495, 168, 'Boalia', 'বোয়ালিয়া', 0, '7noboaliaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1496, 168, 'Hashaigari', 'হাঁসাইগাড়ী', 0, '8nohashaigariup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1497, 168, 'Chandipur', 'চন্ডিপুর', 0, '9nochandipurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1498, 168, 'Bolihar', 'বলিহার', 0, '10noboliharup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1499, 168, 'Shekerpur', 'শিকারপুর', 0, '11noshekerpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1500, 168, 'Shailgachhi', 'শৈলগাছী', 0, '12noshailgachhiup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1501, 169, 'Nitpur', 'নিতপুর', 0, 'nitpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1502, 169, 'Tetulia', 'তেঁতুলিয়া', 0, '2notetuliaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1503, 169, 'Chhaor', 'ছাওড়', 0, '3nochhaorup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1504, 169, 'Ganguria', 'গাঙ্গুরিয়া', 0, '4noganguriaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1505, 169, 'Ghatnagar', 'ঘাটনগর', 0, '5noghatnagarup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1506, 169, 'Moshidpur', 'মশিদপুর', 0, '6nomoshidpurup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1507, 170, 'Sapahar', 'সাপাহার', 0, '1nosapaharup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1508, 170, 'Tilna', 'তিলনা', 0, '3notilnaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1509, 170, 'Aihai', 'আইহাই', 0, '4noaihaiup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1510, 170, 'Shironti', 'শিরন্টী', 0, '6noshirontiup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1511, 170, 'Goala', 'গোয়ালা', 0, 'goalaup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1512, 170, 'Patari', 'পাতাড়ী', 0, 'patariup.naogaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1513, 171, 'Nehalpur', 'নেহালপুর', 0, 'nehalpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1514, 171, 'Hariharnagar', 'হরিহরনগর', 0, 'hariharnagarup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1515, 171, 'Haridaskati', 'হরিদাসকাটি', 0, 'haridaskatiup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1516, 171, 'Shyamkur', 'শ্যামকুড়', 0, 'shyamkurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1517, 171, 'Rohita', 'রোহিতা', 0, 'rohitaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1518, 171, 'Maswimnagar', 'মশ্মিমনগর', 0, 'maswimnagarup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1519, 171, 'Manoharpur', 'মনোহরপুর', 0, 'manoharpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1520, 171, 'Manirampur', 'মনিরামপুর', 0, 'manirampurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1521, 171, 'Bhojgati', 'ভোজগাতি', 0, 'bhojgatiup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1522, 171, 'Durbadanga', 'দুর্বাডাংগা', 0, 'durbadangaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1523, 171, 'Dhakuria', 'ঢাকুরিয়া', 0, 'dhakuriaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1524, 171, 'Jhanpa', 'ঝাঁপা', 0, 'jhanpaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1525, 171, 'Chaluahati', 'চালুয়াহাটি', 0, 'chaluahatiup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1526, 171, 'Khedapara', 'খেদাপাড়া', 0, 'khedaparaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1527, 171, 'Khanpur', 'খানপুর', 0, 'khanpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1528, 171, 'Kultia', 'কুলটিয়া', 0, 'kultiaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1529, 171, 'Kashimnagar', 'কাশিমনগর', 0, 'kashimnagarup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1530, 172, 'Baghutia', 'বাঘুটিয়া', 0, 'baghutia.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1531, 172, 'Chalishia', 'চলিশিয়া', 0, 'chalishiaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1532, 172, 'Sundoli', 'সুন্দলী', 0, 'sundoliup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1533, 172, 'Siddhipasha', 'সিদ্দিপাশা', 0, 'siddhipashaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1534, 172, 'Sreedharpur', 'শ্রীধরপুর', 0, 'sreedharpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1535, 172, 'Subharara', 'শুভরাড়া', 0, 'subhararaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1536, 172, 'Prambag', 'প্রেমবাগ', 0, 'prambagup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1537, 172, 'Payra', 'পায়রা', 0, 'payraup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1538, 173, 'Jaharpur', 'জহুরপুর', 0, 'jaharpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1539, 173, 'Jamdia', 'জামদিয়া', 0, 'jamdiaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1540, 173, 'Darajhat', 'দরাজহাট', 0, 'darajhatup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1541, 173, 'Dhalgram', 'ধলগ্রাম', 0, 'dhalgramup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1542, 173, 'Narikelbaria', 'নারিকেলবাড়ীয়া', 0, 'narikelbariaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1543, 173, 'Bandabilla', 'বন্দবিলা', 0, 'bandabillaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1544, 173, 'Basuari', 'বাসুয়াড়ী', 0, 'basuariup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1545, 173, 'Roypur', 'রায়পুর', 0, 'roypurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1546, 173, 'Dohakula', 'দোহাকুলা', 0, 'dohakulaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1547, 174, 'Chougachha', 'চৌগাছা', 0, 'chougachhaup5.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1548, 174, 'Jagadishpur', 'জগদীশপুর', 0, 'jagadishpurup6.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1549, 174, 'Dhuliani', 'ধুলিয়ানী', 0, 'dhulianiup4.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1550, 174, 'Narayanpur', 'নারায়নপুর', 0, 'narayanpurup10.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1551, 174, 'Patibila', 'পাতিবিলা', 0, 'patibilaup7.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1552, 174, 'Pashapole', 'পাশাপোল', 0, 'pashapoleup2.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1553, 174, 'Fulsara', 'ফুলসারা', 0, 'fulsaraup1.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1554, 174, 'Singhajhuli', 'সিংহঝুলি', 0, 'singhajhuliup3.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1555, 174, 'Sukpukhuria', 'সুখপুকুরিয়া', 0, 'sukpukhuriaup11.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1556, 174, 'Swarupdaha', 'সরুপদাহ', 0, 'swarupdahaup9.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1557, 174, 'Hakimpur', 'হাকিমপুর', 0, 'hakimpurup8.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1558, 175, 'Gangananda', 'গংগানন্দপুর', 0, 'ganganandapurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1559, 175, 'Gadkhali', 'গদখালী', 0, 'gadkhaliup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1560, 175, 'Jhikargachha', 'ঝিকরগাছা', 0, 'jhikargachhaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1561, 175, 'Nabharan', 'নাভারন', 0, 'nabharanup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1562, 175, 'Nibaskhola', 'নির্বাসখোলা', 0, 'nibaskholaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1563, 175, 'Panisara', 'পানিসারা', 0, 'panisaraup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1564, 175, 'Bankra', 'বাঁকড়া', 0, 'bankraup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1565, 175, 'Shankarpur', 'শংকরপুর', 0, 'shankarpurup10.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1566, 175, 'Shimulia', 'শিমুলিয়া', 0, 'shimuliaup3.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1567, 175, 'Hajirbagh', 'হাজিরবাগ', 0, 'hajirbaghup9.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1568, 175, 'Magura', 'মাগুরা', 0, 'maguraup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1569, 176, 'Sufalakati', 'সুফলাকাটি', 0, 'sufalakatiup8.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1570, 176, 'Sagardari', 'সাগরদাড়ী', 0, 'sagardariup2.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1571, 176, 'Majidpur', 'মজিদপুর', 0, 'majidpurup3.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1572, 176, 'Mongolkot', 'মঙ্গলকোর্ট', 0, 'mongolkotup5.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1573, 176, 'Bidyanandakati', 'বিদ্যানন্দকাটি', 0, 'bidyanandakatiup4.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1574, 176, 'Panjia', 'পাজিয়া', 0, 'panjiaup7.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1575, 176, 'Trimohini', 'ত্রিমোহিনী', 0, 'trimohiniup1.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1576, 176, 'Gaurighona', 'গৌরিঘোনা', 0, 'gaurighonaup9.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1577, 176, 'Keshabpur', 'কেশবপুর', 0, 'keshabpurup6.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1578, 177, 'Lebutala', 'লেবুতলা', 0, 'lebutalaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1579, 177, 'Ichhali', 'ইছালী', 0, 'ichhaliup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1580, 177, 'Arabpur', 'আরবপুর', 0, 'arabpurup9.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1581, 177, 'Upasahar', 'উপশহর', 0, 'upasaharup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1582, 177, 'Kachua', 'কচুয়া', 0, 'kachuaup13.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1583, 177, 'Kashimpur', 'কাশিমপুর', 0, 'kashimpurup6.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1584, 177, 'Chanchra', 'চাঁচড়া', 0, 'chanchraup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1585, 177, 'Churamankati', 'চূড়ামনকাটি', 0, 'churamankatiup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1586, 177, 'Narendrapur', 'নরেন্দ্রপুর', 0, 'narendrapurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1587, 177, 'Noapara', 'নওয়াপাড়া', 0, 'noaparaup4.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1588, 177, 'Fathehpur', 'ফতেপুর', 0, 'fathehpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1589, 177, 'Basundia', 'বসুন্দিয়া', 0, 'basundiaup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1590, 177, 'Ramnagar', 'রামনগর', 0, 'ramnagarup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1591, 177, 'Haibatpur', 'হৈবতপুর', 0, 'haibatpurup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1592, 177, 'Dearamodel', 'দেয়ারা মডেল', 0, 'dearamodelup.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1593, 178, 'Ulshi', 'উলশী', 0, 'ulshiup9.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1594, 178, 'Sharsha', 'শার্শা', 0, 'sharshaup10.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1595, 178, 'Lakshmanpur', 'লক্ষণপুর', 0, 'lakshmanpurup2.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1596, 178, 'Benapole', 'বেনাপোল', 0, 'benapoleup4.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1597, 178, 'Bahadurpur', 'বাহাদুরপুর', 0, 'bahadurpurup3.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1598, 178, 'Bagachra', 'বাগআচড়া', 0, 'bagachraup8.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1599, 178, 'Putkhali', 'পুটখালী', 0, 'putkhaliup5.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1600, 178, 'Nizampur', 'নিজামপুর', 0, 'nizampurup11.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1601, 178, 'Dihi', 'ডিহি', 0, 'dihiup1.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1602, 178, 'Goga', 'গোগা', 0, 'gogaup6.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1603, 178, 'Kayba', 'কায়বা', 0, 'kaybaup7.jessore.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1604, 179, 'Anulia', 'আনুলিয়া', 0, 'anuliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1605, 179, 'Assasuni', 'আশাশুনি', 0, 'assasuniup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1606, 179, 'Kadakati', 'কাদাকাটি', 0, 'kadakatiup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1607, 179, 'Kulla', 'কুল্যা', 0, 'kullaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1608, 179, 'Khajra', 'খাজরা', 0, 'khajraup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1609, 179, 'Durgapur', 'দরগাহপুর', 0, 'durgapurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1610, 179, 'Pratapnagar', 'প্রতাপনগর', 0, 'pratapnagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1611, 179, 'Budhhata', 'বুধহাটা', 0, 'budhhataup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1612, 179, 'Baradal', 'বড়দল', 0, 'baradalup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1613, 179, 'Sreeula', 'শ্রীউলা', 0, 'sreeulaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1614, 179, 'Sobhnali', 'শোভনালী', 0, 'sobhnaliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1615, 180, 'Kulia', 'কুলিয়া', 0, 'kuliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1616, 180, 'Debhata', 'দেবহাটা', 0, 'debhataup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1617, 180, 'Noapara', 'নওয়াপাড়া', 0, 'noaparaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1618, 180, 'Parulia', 'পারুলিয়া', 0, 'paruliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1619, 180, 'Sakhipur', 'সখিপুর', 0, 'sakhipurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1620, 181, 'Kushadanga', 'কুশোডাংগা', 0, 'kushadangaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1621, 181, 'Keralkata', 'কেরালকাতা', 0, 'keralkataup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1622, 181, 'Keragachhi', 'কেঁড়াগাছি', 0, 'keragachhiup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1623, 181, 'Kaila', 'কয়লা', 0, 'kailaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1624, 181, 'Jallabad', 'জালালাবাদ', 0, 'jallabadup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1625, 181, 'Jogikhali', 'যুগিখালী', 0, 'jogikhaliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1626, 181, 'Langaljhara', 'লাঙ্গলঝাড়া', 0, 'langaljharaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1627, 181, 'Sonabaria', 'সোনাবাড়িয়া', 0, 'sonabariaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1628, 181, 'Helatala', 'হেলাতলা', 0, 'helatalaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1629, 181, 'Chandanpur', 'চন্দনপুর', 0, 'chandanpurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1630, 181, 'Deara', 'দেয়ারা', 0, 'dearaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1631, 181, 'Joynagar', 'জয়নগর', 0, 'joynagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1632, 182, 'Shibpur', 'শিবপুর', 0, 'shibpurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1633, 182, 'Labsa', 'লাবসা', 0, 'labsaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1634, 182, 'Bhomra', 'ভোমরা', 0, 'bhomraup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1635, 182, 'Brahmarajpur', 'ব্রক্ষ্মরাজপুর', 0, 'brahmarajpurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1636, 182, 'Balli', 'বল্লী', 0, 'balliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1637, 182, 'Banshdaha', 'বাঁশদহ', 0, 'banshdahaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1638, 182, 'Baikari', 'বৈকারী', 0, 'baikariup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1639, 182, 'Fingri', 'ফিংড়ি', 0, 'fingriup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1640, 182, 'Dhulihar', 'ধুলিহর', 0, 'dhuliharup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1641, 182, 'Jhaudanga', 'ঝাউডাঙ্গা', 0, 'jhaudangaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1642, 182, 'Ghona', 'ঘোনা', 0, 'ghonaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1643, 182, 'Kuskhali', 'কুশখালী', 0, 'kuskhaliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1644, 182, 'Alipur', 'আলিপুর', 0, 'alipurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1645, 182, 'Agardari', 'আগরদাড়ী', 0, 'agardariup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1646, 183, 'Atulia', 'আটুলিয়া', 0, 'atuliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1647, 183, 'Ishwaripur', 'ঈশ্বরীপুর', 0, 'ishwaripurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1648, 183, 'Kaikhali', 'কৈখালী', 0, 'kaikhaliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1649, 183, 'Kashimari', 'কাশিমাড়ী', 0, 'kashimariup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1650, 183, 'Nurnagar', 'নুরনগর', 0, 'nurnagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1651, 183, 'Padmapukur', 'পদ্মপুকুর', 0, 'padmapukurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1652, 183, 'Burigoalini', 'বুড়িগোয়ালিনী', 0, 'burigoaliniup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1653, 183, 'Bhurulia', 'ভুরুলিয়া', 0, 'bhuruliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1654, 183, 'Munshiganj', 'মুন্সীগজ্ঞ', 0, 'munshiganjup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1655, 183, 'Ramjannagar', 'রমজাননগর', 0, 'ramjannagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1656, 183, 'Shyamnagar', 'শ্যামনগর', 0, 'shyamnagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1657, 183, 'Gabura', 'গাবুরা', 0, 'gaburaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1658, 184, 'Sarulia', 'সরুলিয়া', 0, 'saruliaup3.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1659, 184, 'Magura', 'মাগুরা', 0, 'maguraup8.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1660, 184, 'Nagarghata', 'নগরঘাটা', 0, 'nagarghataup1.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1661, 184, 'Dhandia', 'ধানদিয়া', 0, 'dhandiaup1.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1662, 184, 'Tentulia', 'তেতুলিয়া', 0, 'tentuliaup5.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1663, 184, 'Tala', 'তালা', 0, 'talaup6.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1664, 184, 'Jalalpur', 'জালালপুর', 0, 'jalalpurup11.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1665, 184, 'Khesra', 'খেশরা', 0, 'khesraup10.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1666, 184, 'Khalishkhali', 'খলিশখালী', 0, 'khalishkhaliup9.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1667, 184, 'Khalilnagar', 'খলিলনগর', 0, 'khalilnagarup12.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1668, 184, 'Kumira', 'কুমিরা', 0, 'kumiraup4.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1669, 184, 'Islamkati', 'ইসলামকাটি', 0, 'islamkatiup7.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1670, 185, 'Kushlia', 'কুশুলিয়া', 0, 'kushliaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1671, 185, 'Champaphul', 'চাম্পাফুল', 0, 'champaphulup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1672, 185, 'Tarali', 'তারালী', 0, 'taraliup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1673, 185, 'Dakshin Sreepur', 'দক্ষিণ শ্রীপুর', 0, 'dakshinsreepurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1674, 185, 'Dhalbaria', 'ধলবাড়িয়া', 0, 'dhalbariaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1675, 185, 'Nalta', 'নলতা', 0, 'naltaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1676, 185, 'Bishnupur', 'বিষ্ণুপুর', 0, 'bishnupurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1677, 185, 'Bharasimla', 'ভাড়াশিমলা', 0, 'bharasimlaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1678, 185, 'Mathureshpur', 'মথুরেশপুর', 0, 'mathureshpurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1679, 185, 'Ratanpur', 'রতনপুর', 0, 'ratanpurup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1680, 185, 'Mautala', 'মৌতলা', 0, 'mautalaup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1681, 185, 'Krishnanagar', 'কৃষ্ণনগর', 0, 'krishnanagarup.satkhira.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1682, 186, 'Dariapur', 'দারিয়াপুর', 0, 'dariapurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1683, 186, 'Monakhali', 'মোনাখালী', 0, 'monakhali.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1684, 186, 'Bagowan', 'বাগোয়ান', 0, 'bagowanup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1685, 186, 'Mohajanpur', 'মহাজনপুর', 0, 'mohajanpurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1686, 187, 'Amjhupi', 'আমঝুপি', 0, 'amjhupi.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1687, 187, 'Pirojpur', 'পিরোজপুর', 0, 'pirojpurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1688, 187, 'Kutubpur', 'কতুবপুর', 0, 'kutubpurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1689, 187, 'Amdah', 'আমদহ', 0, 'amdahup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1690, 187, 'Buripota', 'বুড়িপোতা', 0, 'buripotaup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1691, 188, 'Tentulbaria', 'তেঁতুলবাড়ীয়া', 0, 'tentulbaria.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1692, 188, 'Kazipur', 'কাজিপুর', 0, 'kazipurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1693, 188, 'Bamondi', 'বামন্দী', 0, 'bamondiup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1694, 188, 'Motmura', 'মটমুড়া', 0, 'motmuraup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1695, 188, 'Sholotaka', 'ষোলটাকা', 0, 'sholotakaup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1696, 188, 'Shaharbati', 'সাহারবাটী', 0, 'shaharbatiup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1697, 188, 'Dhankolla', 'ধানখোলা', 0, 'dhankollaup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1698, 188, 'Raipur', 'রায়পুর', 0, 'raipurup.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1699, 188, 'Kathuli', 'কাথুলী', 0, 'kathuli.meherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1700, 189, 'Sheikhati', 'সেখহাটী', 0, 'sheikhatiup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1701, 189, 'Tularampur', 'তুলারামপুর', 0, 'tularampurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1702, 189, 'Kalora', 'কলোড়া', 0, 'kaloraup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1703, 189, 'Shahabad', 'শাহাবাদ', 0, 'shahabadup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1704, 189, 'Bashgram', 'বাশগ্রাম', 0, 'bashgramup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1705, 189, 'Habokhali', 'হবখালী', 0, 'habokhaliup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1706, 189, 'Maijpara', 'মাইজপাড়া', 0, 'maijparaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1707, 189, 'Bisali', 'বিছালী', 0, 'bisaliup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1708, 189, 'Chandiborpur', 'চন্ডিবরপুর', 0, 'chandiborpurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1709, 189, 'Bhadrabila', 'ভদ্রবিলা', 0, 'bhadrabilaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1710, 189, 'Auria', 'আউড়িয়া', 0, 'auriaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1711, 189, 'Singasholpur', 'সিঙ্গাশোলপুর', 0, 'singasholpurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1712, 189, 'Mulia', 'মুলিয়া', 0, 'muliaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1713, 190, 'Lohagora', 'লোহাগড়া', 0, 'lohagoraup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1714, 190, 'Kashipur', 'কাশিপুর', 0, 'kashipurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1715, 190, 'Naldi', 'নলদী', 0, 'naldiup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1716, 190, 'Noagram', 'নোয়াগ্রাম', 0, 'noagramup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1717, 190, 'Lahuria', 'লাহুড়িয়া', 0, 'lahuriaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1718, 190, 'Mallikpur', 'মল্লিকপুর', 0, 'mallikpurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1719, 190, 'Salnagar', 'শালনগর', 0, 'salnagarup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1720, 190, 'Lakshmipasha', 'লক্ষীপাশা', 0, 'lakshmipashaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1721, 190, 'Joypur', 'জয়পুর', 0, 'joypurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1722, 190, 'Kotakol', 'কোটাকোল', 0, 'kotakolup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1723, 190, 'Digholia', 'দিঘলিয়া', 0, 'digholiaup1.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1724, 190, 'Itna', 'ইতনা', 0, 'itnaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1725, 191, 'Jaynagor', 'জয়নগর', 0, 'jaynagorup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1726, 191, 'Pahordanga', 'পহরডাঙ্গা', 0, 'pahordangaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1727, 191, 'Babrahasla', 'বাবরা-হাচলা', 0, 'babrahaslaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1728, 191, 'Salamabad', 'সালামাবাদ', 0, 'salamabadup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1729, 191, 'Baioshona', 'বাঐসোনা', 0, 'baioshonaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1730, 191, 'Chacuri', 'চাচুড়ী', 0, 'chacuriup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1731, 191, 'Hamidpur', 'হামিদপুর', 0, 'hamidpurup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1732, 191, 'Peroli', 'পেড়লী', 0, 'peroliup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1733, 191, 'Khashial', 'খাসিয়াল', 0, 'khashialup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1734, 191, 'Purulia', 'পুরুলিয়া', 0, 'puruliaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1735, 191, 'Kalabaria', 'কলাবাড়ীয়া', 0, 'kalabariaup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1736, 191, 'Mauli', 'মাউলী', 0, 'mauliup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1737, 191, 'Boronaleliasabad', 'বড়নাল-ইলিয়াছাবাদ', 0, 'boronaleliasabadup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1738, 191, 'Panchgram', 'পাঁচগ্রাম', 0, 'panchgramup.narail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1739, 192, 'Alukdia', 'আলুকদিয়া', 0, 'alukdia.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1740, 192, 'Mominpur', 'মোমিনপুর', 0, 'mominpur.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1741, 192, 'Titudah', 'তিতুদাহ', 0, 'titudah.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1742, 192, 'Shankarchandra', 'শংকরচন্দ্র', 0, 'shankarchandra.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1743, 192, 'Begumpur', 'বেগমপুর', 0, 'begumpur.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1744, 192, 'Kutubpur', 'কুতুবপুর', 0, 'kutubpur.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1745, 192, 'Padmabila', 'পদ্মবিলা', 0, 'padmabila.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1746, 193, 'Bhangbaria', 'ভাংবাড়ীয়া', 0, 'bhangbaria.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1747, 193, 'Baradi', 'বাড়াদী', 0, 'baradiup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1748, 193, 'Gangni', 'গাংনী', 0, 'gangniup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1749, 193, 'Khadimpur', 'খাদিমপুর', 0, 'khadimpurup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1750, 193, 'Jehala', 'জেহালা', 0, 'jehalaup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1751, 193, 'Belgachi', 'বেলগাছি', 0, 'belgachiup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1752, 193, 'Dauki', 'ডাউকী', 0, 'daukiup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1753, 193, 'Jamjami', 'জামজামি', 0, 'jamjamiup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1754, 193, 'Nagdah', 'নাগদাহ', 0, 'nagdahup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1755, 193, 'Kashkorara', 'খাসকররা', 0, 'kashkoraraup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1756, 193, 'Chitla', 'চিৎলা', 0, 'chitlaup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1757, 193, 'Kalidashpur', 'কালিদাসপুর', 0, 'kalidashpurup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1758, 193, 'Kumari', 'কুমারী', 0, 'kumariup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1759, 193, 'Hardi', 'হারদী', 0, 'hardiup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1760, 193, 'Ailhash', 'আইলহাঁস', 0, 'ailhashup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1761, 194, 'Damurhuda', 'দামুড়হুদা', 0, 'damurhudaup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1762, 194, 'Karpashdanga', 'কার্পাসডাঙ্গা', 0, 'karpashdanga.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1763, 194, 'Natipota', 'নতিপোতা', 0, 'natipota.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1764, 194, 'Hawli', 'হাওলী', 0, 'hawli.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1765, 194, 'Kurulgachhi', 'কুড়ালগাছী', 0, 'kurulgachhi.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1766, 194, 'Perkrishnopur Madna', 'পারকৃষ্ণপুর মদনা', 0, 'perkrishnopurmadna.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1767, 194, 'Juranpur', 'জুড়ানপুর', 0, 'juranpurup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1768, 195, 'Uthali', 'উথলী', 0, 'uthaliup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1769, 195, 'Andulbaria', 'আন্দুলবাড়ীয়া', 0, 'andulbaria.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1770, 195, 'Banka', 'বাঁকা', 0, 'bankaup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1771, 195, 'Shimanto', 'সীমান্ত', 0, 'shimanto.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1772, 195, 'Raypur', 'রায়পুর', 0, 'raypurup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1773, 195, 'Hasadah', 'হাসাদাহ', 0, 'hasadahup.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1774, 196, 'Hatash Haripur', 'হাটশ হরিপুর', 0, '1nohatashharipurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1775, 196, 'Barkhada', 'বারখাদা', 0, '2nobarkhadaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1776, 196, 'Mazampur', 'মজমপুর', 0, '3nomazampurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1777, 196, 'Bottail', 'বটতৈল', 0, '4nobottailup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1778, 196, 'Alampur', 'আলামপুর', 0, '5noalampurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1779, 196, 'Ziaraakhi', 'জিয়ারাখী', 0, '6noziaraakhiup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1780, 196, 'Ailchara', 'আইলচারা', 0, '7noailcharaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1781, 196, 'Patikabari', 'পাটিকাবাড়ী', 0, '8nopatikabariup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1782, 196, 'Jhaudia', 'ঝাউদিয়া', 0, '9nojhaudiaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1783, 196, 'Ujangram', 'উজানগ্রাম', 0, '10noujangramup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1784, 196, 'Abdulpur', 'আব্দালপুর', 0, '11noabdulpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1785, 196, 'Harinarayanpur', 'হরিনারায়নপুর', 0, '12noharinarayanpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1786, 196, 'Monohardia', 'মনোহরদিয়া', 0, '13nomonohardiaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1787, 196, 'Goswami Durgapur', 'গোস্বামী দুর্গাপুর', 0, '14nogoswamidurgapurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1788, 197, 'Kaya', 'কয়া', 0, '1nokayaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1789, 197, 'Jagonnathpur', 'জগন্নাথপুর', 0, '3nojagonnathpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1790, 197, 'Sadki', 'সদকী', 0, '4nosadkiup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1791, 197, 'Shelaidah', 'শিলাইদহ', 0, '2noshelaidahup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1792, 197, 'Nandolalpur', 'নন্দলালপুর', 0, '5nonandolalpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1793, 197, 'Chapra', 'চাপড়া', 0, '6nochapraup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1794, 197, 'Bagulat', 'বাগুলাট', 0, '7nobagulatup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1795, 197, 'Jaduboyra', 'যদুবয়রা', 0, '8nojaduboyraup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1796, 197, 'Chadpur', 'চাঁদপুর', 0, '9nochadpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1797, 197, 'Panti', 'পান্টি', 0, '10nopantiup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1798, 197, 'Charsadipur', 'চরসাদীপুর', 0, '11nocharsadipurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1799, 198, 'Khoksa', 'খোকসা', 0, '1nokhoksaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1800, 198, 'Osmanpur', 'ওসমানপুর', 0, '2noosmanpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1801, 198, 'Janipur', 'জানিপুর', 0, '4nojanipurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1802, 198, 'Shimulia', 'শিমুলিয়া', 0, '5noshimuliaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1803, 198, 'Joyntihazra', 'জয়ন্তীহাজরা', 0, '8nojoyntihazraup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1804, 198, 'Ambaria', 'আমবাড়ীয়া', 0, '9noambariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1805, 198, 'Bethbaria', 'বেতবাড়ীয়া', 0, '3nobethbariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1806, 198, 'Shomospur', 'শোমসপুর', 0, '6noshomospurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1807, 198, 'Gopgram', 'গোপগ্রাম', 0, 'gopgram7up.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1808, 199, 'Chithalia', 'চিথলিয়া', 0, 'chithaliaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1809, 199, 'Bahalbaria', 'বহলবাড়ীয়া', 0, 'bahalbariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1810, 199, 'Talbaria', 'তালবাড়ীয়া', 0, 'talbariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1811, 199, 'Baruipara', 'বারুইপাড়া', 0, 'baruiparaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1812, 199, 'Fulbaria', 'ফুলবাড়ীয়া', 0, 'fulbariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1813, 199, 'Amla', 'আমলা', 0, 'amlaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1814, 199, 'Sadarpur', 'সদরপুর', 0, 'sadarpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1815, 199, 'Chhatian', 'ছাতিয়ান', 0, 'chhatianup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1816, 199, 'Poradaha', 'পোড়াদহ', 0, 'poradahaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1817, 199, 'Kursha', 'কুর্শা', 0, 'kurshaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1818, 199, 'Ambaria', 'আমবাড়ীয়া', 0, 'ambariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1819, 199, 'Dhubail', 'ধূবইল', 0, 'dhubailup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1820, 199, 'Malihad', 'মালিহাদ', 0, '11nomalihadup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1821, 200, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1822, 200, 'Adabaria', 'ড়ীয়া', 0, 'adabariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1823, 200, 'Hogolbaria', 'হোগলবাড়ীয়া', 0, 'hogolbariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1824, 200, 'Boalia', 'বোয়ালি', 0, 'boaliaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1825, 200, 'Philipnagor', 'ফিলিপনগর', 0, 'philipnagorup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1826, 200, 'Aria', 'আড়িয়া', 0, 'ariaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1827, 200, 'Khalishakundi', 'খলিশাকুন্ডি', 0, 'khalishakundiup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1828, 200, 'Chilmary', 'চিলমারী', 0, 'chilmaryup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1829, 200, 'Mothurapur', 'মথুরাপুর', 0, 'mothurapurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1830, 200, 'Pragpur', 'প্রাগপুর', 0, 'pragpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1831, 200, 'Piarpur', 'পিয়ারপুর', 0, 'piarpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1832, 200, 'Moricha', 'মরিচা', 0, 'morichaup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1833, 200, 'Refaitpur', 'রিফাইতপুর', 0, '9norefaitpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1834, 200, 'Ramkrishnopur', 'রামকৃষ্ণপুর', 0, '5noramkrishnopurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1835, 201, 'Dharampur', 'ধরমপুর', 0, '5nodharampurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1836, 201, 'Bahirchar', 'বাহিরচর', 0, '3nobahircharup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1837, 201, 'Mukarimpur', 'মোকারিমপুর', 0, '2nomukarimpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1838, 201, 'Juniadah', 'জুনিয়াদহ', 0, '6nojuniadahup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1839, 201, 'Chandgram', 'চাঁদগ্রাম', 0, '4nochandgramup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1840, 201, 'Bahadurpur', 'বাহাদুরপুর', 0, '1nobahadurpurup.kushtia.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1841, 202, 'Dhaneshwargati', 'ধনেশ্বরগাতী', 0, 'dhaneshwargatiup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1842, 202, 'Talkhari', 'তালখড়ি', 0, 'talkhariup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1843, 202, 'Arpara', 'আড়পাড়া', 0, 'arparaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1844, 202, 'Shatakhali', 'শতখালী', 0, 'shatakhaliup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1845, 202, 'Shalikha', 'শালিখা', 0, 'shalikhaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1846, 202, 'Bunagati', 'বুনাগাতী', 0, 'bunagatiup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1847, 202, 'Gongarampur', 'গঙ্গারামপুর', 0, 'gongarampurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1848, 203, 'Goyespur', 'গয়েশপুর', 0, 'goyespurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1849, 203, 'Sreekol', 'শ্রীকোল', 0, 'sreekolup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1850, 203, 'Dariapur', 'দ্বারিয়াপুর', 0, 'dariapurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1851, 203, 'Kadirpara', 'কাদিরপাড়া', 0, 'kadirparaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1852, 203, 'Shobdalpur', 'সব্দালপুর', 0, 'shobdalpurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1853, 203, 'Sreepur', 'শ্রীপুর', 0, 'sreepurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1854, 203, 'Nakol', 'নাকোল', 0, 'nakolup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1855, 203, 'Amalshar', 'আমলসার', 0, 'amalsharup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1856, 204, 'Hazipur', 'হাজীপুর', 0, 'hazipurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1857, 204, 'Atharokhada', 'আঠারখাদা', 0, 'atharokhadaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1858, 204, 'Kosundi', 'কছুন্দী', 0, 'kosundiup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1859, 204, 'Bogia', 'বগিয়া', 0, 'bogiaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1860, 204, 'Hazrapur', 'হাজরাপুর', 0, 'hazrapurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1861, 204, 'Raghobdair', 'রাঘবদাইড়', 0, 'raghobdairup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1862, 204, 'Jagdal', 'জগদল', 0, 'jagdalup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1863, 204, 'Chawlia', 'চাউলিয়া', 0, 'chawliaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1864, 204, 'Satrijitpur', 'শত্রুজিৎপুর', 0, 'satrijitpurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1865, 204, 'Baroilpolita', 'বেরইল পলিতা', 0, 'baroilpolitaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1866, 204, 'Kuchiamora', 'কুচিয়ামো', 0, 'kuchiamoraup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1867, 204, 'Gopalgram', 'গোপালগ্রাম', 0, 'gopalgramup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1868, 204, 'Moghi', 'মঘী', 0, 'moghiup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1869, 205, 'Digha', 'দীঘা', 0, 'dighaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1870, 205, 'Nohata', 'নহাটা', 0, 'nohataup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1871, 205, 'Palashbaria', 'পলাশবাড়ীয়া', 0, 'palashbariaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1872, 205, 'Babukhali', 'বাবুখালী', 0, 'babukhaliup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1873, 205, 'Balidia', 'বালিদিয়া', 0, 'balidiaup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1874, 205, 'Binodpur', 'বিনোদপুর', 0, 'binodpurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1875, 205, 'Mohammadpur', 'মহম্মদপুর', 0, 'mohammadpurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1876, 205, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.magura.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1877, 206, 'Horidhali', 'হরিঢালী', 0, 'horidhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1878, 206, 'Goroikhali', 'গড়ইখালী', 0, 'goroikhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1879, 206, 'Kopilmuni', 'কপিলমুনি', 0, 'kopilmuniup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1880, 206, 'Lota', 'লতা', 0, 'lotaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1881, 206, 'Deluti', 'দেলুটি', 0, 'delutiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1882, 206, 'Loskor', 'লস্কর', 0, 'loskorup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1883, 206, 'Godaipur', 'গদাইপুর', 0, 'godaipurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1884, 206, 'Raruli', 'রাড়ুলী', 0, 'www.raruliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1885, 206, 'Chandkhali', 'চাঁদখালী', 0, 'chandkhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1886, 206, 'Soladana', 'সোলাদানা', 0, 'soladanaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1887, 207, 'Fultola', 'ফুলতলা', 0, 'www.fultolaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1888, 207, 'Damodar', 'দামোদর', 0, 'www.damodarup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1889, 207, 'Atra Gilatola', 'আটরা গিলাতলা', 0, 'www.atragilatolaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1890, 207, 'Jamira', 'জামিরা', 0, 'www.jamiraup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1891, 208, 'Senhati', 'সেনহাটি', 0, 'www.senhatiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1892, 208, 'Gajirhat', 'গাজীরহাট', 0, 'www.gajirhatup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1893, 208, 'Barakpur', 'বারাকপুর', 0, 'www.barakpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1894, 208, 'Aronghata', 'আড়ংঘাটা', 0, 'www.aronghataup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1895, 208, 'Jogipol', 'যোগীপোল', 0, 'www.jogipolup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1896, 208, 'Digholia', 'দিঘলিয়া', 0, 'www.digholiaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1897, 209, 'Aichgati', 'আইচগাতী', 0, 'aichgatiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1898, 209, 'Srifoltola', 'শ্রীফলতলা', 0, 'srifoltolaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1899, 209, 'Noihati', 'নৈহাটি', 0, 'noihatiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1900, 209, 'Tsb', 'টিএসবি', 0, 'tsbup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1901, 209, 'Ghatvog', 'ঘাটভোগ', 0, 'ghatvogup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1902, 210, 'Terokhada', 'তেরখাদা', 0, 'terokhadaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1903, 210, 'Chagladoho', 'ছাগলাদহ', 0, 'chagladohoup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1904, 210, 'Barasat', 'বারাসাত', 0, 'www.barasatup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1905, 210, 'Sochiadaho', 'সাচিয়াদাহ', 0, 'www.sochiadahoup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1906, 210, 'Modhupur', 'মধুপুর', 0, 'www.modhupurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1907, 210, 'Ajgora', 'আজগড়া', 0, 'www.ajgoraup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1908, 211, 'Dumuria', 'ডুমুরিয়া', 0, 'dumuriaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1909, 211, 'Magurghona', 'মাগুরাঘোনা', 0, 'magurghonaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1910, 211, 'Vandarpara', 'ভান্ডারপাড়া', 0, 'vandarparaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1911, 211, 'Sahos', 'সাহস', 0, 'sahosup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1912, 211, 'Rudaghora', 'রুদাঘরা', 0, 'rudaghoraup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1913, 211, 'Ghutudia', 'গুটুদিয়া', 0, 'ghutudiaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1914, 211, 'Shovna', 'শোভনা', 0, 'shovnaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1915, 211, 'Khornia', 'খর্ণিয়া', 0, 'khorniaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1916, 211, 'Atlia', 'আটলিয়া', 0, 'atliaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1917, 211, 'Dhamalia', 'ধামালিয়া', 0, 'dhamaliaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1918, 211, 'Raghunathpur', 'রঘুনাথপুর', 0, 'raghunathpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1919, 211, 'Rongpur', 'রংপুর', 0, 'rongpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1920, 211, 'Shorafpur', 'শরাফপুর', 0, 'shorafpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1921, 211, 'Magurkhali', 'মাগুরখালি', 0, 'magurkhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1922, 212, 'Botiaghata', 'বটিয়াঘাটা', 0, 'www.botiaghataup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1923, 212, 'Amirpur', 'আমিরপুর', 0, 'www.amirpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1924, 212, 'Gongarampur', 'গঙ্গারামপুর', 0, 'www.gongarampurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1925, 212, 'Surkhali', 'সুরখালী', 0, 'www.surkhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1926, 212, 'Vandarkot', 'ভান্ডারকোট', 0, 'www.vandarkotup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1927, 212, 'Baliadanga', 'বালিয়াডাঙ্গা', 0, 'www.baliadangaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1928, 212, 'Jolma', 'জলমা', 0, 'www.jolmaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1929, 213, 'Dakop', 'দাকোপ', 0, 'www.dakopup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1930, 213, 'Bajua', 'বাজুয়া', 0, 'bajuaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1931, 213, 'Kamarkhola', 'কামারখোলা', 0, 'www.kamarkholaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1932, 213, 'Tildanga', 'তিলডাঙ্গা', 0, 'www.tildangaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1933, 213, 'Sutarkhali', 'সুতারখালী', 0, 'www.sutarkhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1934, 213, 'Laudoba', 'লাউডোব', 0, 'laudobaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1935, 213, 'Pankhali', 'পানখালী', 0, 'pankhaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1936, 213, 'Banishanta', 'বানিশান্তা', 0, 'banishantaup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1937, 213, 'Koilashgonj', 'কৈলাশগঞ্জ', 0, 'koilashgonjup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1938, 214, 'Koyra', 'কয়রা', 0, 'koyraup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1939, 214, 'Moharajpur', 'মহারাজপুর', 0, 'moharajpurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1940, 214, 'Moheswaripur', 'মহেশ্বরীপুর', 0, 'moheswaripurup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1941, 214, 'North Bedkashi', 'উত্তর বেদকাশী', 0, 'northbedkashiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1942, 214, 'South Bedkashi', 'দক্ষিণ বেদকাশী', 0, 'southbedkashiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1943, 214, 'Amadi', 'আমাদি', 0, 'amadiup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1944, 214, 'Bagali', 'বাগালী', 0, 'bagaliup.khulna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1945, 215, 'Betaga', 'বেতাগা', 0, 'betagaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1946, 215, 'Lakhpur', 'লখপুর', 0, 'lakhpurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1947, 215, 'Fakirhat', 'ফকিরহাট', 0, 'fakirhatup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1948, 215, 'Bahirdia-Mansa', 'বাহিরদিয়া-মানসা', 0, 'bahirdiamansaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1949, 215, 'Piljanga', 'পিলজংগ', 0, 'piljangaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1950, 215, 'Naldha-Mouvhog', 'নলধা-মৌভোগ', 0, 'naldhamauvhogup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1951, 215, 'Mulghar', 'মূলঘর', 0, 'mulgharup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1952, 215, 'Suvhadia', 'শুভদিয়া', 0, 'suvhadiaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1953, 216, 'Karapara', 'কাড়াপাড়া', 0, 'karaparaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1954, 216, 'Bamorta', 'বেমরতা', 0, 'bamortaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1955, 216, 'Gotapara', 'গোটাপাড়া', 0, 'gotaparaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1956, 216, 'Bishnapur', 'বিষ্ণুপুর', 0, 'bishnapurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1957, 216, 'Baruipara', 'বারুইপাড়া', 0, 'baruiparaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1958, 216, 'Jatharapur', 'যাত্রাপুর', 0, 'jatharapurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1959, 216, 'Shaitgomboj', 'ষাটগুম্বজ', 0, 'shaitgombojup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1960, 216, 'Khanpur', 'খানপুর', 0, 'khanpurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1961, 216, 'Rakhalgachi', 'রাখালগাছি', 0, 'rakhalgachiup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1962, 216, 'Dema', 'ডেমা', 0, 'demaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1963, 217, 'Udoypur', 'উদয়পুর', 0, 'udoypurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1964, 217, 'Chunkhola', 'চুনখোলা', 0, 'chunkholaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1965, 217, 'Gangni', 'গাংনী', 0, 'gangniup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1966, 217, 'Kulia', 'কুলিয়া', 0, 'kuliaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1967, 217, 'Gaola', 'গাওলা', 0, 'gaolaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1968, 217, 'Kodalia', 'কোদালিয়া', 0, 'kodaliaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1969, 217, 'Atjuri', 'আটজুড়ী', 0, 'atjuriup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1970, 218, 'Dhanshagor', 'ধানসাগর', 0, 'dhanshagorup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1971, 218, 'Khontakata', 'খোন্তাকাটা', 0, 'khontakataup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1972, 218, 'Rayenda', 'রায়েন্দা', 0, 'rayendaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1973, 218, 'Southkhali', 'সাউথখালী', 0, 'southkhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1974, 219, 'Gouramva', 'গৌরম্ভা', 0, 'gouramvaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1975, 219, 'Uzzalkur', 'উজলকুড়', 0, 'uzzalkurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1976, 219, 'Baintala', 'বাইনতলা', 0, 'baintalaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1977, 219, 'Rampal', 'রামপাল', 0, 'rampalup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1978, 219, 'Rajnagar', 'রাজনগর', 0, 'rajnagarup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1979, 219, 'Hurka', 'হুড়কা', 0, 'hurkaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1980, 219, 'Perikhali', 'পেড়িখালী', 0, 'perikhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1981, 219, 'Vospatia', 'ভোজপাতিয়া', 0, 'vospatiaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1982, 219, 'Mollikerbar', 'মল্লিকেরবেড়', 0, 'mollikerbarup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1983, 219, 'Bastoli', 'বাঁশতলী', 0, 'bastoliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1984, 220, 'Teligati', 'তেলিগাতী', 0, 'teligatiup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1985, 220, 'Panchakaran', 'পঞ্চকরণ', 0, 'panchakaranup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1986, 220, 'Putikhali', 'পুটিখালী', 0, 'putikhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1987, 220, 'Daibagnyahati', 'দৈবজ্ঞহাটি', 0, 'daibagnyahatiup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1988, 220, 'Ramchandrapur', 'রামচন্দ্রপুর', 0, 'ramchandrapurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1989, 220, 'Chingrakhali', 'চিংড়াখালী', 0, 'chingrakhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1990, 220, 'Jiudhara', 'জিউধরা', 0, 'jiudharaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1991, 220, 'Hoglapasha', 'হোগলাপাশা', 0, 'hoglapashaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1992, 220, 'Banagram', 'বনগ্রাম', 0, 'banagramup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1993, 220, 'Balaibunia', 'বলইবুনিয়া', 0, 'balaibuniaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1994, 220, 'Hoglabunia', 'হোগলাবুনিয়া', 0, 'hoglabuniaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1995, 220, 'Baharbunia', 'বহরবুনিয়া', 0, 'baharbuniaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1996, 220, 'Morrelganj', 'মোড়েলগঞ্জ', 0, 'morrelganjup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1997, 220, 'Khaulia', 'খাউলিয়া', 0, 'khauliaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1998, 220, 'Nishanbaria', 'নিশানবাড়িয়া', 0, 'nishanbariaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (1999, 220, 'Baraikhali', 'বারইখালী', 0, 'baraikhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2000, 221, 'Gojalia', 'গজালিয়া', 0, 'gojaliaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2001, 221, 'Dhopakhali', 'ধোপাখালী', 0, 'dhopakhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2002, 221, 'Moghia', 'মঘিয়া', 0, 'moghiaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2003, 221, 'Kachua', 'কচুয়া', 0, 'kachuaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2004, 221, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2005, 221, 'Raripara', 'রাড়ীপাড়া', 0, 'rariparaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2006, 221, 'Badhal', 'বাধাল', 0, 'badhalup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2007, 222, 'Burrirdangga', 'বুড়িরডাঙ্গা', 0, 'burrirdanggaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2008, 222, 'Mithakhali', 'মিঠাখালী', 0, 'mithakhaliup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2009, 222, 'Sonailtala', 'সোনাইলতলা', 0, 'sonailtalaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2010, 222, 'Chadpai', 'চাঁদপাই', 0, 'chadpaiup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2011, 222, 'Chila', 'চিলা', 0, 'chilaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2012, 222, 'Sundarban', 'সুন্দরবন', 0, 'sundarbanup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2013, 223, 'Barobaria', 'বড়বাড়িয়া', 0, 'barobariaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2014, 223, 'Kalatala', 'কলাতলা', 0, 'kalatalaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2015, 223, 'Hizla', 'হিজলা', 0, 'hizlaup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2016, 223, 'Shibpur', 'শিবপুর', 0, 'shibpurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2017, 223, 'Chitalmari', 'চিতলমারী', 0, 'chitalmariup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2018, 223, 'Charbaniri', 'চরবানিয়ারী', 0, 'charbaniriup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2019, 223, 'Shantoshpur', 'সন্তোষপুর', 0, 'shantoshpurup.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2020, 224, 'Sadhuhati', 'সাধুহাটী', 0, 'sadhuhatiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2021, 224, 'Modhuhati', 'মধুহাটী', 0, 'modhuhatiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2022, 224, 'Saganna', 'সাগান্না', 0, 'sagannaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2023, 224, 'Halidhani', 'হলিধানী', 0, 'halidhaniup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2024, 224, 'Kumrabaria', 'কুমড়াবাড়ীয়া', 0, 'kumrabariaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2025, 224, 'Ganna', 'গান্না', 0, 'gannaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2026, 224, 'Maharazpur', 'মহারাজপুর', 0, 'maharazpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2027, 224, 'Paglakanai', 'পাগলাকানাই', 0, 'paglakanaiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2028, 224, 'Porahati', 'পোড়াহাটী', 0, 'porahatiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2029, 224, 'Harishongkorpur', 'হরিশংকরপুর', 0, 'harishongkorpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2030, 224, 'Padmakar', 'পদ্মাকর', 0, 'padmakarup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2031, 224, 'Dogachhi', 'দোগাছি', 0, 'dogachhiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2032, 224, 'Furshondi', 'ফুরসন্দি', 0, 'furshondiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2033, 224, 'Ghorshal', 'ঘোড়শাল', 0, 'ghorshalup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2034, 224, 'Kalicharanpur', 'কালীচরণপুর', 0, 'kalicharanpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2035, 224, 'Surat', 'সুরাট', 0, 'suratup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2036, 224, 'Naldanga', 'নলডাঙ্গা', 0, 'naldangaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2037, 225, 'Tribeni', 'ত্রিবেনী', 0, 'tribeniup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2038, 225, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2039, 225, 'Dignagore', 'দিগনগর', 0, 'dignagoreup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2040, 225, 'Kancherkol', 'কাঁচেরকোল', 0, 'kancherkolup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2041, 225, 'Sarutia', 'সারুটিয়া', 0, 'sarutiaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2042, 225, 'Hakimpur', 'হাকিমপুর', 0, 'hakimpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2043, 225, 'Dhaloharachandra', 'ধলহরাচন্দ্র', 0, 'dhaloharachandraup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2044, 225, 'Manoharpur', 'মনোহরপুর', 0, 'manoharpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2045, 225, 'Bogura', 'বগুড়া', 0, 'boguraup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2046, 225, 'Abaipur', 'আবাইপুর', 0, 'abaipurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2047, 225, 'Nityanandapur', 'নিত্যানন্দপুর', 0, 'nityanandapurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2048, 225, 'Umedpur', 'উমেদপুর', 0, 'umedpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2049, 225, 'Dudshar', 'দুধসর', 0, 'dudsharup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2050, 225, 'Fulhari', 'ফুলহরি', 0, 'fulhariup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2051, 226, 'Bhayna', 'ভায়না', 0, 'bhaynaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2052, 226, 'Joradah', 'জোড়াদহ', 0, 'joradahup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2053, 226, 'Taherhuda', 'তাহেরহুদা', 0, 'taherhudaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2054, 226, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2055, 226, 'Kapashatia', 'কাপাশহাটিয়া', 0, 'kapashatiaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2056, 226, 'Falsi', 'ফলসী', 0, 'falsiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2057, 226, 'Raghunathpur', 'রঘুনাথপুর', 0, 'raghunathpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2058, 226, 'Chandpur', 'চাঁদপুর', 0, 'chandpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2059, 227, 'Sundarpurdurgapur', 'সুন্দরপুর-দূর্গাপুর', 0, 'sundarpurdurgapurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2060, 227, 'Jamal', 'জামাল', 0, 'jamalup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2061, 227, 'Kola', 'কোলা', 0, 'kolaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2062, 227, 'Niamatpur', 'নিয়ামতপুর', 0, 'niamatpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2063, 227, 'Simla-Rokonpur', 'শিমলা-রোকনপুর', 0, 'simlarokonpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2064, 227, 'Trilochanpur', 'ত্রিলোচনপুর', 0, 'trilochanpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2065, 227, 'Raygram', 'রায়গ্রাম', 0, 'raygramup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2066, 227, 'Maliat', 'মালিয়াট', 0, 'maliatup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2067, 227, 'Barabazar', 'বারবাজার', 0, 'barabazarup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2068, 227, 'Kashtabhanga', 'কাষ্টভাঙ্গা', 0, 'kashtabhangaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2069, 227, 'Rakhalgachhi', 'রাখালগাছি', 0, 'rakhalgachhiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2070, 228, 'Sabdalpur', 'সাবদালপুর', 0, 'sabdalpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2071, 228, 'Dora', 'দোড়া', 0, 'doraup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2072, 228, 'Kushna', 'কুশনা', 0, 'kushnaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2073, 228, 'Baluhar', 'বলুহর', 0, 'baluharup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2074, 228, 'Elangi', 'এলাঙ্গী', 0, 'elangiup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2075, 229, 'Sbk', 'এস, বি, কে', 0, 'sbkup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2076, 229, 'Fatepur', 'ফতেপুর', 0, 'fatepurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2077, 229, 'Panthapara', 'পান্থপাড়া', 0, 'panthaparaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2078, 229, 'Swaruppur', 'স্বরুপপুর', 0, 'swaruppurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2079, 229, 'Shyamkur', 'শ্যামকুড়', 0, 'shyamkurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2080, 229, 'Nepa', 'নেপা', 0, 'nepaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2081, 229, 'Kazirber', 'কাজীরবেড়', 0, 'kazirberup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2082, 229, 'Banshbaria', 'বাঁশবাড়ীয়া', 0, 'banshbariaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2083, 229, 'Jadabpur', 'যাদবপুর', 0, 'jadabpurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2084, 229, 'Natima', 'নাটিমা', 0, 'natimaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2085, 229, 'Manderbaria', 'মান্দারবাড়ীয়া', 0, 'manderbariaup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2086, 229, 'Azampur', 'আজমপুর', 0, 'azampurup.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2087, 230, 'Basanda', 'বাসন্ডা', 0, 'basandaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2088, 230, 'Binoykati', 'বিনয়কাঠী', 0, 'binoykatiup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2089, 230, 'Gabharamchandrapur', 'গাভারামচন্দ্রপুর', 0, 'gabharamchandrapurup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2090, 230, 'Keora', 'কেওড়া', 0, 'keoraup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2091, 230, 'Kirtipasha', 'কীর্তিপাশা', 0, 'kirtipashaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2092, 230, 'Nabagram', 'নবগ্রাম', 0, 'nabagramup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2093, 230, 'Nathullabad', 'নথুলল্লাবাদ', 0, 'nathullabadup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2094, 230, 'Ponabalia', 'পোনাবালিয়া', 0, 'ponabaliaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2095, 230, 'Sekherhat', 'শেখেরহাট', 0, 'sekherhatup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2096, 230, 'Gabkhandhansiri', 'গাবখান ধানসিঁড়ি', 0, 'gabkhandhansiriup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2097, 231, 'Amua', 'আমুয়া', 0, 'amuaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2098, 231, 'Awrabunia', 'আওরাবুনিয়া', 0, 'awrabuniaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2099, 231, 'Chenchrirampur', 'চেঁচরীরামপুর', 0, 'chenchrirampurup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2100, 231, 'Kanthalia', 'কাঠালিয়া', 0, 'kanthaliaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2101, 231, 'Patikhalghata', 'পাটিখালঘাটা', 0, 'patikhalghataup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2102, 231, 'Shaulajalia', 'শৌলজালিয়া', 0, 'shaulajaliaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2103, 232, 'Subidpur', 'সুবিদপুর', 0, 'subidpurup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2104, 232, 'Siddhakati', 'সিদ্ধকাঠী', 0, 'siddhakatiup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2105, 232, 'Ranapasha', 'রানাপাশা', 0, 'ranapashaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2106, 232, 'Nachanmohal', 'নাচনমহল', 0, 'nachanmohalup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2107, 232, 'Mollahat', 'মোল্লারহাট', 0, 'mollahatup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2108, 232, 'Magar', 'মগর', 0, 'magarup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2109, 232, 'Kusanghal', 'কুশঙ্গল', 0, 'kusanghalup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2110, 232, 'Kulkathi', 'কুলকাঠী', 0, 'kulkathiup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2111, 232, 'Dapdapia', 'দপদপিয়া', 0, 'dapdapiaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2112, 232, 'Bharabpasha', 'ভৈরবপাশা', 0, 'bharabpashaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2113, 233, 'Suktagarh', 'শুক্তাগড়', 0, 'suktagarhup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2114, 233, 'Saturia', 'সাতুরিয়া', 0, 'saturiaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2115, 233, 'Mathbari', 'মঠবাড়ী', 0, 'mathbariup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2116, 233, 'Galua', 'গালুয়া', 0, 'galuaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2117, 233, 'Baraia', 'বড়ইয়া', 0, 'baraiaup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2118, 233, 'Rajapur', 'রাজাপুর', 0, 'rajapurup.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2119, 234, 'Adabaria', 'আদাবারিয়া', 0, 'adabariaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2120, 234, 'Bauphal', 'বাউফল', 0, 'bauphalup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2121, 234, 'Daspara', 'দাস পাড়া', 0, 'dasparaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2122, 234, 'Kalaiya', 'কালাইয়া', 0, 'kalaiyaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2123, 234, 'Nawmala', 'নওমালা', 0, 'nawmalaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2124, 234, 'Najirpur', 'নাজিরপুর', 0, 'najirpurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2125, 234, 'Madanpura', 'মদনপুরা', 0, 'madanpuraup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2126, 234, 'Boga', 'বগা', 0, 'bogaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2127, 234, 'Kanakdia', 'কনকদিয়া', 0, 'kanakdiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2128, 234, 'Shurjamoni', 'সূর্য্যমনি', 0, 'shurjamoniup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2129, 234, 'Keshabpur', 'কেশবপুর', 0, 'keshabpurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2130, 234, 'Dhulia', 'ধুলিয়া', 0, 'dhuliaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2131, 234, 'Kalisuri', 'কালিশুরী', 0, 'kalisuriup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2132, 234, 'Kachipara', 'কাছিপাড়া', 0, 'kachiparaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2133, 235, 'Laukathi', 'লাউকাঠী', 0, 'laukathiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2134, 235, 'Lohalia', 'লোহালিয়া', 0, 'lohaliaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2135, 235, 'Kamalapur', 'কমলাপুর', 0, 'kamalapurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2136, 235, 'Jainkathi', 'জৈনকাঠী', 0, 'jainkathiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2137, 235, 'Kalikapur', 'কালিকাপুর', 0, 'kalikapurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2138, 235, 'Badarpur', 'বদরপুর', 0, 'badarpurup.patuakhali.gov.bd ', NULL, NULL);
INSERT INTO `unions` VALUES (2139, 235, 'Itbaria', 'ইটবাড়ীয়া', 0, 'itbariaup.patuakhali.gov.bd ', NULL, NULL);
INSERT INTO `unions` VALUES (2140, 235, 'Marichbunia', 'মরিচবুনিয়া', 0, 'marichbuniaup.patuakhali.gov.bd ', NULL, NULL);
INSERT INTO `unions` VALUES (2141, 235, 'Auliapur', 'আউলিয়াপুর', 0, 'auliapurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2142, 235, 'Chotobighai', 'ছোট বিঘাই', 0, 'chotobighaiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2143, 235, 'Borobighai', 'বড় বিঘাই', 0, 'borobighaiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2144, 235, 'Madarbunia', 'মাদারবুনিয়া', 0, 'madarbuniaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2145, 236, 'Pangasia', 'পাংগাশিয়া', 0, 'pangasiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2146, 236, 'Muradia', 'মুরাদিয়া', 0, 'muradiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2147, 236, 'Labukhali', 'লেবুখালী', 0, 'labukhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2148, 236, 'Angaria', 'আংগারিয়া', 0, 'angariaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2149, 236, 'Sreerampur', 'শ্রীরামপুর', 0, 'sreerampurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2150, 237, 'Bashbaria', 'বাঁশবাড়ীয়া', 0, 'bashbariaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2151, 237, 'Rangopaldi', 'রণগোপালদী', 0, 'rangopaldiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2152, 237, 'Alipur', 'আলীপুর', 0, 'alipurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2153, 237, 'Betagi Shankipur', 'বেতাগী সানকিপুর', 0, 'betagishankipurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2154, 237, 'Dashmina', 'দশমিনা', 0, 'dashminaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2155, 237, 'Baharampur', 'বহরমপুর', 0, 'baharampurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2156, 238, 'Chakamaia', 'চাকামইয়া', 0, 'chakamaiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2157, 238, 'Tiakhali', 'টিয়াখালী', 0, 'tiakhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2158, 238, 'Lalua', 'লালুয়া', 0, 'laluaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2159, 238, 'Dhankhali', 'ধানখালী', 0, 'dhankhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2160, 238, 'Mithagonj', 'মিঠাগঞ্জ', 0, 'mithagonjup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2161, 238, 'Nilgonj', 'নীলগঞ্জ', 0, 'nilgonjup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2162, 238, 'Dulaser', 'ধুলাসার', 0, 'dulaserup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2163, 238, 'Latachapli', 'লতাচাপলী', 0, 'latachapliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2164, 238, 'Mahipur', 'মহিপুর', 0, 'mahipurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2165, 238, 'Dalbugonj', 'ডালবুগঞ্জ', 0, 'dalbugonjup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2166, 238, 'Baliatali', 'বালিয়াতলী', 0, 'baliataliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2167, 238, 'Champapur', 'চম্পাপুর', 0, 'champapurup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2168, 239, 'Madhabkhali', 'মাধবখালী', 0, 'madhabkhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2169, 239, 'Mirzaganj', 'মির্জাগঞ্জ', 0, 'mirzaganjup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2170, 239, 'Amragachia', 'আমড়াগাছিয়া', 0, 'amragachiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2171, 239, 'Deuli Subidkhali', 'দেউলী সুবিদখালী', 0, 'deulisubidkhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2172, 239, 'Kakrabunia', 'কাকড়াবুনিয়া', 0, 'kakrabuniaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2173, 239, 'Majidbaria', 'মজিদবাড়িয়া', 0, 'majidbariaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2174, 240, 'Amkhola', 'আমখোলা', 0, 'amkholaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2175, 240, 'Golkhali', 'গোলখালী', 0, 'golkhaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2176, 240, 'Galachipa', 'গলাচিপা', 0, 'galachipaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2177, 240, 'Panpatty', 'পানপট্টি', 0, 'panpattyup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2178, 240, 'Ratandi Taltali', 'রতনদী তালতলী', 0, 'ratanditaltaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2179, 240, 'Dakua', 'ডাকুয়া', 0, 'dakuaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2180, 240, 'Chiknikandi', 'চিকনিকান্দী', 0, 'chiknikandiup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2181, 240, 'Gazalia', 'গজালিয়া', 0, 'gazaliaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2182, 240, 'Charkajol', 'চরকাজল', 0, 'charkajolup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2183, 240, 'Charbiswas', 'চরবিশ্বাস', 0, 'charbiswasup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2184, 240, 'Bakulbaria', 'বকুলবাড়ীয়া', 0, 'bakulbariaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2185, 240, 'Kalagachhia', 'কলাগাছিয়া', 0, 'kalagachhiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2186, 241, 'Rangabali', 'রাঙ্গাবালী', 0, 'rangabaliup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2187, 241, 'Barobaisdia', 'বড়বাইশদিয়া', 0, 'barobaisdiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2188, 241, 'Chattobaisdia', 'ছোটবাইশদিয়া', 0, 'chattobaisdiaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2189, 241, 'Charmontaz', 'চরমোন্তাজ', 0, 'charmontaz.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2190, 241, 'Chalitabunia', 'চালিতাবুনিয়া', 0, 'chalitabuniaup.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2191, 242, 'Shikder Mallik', 'শিকদার মল্লিক', 0, 'shikdermallikup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2192, 242, 'Kodomtala', 'কদমতলা', 0, 'kodomtalaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2193, 242, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2194, 242, 'Kolakhali', 'কলাখালী', 0, 'kolakhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2195, 242, 'Tona', 'টোনা', 0, 'tonaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2196, 242, 'Shariktola', 'শরিকতলা', 0, 'shariktolaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2197, 242, 'Shankorpasa', 'শংকরপাশা', 0, 'shankorpasaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2198, 243, 'Mativangga', 'মাটিভাংগা', 0, 'mativanggaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2199, 243, 'Malikhali', 'মালিখালী', 0, 'malikhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2200, 243, 'Daulbari Dobra', 'দেউলবাড়ী দোবড়া', 0, 'daulbaridobraup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2201, 243, 'Dirgha', 'দীর্ঘা', 0, 'dirghaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2202, 243, 'Kolardoania', 'কলারদোয়ানিয়া', 0, 'kolardoaniaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2203, 243, 'Sriramkathi', 'শ্রীরামকাঠী', 0, 'sriramkathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2204, 243, 'Shakhmatia', 'সেখমাটিয়া', 0, 'shakhmatiaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2205, 243, 'Nazirpur Sadar', 'নাজিরপুর সদর', 0, 'nazirpursadarup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2206, 243, 'Shakharikathi', 'শাখারীকাঠী', 0, 'shakharikathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2207, 244, 'Sayna Rogunathpur', 'সয়না রঘুনাথপুর', 0, 'saynarogunathpurup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2208, 244, 'Amrazuri', 'আমড়াজুড়ি', 0, 'amrazuriup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2209, 244, 'Kawkhali Sadar', 'কাউখালি সদর', 0, 'kawkhalisadarup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2210, 244, 'Chirapara', 'চিরাপাড়া', 0, 'chiraparaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2211, 244, 'Shialkhathi', 'শিয়ালকাঠী', 0, 'shialkhathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2212, 245, 'Balipara', 'বালিপাড়া', 0, 'baliparaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2213, 245, 'Pattashi', 'পত্তাশি', 0, 'pattashiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2214, 245, 'Parerhat', 'পাড়েরহাট', 0, 'parerhatup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2215, 246, 'Vitabaria', 'ভিটাবাড়িয়া', 0, 'vitabariaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2216, 246, 'Nodmulla', 'নদমূলা শিয়ালকাঠী', 0, 'nodmullaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2217, 246, 'Telikhali', 'তেলিখালী', 0, 'telikhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2218, 246, 'Ekree', 'ইকড়ী', 0, 'ekreeup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2219, 246, 'Dhaoa', 'ধাওয়া', 0, 'dhaoaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2220, 246, 'Vandaria Sadar', 'ভান্ডারিয়া সদর', 0, 'vandariasadarup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2221, 246, 'Gouripur', 'গৌরীপুর', 0, 'gouripurup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2222, 247, 'Tuskhali', 'তুষখালী', 0, 'tuskhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2223, 247, 'Dhanisafa', 'ধানীসাফা', 0, 'dhanisafaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2224, 247, 'Mirukhali', 'মিরুখালী', 0, 'mirukhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2225, 247, 'Tikikata', 'টিকিকাটা', 0, 'tikikataup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2226, 247, 'Betmor Rajpara', 'বেতমোর রাজপাড়া', 0, 'betmorrajparaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2227, 247, 'Amragachia', 'আমড়াগাছিয়া', 0, 'amragachiaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2228, 247, 'Shapleza', 'শাপলেজা', 0, 'shaplezaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2229, 247, 'Daudkhali', 'দাউদখালী', 0, 'daudkhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2230, 247, 'Mathbaria', 'মঠবাড়িয়া', 0, 'mathbariaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2231, 247, 'Baramasua', 'বড়মাছুয়া', 0, 'baramasuaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2232, 247, 'Haltagulishakhali', 'হলতাগুলিশাখালী', 0, 'haltagulishakhaliup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2233, 248, 'Boldia', 'বলদিয়া', 0, 'boldiaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2234, 248, 'Sohagdal', 'সোহাগদল', 0, 'sohagdalup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2235, 248, 'Atghorkuriana', 'আটঘর কুড়িয়ানা', 0, 'atghorkurianaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2236, 248, 'Jolabari', 'জলাবাড়ী', 0, 'jolabariup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2237, 248, 'Doyhary', 'দৈহারী', 0, 'doyharyup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2238, 248, 'Guarekha', 'গুয়ারেখা', 0, 'guarekhaup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2239, 248, 'Somudoykathi', 'সমুদয়কাঠী', 0, 'somudoykathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2240, 248, 'Sutiakathi', 'সুটিয়াকাঠী', 0, 'sutiakathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2241, 248, 'Sarengkathi', 'সারেংকাঠী', 0, 'sarengkathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2242, 248, 'Shorupkathi', 'স্বরুপকাঠী', 0, 'shorupkathiup.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2243, 249, 'Raipasha Karapur', 'রায়পাশা কড়াপুর', 0, 'raipashakarapurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2244, 249, 'Kashipur', 'কাশীপুর', 0, 'kashipurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2245, 249, 'Charbaria', 'চরবাড়িয়া', 0, 'charbariaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2246, 249, 'Shyastabad', 'সায়েস্তাবাদ', 0, 'shyastabadup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2247, 249, 'Charmonai', 'চরমোনাই', 0, 'charmonaiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2248, 249, 'Zagua', 'জাগুয়া', 0, 'zaguaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2249, 249, 'Charcowa', 'চরকাউয়া', 0, 'charcowaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2250, 249, 'Chandpura', 'চাঁদপুরা', 0, 'chandpuraup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2251, 249, 'Tungibaria', 'টুঙ্গীবাড়িয়া', 0, 'tungibariaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2252, 249, 'Chandramohan', 'চন্দ্রমোহন', 0, 'chandramohanup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2253, 250, 'Charamaddi', 'চরামদ্দি', 0, 'charamaddiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2254, 250, 'Charade', 'চরাদি', 0, 'charadeup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2255, 250, 'Darial', 'দাড়িয়াল', 0, 'darialup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2256, 250, 'Dudhal', 'দুধল', 0, 'dudhalup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2257, 250, 'Durgapasha', 'দুর্গাপাশা', 0, 'durgapashaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2258, 250, 'Faridpur', 'ফরিদপুর', 0, 'faridpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2259, 250, 'Kabai', 'কবাই', 0, 'kabaiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2260, 250, 'Nalua', 'নলুয়া', 0, 'naluaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2261, 250, 'Kalashkathi', 'কলসকাঠী', 0, 'kalashkathiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2262, 250, 'Garuria', 'গারুরিয়া', 0, 'garuriaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2263, 250, 'Bharpasha', 'ভরপাশা', 0, 'bharpashaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2264, 250, 'Rangasree', 'রঙ্গশ্রী', 0, 'rangasreeup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2265, 250, 'Padreeshibpur', 'পাদ্রিশিবপুর', 0, 'padreeshibpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2266, 250, 'Niamoti', 'নিয়ামতি', 0, 'niamotiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2267, 251, 'Jahangir Nagar', 'জাহাঙ্গীর নগর', 0, 'jahangirnagorup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2268, 251, 'Kaderpur', 'কেদারপুর', 0, 'kaderpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2269, 251, 'Deherhoti', 'দেহেরগতি', 0, 'deherhotiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2270, 251, 'Chandpasha', 'চাঁদপাশা', 0, 'chandpashaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2271, 251, 'Rahamtpur', 'রহমতপুর', 0, 'rahamtpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2272, 251, 'Madhbpasha', 'মাধবপাশা', 0, 'madhbpashaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2273, 252, 'Shatla', 'সাতলা', 0, 'shatlaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2274, 252, 'Harta', 'হারতা', 0, 'hartaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2275, 252, 'Jalla', 'জল্লা', 0, 'jallaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2276, 252, 'Otra', 'ওটরা', 0, 'otraup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2277, 252, 'Sholok', 'শোলক', 0, 'sholokup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2278, 252, 'Barakhota', 'বরাকোঠা', 0, 'barakhotaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2279, 252, 'Bamrail', 'বামরাইল', 0, 'bamrailup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2280, 252, 'Shikerpur Wazirpur', 'শিকারপুর উজিরপুর', 0, 'shikerpurwazirpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2281, 252, 'Gouthia', 'গুঠিয়া', 0, 'gouthiaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2282, 253, 'Bisharkandi', 'বিশারকান্দি', 0, 'bisharkandiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2283, 253, 'Illuhar', 'ইলুহার', 0, 'illuharup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2284, 253, 'Sayedkathi', 'সৈয়দকাঠী', 0, 'sayedkathiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2285, 253, 'Chakhar', 'চাখার', 0, 'chakharup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2286, 253, 'Saliabakpur', 'সলিয়াবাকপুর', 0, 'saliabakpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2287, 253, 'Baishari', 'বাইশারী', 0, 'baishariup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2288, 253, 'Banaripara', 'বানারিপাড়া', 0, 'banariparaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2289, 253, 'Udykhati', 'উদয়কাঠী', 0, 'udykhatiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2290, 254, 'Khanjapur', 'খাঞ্জাপুর', 0, 'khanjapurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2291, 254, 'Barthi', 'বার্থী', 0, 'barthiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2292, 254, 'Chandshi', 'চাঁদশী', 0, 'chandshiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2293, 254, 'Mahilara', 'মাহিলারা', 0, 'mahilaraup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2294, 254, 'Nalchira', 'নলচিড়া', 0, 'nalchiraup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2295, 254, 'Batajore', 'বাটাজোর', 0, 'batajoreup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2296, 254, 'Sarikal', 'সরিকল', 0, 'sarikalup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2297, 255, 'Rajihar', 'রাজিহার', 0, 'rajiharup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2298, 255, 'Bakal', 'বাকাল', 0, 'bakalup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2299, 255, 'Bagdha', 'বাগধা', 0, 'bagdhaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2300, 255, 'Goila', 'গৈলা', 0, 'goilaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2301, 255, 'Ratnapur', 'রত্নপুর', 0, 'ratnapurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2302, 256, 'Andarmanik', 'আন্দারমানিক', 0, 'andarmanikup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2303, 256, 'Lata', 'লতা', 0, 'lataup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2304, 256, 'Charakkorea', 'চরএককরিয়া', 0, 'charakkoreaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2305, 256, 'Ulania', 'উলানিয়া', 0, 'ulaniaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2306, 256, 'Mehendigong', 'মেহেন্দিগঞ্জ', 0, 'mehendigongup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2307, 256, 'Biddanandapur', 'বিদ্যানন্দনপুর', 0, 'biddanandapurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2308, 256, 'Bhashanchar', 'ভাষানচর', 0, 'bhashancharup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2309, 256, 'Jangalia', 'জাঙ্গালিয়া', 0, 'jangaliaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2310, 256, 'Alimabad', 'আলিমাবাদ', 0, 'alimabadup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2311, 256, 'Chandpur', 'চানপুর', 0, 'chandpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2312, 256, 'Darirchar Khajuria', 'দড়িরচর খাজুরিয়া', 0, 'darircharkhajuriaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2313, 256, 'Gobindapur', 'গোবিন্দপুর', 0, 'gobindapurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2314, 256, 'Chargopalpur', 'চরগোপালপুর', 0, 'chargopalpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2315, 257, 'Batamara', 'বাটামারা', 0, 'batamaraup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2316, 257, 'Nazirpur', 'নাজিরপুর', 0, 'nazirpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2317, 257, 'Safipur', 'সফিপুর', 0, 'safipurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2318, 257, 'Gaschua', 'গাছুয়া', 0, 'gaschuaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2319, 257, 'Charkalekha', 'চরকালেখা', 0, 'charkalekhaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2320, 257, 'Muladi', 'মুলাদী', 0, 'muladiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2321, 257, 'Kazirchar', 'কাজিরচর', 0, 'kazircharup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2322, 258, 'Harinathpur', 'হরিনাথপুর', 0, 'harinathpurup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2323, 258, 'Memania', 'মেমানিয়া', 0, 'memaniaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2324, 258, 'Guabaria', 'গুয়াবাড়িয়া', 0, 'guabariaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2325, 258, 'Barjalia', 'বড়জালিয়া', 0, 'barjaliaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2326, 258, 'Hizla Gourabdi', 'হিজলা গৌরাব্দি', 0, 'hizlagourabdiup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2327, 258, 'Dhulkhola', 'ধুলখোলা', 0, 'dhulkholaup.barisal.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2328, 259, 'Razapur', 'রাজাপুর', 0, 'razapurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2329, 259, 'Ilisha', 'ইলিশা', 0, 'ilishaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2330, 259, 'Westilisa', 'পশ্চিম ইলিশা', 0, 'westilisaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2331, 259, 'Kachia', 'কাচিয়া', 0, 'kachiaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2332, 259, 'Bapta', 'বাপ্তা', 0, 'baptaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2333, 259, 'Dhania', 'ধনিয়া', 0, 'dhaniaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2334, 259, 'Shibpur', 'শিবপুর', 0, 'shibpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2335, 259, 'Alinagor', 'আলীনগর', 0, 'alinagorup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2336, 259, 'Charshamya', 'চরসামাইয়া', 0, 'charshamyaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2337, 259, 'Vhelumia', ' ভেলুমিয়া', 0, 'vhelumiaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2338, 259, 'Vheduria', 'ভেদুরিয়া', 0, 'vheduriaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2339, 259, 'North Digholdi', 'উত্তর দিঘলদী', 0, 'northdigholdiup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2340, 259, 'South Digholdi', 'দক্ষিণ দিঘলদী', 0, 'southdigholdiup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2341, 260, 'Boromanika', 'বড় মানিকা', 0, 'boromanikaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2342, 260, 'Deula', 'দেউলা', 0, 'deulaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2343, 260, 'Kutuba', 'কুতুবা', 0, 'kutubaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2344, 260, 'Pakshia', 'পক্ষিয়া', 0, 'pakshiaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2345, 260, 'Kachia', 'কাচিয়া', 0, 'kachiaup4.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2346, 261, 'Osmangonj', 'ওসমানগঞ্জ', 0, 'osmangonjup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2347, 261, 'Aslampur', 'আছলামপুর', 0, 'aslampurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2348, 261, 'Zinnagor', 'জিন্নাগড়', 0, 'zinnagorup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2349, 261, 'Aminabad', 'আমিনাবাদ', 0, 'aminabadup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2350, 261, 'Nilkomol', 'নীলকমল', 0, 'nilkomolup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2351, 261, 'Charmadraj', 'চরমাদ্রাজ', 0, 'charmadrajup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2352, 261, 'Awajpur', 'আওয়াজপুর', 0, 'awajpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2353, 261, 'Awajpur', 'আওয়াজপুর', 0, 'awajpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2354, 261, 'Charkolmi', 'চরকলমী', 0, 'charkolmiup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2355, 261, 'Charmanika', 'চরমানিকা', 0, 'charmanikaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2356, 261, 'Hazarigonj', 'হাজারীগঞ্জ', 0, 'hazarigonjup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2357, 261, 'Jahanpur', 'জাহানপুর', 0, 'jahanpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2358, 261, 'Nurabad', 'নুরাবাদ', 0, 'nurabadup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2359, 261, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2360, 261, 'Kukrimukri', 'কুকরীমূকরী', 0, 'kukrimukriup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2361, 261, 'Abubakarpur', 'আবুবকরপুর', 0, 'abubakarpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2362, 261, 'Abdullahpur', 'আবদুল্লাহ', 0, 'abdullahpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2363, 261, 'Nazrulnagar', 'নজরুল নগর', 0, 'nazrulnagarup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2364, 261, 'Mujibnagar', 'মুজিব নগর', 0, 'mujibnagarup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2365, 261, 'Dalchar', 'ঢালচর', 0, 'dalcharup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2366, 262, 'Madanpur', 'মদনপুর', 0, 'madanpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2367, 262, 'Madua', 'মেদুয়া', 0, 'maduaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2368, 262, 'Charpata', 'চরপাতা', 0, 'charpataup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2369, 262, 'North Joy Nagar', 'উত্তর জয়নগর', 0, 'northjoynagarup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2370, 262, 'South Joy Nagar', 'দক্ষিন জয়নগর', 0, 'southjoynagarup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2371, 262, 'Char Khalipa', 'চর খলিফা', 0, 'charkhalipaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2372, 262, 'Sayedpur', 'সৈয়দপুর', 0, 'sayedpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2373, 262, 'Hazipur', 'হাজীপুর', 0, 'hazipurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2374, 262, 'Vhovanipur', 'ভবানীপুর', 0, 'vhovanipurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2375, 263, 'Hazirhat', 'হাজীর হাট', 0, 'hazirhatup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2376, 263, 'Monpura', 'মনপুরা', 0, 'monpuraup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2377, 263, 'North Sakuchia', 'উত্তর সাকুচিয়া', 0, 'sakuchianorthup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2378, 263, 'South Sakuchia', 'দক্ষিন সাকুচিয়া', 0, 'sakuchiasouthup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2379, 264, 'Chanchra', 'চাচঁড়া', 0, 'chanchraup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2380, 264, 'Shambupur', 'শম্ভুপুর', 0, 'shambupurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2381, 264, 'Sonapur', 'সোনাপুর', 0, 'sonapurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2382, 264, 'Chadpur', 'চাঁদপুর', 0, 'chadpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2383, 264, 'Baro Molongchora', 'বড় মলংচড়া', 0, 'baromolongchoraup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2384, 265, 'Badarpur', 'বদরপুর', 0, 'badarpurup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2385, 265, 'Charbhuta', 'চরভূতা', 0, 'charbhutaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2386, 265, 'Kalma', ' কালমা', 0, 'kalmaup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2387, 265, 'Dholigour Nagar', 'ধলীগৌর নগর', 0, 'dholigournagarup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2388, 265, 'Lalmohan', 'লালমোহন', 0, 'lalmohanup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2389, 265, 'Lord Hardinge', 'লর্ড হার্ডিঞ্জ', 0, 'lordhardingeup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2390, 265, 'Ramagonj', 'রমাগঞ্জ', 0, 'ramagonjup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2391, 265, 'Paschim Char Umed', 'পশ্চিম চর উমেদ', 0, 'paschimcharumedup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2392, 265, 'Farajgonj', 'ফরাজগঞ্জ', 0, 'farajgonjup.bhola.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2393, 266, 'Amtali', 'আমতলী', 0, 'amtaliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2394, 266, 'Gulishakhali', 'গুলিশাখালী', 0, 'gulishakhaliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2395, 266, 'Athrogasia', 'আঠারগাছিয়া', 0, 'athrogasiaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2396, 266, 'Kukua', 'কুকুয়া', 0, 'kukuaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2397, 266, 'Haldia', 'হলদিয়া', 0, 'haldiaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2398, 266, 'Chotobogi', 'ছোটবগী', 0, 'chotobogiup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2399, 266, 'Arpangasia', 'আড়পাঙ্গাশিয়া', 0, 'arpangasiaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2400, 266, 'Chowra', 'চাওড়া', 0, 'chowraup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2401, 267, 'M. Baliatali', 'এম. বালিয়াতলী', 0, 'm.baliataliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2402, 267, 'Noltona', 'নলটোনা', 0, 'noltonaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2403, 267, 'Bodorkhali', 'বদরখালী', 0, 'bodorkhaliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2404, 267, 'Gowrichanna', 'গৌরিচন্না', 0, 'gowrichannaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2405, 267, 'Fuljhuri', 'ফুলঝুড়ি', 0, 'fuljhuriup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2406, 267, 'Keorabunia', 'কেওড়াবুনিয়া', 0, 'keorabuniaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2407, 267, 'Ayla Patakata', 'আয়লা পাতাকাটা', 0, 'aylaPatakataup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2408, 267, 'Burirchor', 'বুড়িরচর', 0, 'burirchorup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2409, 267, 'Dhalua', 'ঢলুয়া', 0, 'dhaluaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2410, 267, 'Barguna', 'বরগুনা', 0, 'bargunaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2411, 268, 'Bibichini', 'বিবিচিন', 0, 'bibichiniup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2412, 268, 'Betagi', 'বেতাগী', 0, 'betagiup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2413, 268, 'Hosnabad', 'হোসনাবাদ', 0, 'hosnabadup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2414, 268, 'Mokamia', 'মোকামিয়া', 0, 'mokamiaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2415, 268, 'Buramajumder', 'বুড়ামজুমদার', 0, 'buramajumderup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2416, 268, 'Kazirabad', 'কাজীরাবাদ', 0, 'kazirabadup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2417, 268, 'Sarisamuri', 'সরিষামুড়ী', 0, 'sarisamuriup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2418, 269, 'Bukabunia', 'বুকাবুনিয়া', 0, 'bukabuniaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2419, 269, 'Bamna', 'বামনা', 0, 'bamnaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2420, 269, 'Ramna', 'রামনা', 0, 'ramnaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2421, 269, 'Doutola', 'ডৌয়াতলা', 0, 'doutolaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2422, 270, 'Raihanpur', 'রায়হানপুর', 0, 'raihanpurup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2423, 270, 'Nachnapara', 'নাচনাপাড়া', 0, 'nachnaparaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2424, 270, 'Charduany', 'চরদুয়ানী', 0, 'charduanyup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2425, 270, 'Patharghata', 'পাথরঘাটা', 0, 'patharghataup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2426, 270, 'Kalmegha', 'কালমেঘা', 0, 'kalmeghaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2427, 270, 'Kakchira', 'কাকচিঢ়া', 0, 'kakchiraup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2428, 270, 'Kathaltali', 'কাঠালতলী', 0, 'kathaltaliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2429, 271, 'Karibaria', 'কড়ইবাড়ীয়া', 0, 'karibariaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2430, 271, 'Panchakoralia', 'পচাকোড়ালিয়া', 0, 'panchakoraliaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2431, 271, 'Barabagi', 'বড়বগি', 0, 'barabagiup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2432, 271, 'Chhotabagi', 'ছোটবগি', 0, 'chhotabagiup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2433, 271, 'Nishanbaria', 'নিশানবাড়ীয়া', 0, 'nishanbariaup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2434, 271, 'Sarikkhali', 'শারিকখালি', 0, 'sarikkhaliup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2435, 271, 'Sonakata', 'সোনাকাটা', 0, 'sonakataup.barguna.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2436, 284, 'Tazpur', 'তাজপুর', 0, 'tazpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2437, 284, 'Umorpur', 'উমরপুর', 0, 'umorpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2438, 284, 'West Poilanpur', 'পশ্চিম পৈলনপুর', 0, 'westpoilanpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2439, 272, 'East Poilanpur', 'পূর্ব পৈলনপুর', 0, 'eastpoilanpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2440, 272, 'Boaljur', 'বোয়ালজুর', 0, 'boaljurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2441, 284, 'Burungabazar', 'বুরুঙ্গাবাজার', 0, 'burungabazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2442, 284, 'Goalabazar', 'গোয়ালাবাজার', 0, 'goalabazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2443, 284, 'Doyamir', 'দয়ামীর', 0, 'doyamirup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2444, 284, 'Usmanpur', 'উসমানপুর', 0, 'usmanpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2445, 272, 'Dewanbazar', 'দেওয়ান বাজার', 0, 'dewanbazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2446, 272, 'West Gouripur', 'পশ্চিম গৌরীপুর', 0, 'westgouripurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2447, 272, 'East Gouripur', 'পূর্ব গৌরীপুর', 0, 'eastgouripurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2448, 272, 'Balaganj', 'বালাগঞ্জ', 0, 'balaganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2449, 284, 'Sadipur', 'সাদিরপুর', 0, 'sadipurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2450, 273, 'Tilpara', 'তিলপাড়া', 0, 'tilparaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2451, 273, 'Alinagar', 'আলীনগর', 0, 'alinagarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2452, 273, 'Charkhai', 'চরখাই', 0, 'charkhaiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2453, 273, 'Dubag', 'দুবাগ', 0, 'dubagup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2454, 273, 'Sheola', 'শেওলা', 0, 'sheolaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2455, 273, 'Kurarbazar', 'কুড়ারবাজার', 0, 'kurarbazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2456, 273, 'Mathiura', 'মাথিউরা', 0, 'mathiuraup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2457, 273, 'Mullapur', 'মোল্লাপুর', 0, 'mullapurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2458, 273, 'Muria', 'মুড়িয়া', 0, 'muriaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2459, 273, 'Lauta', 'লাউতা', 0, 'lautaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2460, 274, 'Rampasha', 'রামপাশা', 0, 'rampashaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2461, 274, 'Lamakazi', 'লামাকাজী', 0, 'lamakaziup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2462, 274, 'Khajanchi', 'খাজাঞ্চী', 0, 'khajanchiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2463, 274, 'Alankari', 'অলংকারী', 0, 'alankariup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2464, 274, 'Dewkalash', 'দেওকলস', 0, 'dewkalashup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2465, 274, 'Bishwanath', 'বিশ্বনাথ', 0, 'bishwanathup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2466, 274, 'Doshghar', 'দশঘর', 0, 'doshgharup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2467, 274, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2468, 275, 'Telikhal', 'তেলিখাল', 0, 'telikhalup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2469, 275, 'Islampur Paschim', 'ইসলামপুর পশ্চিম', 0, 'islampurpaschimup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2470, 275, 'Islampur Purba', 'ইসলামপুর পূর্ব', 0, 'islampurpurbaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2471, 275, 'Isakalas', 'ইসাকলস', 0, 'isakalasup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2472, 275, 'Uttor Ronikhai', 'উত্তর রনিখাই', 0, 'uttorronikhaiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2473, 275, 'Dakkin Ronikhai', 'দক্ষিন রনিখাই', 0, 'dakkinronikhaiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2474, 276, 'Ghilachora', 'ঘিলাছড়া', 0, 'ghilachoraup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2475, 276, 'Fenchuganj', 'ফেঞ্চুগঞ্জ', 0, '1nofenchuganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2476, 276, 'Uttar Kushiara', 'উত্তর কুশিয়ারা', 0, 'uttarkushiaraup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2477, 276, 'Uttar Fenchuganj', 'উত্তর ফেঞ্চুগঞ্জ', 0, 'uttarfenchuganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2478, 276, 'Maijgaon', 'মাইজগাঁও', 0, 'maijgaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2479, 277, 'Golapganj', 'গোলাপগঞ্জ', 0, 'golapganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2480, 277, 'Fulbari', 'ফুলবাড়ী', 0, 'fulbariup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2481, 277, 'Lakshmipasha', 'লক্ষ্মীপাশা', 0, 'lakshmipashaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2482, 277, 'Budhbaribazar', 'বুধবারীবাজার', 0, 'budhbaribazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2483, 277, 'Dhakadakshin', 'ঢাকাদক্ষিন', 0, 'dhakadakshinup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2484, 277, 'Sharifganj', 'শরিফগঞ্জ', 0, 'sharifganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2485, 277, 'Uttar Badepasha', 'উত্তর বাদেপাশা', 0, 'uttarbadepashaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2486, 277, 'Lakshanaband', 'লক্ষনাবন্দ', 0, 'lakshanabandup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2487, 277, 'Bhadeshwar', 'ভাদেশ্বর', 0, 'bhadeshwarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2488, 277, 'West Amura', 'পশ্চিম আমুরা', 0, 'westamuraup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2489, 278, 'Fothepur', 'ফতেপুর', 0, 'fothepurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2490, 278, 'Rustampur', 'রুস্তমপুর', 0, 'rustampurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2491, 278, 'Paschim Jaflong', 'পশ্চিম জাফলং', 0, 'paschimjaflongup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2492, 278, 'Purba Jaflong', 'পূর্ব জাফলং', 0, 'purbajaflongup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2493, 278, 'Lengura', 'লেঙ্গুড়া', 0, 'lenguraup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2494, 278, 'Alirgaon', 'আলীরগাঁও', 0, 'alirgaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2495, 278, 'Nandirgaon', 'নন্দিরগাঁও', 0, 'nandirgaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2496, 278, 'Towakul', 'তোয়াকুল', 0, 'towakulup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2497, 278, 'Daubari', 'ডৌবাড়ী', 0, 'daubariup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2498, 279, 'Nijpat', 'নিজপাট', 0, 'nijpatup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2499, 279, 'Jaintapur', 'জৈন্তাপুর', 0, 'jaintapurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2500, 279, 'Charikatha', 'চারিকাটা', 0, 'charikathaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2501, 279, 'Darbast', 'দরবস্ত', 0, 'darbastup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2502, 279, 'Fatehpur', 'ফতেপুর', 0, 'fatehpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2503, 279, 'Chiknagul', 'চিকনাগুল', 0, 'chiknagulup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2504, 280, 'Rajagonj', 'রাজাগঞ্জ', 0, 'rajagonjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2505, 280, 'Lakshiprashad Purbo', 'লক্ষীপ্রাসাদ পূর্ব', 0, 'lakshiprashadpurboup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2506, 280, 'Lakshiprashad Pashim', 'লক্ষীপ্রাসাদ পশ্চিম', 0, 'lakshiprashadpashimup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2507, 280, 'Digirpar Purbo', 'দিঘিরপার পূর্ব', 0, 'digirparpurboup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2508, 280, 'Satbakh', 'সাতবাক', 0, 'satbakhup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2509, 280, 'Barachotul', 'বড়চতুল', 0, 'barachotulup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2510, 280, 'Kanaighat', 'কানাইঘাট', 0, 'kanaighatup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2511, 280, 'Dakhin Banigram', 'দক্ষিন বানিগ্রাম', 0, 'dakhinbanigramup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2512, 280, 'Jinghabari', 'ঝিঙ্গাবাড়ী', 0, 'jinghabariup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2513, 281, 'Jalalabad', 'জালালাবাদ', 0, 'jalalabadup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2514, 281, 'Hatkhula', 'হাটখোলা', 0, 'hatkhulaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2515, 281, 'Khadimnagar', 'খাদিমনগর', 0, 'khadimnagarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2516, 281, 'Khadimpara', 'খাদিমপাড়া', 0, 'khadimparaup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2517, 281, 'Tultikor', 'টুলটিকর', 0, 'tultikorup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2518, 281, 'Tukerbazar', 'টুকেরবাজার', 0, 'tukerbazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2519, 281, 'Mugolgaon', 'মোগলগাও', 0, 'mugolgaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2520, 281, 'Kandigaon', 'কান্দিগাও', 0, 'kandigaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2521, 282, 'Manikpur', 'মানিকপুর', 0, 'manikpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2522, 282, 'Sultanpur', 'সুলতানপুর', 0, 'sultanpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2523, 282, 'Barohal', 'বারহাল', 0, 'barohalup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2524, 282, 'Birorsri', 'বিরশ্রী', 0, 'birorsriup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2525, 282, 'Kajalshah', 'কাজলশার', 0, 'kajalshahup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2526, 282, 'Kolachora', 'কলাছড়া', 0, 'kolachora.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2527, 282, 'Zakiganj', 'জকিগঞ্জ', 0, 'zakiganjup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2528, 282, 'Barothakuri', 'বারঠাকুরী', 0, 'barothakuriup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2529, 282, 'Kaskanakpur', 'কসকনকপুর', 0, 'kaskanakpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2530, 283, 'Lalabazar', 'লালাবাজার', 0, 'lalabazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2531, 283, 'Moglabazar', 'মোগলাবাজার', 0, 'moglabazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2532, 283, 'Boroikandi', 'বড়ইকান্দি', 0, 'boroikandiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2533, 283, 'Silam', 'সিলাম', 0, 'silamup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2534, 283, 'Daudpur', 'দাউদপুর', 0, 'daudpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2535, 283, 'Mollargaon', 'মোল্লারগাঁও', 0, 'mollargaonup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2536, 283, 'Kuchai', 'কুচাই', 0, 'kuchaiup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2537, 283, 'Kamalbazar', 'কামালবাজার', 0, 'kamalbazarup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2538, 283, 'Jalalpur', 'জালালপুর', 0, 'jalalpurup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2539, 283, 'Tetli', 'তেতলী', 0, 'tetliup.sylhet.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2540, 285, 'Talimpur', 'তালিমপুর', 0, 'talimpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2541, 285, 'Borni', 'বর্ণি', 0, 'borniup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2542, 285, 'Dasherbazar', 'দাসেরবাজার', 0, 'dasherbazarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2543, 285, 'Nizbahadurpur', 'নিজবাহাদুরপুর', 0, 'nizbahadurpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2544, 285, 'Uttar Shahbajpur', 'উত্তর শাহবাজপুর', 0, 'shahbajpuruttarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2545, 285, 'Dakkhin Shahbajpur', 'দক্ষিণ শাহবাজপুর', 0, 'shahbajpurdakshinup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2546, 285, 'Talimpur', 'তালিমপুর', 0, 'talimpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2547, 285, 'Baralekha', 'বড়লেখা', 0, 'baralekhaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2548, 285, 'Dakshinbhag Uttar', 'দক্ষিণভাগ (উত্তর)', 0, 'dakshinbhaguttarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2549, 285, 'Dakshinbhag Dakkhin', 'দক্ষিণভাগ (দক্ষিণ)', 0, 'dakshinbhagdakshinup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2550, 285, 'Sujanagar', 'সুজানগর', 0, 'sujanagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2551, 286, 'Adampur', 'আদমপুর', 0, 'adampurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2552, 286, 'Patanushar', 'পতনঊষার', 0, 'patanusharup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2553, 286, 'Madhabpur', 'মাধবপুর', 0, 'madhabpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2554, 286, 'Rahimpur', 'রহিমপুর', 0, 'rahimpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2555, 286, 'Shamshernagar', 'শমশেরনগর', 0, 'shamshernagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2556, 286, 'Kamalgonj', 'কমলগঞ্জ', 0, 'kamalgonjup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2557, 286, 'Islampur', 'ইসলামপুর', 0, 'islampurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2558, 286, 'Munshibazar', 'মুন্সিবাজার', 0, 'munshibazarup3.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2559, 286, 'Alinagar', 'আলী নগর', 0, 'alinagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2560, 287, 'Baramchal', 'বরমচাল', 0, 'baramchalup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2561, 287, 'Bhukshimail', 'ভূকশিমইল', 0, 'bhukshimailup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2562, 287, 'Joychandi', 'জয়চন্ডি', 0, 'joychandiup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2563, 287, 'Brammanbazar', 'ব্রাহ্মণবাজার', 0, 'brammanbazarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2564, 287, 'Kadipur', 'কাদিপুর', 0, 'kadipurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2565, 287, 'Kulaura', 'কুলাউড়া', 0, 'kulauraup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2566, 287, 'Rauthgaon', 'রাউৎগাঁও', 0, 'rauthgaonup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2567, 287, 'Tilagaon', 'টিলাগাঁও', 0, 'tilagaonup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2568, 287, 'Sharifpur', 'শরীফপুর', 0, 'sharifpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2569, 287, 'Prithimpassa', 'পৃথিমপাশা', 0, 'prithimpassaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2570, 287, 'Kormodha', 'কর্মধা', 0, 'kormodhaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2571, 287, 'Bhatera', 'ভাটেরা', 0, 'bhateraup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2572, 287, 'Hazipur', 'হাজীপুর', 0, 'hazipurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2573, 288, 'Amtail', 'আমতৈল', 0, 'amtailup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2574, 288, 'Khalilpur', 'খলিলপুর', 0, 'khalilpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2575, 288, 'Monumukh', 'মনুমুখ', 0, 'monumukhup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2576, 288, 'Kamalpur', 'কামালপুর', 0, 'kamalpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2577, 288, 'Apar Kagabala', 'আপার কাগাবলা', 0, 'uparkagabalaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2578, 288, 'Akhailkura', 'আখাইলকুড়া', 0, 'akhailkuraup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2579, 288, 'Ekatuna', 'একাটুনা', 0, 'ekatunaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2580, 288, 'Chadnighat', 'চাঁদনীঘাট', 0, 'chadnighatup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2581, 288, 'Konokpur', 'কনকপুর', 0, 'konokpurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2582, 288, 'Nazirabad', 'নাজিরাবাদ', 0, 'nazirabadup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2583, 288, 'Mostafapur', 'মোস্তফাপুর', 0, 'mostafapurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2584, 288, 'Giasnagar', 'গিয়াসনগর', 0, 'giasnagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2585, 289, 'Fotepur', 'ফতেপুর', 0, 'fotepurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2586, 289, 'Uttorbhag', 'উত্তরভাগ', 0, 'uttorbhagup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2587, 289, 'Munsibazar', 'মুন্সিবাজার', 0, 'munsibazarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2588, 289, 'Panchgaon', 'পাঁচগাঁও', 0, 'panchgaonup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2589, 289, 'Rajnagar', 'রাজনগর', 0, 'rajnagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2590, 289, 'Tengra', 'টেংরা', 0, 'tengraup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2591, 289, 'Kamarchak', 'কামারচাক', 0, 'kamarchakup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2592, 289, 'Munsurnagar', 'মনসুরনগর', 0, 'munsurnagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2593, 290, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2594, 290, 'Bhunabir', 'ভূনবীর', 0, 'bhunabirup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2595, 290, 'Sreemangal', 'শ্রীমঙ্গল', 0, 'sreemangalup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2596, 290, 'Sindurkhan', 'সিন্দুরখান', 0, 'sindurkhanup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2597, 290, 'Kalapur', 'কালাপুর', 0, 'kalapurup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2598, 290, 'Ashidron', 'আশিদ্রোন', 0, 'ashidronup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2599, 290, 'Rajghat', 'রাজঘাট', 0, 'rajghatup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2600, 290, 'Kalighat', 'কালীঘাট', 0, 'kalighatup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2601, 290, 'Satgaon', 'সাতগাঁও', 0, 'satgaonup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2602, 291, 'Jafornagar', 'জায়ফরনগর', 0, 'jafornagarup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2603, 291, 'West Juri', 'পশ্চিম জুড়ী', 0, 'westjuriup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2604, 291, 'Gualbari', 'গোয়ালবাড়ী', 0, 'gualbariup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2605, 291, 'Sagornal', 'সাগরনাল', 0, 'sagornalup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2606, 291, 'Fultola', 'ফুলতলা', 0, 'fultolaup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2607, 291, 'Eastjuri', 'পুর্ব জুড়ী', 0, 'eastjuriup.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2608, 292, 'Barabhakoir Paschim', 'বড় ভাকৈর (পশ্চিম)', 0, 'barabhakoirpaschimup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2609, 292, 'Barabhakoir Purba', 'বড় ভাকৈর (পূর্ব)', 0, 'barabhakoirpurbaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2610, 292, 'Inatganj', 'ইনাতগঞ্জ', 0, 'inatganjup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2611, 292, 'Digholbak', 'দীঘলবাক', 0, 'digholbakup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2612, 292, 'Aushkandi', 'আউশকান্দি', 0, 'aushkandiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2613, 292, 'Kurshi', 'কুর্শি', 0, 'kurshiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2614, 292, 'Kargoan', 'করগাঁও', 0, 'kargoanup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2615, 292, 'Nabiganj Sadar', 'নবীগঞ্জ সদর', 0, 'nabiganjsadarup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2616, 292, 'Bausha', 'বাউসা', 0, 'baushaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2617, 292, 'Debparra', 'দেবপাড়া', 0, 'debparraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2618, 292, 'Gaznaipur', 'গজনাইপুর', 0, 'gaznaipurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2619, 292, 'Kaliarbhanga', 'কালিয়ারভাংগা', 0, 'kaliarbhangaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2620, 292, 'Paniumda', 'পানিউমদা', 0, 'paniumdaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2621, 293, 'Snanghat', 'স্নানঘাট', 0, 'snanghatup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2622, 293, 'Putijuri', 'পুটিজুরী', 0, 'putijuriup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2623, 293, 'Satkapon', 'সাতকাপন', 0, 'satkaponup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2624, 293, 'Bahubal Sadar', 'বাহুবল সদর', 0, 'bahubalsadarup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2625, 293, 'Lamatashi', 'লামাতাশী', 0, 'lamatashiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2626, 293, 'Mirpur', 'মিরপুর', 0, 'mirpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2627, 293, 'Bhadeshwar', 'ভাদেশ্বর', 0, 'bhadeshwarup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2628, 294, 'Shibpasha', 'শিবপাশা', 0, 'shibpashaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2629, 294, 'Kakailsao', 'কাকাইলছেও', 0, 'kakailsaoup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2630, 294, 'Ajmiriganj Sadar', 'আজমিরীগঞ্জ সদর', 0, 'ajmiriganjsadarup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2631, 294, 'Badolpur', 'বদলপুর', 0, 'badolpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2632, 294, 'Jolsuka', 'জলসুখা', 0, 'jolsukaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2633, 295, 'Baniachong North East', 'বানিয়াচং উত্তর পূর্ব', 0, 'baniachongnortheastup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2634, 295, 'Baniachong North West', 'বানিয়াচং উত্তর পশ্চিম', 0, 'baniachongnorthwestup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2635, 295, 'Baniachong South East', 'বানিয়াচং দক্ষিণ পূর্ব', 0, 'baniachongsoutheastup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2636, 295, 'Baniachong South West', 'বানিয়াচং দক্ষিণ পশ্চিম', 0, 'baniachongsouthwestup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2637, 295, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpur.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2638, 295, 'Khagaura', 'খাগাউড়া', 0, 'khagauraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2639, 295, 'Baraiuri', 'বড়ইউড়ি', 0, 'baraiuriup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2640, 295, 'Kagapasha', 'কাগাপাশা', 0, 'kagapashaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2641, 295, 'Pukra', 'পুকড়া', 0, 'pukraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2642, 295, 'Subidpur', 'সুবিদপুর', 0, 'subidpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2643, 295, 'Makrampur', 'মক্রমপুর', 0, 'makrampurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2644, 295, 'Sujatpur', 'সুজাতপুর', 0, 'sujatpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2645, 295, 'Mandari', 'মন্দরী', 0, 'mandariup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2646, 295, 'Muradpur', 'মুরাদপুর', 0, 'muradpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2647, 295, 'Pailarkandi', 'পৈলারকান্দি', 0, 'pailarkandiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2648, 296, 'Lakhai', 'লাখাই', 0, 'lakhaiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2649, 296, 'Murakari', 'মোড়াকরি', 0, 'murakariup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2650, 296, 'Muriauk', 'মুড়িয়াউক', 0, 'muriaukup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2651, 296, 'Bamoi', 'বামৈ', 0, 'bamoiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2652, 296, 'Karab', 'করাব', 0, 'karabup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2653, 296, 'Bulla', 'বুল্লা', 0, 'bullaup6.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2654, 297, 'Gazipur', 'গাজীপুর', 0, 'gazipurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2655, 297, 'Ahammadabad', 'আহম্মদাবাদ', 0, 'ahammadabadup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2656, 297, 'Deorgach', 'দেওরগাছ', 0, 'deorgachup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2657, 297, 'Paikpara', 'পাইকপাড়া', 0, 'paikparaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2658, 297, 'Shankhala', 'শানখলা', 0, 'shankhalaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2659, 297, 'Chunarughat', 'চুনারুঘাট', 0, 'chunarughatup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2660, 297, 'Ubahata', 'উবাহাটা', 0, 'ubahataup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2661, 297, 'Shatiajuri', 'সাটিয়াজুরী', 0, 'shatiajuriup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2662, 297, 'Ranigaon', 'রাণীগাঁও', 0, 'ranigaonup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2663, 297, 'Mirashi', 'মিরাশী', 0, 'mirashiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2664, 298, 'Lukra', 'লুকড়া', 0, 'lukraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2665, 298, 'Richi', 'রিচি', 0, 'richiup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2666, 298, 'Teghoria', 'তেঘরিয়া', 0, 'teghoriaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2667, 298, 'Poil', 'পইল', 0, 'poilup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2668, 298, 'Gopaya', 'গোপায়া', 0, 'gopayaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2669, 298, 'Rajiura', 'রাজিউড়া', 0, 'rajiuraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2670, 298, 'Nurpur', 'নুরপুর', 0, 'nurpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2671, 298, 'Shayestaganj', 'শায়েস্তাগঞ্জ', 0, 'shayestaganjup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2672, 298, 'Nijampur', 'নিজামপুর', 0, 'nijampurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2673, 298, 'Laskerpur', 'লস্করপুর', 0, 'laskerpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2674, 299, 'Dharmaghar', 'ধর্মঘর', 0, 'dharmagharup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2675, 299, 'Choumohani', 'চৌমুহনী', 0, 'choumohaniup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2676, 299, 'Bahara', 'বহরা', 0, 'baharaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2677, 299, 'Adaoir', 'আদাঐর', 0, 'adaoirup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2678, 299, 'Andiura', 'আন্দিউড়া', 0, 'andiuraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2679, 299, 'Shahjahanpur', 'শাহজাহানপুর', 0, 'shahjahanpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2680, 299, 'Jagadishpur', 'জগদীশপুর', 0, 'jagadishpurup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2681, 299, 'Bulla', 'বুল্লা', 0, 'bullaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2682, 299, 'Noapara', 'নোয়াপাড়া', 0, 'noaparaup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2683, 299, 'Chhatiain', 'ছাতিয়াইন', 0, 'chhatiainup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2684, 299, 'Bagashura', 'বাঘাসুরা', 0, 'bagashuraup.habiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2685, 300, 'Jahangirnagar', 'জাহাঙ্গীরনগর', 0, 'jahangirnagarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2686, 300, 'Rangarchar', 'রংগারচর', 0, 'rangarcharup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2687, 300, 'Aptabnagar', 'আপ্তাবনগর', 0, 'aptabnagarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2688, 300, 'Gourarang', 'গৌরারং', 0, 'gourarang.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2689, 300, 'Mollapara', 'মোল্লাপাড়া', 0, 'mollaparaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2690, 300, 'Laxmansree', 'লক্ষণশ্রী', 0, 'laxmansreeup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2691, 300, 'Kathair', 'কাঠইর', 0, 'kathairup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2692, 300, 'Surma', 'সুরমা', 0, 'surmaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2693, 300, 'Mohonpur', 'মোহনপুর', 0, 'mohonpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2694, 301, 'Shimulbak', 'শিমুলবাক', 0, 'shimulbak.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2695, 301, 'Paschim Pagla', 'পশ্চিম পাগলা', 0, 'paschimpagla.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2696, 301, 'Joykalash', 'জয়কলস', 0, 'joykalashup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2697, 301, 'Purba Pagla', 'পূর্ব পাগলা', 0, 'purbapaglaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2698, 301, 'Patharia', 'পাথারিয়া', 0, 'pathariaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2699, 301, 'Purba Birgaon', 'পূর্ব বীরগাঁও', 0, 'purbabirgaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2700, 301, 'Dargapasha', 'দরগাপাশা', 0, 'dargapashaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2701, 301, 'Paschim Birgaon', 'পশ্চিম বীরগাঁও', 0, 'paschimbirgaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2702, 302, 'Palash', 'পলাশ', 0, 'palashup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2703, 302, 'Solukabad', 'সলুকাবাদ', 0, 'solukabadup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2704, 302, 'Dhanpur', 'ধনপুর', 0, 'dhanpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2705, 302, 'Badaghat South', 'বাদাঘাট দক্ষিণ', 0, 'badaghatsouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2706, 302, 'Fatepur', 'ফতেপুর', 0, 'fatepurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2707, 303, 'Islampur', 'ইসলামপুর', 0, 'islampurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2708, 303, 'Noarai', 'নোয়ারাই', 0, 'noaraiup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2709, 303, 'Chhatak Sadar', 'ছাতক সদর', 0, 'chhataksadarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2710, 303, 'Kalaruka', 'কালারুকা', 0, 'kalarukaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2711, 303, 'Gobindganj-Syedergaon', 'গোবিন্দগঞ্জ-সৈদেরগাঁও', 0, 'gobindganjsyedergaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2712, 303, 'Chhaila Afjalabad', 'ছৈলা আফজলাবাদ', 0, 'chhailaafjalabadup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2713, 303, 'Khurma North', 'খুরমা উত্তর', 0, 'khurmanorthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2714, 303, 'Khurma South', 'খুরমা দক্ষিণ', 0, 'khurmasouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2715, 303, 'Chormohalla', 'চরমহল্লা', 0, 'chormohallaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2716, 303, 'Jauwabazar', 'জাউয়া বাজার', 0, 'jauwabazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2717, 303, 'Singchapair', 'সিংচাপইড়', 0, 'singchapairup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2718, 303, 'Dolarbazar', 'দোলারবাজার', 0, 'dolarbazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2719, 303, 'Bhatgaon', 'ভাতগাঁও', 0, 'bhatgaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2720, 304, 'Kolkolia', 'কলকলিয়া', 0, 'kolkoliaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2721, 304, 'Patli', 'পাটলী', 0, 'patliup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2722, 304, 'Mirpur', 'মীরপুর', 0, 'mirpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2723, 304, 'Chilaura Holdipur', 'চিলাউড়া হলদিপুর', 0, 'chilauraholdipurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2724, 304, 'Raniganj', 'রানীগঞ্জ', 0, 'raniganjup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2725, 304, 'Syedpur Shaharpara', 'সৈয়দপুর শাহাড়পাড়া', 0, 'syedpurshaharparaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2726, 304, 'Asharkandi', 'আশারকান্দি', 0, 'asharkandiup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2727, 304, 'Pailgaon', 'পাইলগাঁও', 0, 'pailgaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2728, 305, 'Banglabazar', 'বাংলাবাজার', 0, 'banglabazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2729, 305, 'Norsingpur', 'নরসিংহপুর', 0, 'norsingpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2730, 305, 'Dowarabazar', 'দোয়ারাবাজার', 0, 'dowarabazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2731, 305, 'Mannargaon', 'মান্নারগাঁও', 0, 'mannargaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2732, 305, 'Pandargaon', 'পান্ডারগাঁও', 0, 'pandargaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2733, 305, 'Dohalia', 'দোহালিয়া', 0, 'dohaliaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2734, 305, 'Laxmipur', 'লক্ষীপুর', 0, 'laxmipurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2735, 305, 'Boglabazar', 'বোগলাবাজার', 0, 'boglabazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2736, 305, 'Surma', 'সুরমা', 0, 'surma2up.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2737, 306, 'Sreepur North', 'শ্রীপুর উত্তর', 0, 'sreepurnorthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2738, 306, 'Sreepur South', 'শ্রীপুর দক্ষিণ', 0, 'sreepursouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2739, 306, 'Bordal South', 'বড়দল দক্ষিণ', 0, 'bordalsouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2740, 306, 'Bordal North', 'বড়দল উত্তর', 0, 'bordalnorthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2741, 306, 'Badaghat', 'বাদাঘাট', 0, 'badaghatup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2742, 306, 'Tahirpur Sadar', 'তাহিরপুর সদর', 0, 'tahirpursadarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2743, 306, 'Balijuri', 'বালিজুরী', 0, 'balijuriup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2744, 307, 'Bongshikunda North', 'বংশীকুন্ডা উত্তর', 0, 'bongshikundanorthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2745, 307, 'Bongshikunda South', 'বংশীকুন্ডা দক্ষিণ', 0, 'bongshikundasouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2746, 307, 'Chamordani', 'চামরদানী', 0, 'chamordaniup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2747, 307, 'Madhyanagar', 'মধ্যনগর', 0, 'madhyanagarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2748, 307, 'Paikurati', 'পাইকুরাটী', 0, 'paikuratiup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2749, 307, 'Selbarash', 'সেলবরষ', 0, 'selbarashup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2750, 307, 'Dharmapasha Sadar', 'ধর্মপাশা সদর', 0, 'dharmapashasadarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2751, 307, 'Joyasree', 'জয়শ্রী', 0, 'joyasreeup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2752, 307, 'Sukhair Rajapur North', 'সুখাইড় রাজাপুর উত্তর', 0, 'sukhairrajapurnorthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2753, 307, 'Sukhair Rajapur South', 'সুখাইড় রাজাপুর দক্ষিণ', 0, 'sukhairrajapursouthup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2754, 308, 'Beheli', 'বেহেলী', 0, 'beheliup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2755, 308, 'Sachnabazar', 'সাচনাবাজার', 0, 'sachnabazarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2756, 308, 'Bhimkhali', 'ভীমখালী', 0, 'bhimkhaliup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2757, 308, 'Fenerbak', 'ফেনারবাক', 0, 'fenerbakup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2758, 308, 'Jamalganj Sadar', 'জামালগঞ্জ সদর', 0, 'jamalganjsadarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2759, 309, 'Atgaon', 'আটগাঁও', 0, 'atgaonup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2760, 309, 'Habibpur', 'হবিবপুর', 0, 'habibpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2761, 309, 'Bahara', 'বাহারা', 0, 'baharaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2762, 309, 'Shalla Sadar', 'শাল্লা সদর', 0, 'shallasadarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2763, 310, 'Rafinagar', 'রফিনগর', 0, 'rafinagarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2764, 310, 'Bhatipara', 'ভাটিপাড়া', 0, 'bhatiparaup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2765, 310, 'Rajanagar', 'রাজানগর', 0, 'rajanagarup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2766, 310, 'Charnarchar', 'চরনারচর', 0, 'charnarcharup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2767, 310, 'Derai Sarmangal', 'দিরাই সরমঙ্গল', 0, 'deraisarmangalup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2768, 310, 'Karimpur', 'করিমপুর', 0, 'karimpurup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2769, 310, 'Jagddol', 'জগদল', 0, 'jagddolup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2770, 310, 'Taral', 'তাড়ল', 0, 'taralup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2771, 310, 'Kulanj', 'কুলঞ্জ', 0, 'kulanjup.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2772, 311, 'Amlaba', 'আমলাব', 0, 'amlabaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2773, 311, 'Bajnaba', 'বাজনাব', 0, 'bajnabaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2774, 311, 'Belabo', 'বেলাব', 0, 'belaboup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2775, 311, 'Binnabayd', 'বিন্নাবাইদ', 0, 'binnabaydup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2776, 311, 'Charuzilab', 'চরউজিলাব', 0, 'charuzilabup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2777, 311, 'Naraynpur', 'নারায়নপুর', 0, 'naraynpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2778, 311, 'Sallabad', 'সল্লাবাদ', 0, 'sallabadup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2779, 311, 'Patuli', 'পাটুলী', 0, 'patuliup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2780, 311, 'Diara', 'দেয়ারা মডেল', 0, 'diaraup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2781, 312, 'Barachapa', 'বড়চাপা', 0, 'barachapaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2782, 312, 'Chalakchar', 'চালাকচর', 0, 'chalakcharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2783, 312, 'Charmandalia', 'চরমান্দালিয়া', 0, 'charmandaliaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2784, 312, 'Ekduaria', 'একদুয়ারিয়া', 0, 'ekduariaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2785, 312, 'Gotashia', 'গোতাশিয়া', 0, 'gotashiaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2786, 312, 'Kanchikata', 'কাচিকাটা', 0, 'kanchikataup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2787, 312, 'Khidirpur', 'খিদিরপুর', 0, 'khidirpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2788, 312, 'Shukundi', 'শুকুন্দি', 0, 'shukundiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2789, 312, 'Dawlatpur', 'দৌলতপুর', 0, 'dawlatpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2790, 312, 'Krisnopur', 'কৃষ্ণপুর', 0, 'krisnopurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2791, 312, 'Labutala', 'লেবুতলা', 0, 'labutalaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2792, 312, 'Chandanbari', 'চন্দনবাড়ী', 0, 'chandanbariup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2793, 313, 'Alokbali', 'আলোকবালী', 0, 'alokbaliup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2794, 313, 'Chardighaldi', 'চরদিঘলদী', 0, 'chardighaldiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2795, 313, 'Chinishpur', 'চিনিশপুর', 0, 'chinishpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2796, 313, 'Hajipur', 'হাজীপুর', 0, 'hajipurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2797, 313, 'Karimpur', 'করিমপুর', 0, 'karimpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2798, 313, 'Khathalia', 'কাঠালিয়া', 0, 'khathaliaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2799, 313, 'Nuralapur', 'নূরালাপুর', 0, 'nuralapurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2800, 313, 'Mahishasura', 'মহিষাশুড়া', 0, 'mahishasuraup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2801, 313, 'Meherpara', 'মেহেড়পাড়া', 0, 'meherparaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2802, 313, 'Nazarpur', 'নজরপুর', 0, 'nazarpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2803, 313, 'Paikarchar', 'পাইকারচর', 0, 'paikarcharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2804, 313, 'Panchdona', 'পাঁচদোনা', 0, 'panchdonaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2805, 313, 'Silmandi', 'শিলমান্দী', 0, 'silmandiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2806, 313, 'Amdia', 'আমদিয়া ২', 0, 'amdiaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2807, 314, 'Danga', 'ডাংঙ্গা', 0, 'dangaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2808, 314, 'Charsindur', 'চরসিন্দুর', 0, 'charsindurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2809, 314, 'Jinardi', 'জিনারদী', 0, 'jinardiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2810, 314, 'Gazaria', 'গজারিয়া', 0, 'gazariaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2811, 315, 'Chanpur', 'চানপুর', 0, 'chanpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2812, 315, 'Alipura', 'অলিপুরা', 0, 'alipuraup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2813, 315, 'Amirganj', 'আমিরগঞ্জ', 0, 'amirganjup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2814, 315, 'Adiabad', 'আদিয়াবাদ', 0, 'adiabadup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2815, 315, 'Banshgari', 'বাঁশগাড়ী', 0, 'banshgariup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2816, 315, 'Chanderkandi', 'চান্দেরকান্দি', 0, 'chanderkandiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2817, 315, 'Chararalia', 'চরআড়ালিয়া', 0, 'chararaliaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2818, 315, 'Charmadhua', 'চরমধুয়া', 0, 'charmadhuaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2819, 315, 'Charsubuddi', 'চরসুবুদ্দি', 0, 'charsubuddiup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2820, 315, 'Daukarchar', 'ডৌকারচর', 0, 'daukarcharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2821, 315, 'Hairmara', 'হাইরমারা', 0, 'hairmaraup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2822, 315, 'Maheshpur', 'মহেষপুর', 0, 'maheshpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2823, 315, 'Mirzanagar', 'মির্জানগর', 0, 'mirzanagarup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2824, 315, 'Mirzarchar', 'মির্জারচর', 0, 'mirzarcharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2825, 315, 'Nilakhya', 'নিলক্ষ্যা', 0, 'nilakhyaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2826, 315, 'Palashtali', 'পলাশতলী', 0, 'palashtaliup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2827, 315, 'Paratali', 'পাড়াতলী', 0, 'parataliup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2828, 315, 'Sreenagar', 'শ্রীনগর', 0, 'sreenagarup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2829, 315, 'Roypura', 'রায়পুরা', 0, 'roypuraup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2830, 315, 'Musapur', 'মুছাপুর', 0, 'musapurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2831, 315, 'Uttar Bakharnagar', 'উত্তর বাখরনগর', 0, 'uttarbakharnagarup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2832, 315, 'Marjal', 'মরজাল', 0, 'marjal2up.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2833, 316, 'Dulalpur', 'দুলালপুর', 0, 'dulalpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2834, 316, 'Joynagar', 'জয়নগর', 0, 'joynagarup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2835, 316, 'Sadharchar', 'সাধারচর', 0, 'sadharcharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2836, 316, 'Masimpur', 'মাছিমপুর', 0, 'masimpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2837, 316, 'Chakradha', 'চক্রধা', 0, 'chakradhaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2838, 316, 'Joshar', 'যোশর', 0, 'josharup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2839, 316, 'Baghabo', 'বাঘাব', 0, 'baghaboup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2840, 316, 'Ayubpur', 'আয়ুবপুর', 0, 'ayubpurup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2841, 316, 'Putia', 'পুটিয়া', 0, 'putiaup.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2842, 317, 'Bahadursadi', 'বাহাদুরশাদী', 0, 'bahadursadi.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2843, 317, 'Baktarpur', 'বক্তারপুর', 0, 'baktarpur.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2844, 317, 'Jamalpurnew', 'জামালপুর', 0, 'jamalpurnew.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2845, 317, 'Jangalia', 'জাঙ্গালিয়া', 0, 'jangalia.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2846, 317, 'Moktarpur', 'মোক্তারপুর', 0, 'moktarpur.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2847, 317, 'Nagari', 'নাগরী', 0, 'nagari.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2848, 317, 'Tumulia', 'তুমুলিয়া', 0, 'tumulia.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2849, 318, 'Atabaha', 'আটাবহ', 0, 'atabahaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2850, 318, 'Boali', 'বোয়ালী', 0, 'boaliup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2851, 318, 'Chapair', 'চাপাইর', 0, 'chapairup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2852, 318, 'Dhaliora', 'ঢালজোড়া', 0, 'dhalioraup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2853, 318, 'Fulbaria', 'ফুলবাড়ীয়া', 0, 'fulbariaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2854, 318, 'Madhyapara', 'মধ্যপাড়া', 0, 'madhyapara.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2855, 318, 'Mouchak', 'মৌচাক', 0, 'mouchakup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2856, 318, 'Sutrapur', 'সূত্রাপুর', 0, 'sutrapurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2857, 318, 'Srifaltali', 'শ্রীফলতলী', 0, 'srifaltaliup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2858, 319, 'Barishaba', 'বারিষাব', 0, 'barishabaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2859, 319, 'Ghagotia', 'ঘাগটিয়া', 0, 'ghagotiaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2860, 319, 'Kapasia', 'কাপাসিয়া', 0, 'kapasiaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2861, 319, 'Chandpur', 'চাঁদপুর', 0, 'chandpur.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2862, 319, 'Targoan', 'তরগাঁও', 0, 'targoan.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2863, 319, 'Karihata', 'কড়িহাতা', 0, 'karihata.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2864, 319, 'Tokh', 'টোক', 0, 'tokh.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2865, 319, 'Sinhasree', 'সিংহশ্রী', 0, 'sinhasree.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2866, 319, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2867, 319, 'Sonmania', 'সনমানিয়া', 0, 'sonmaniaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2868, 319, 'Rayed', 'রায়েদ', 0, 'rayedup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2869, 320, 'Baria', 'বাড়ীয়া', 0, 'bariaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2870, 320, 'Basan', 'বাসন', 0, 'basanup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2871, 320, 'Gachha', 'গাছা', 0, 'gachhaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2872, 320, 'Kashimpur', 'কাশিমপুর', 0, 'kashimpurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2873, 320, 'Kayaltia', 'কাউলতিয়া', 0, 'kayaltiaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2874, 320, 'Konabari', 'কোনাবাড়ী', 0, 'konabariup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2875, 320, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2876, 320, 'Pubail', 'পূবাইল', 0, 'pubailup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2877, 321, 'Barmi', 'বরমী', 0, 'barmiup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2878, 321, 'Gazipur', 'গাজীপুর', 0, 'gazipurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2879, 321, 'Gosinga', 'গোসিংগা', 0, 'gosingaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2880, 321, 'Maona', 'মাওনা', 0, 'maonaup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2881, 321, 'Kaoraid', 'কাওরাইদ', 0, 'kaoraidup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2882, 321, 'Prahladpur', 'প্রহলাদপুর', 0, 'prahladpurup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2883, 321, 'Rajabari', 'রাজাবাড়ী', 0, 'rajabariup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2884, 321, 'Telihati', 'তেলিহাটী', 0, 'telihatiup.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2885, 322, 'Binodpur', 'বিনোদপুর', 0, 'binodpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2886, 322, 'Tulasar', 'তুলাসার', 0, 'tulasarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2887, 322, 'Palong', 'পালং', 0, 'palongup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2888, 322, 'Domshar', 'ডোমসার', 0, 'domsharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2889, 322, 'Rudrakar', 'রুদ্রকর', 0, 'rudrakarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2890, 322, 'Angaria', 'আংগারিয়া', 0, 'angariaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2891, 322, 'Chitolia', 'চিতলয়া', 0, 'chitoliaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2892, 322, 'Mahmudpur', 'মাহমুদপুর', 0, 'mahmudpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2893, 322, 'Chikondi', 'চিকন্দি', 0, 'chikondiup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2894, 322, 'Chandrapur', 'চন্দ্রপুর', 0, 'chandrapurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2895, 322, 'Shulpara', 'শৌলপাড়া', 0, 'shulparaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2896, 323, 'Kedarpur', 'কেদারপুর', 0, 'kedarpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2897, 323, 'Dingamanik', 'ডিংগামানিক', 0, 'dingamanikup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2898, 323, 'Garishar', 'ঘড়িষার', 0, 'garisharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2899, 323, 'Nowpara', 'নওপাড়া', 0, 'nowparaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2900, 323, 'Moktererchar', 'মোত্তারেরচর', 0, 'mokterercharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2901, 323, 'Charatra', 'চরআত্রা', 0, 'charatraup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2902, 323, 'Rajnagar', 'রাজনগর', 0, 'rajnagarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2903, 323, 'Japsa', 'জপসা', 0, 'japsaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2904, 323, 'Vojeshwar', 'ভোজেশ্বর', 0, 'vojeshwarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2905, 323, 'Fategongpur', 'ফতেজংপুর', 0, 'fategongpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2906, 323, 'Bijari', 'বিঝারি', 0, 'bijariup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2907, 323, 'Vumkhara', 'ভূমখাড়া', 0, 'vumkharaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2908, 323, 'Nashason', 'নশাসন', 0, 'nashasonup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2909, 324, 'Zajira Sadar', 'জাজিরা সদর', 0, 'zajirasadarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2910, 324, 'Mulna', 'মূলনা', 0, 'mulnaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2911, 324, 'Barokandi', 'বড়কান্দি', 0, 'barokandiup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2912, 324, 'Bilaspur', 'বিলাসপুর', 0, 'bilaspurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2913, 324, 'Kundarchar', 'কুন্ডেরচর', 0, 'kundarcharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2914, 324, 'Palerchar', 'পালেরচর', 0, 'palercharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2915, 324, 'Purba Nawdoba', 'পুর্ব নাওডোবা', 0, 'purbanawdobaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2916, 324, 'Nawdoba', 'নাওডোবা', 0, 'nawdobaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2917, 324, 'Shenerchar', 'সেনেরচর', 0, 'shenercharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2918, 324, 'Bknagar', 'বি. কে. নগর', 0, 'bknagarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2919, 324, 'Barogopalpur', 'বড়গোপালপুর', 0, 'barogopalpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2920, 324, 'Jaynagor', 'জয়নগর', 0, 'jaynagorup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2921, 325, 'Nager Para', 'নাগের পাড়া', 0, 'nagerparaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2922, 325, 'Alaolpur', 'আলাওলপুর', 0, 'alaolpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2923, 325, 'Kodalpur', 'কোদালপুর', 0, 'kodalpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2924, 325, 'Goshairhat', 'গোসাইরহাট', 0, 'goshairhatup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2925, 325, 'Edilpur', 'ইদিলপুর', 0, 'edilpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2926, 325, 'Nalmuri', 'নলমুড়ি', 0, 'nalmuriup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2927, 325, 'Samontasar', 'সামন্তসার', 0, 'samontasarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2928, 325, 'Kuchipatti', 'কুচাইপট্টি', 0, 'kuchipattiup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2929, 326, 'Ramvadrapur', 'রামভদ্রপুর', 0, 'ramvadrapurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2930, 326, 'Mahisar', 'মহিষার', 0, 'mahisarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2931, 326, 'Saygaon', 'ছয়গাঁও', 0, 'saygaonup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2932, 326, 'Narayanpur', 'নারায়নপুর', 0, 'narayanpurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2933, 326, 'D.M Khali', 'ডি.এম খালি', 0, 'dmkhaliup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2934, 326, 'Charkumaria', 'চরকুমারিয়া', 0, 'charkumariaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2935, 326, 'Sakhipur', 'সখিপুর', 0, 'sakhipurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2936, 326, 'Kachikata', 'কাচিকাঁটা', 0, 'kachikataup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2937, 326, 'North Tarabunia', 'উত্তর তারাবুনিয়া', 0, 'northtarabuniaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2938, 326, 'Charvaga', 'চরভাগা', 0, 'charvagaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2939, 326, 'Arsinagar', 'আরশিনগর', 0, 'arsinagarup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2940, 326, 'South Tarabunia', 'দক্ষিন তারাবুনিয়া', 0, 'southtarabuniaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2941, 326, 'Charsensas', 'চরসেনসাস', 0, 'charsensasup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2942, 327, 'Shidulkura', 'শিধলকুড়া', 0, 'shidulkuraup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2943, 327, 'Kaneshar', 'কনেস্বর', 0, 'kanesharup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2944, 327, 'Purba Damudya', 'পুর্ব ডামুড্যা', 0, 'purbadamudyaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2945, 327, 'Islampur', 'ইসলামপুর', 0, 'islampurup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2946, 327, 'Dankati', 'ধানকাটি', 0, 'dankatiup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2947, 327, 'Sidya', 'সিড্যা', 0, 'sidyaup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2948, 327, 'Darulaman', 'দারুল আমান', 0, 'darulamanup.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2949, 328, 'Satgram', 'সাতগ্রাম', 0, 'satgramup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2950, 328, 'Duptara', 'দুপ্তারা', 0, 'duptaraup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2951, 328, 'Brahammandi', 'ব্রা‏হ্মন্দী', 0, 'brahammandiup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2952, 328, 'Fatepur', 'ফতেপুর', 0, 'fatepurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2953, 328, 'Bishnandi', 'বিশনন্দী', 0, 'bishnandiup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2954, 328, 'Mahmudpur', 'মাহমুদপুর', 0, 'mahmudpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2955, 328, 'Highjadi', 'হাইজাদী', 0, 'highjadiup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2956, 328, 'Uchitpura', 'উচিৎপুরা', 0, 'uchitpuraup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2957, 328, 'Kalapaharia', 'কালাপাহাড়িয়া', 0, 'kalapahariaup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2958, 328, 'Kagkanda', 'খাগকান্দা', 0, 'kagkandaUP.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2959, 329, 'Musapur', 'মুছাপুর', 0, 'musapurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2960, 329, 'Modonpur', 'মদনপুর', 0, 'modonpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2961, 329, 'Bandar', 'বন্দর', 0, 'bandarup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2962, 329, 'Dhamgar', 'ধামগর', 0, 'dhamgar.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2963, 329, 'Kolagathia', ' কলাগাছিয়া', 0, 'kolagathiaup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2964, 330, 'Alirtek', 'আলিরটেক', 0, 'alirtekup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2965, 330, 'Kashipur', 'কাশীপুর', 0, 'kashipurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2966, 330, 'Kutubpur', 'কুতুবপুর', 0, 'kutubpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2967, 330, 'Gognagar', 'গোগনগর', 0, 'gognagarup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2968, 330, 'Baktaboli', 'বক্তাবলী', 0, 'baktaboliup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2969, 330, 'Enayetnagor', 'এনায়েত নগর', 0, 'enayetnagorup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2970, 331, 'Murapara', 'মুড়াপাড়া', 0, 'muraparaup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2971, 331, 'Bhulta', 'ভূলতা', 0, 'bhultaup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2972, 331, 'Golakandail', 'গোলাকান্দাইল', 0, 'golakandailup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2973, 331, 'Daudpur', 'দাউদপুর', 0, 'daudpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2974, 331, 'Rupganj', 'রূপগঞ্জ', 0, 'rupganjup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2975, 331, 'Kayetpara', 'কায়েতপাড়া', 0, 'kayetparaup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2976, 331, 'Bholobo', 'ভোলাব', 0, 'bholoboup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2977, 332, 'Pirojpur', 'পিরোজপুর', 0, 'pirojpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2978, 332, 'Shambhupura', 'শম্ভুপুরা', 0, 'shambhupura.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2979, 332, 'Mograpara', 'মোগরাপাড়া', 0, 'mograpara.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2980, 332, 'Baidyerbazar', 'বৈদ্যেরবাজার', 0, 'baidyerbazar.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2981, 332, 'Baradi', 'বারদী', 0, 'baradiup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2982, 332, 'Noagaon', 'নোয়াগাঁও', 0, 'noagaonup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2983, 332, 'Jampur', 'জামপুর', 0, 'jampurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2984, 332, 'Sadipur', 'সাদীপুর', 0, 'sadipurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2985, 332, 'Sonmandi', 'সনমান্দি', 0, 'sonmandiup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2986, 332, 'Kanchpur', 'কাচপুর', 0, 'kanchpurup.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2987, 333, 'Basail', 'বাসাইল', 0, 'basailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2988, 333, 'Kanchanpur', 'কাঞ্চনপুর', 0, 'kanchanpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2989, 333, 'Habla', 'হাবলা', 0, 'hablaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2990, 333, 'Kashil', 'কাশিল', 0, 'kashilup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2991, 333, 'Fulki', 'ফুলকি', 0, 'fulkiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2992, 333, 'Kauljani', 'কাউলজানী', 0, 'kauljaniup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2993, 334, 'Arjuna', 'অর্জুনা', 0, 'arjunaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2994, 334, 'Gabshara', 'গাবসারা', 0, 'gabsharaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2995, 334, 'Falda', 'ফলদা', 0, 'faldaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2996, 334, 'Gobindashi', 'গোবিন্দাসী', 0, 'gobindashiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2997, 334, 'Aloa', 'আলোয়া', 0, 'aloaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2998, 334, 'Nikrail', 'নিকরাইল', 0, 'nikrailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (2999, 335, 'Deuli', 'দেউলী', 0, 'deuliup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3000, 335, 'Lauhati', 'লাউহাটি', 0, 'lauhatiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3001, 335, 'Patharail', 'পাথরাইল', 0, 'patharailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3002, 335, 'Delduar', 'দেলদুয়ার', 0, 'delduarup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3003, 335, 'Fazilhati', 'ফাজিলহাটি', 0, 'fazilhatiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3004, 335, 'Elasin', 'এলাসিন', 0, 'elasinup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3005, 335, 'Atia', 'আটিয়া', 0, 'atiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3006, 335, 'Dubail', 'ডুবাইল', 0, 'dubailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3007, 336, 'Deulabari', 'দেউলাবাড়ী', 0, 'deulabariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3008, 336, 'Ghatail', 'ঘাটাইল', 0, 'ghatailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3009, 336, 'Jamuria', 'জামুরিয়া', 0, 'jamuriaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3010, 336, 'Lokerpara', 'লোকেরপাড়া', 0, 'lokerparaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3011, 336, 'Anehola', 'আনেহলা', 0, 'aneholaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3012, 336, 'Dighalkandia', 'দিঘলকান্দি', 0, 'dighalkandiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3013, 336, 'Digar', 'দিগড়', 0, 'digarup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3014, 336, 'Deopara', 'দেওপাড়া', 0, 'deoparaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3015, 336, 'Sandhanpur', 'সন্ধানপুর', 0, 'sandhanpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3016, 336, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3017, 336, 'Dhalapara', 'ধলাপাড়া', 0, 'dhalaparaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3018, 337, 'Hadera', 'হাদিরা', 0, 'haderaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3019, 337, 'Jhawail', 'ঝাওয়াইল', 0, 'jhawailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3020, 337, 'Nagdashimla', 'নগদাশিমলা', 0, 'nagdashimlaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3021, 337, 'Dhopakandi', 'ধোপাকান্দি', 0, 'dhopakandiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3022, 337, 'Alamnagor', 'আলমনগর', 0, 'alamnagorup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3023, 337, 'Hemnagor', 'হেমনগর', 0, 'hemnagorup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3024, 337, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3025, 338, 'Alokdia', 'আলোকদিয়া', 0, 'alokdiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3026, 338, 'Aushnara', 'আউশনারা', 0, 'aushnaraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3027, 338, 'Aronkhola', 'অরণখোলা', 0, 'aronkholaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3028, 338, 'Sholakuri', 'শোলাকুড়ি', 0, 'sholakuriup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3029, 338, 'Golabari', 'গোলাবাড়ী', 0, 'golabariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3030, 338, 'Mirjabari', 'মির্জাবাড়ী', 0, 'mirjabariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3031, 339, 'Mahera', 'মহেড়া', 0, 'maheraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3032, 339, 'Jamurki', 'জামুর্কী', 0, 'jamurkiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3033, 339, 'Fatepur', 'ফতেপুর', 0, 'fatepurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3034, 339, 'Banail', 'বানাইল', 0, 'banailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3035, 339, 'Anaitara', 'আনাইতারা', 0, 'anaitaraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3036, 339, 'Warshi', 'ওয়ার্শী', 0, 'warshiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3037, 339, 'Bhatram', 'ভাতগ্রাম', 0, 'bhatramup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3038, 339, 'Bahuria', 'বহুরিয়া', 0, 'bahuriaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3039, 339, 'Gorai', 'গোড়াই', 0, 'goraiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3040, 339, 'Ajgana', 'আজগানা', 0, 'ajganaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3041, 339, 'Tarafpur', 'তরফপুর', 0, 'tarafpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3042, 339, 'Bastail', 'বাঁশতৈল', 0, 'bastailup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3043, 339, 'Baora', 'ভাওড়া', 0, 'baoraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3044, 339, 'Latifpur', 'লতিফপুর', 0, 'latifpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3045, 340, 'Bharra', 'ভারড়া', 0, 'bharraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3046, 340, 'Sahabathpur', 'সহবতপুর', 0, 'sahabathpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3047, 340, 'Goyhata', 'গয়হাটা', 0, 'goyhataup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3048, 340, 'Solimabad', 'সলিমাবাদ', 0, 'solimabadup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3049, 340, 'Nagorpur', 'নাগরপুর', 0, 'nagorpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3050, 340, 'Mamudnagor', 'মামুদনগর', 0, 'mamudnagorup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3051, 340, 'Mokna', 'মোকনা', 0, 'moknaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3052, 340, 'Pakutia', 'পাকুটিয়া', 0, 'pakutiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3053, 340, 'Bekrah Atgram', 'বেকরা আটগ্রাম', 0, 'bekrahatgramup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3054, 340, 'Dhuburia', 'ধুবড়িয়া', 0, 'dhuburiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3055, 340, 'Bhadra', 'ভাদ্রা', 0, 'bhadraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3056, 340, 'Doptior', 'দপ্তিয়র', 0, 'doptiorup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3057, 341, 'Kakrajan', 'কাকড়াজান', 0, 'kakrajanup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3058, 341, 'Gajaria', 'গজারিয়া', 0, 'gajariaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3059, 341, 'Jaduppur', 'যাদবপুর', 0, 'jaduppurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3060, 341, 'Hatibandha', 'হাতীবান্ধা', 0, 'hatibandhaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3061, 341, 'Kalia', 'কালিয়া', 0, 'kaliaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3062, 341, 'Dariapur', 'দরিয়াপুর', 0, 'dariapurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3063, 341, 'Kalmegha', 'কালমেঘা', 0, 'kalmeghaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3064, 341, 'Baharatoil', 'বহেড়াতৈল', 0, 'baharatoilup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3065, 342, 'Mogra', 'মগড়া', 0, 'mograup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3066, 342, 'Gala', 'গালা', 0, 'galaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3067, 342, 'Gharinda', 'ঘারিন্দা', 0, 'gharindaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3068, 342, 'Karatia', 'করটিয়া', 0, 'karatiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3069, 342, 'Silimpur', 'ছিলিমপুর', 0, 'silimpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3070, 342, 'Porabari', 'পোড়াবাড়ী', 0, 'porabariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3071, 342, 'Dyenna', 'দাইন্যা', 0, 'dyennaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3072, 342, 'Baghil', 'বাঘিল', 0, 'baghilup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3073, 342, 'Kakua', 'কাকুয়া', 0, 'kakuaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3074, 342, 'Hugra', 'হুগড়া', 0, 'hugraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3075, 342, 'Katuli', 'কাতুলী', 0, 'katuliup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3076, 342, 'Mahamudnagar', 'মাহমুদনগর', 0, 'mahamudnagarup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3077, 343, 'Durgapur', 'দুর্গাপুর', 0, 'durgapurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3078, 343, 'Birbashinda', 'বীরবাসিন্দা', 0, 'birbashindaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3079, 343, 'Narandia', 'নারান্দিয়া', 0, 'narandiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3080, 343, 'Shahadebpur', 'সহদেবপুর', 0, 'shahadebpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3081, 343, 'Kokdahara', 'কোকডহরা', 0, 'kokdaharaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3082, 343, 'Balla', 'বল্লা', 0, 'ballaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3083, 343, 'Salla', 'সল্লা', 0, 'sallaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3084, 343, 'Nagbari', 'নাগবাড়ী', 0, 'nagbariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3085, 343, 'Bangra', 'বাংড়া', 0, 'bangraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3086, 343, 'Paikora', 'পাইকড়া', 0, 'paikoraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3087, 343, 'Dashokia', 'দশকিয়া', 0, 'dashokiaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3088, 343, 'Parkhi', 'পারখী', 0, 'parkhiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3089, 343, 'Gohaliabari', 'গোহালিয়াবাড়ী', 0, 'gohaliabariup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3090, 344, 'Dhopakhali', 'ধোপাখালী', 0, 'dhopakhaliup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3091, 344, 'Paiska', 'পাইস্কা', 0, 'paiskaup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3092, 344, 'Mushuddi', 'মুশুদ্দি', 0, 'mushuddiup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3093, 344, 'Bolibodrow', 'বলিভদ্র', 0, 'bolibodrowup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3094, 344, 'Birtara', 'বীরতারা', 0, 'birtaraup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3095, 344, 'Baniajan', 'বানিয়াজান', 0, 'baniajanup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3096, 344, 'Jadunathpur', 'যদুনাথপুর', 0, 'jadunathpurup.tangail.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3097, 345, 'Chawganga', 'চৌগাংগা', 0, 'chawgangaup.kishoreganj.gov.bd      ', NULL, NULL);
INSERT INTO `unions` VALUES (3098, 345, 'Joysiddi', 'জয়সিদ্ধি', 0, 'joysiddiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3099, 345, 'Alonjori', 'এলংজুরী', 0, 'alonjoriup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3100, 345, 'Badla', 'বাদলা', 0, 'badlaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3101, 345, 'Boribari', 'বড়িবাড়ি', 0, 'boribariup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3102, 345, 'Itna', 'ইটনা', 0, 'itnaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3103, 345, 'Mriga', 'মৃগা', 0, 'mrigaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3104, 345, 'Dhonpur', 'ধনপুর', 0, 'dhonpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3105, 345, 'Raytoti', 'রায়টুটি', 0, 'raytotiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3106, 346, 'Banagram', 'বনগ্রাম', 0, 'banagramup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3107, 346, 'Shahasram Dhuldia', 'সহশ্রাম ধুলদিয়া', 0, 'shahasramdhuldiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3108, 346, 'Kargaon', 'কারগাঁও', 0, 'kargaonup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3109, 346, 'Chandpur', 'চান্দপুর', 0, 'chandpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3110, 346, 'Mumurdia', 'মুমুরদিয়া', 0, 'mumurdiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3111, 346, 'Acmita', 'আচমিতা', 0, 'acmitaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3112, 346, 'Mosua', 'মসূয়া', 0, 'mosuaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3113, 346, 'Lohajuree', 'লোহাজুরী', 0, 'lohajureeup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3114, 346, 'Jalalpur', 'জালালপুর', 0, 'jalalpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3115, 347, 'Sadekpur', 'সাদেকপুর', 0, 'sadekpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3116, 347, 'Aganagar', 'আগানগর', 0, 'aganagarup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3117, 347, 'Shimulkandi', 'শিমুলকান্দি', 0, 'shimulkandiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3118, 347, 'Gajaria', 'গজারিয়া', 0, 'gajariaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3119, 347, 'Kalika Prashad', 'কালিকা প্রসাদ', 0, 'kalikaprashadup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3120, 347, 'Sreenagar', 'শ্রীনগর', 0, 'sreenagarup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3121, 347, 'Shibpur', 'শিবপুর', 0, 'shibpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3122, 348, 'Taljanga', 'তালজাঙ্গা', 0, 'taljangaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3123, 348, 'Rauti', 'রাউতি', 0, 'rautiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3124, 348, 'Dhola', 'ধলা', 0, 'dholaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3125, 348, 'Jawar', 'জাওয়ার', 0, 'jawarup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3126, 348, 'Damiha', 'দামিহা', 0, 'damihaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3127, 348, 'Digdair', 'দিগদাইর', 0, 'digdairup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3128, 348, 'Tarail-Sachail', 'তাড়াইল-সাচাইল', 0, 'tarailsachailup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3129, 349, 'Jinari', 'জিনারী', 0, 'jinariup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3130, 349, 'Gobindapur', 'গোবিন্দপুর', 0, 'gobindapurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3131, 349, 'Sidhla', 'সিদলা', 0, 'sidhlaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3132, 349, 'Araibaria', 'আড়াইবাড়িয়া', 0, 'araibariaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3133, 349, 'Sahedal', 'সাহেদল', 0, 'sahedalup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3134, 349, 'Pumdi', 'পুমদি', 0, 'pumdiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3135, 350, 'Jangalia', 'জাঙ্গালিয়া', 0, 'jangaliaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3136, 350, 'Hosendi', 'হোসেনদি', 0, 'hosendiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3137, 350, 'Narandi', 'নারান্দি', 0, 'narandiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3138, 350, 'Shukhia', 'সুখিয়া', 0, 'shukhiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3139, 350, 'Patuavabga', 'পটুয়াভাঙ্গা', 0, 'patuavabgaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3140, 350, 'Chandipasha', 'চান্দিপাশা', 0, 'chandipashaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3141, 350, 'Charfaradi', 'চারফারাদি', 0, 'charfaradiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3142, 350, 'Burudia', 'বুড়ুদিয়া', 0, 'burudiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3143, 350, 'Egarasindur', 'ইজারাসিন্দুর', 0, 'egarasindurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3144, 350, 'Pakundia', 'পাকন্দিয়া', 0, 'pakundiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3145, 351, 'Ramdi', 'রামদী', 0, 'ramdiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3146, 351, 'Osmanpur', 'উছমানপুর', 0, 'osmanpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3147, 351, 'Chhaysuti', 'ছয়সূতী', 0, 'chhaysutiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3148, 351, 'Salua', 'সালুয়া', 0, 'saluaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3149, 351, 'Gobaria Abdullahpur', 'গোবরিয়া আব্দুল্লাহপুর', 0, 'gobariaabdullahpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3150, 351, 'Faridpur', 'ফরিদপুর', 0, 'faridpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3151, 352, 'Rashidabad', 'রশিদাবাদ', 0, 'rashidabadup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3152, 352, 'Latibabad', 'লতিবাবাদ', 0, 'latibabadup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3153, 352, 'Maizkhapan', 'মাইজখাপন', 0, 'maizkhapanup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3154, 352, 'Mohinanda', 'মহিনন্দ', 0, 'mohinandaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3155, 352, 'Joshodal', 'যশোদল', 0, 'joshodalup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3156, 352, 'Bowlai', 'বৌলাই', 0, 'bowlaiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3157, 352, 'Binnati', 'বিন্নাটি', 0, 'binnatiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3158, 352, 'Maria', 'মারিয়া', 0, 'mariaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3159, 352, 'Chowddoshata', 'চৌদ্দশত', 0, 'chowddoshataup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3160, 352, 'Karshakarial', 'কর্শাকড়িয়াইল', 0, 'karshakarialup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3161, 352, 'Danapatuli', 'দানাপাটুলী', 0, 'danapatuliup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3162, 353, 'Kadirjangal', 'কাদিরজঙ্গল', 0, 'kadirjangalup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3163, 353, 'Gujadia', 'গুজাদিয়া', 0, 'gujadiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3164, 353, 'Kiraton', 'কিরাটন', 0, 'kiratonup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3165, 353, 'Barogharia', 'বারঘড়িয়া', 0, 'baroghariaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3166, 353, 'Niamatpur', 'নিয়ামতপুর', 0, 'niamatpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3167, 353, 'Dehunda', 'দেহুন্দা', 0, 'dehundaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3168, 353, 'Sutarpara', 'সুতারপাড়া', 0, 'sutarparaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3169, 353, 'Gunodhar', 'গুনধর', 0, 'gunodharup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3170, 353, 'Joyka', 'জয়কা', 0, 'joykaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3171, 353, 'Zafrabad', 'জাফরাবাদ', 0, 'zafrabadup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3172, 353, 'Noabad', 'নোয়াবাদ', 0, 'noabadup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3173, 354, 'Kailag', 'কৈলাগ', 0, 'kailagup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3174, 354, 'Pirijpur', 'পিরিজপুর', 0, 'pirijpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3175, 354, 'Gazirchar', 'গাজীরচর', 0, 'gazircharup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3176, 354, 'Hilochia', 'হিলচিয়া', 0, 'hilochiaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3177, 354, 'Maijchar9', 'মাইজচর', 0, 'maijchar9up.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3178, 354, 'Homypur', 'হুমাইপর', 0, 'homypurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3179, 354, 'Halimpur', 'হালিমপুর', 0, 'halimpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3180, 354, 'Sararchar', 'সরারচর', 0, 'sararcharup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3181, 354, 'Dilalpur', 'দিলালপুর', 0, 'dilalpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3182, 354, 'Dighirpar', 'দিঘীরপাড়', 0, 'dighirparup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3183, 354, 'Boliardi', 'বলিয়ার্দী', 0, 'boliardiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3184, 355, 'Dewghar', 'দেওঘর', 0, 'dewgharup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3185, 355, 'Kastul', 'কাস্তুল', 0, 'kastulup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3186, 355, 'Austagram Sadar', 'অষ্টগ্রাম সদর', 0, 'austagramsadarup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3187, 355, 'Bangalpara', 'বাঙ্গালপাড়া', 0, 'bangalparaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3188, 355, 'Kalma', 'কলমা', 0, 'kalmaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3189, 355, 'Adampur', 'আদমপুর', 0, 'adampurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3190, 355, 'Khyerpur-Abdullahpur', 'খয়েরপুর-আব্দুল্লাপুর', 0, 'khyerpurabdullahpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3191, 355, 'Purba Austagram', 'পূর্ব অষ্টগ্রাম', 0, 'purbaaustagramup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3192, 356, 'Gopdighi', 'গোপদিঘী', 0, 'gopdighiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3193, 356, 'Mithamoin', 'মিঠামইন', 0, 'mithamoinup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3194, 356, 'Dhaki', 'ঢাকী', 0, 'dhakiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3195, 356, 'Ghagra', 'ঘাগড়া', 0, 'ghagraup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3196, 356, 'Keoarjore', 'কেওয়ারজোর', 0, 'keoarjoreup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3197, 356, 'Katkhal', 'কাটখাল', 0, 'katkhalup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3198, 356, 'Bairati', 'বৈরাটি', 0, 'bairatiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3199, 357, 'Chatirchar', 'ছাতিরচর', 0, 'chatircharup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3200, 357, 'Guroi', 'গুরই', 0, 'guroiup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3201, 357, 'Jaraitala', 'জারইতলা', 0, 'jaraitalaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3202, 357, 'Nikli Sadar', 'নিকলী সদর', 0, 'niklisadarup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3203, 357, 'Karpasa', 'কারপাশা', 0, 'karpasaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3204, 357, 'Dampara', 'দামপাড়া', 0, 'damparaup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3205, 357, 'Singpur', 'সিংপুর', 0, 'singpurup.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3206, 358, 'Balla', 'বাল্লা', 0, 'ballaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3207, 358, 'Gala', 'গালা', 0, 'galaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3208, 358, 'Chala', 'চালা', 0, 'chalaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3209, 358, 'Blara', 'বলড়া', 0, 'blaraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3210, 358, 'Harukandi', 'হারুকান্দি', 0, 'harukandiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3211, 358, 'Baira', 'বয়রা', 0, 'bairaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3212, 358, 'Ramkrishnapur', 'রামকৃঞ্চপুর', 0, 'ramkrishnapurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3213, 358, 'Gopinathpur', 'গোপীনাথপুর', 0, 'gopinathpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3214, 358, 'Kanchanpur', 'কাঞ্চনপুর', 0, 'kanchanpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3215, 358, 'Lacharagonj', 'লেছড়াগঞ্জ', 0, 'lacharagonjup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3216, 358, 'Sutalorie', 'সুতালড়ী', 0, 'sutalorieup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3217, 358, 'Dhulsura', 'ধূলশুড়া', 0, 'dhulsuraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3218, 358, 'Azimnagar', 'আজিমনগর', 0, 'azimnagarup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3219, 359, 'Baried', 'বরাইদ', 0, 'bariedup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3220, 359, 'Dighulia', 'দিঘুলিয়া', 0, 'dighuliaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3221, 359, 'Baliyati', 'বালিয়াটি', 0, 'baliyatiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3222, 359, 'Dargram', 'দড়গ্রাম', 0, 'dargramup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3223, 359, 'Tilli', 'তিল্লী', 0, 'tilliup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3224, 359, 'Hargaj', 'হরগজ', 0, 'hargajup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3225, 359, 'Saturia', 'সাটুরিয়া', 0, 'saturiaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3226, 359, 'Dhankora', 'ধানকোড়া', 0, 'dhankoraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3227, 359, 'Fukurhati', 'ফুকুরহাটি', 0, 'fukurhatiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3228, 360, 'Betila-Mitara', 'বেতিলা-মিতরা', 0, 'betilamitaraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3229, 360, 'Jagir', 'জাগীর', 0, 'jagirup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3230, 360, 'Atigram', 'আটিগ্রাম', 0, 'atigramup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3231, 360, 'Dighi', 'দিঘী', 0, 'dighiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3232, 360, 'Putile', 'পুটাইল', 0, 'putileup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3233, 360, 'Hatipara', 'হাটিপাড়া', 0, 'hatiparaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3234, 360, 'Vararia', 'ভাড়ারিয়া', 0, 'varariaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3235, 360, 'Nbogram', 'নবগ্রাম', 0, 'nbogramup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3236, 360, 'Garpara', 'গড়পাড়া', 0, 'garparaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3237, 360, 'Krishnapur', 'কৃঞ্চপুর', 0, 'krishnapurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3238, 361, 'Paila', 'পয়লা', 0, 'pailaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3239, 361, 'Shingzuri', 'সিংজুড়ী', 0, 'shingzuriup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3240, 361, 'Baliyakhora', 'বালিয়াখোড়া', 0, 'baliyakhoraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3241, 361, 'Gior', 'ঘিওর', 0, 'giorup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3242, 361, 'Bartia', 'বড়টিয়া', 0, 'bartiaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3243, 361, 'Baniazuri', 'বানিয়াজুড়ী', 0, 'baniazuriup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3244, 361, 'Nalee', 'নালী', 0, 'naleeup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3245, 362, 'Teota', 'তেওতা', 0, 'teotaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3246, 362, 'Utholi', 'উথলী', 0, 'utholiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3247, 362, 'Shibaloy', 'শিবালয়', 0, 'shibaloyup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3248, 362, 'Ulayel', 'উলাইল', 0, 'ulayelup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3249, 362, 'Aruoa', 'আরুয়া', 0, 'aruoaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3250, 362, 'Mohadebpur', 'মহাদেবপুর', 0, 'mohadebpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3251, 362, 'Shimulia', 'শিমুলিয়া', 0, 'shimuliaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3252, 363, 'Charkataree', 'চরকাটারী', 0, 'charkatareeup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3253, 363, 'Bachamara', 'বাচামারা', 0, 'bachamaraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3254, 363, 'Baghutia', 'বাঘুটিয়া', 0, 'baghutiaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3255, 363, 'Zionpur', 'জিয়নপুর', 0, 'zionpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3256, 363, 'Khalshi', 'খলশী', 0, 'khalshiup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3257, 363, 'Chakmirpur', 'চকমিরপুর', 0, 'chakmirpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3258, 363, 'Klia', 'কলিয়া', 0, 'kliaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3259, 363, 'Dhamswar', 'ধামশ্বর', 0, 'dhamswarup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3260, 364, 'Buyra', 'বায়রা', 0, 'buyraup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3261, 364, 'Talebpur', 'তালেবপুর', 0, 'talebpurup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3262, 364, 'Singiar', 'সিংগাইর', 0, 'singiarup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3263, 364, 'Baldhara', 'বলধারা', 0, 'baldharaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3264, 364, 'Zamsha', 'জামশা', 0, 'zamshaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3265, 364, 'Charigram', 'চারিগ্রাম', 0, 'charigramup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3266, 364, 'Shayesta', 'শায়েস্তা', 0, 'shayestaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3267, 364, 'Joymonto', 'জয়মন্টপ', 0, 'joymontopup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3268, 364, 'Dhalla', 'ধল্লা', 0, 'dhallaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3269, 364, 'Jamirta', 'জার্মিতা', 0, 'jamirtaup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3270, 364, 'Chandhar', 'চান্দহর', 0, 'chandharup.manikganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3271, 365, 'Savar', 'সাভার', 0, 'savarup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3272, 365, 'Birulia', 'বিরুলিয়া', 0, 'birulia.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3273, 365, 'Dhamsona', 'ধামসোনা', 0, 'dhamsonaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3274, 365, 'Shimulia', 'শিমুলিয়া', 0, 'shimuliaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3275, 365, 'Ashulia', 'আশুলিয়া', 0, 'ashuliaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3276, 365, 'Yearpur', 'ইয়ারপুর', 0, 'yearpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3277, 365, 'Vakurta', 'ভাকুর্তা', 0, 'vakurtaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3278, 365, 'Pathalia', 'পাথালিয়া', 0, 'pathaliaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3279, 365, 'Bongaon', 'বনগাঁও', 0, 'bongaonup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3280, 365, 'Kaundia', 'কাউন্দিয়া', 0, 'kaundiaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3281, 365, 'Tetuljhora', 'তেঁতুলঝোড়া', 0, 'tetuljhora.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3282, 365, 'Aminbazar', 'আমিনবাজার', 0, 'aminbazar.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3283, 366, 'Chauhat', 'চৌহাট', 0, 'chauhatup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3284, 366, 'Amta', 'আমতা', 0, 'amtaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3285, 366, 'Balia', 'বালিয়া', 0, 'baliaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3286, 366, 'Jadabpur', 'যাদবপুর', 0, 'jadabpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3287, 366, 'Baisakanda', 'বাইশাকান্দা', 0, 'baisakandaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3288, 366, 'Kushura', 'কুশুরা', 0, 'kushuraup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3289, 366, 'Gangutia', 'গাংগুটিয়া', 0, 'gangutiaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3290, 366, 'Sanora', 'সানোড়া', 0, 'sanoraup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3291, 366, 'Sutipara', 'সূতিপাড়া', 0, 'sutiparaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3292, 366, 'Sombhag', 'সোমভাগ', 0, 'sombhagup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3293, 366, 'Vararia', 'ভাড়ারিয়া', 0, 'varariaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3294, 366, 'Dhamrai', 'ধামরাই', 0, 'dhamraiup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3295, 366, 'Kulla', 'কুল্লা', 0, 'kullaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3296, 366, 'Rowail', 'রোয়াইল', 0, 'rowailup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3297, 366, 'Suapur', 'সুয়াপুর', 0, 'suapurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3298, 366, 'Nannar', 'নান্নার', 0, 'nannarup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3299, 367, 'Hazratpur', 'হযরতপুর', 0, 'hazratpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3300, 367, 'Kalatia', 'কলাতিয়া', 0, 'kalatiaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3301, 367, 'Taranagar', 'তারানগর', 0, 'taranagarup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3302, 367, 'Sakta', 'শাক্তা', 0, 'saktaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3303, 367, 'Ruhitpur', 'রোহিতপুর', 0, 'ruhitpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3304, 367, 'Basta', 'বাস্তা', 0, 'bastaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3305, 367, 'Kalindi', 'কালিন্দি', 0, 'kalindiup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3306, 367, 'Zinzira', 'জিনজিরা', 0, 'zinziraup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3307, 367, 'Suvadda', 'শুভাঢ্যা', 0, 'suvaddaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3308, 367, 'Taghoria', 'তেঘরিয়া', 0, 'taghoriaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3309, 367, 'Konda', 'কোন্ডা', 0, 'kondaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3310, 367, 'Aganagar', 'আগানগর', 0, 'aganagarup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3311, 368, 'Shikaripara', 'শিকারীপাড়া', 0, 'shikariparaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3312, 368, 'Joykrishnapur', 'জয়কৃষ্ণপুর', 0, 'joykrishnapurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3313, 368, 'Baruakhali', 'বারুয়াখালী', 0, 'baruakhaliup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3314, 368, 'Nayansree', 'নয়নশ্রী', 0, 'nayansreeup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3315, 368, 'Sholla', 'শোল্লা', 0, 'shollaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3316, 368, 'Jantrail', 'যন্ত্রাইল', 0, 'jantrailup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3317, 368, 'Bandura', 'বান্দুরা', 0, 'banduraup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3318, 368, 'Kalakopa', 'কলাকোপা', 0, 'kalakopaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3319, 368, 'Bakshanagar', 'বক্সনগর', 0, 'bakshanagarup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3320, 368, 'Barrah', 'বাহ্রা', 0, 'barrahup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3321, 368, 'Kailail', 'কৈলাইল', 0, 'kailailup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3322, 368, 'Agla', 'আগলা', 0, 'aglaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3323, 368, 'Galimpur', 'গালিমপুর', 0, 'galimpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3324, 368, 'Churain', 'চুড়াইন', 0, 'churainup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3325, 369, 'Nayabari', 'নয়াবাড়ী', 0, 'nayabariup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3326, 369, 'Kusumhathi', 'কুসুমহাটি', 0, 'kusumhathiup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3327, 369, 'Raipara', 'রাইপাড়া', 0, 'raiparaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3328, 369, 'Sutarpara', 'সুতারপাড়া', 0, 'sutarparaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3329, 369, 'Narisha', 'নারিশা', 0, 'narishaup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3330, 369, 'Muksudpur', 'মুকসুদপুর', 0, 'muksudpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3331, 369, 'Mahmudpur', 'মাহমুদপুর', 0, 'mahmudpurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3332, 369, 'Bilaspur', 'বিলাসপুর', 0, 'bilaspurup.dhaka.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3333, 370, 'Rampal', 'রামপাল', 0, 'rampalup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3334, 370, 'Panchashar', 'পঞ্চসার', 0, 'panchasharup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3335, 370, 'Bajrajogini', 'বজ্রযোগিনী', 0, 'bajrajoginiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3336, 370, 'Mohakali', 'মহাকালী', 0, 'mohakaliup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3337, 370, 'Charkewar', 'চরকেওয়ার', 0, 'charkewarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3338, 370, 'Mollakandi', 'মোল্লাকান্দি', 0, 'mollakandiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3339, 370, 'Adhara', 'আধারা', 0, 'adharaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3340, 370, 'Shiloy', 'শিলই', 0, 'shiloyup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3341, 370, 'Banglabazar', 'বাংলাবাজার', 0, 'banglabazarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3342, 371, 'Baraikhali', 'বাড়েখাল', 0, 'baraikhaliup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3343, 371, 'Hashara', 'হাসাড়া', 0, 'hasharaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3344, 371, 'Birtara', 'বাড়তারা', 0, 'birtaraup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3345, 371, 'Shologhor', 'ষোলঘর', 0, 'shologhorup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3346, 371, 'Sreenagar', 'শ্রীনগর', 0, 'sreenagarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3347, 371, 'Patabhog', 'পাঢাভোগ', 0, 'patabhogup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3348, 371, 'Shamshiddi', 'শ্যামসিদ্দি', 0, 'shamshiddiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3349, 371, 'Kolapara', 'কুলাপাড়া', 0, 'kolaparaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3350, 371, 'Vaggakol', 'ভাগ্যকুল', 0, 'vaggakolup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3351, 371, 'Bagra', 'বাঘড়া', 0, 'bagraup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3352, 371, 'Rarikhal', 'রাঢ়ীখাল', 0, 'rarikhalup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3353, 371, 'Kukutia', 'কুকুটিয়া', 0, 'kukutiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3354, 371, 'Atpara', 'আটপাড়া', 0, 'atparaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3355, 371, 'Tantor', 'তন্তর', 0, 'tantorup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3356, 372, 'Chitracoat', 'চিত্রকোট', 0, 'chitracoatup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3357, 372, 'Sekhornagar', 'শেখরনগার', 0, 'sekhornagarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3358, 372, 'Rajanagar', 'রাজানগর', 0, 'rajanagarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3359, 372, 'Keyain', 'কেয়াইন', 0, 'keyainup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3360, 372, 'Basail', 'বাসাইল', 0, 'basailup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3361, 372, 'Baluchar', 'বালুচর', 0, 'balucharup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3362, 372, 'Latabdi', 'লতাব্দী', 0, 'latabdiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3363, 372, 'Rasunia', 'রশুনিয়া', 0, 'rasuniaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3364, 372, 'Ichhapura', 'ইছাপুরা', 0, 'ichhapuraup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3365, 372, 'Bairagadi', 'বয়রাগাদি', 0, 'bairagadiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3366, 372, 'Malkhanagar', 'মালখানগর', 0, 'malkhanagarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3367, 372, 'Madhypara', 'মধ্যপাড়া', 0, 'madhyparaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3368, 372, 'Kola', 'কোলা', 0, 'kolaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3369, 372, 'Joyinshar', 'জৈনসার', 0, 'joyinsharup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3370, 373, 'Medinimandal', 'মেদিনীমন্ডল', 0, 'medinimandalup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3371, 373, 'Kumarbhog', 'কুমারভোগ', 0, 'kumarbhogup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3372, 373, 'Haldia', 'হলদিয়া', 0, 'haldiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3373, 373, 'Kanaksar', 'কনকসার', 0, 'kanaksarup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3374, 373, 'Lohajang-Teotia', 'লৌহজং-তেওটিয়া', 0, 'lohajangteotiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3375, 373, 'Bejgaon', 'বেজগাঁও', 0, 'bejgaonup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3376, 373, 'Baultoli', 'বৌলতলী', 0, 'baultoliup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3377, 373, 'Khidirpara', 'খিদিরপাড়া', 0, 'khidirparaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3378, 373, 'Gaodia', 'গাওদিয়া', 0, 'gaodiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3379, 373, 'Kalma', 'কলমা', 0, 'kalmaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3380, 374, 'Gajaria', 'গজারিয়া', 0, 'gajariaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3381, 374, 'Baushia', 'বাউশিয়া', 0, 'baushiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3382, 374, 'Vaberchar', 'ভবেরচর', 0, 'vabercharup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3383, 374, 'Baluakandi', 'বালুয়াকান্দী', 0, 'baluakandiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3384, 374, 'Tengarchar', 'টেংগারচর', 0, 'tengarcharup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3385, 374, 'Hosendee', 'হোসেন্দী', 0, 'hosendeeup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3386, 374, 'Guagachia', 'গুয়াগাছিয়া', 0, 'guagachiaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3387, 374, 'Imampur', 'ইমামপুর', 0, 'imampurup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3388, 375, 'Betka', 'বেতকা', 0, 'betkaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3389, 375, 'Abdullapur', 'আব্দুল্লাপুর', 0, 'abdullapurup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3390, 375, 'Sonarong Tongibari', 'সোনারং টংগীবাড়ী', 0, 'sonarongtongibariup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3391, 375, 'Autshahi', 'আউটশাহী', 0, 'autshahiup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3392, 375, 'Arial Baligaon', 'আড়িয়ল বালিগাঁও', 0, 'arialbaligaonup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3393, 375, 'Dhipur', 'ধীপুর', 0, 'dhipurup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3394, 375, 'Kathadia Shimolia', 'কাঠাদিয়া শিমুলিয়া', 0, 'kathadiashimoliaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3395, 375, 'Joslong', 'যশলং', 0, 'joslongup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3396, 375, 'Panchgaon', 'পাঁচগাও', 0, 'panchgaonup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3397, 375, 'Kamarkhara', 'কামারখাড়া', 0, 'kamarkharaup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3398, 375, 'Hasailbanari', 'হাসাইল বানারী', 0, 'hasailbanariup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3399, 375, 'Dighirpar', 'দিঘীরপাড়', 0, 'dighirparup.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3400, 376, 'Mijanpur', 'মিজানপুর', 0, 'mijanpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3401, 376, 'Borat', 'বরাট', 0, 'boratup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3402, 376, 'Chandoni', 'চন্দনী', 0, 'chandoniup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3403, 376, 'Khangonj', 'খানগঞ্জ', 0, 'khangonjup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3404, 376, 'Banibaha', 'বানীবহ', 0, 'banibahaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3405, 376, 'Dadshee', 'দাদশী', 0, 'dadsheeup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3406, 376, 'Mulghar', 'মুলঘর', 0, 'mulgharup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3407, 376, 'Basantapur', 'বসন্তপুর', 0, 'basantapurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3408, 376, 'Khankhanapur', 'খানখানাপুর', 0, 'khankhanapurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3409, 376, 'Alipur', 'আলীপুর', 0, 'alipurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3410, 376, 'Ramkantapur', 'রামকান্তপুর', 0, 'ramkantapurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3411, 376, 'Shahidwahabpur', 'শহীদওহাবপুর', 0, 'shahidwahabpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3412, 376, 'Panchuria', 'পাঁচুরিয়া', 0, 'panchuriaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3413, 376, 'Sultanpur', 'সুলতানপুর', 0, 'sultanpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3414, 377, 'Doulatdia', 'দৌলতদিয়া', 0, 'doulatdiaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3415, 377, 'Debugram', 'দেবগ্রাম', 0, 'debugramup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3416, 377, 'Uzancar', 'উজানচর', 0, 'uzancarup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3417, 377, 'Chotovakla', 'ছোটভাকলা', 0, 'chotovaklaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3418, 378, 'Bahadurpur', 'বাহাদুরপুর', 0, 'bahadurpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3419, 378, 'Habashpur', 'হাবাসপুর', 0, 'habashpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3420, 378, 'Jashai', 'যশাই', 0, 'jashaiup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3421, 378, 'Babupara', 'বাবুপাড়া', 0, 'babuparaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3422, 378, 'Mourat', 'মৌরাট', 0, 'mouratup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3423, 378, 'Patta', 'পাট্টা', 0, 'pattaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3424, 378, 'Sarisha', 'সরিষা', 0, 'sarishaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3425, 378, 'Kalimahar', 'কলিমহর', 0, 'kalimaharup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3426, 378, 'Kasbamajhail', 'কসবামাজাইল', 0, 'kasbamajhailup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3427, 378, 'Machhpara', 'মাছপাড়া', 0, 'machhparaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3428, 379, 'Islampur', 'ইসলামপুর', 0, 'islampurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3429, 379, 'Baharpur', 'বহরপুর', 0, 'baharpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3430, 379, 'Nawabpur', 'নবাবপুর', 0, 'nawabpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3431, 379, 'Narua', 'নারুয়া', 0, 'naruaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3432, 379, 'Baliakandi', 'বালিয়াকান্দি', 0, 'baliakandiup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3433, 379, 'Janjal', 'জঙ্গল', 0, 'janjalup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3434, 379, 'Jamalpur', 'জামালপুর', 0, 'jamalpurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3435, 380, 'Kalukhali', 'কালুখালী', 0, 'kalukhaliup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3436, 380, 'Ratandia', 'রতনদিয়া', 0, 'ratandiaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3437, 380, 'Kalikapur', 'কালিকাপুর', 0, 'kalikapurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3438, 380, 'Boalia', 'বোয়ালিয়া', 0, 'boaliaup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3439, 380, 'Majbari', 'মাজবাড়ী', 0, 'majbariup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3440, 380, 'Madapur', 'মদাপুর', 0, 'madapurup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3441, 380, 'Shawrail', 'সাওরাইল', 0, 'shawrailup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3442, 380, 'Mrigi', 'মৃগী', 0, 'mrigiup.rajbari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3443, 381, 'Sirkhara', 'শিড়খাড়া', 0, 'sirkharaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3444, 381, 'Bahadurpur', 'বাহাদুরপুর', 0, 'bahadurpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3445, 381, 'Kunia', 'কুনিয়া', 0, 'kuniaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3446, 381, 'Peyarpur', 'পেয়ারপুর', 0, 'peyarpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3447, 381, 'Kandua', 'কেন্দুয়া', 0, 'kanduaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3448, 381, 'Mastofapur', 'মস্তফাপুর', 0, 'mastofapurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3449, 381, 'Dudkhali', 'দুধখালী', 0, 'dudkhaliup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3450, 381, 'Kalikapur', 'কালিকাপুর', 0, 'kalikapurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3451, 381, 'Chilarchar', 'ছিলারচর', 0, 'chilarcharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3452, 381, 'Panchkhola', 'পাঁচখোলা', 0, 'panchkholaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3453, 381, 'Ghatmajhi', 'ঘটমাঝি', 0, 'ghatmajhiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3454, 381, 'Jhaoudi', 'ঝাউদী', 0, 'jhaoudiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3455, 381, 'Khoajpur', 'খোয়াজপুর', 0, 'khoajpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3456, 381, 'Rasti', 'রাস্তি', 0, 'rastiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3457, 381, 'Dhurail', 'ধুরাইল', 0, 'dhurailup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3458, 382, 'Shibchar', 'শিবচর', 0, 'shibcharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3459, 382, 'Ditiyakhando', 'দ্বিতীয়খন্ড', 0, 'ditiyakhandoup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3460, 382, 'Nilokhe', 'নিলখি', 0, 'nilokheup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3461, 382, 'Bandarkhola', 'বন্দরখোলা', 0, 'bandarkholaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3462, 382, 'Charjanazat', 'চরজানাজাত', 0, 'charjanazatup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3463, 382, 'Madbarerchar', 'মাদবরেরচর', 0, 'madbarercharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3464, 382, 'Panchar', 'পাঁচচর', 0, 'pancharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3465, 382, 'Sannasirchar', 'সন্যাসিরচর', 0, 'sannasircharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3466, 382, 'Kathalbari', 'কাঁঠালবাড়ী', 0, 'kathalbariup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3467, 382, 'Kutubpur', 'কুতুবপুর', 0, 'kutubpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3468, 382, 'Kadirpur', 'কাদিরপুর', 0, 'kadirpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3469, 382, 'Vhandarikandi', 'ভান্ডারীকান্দি', 0, 'vhandarikandiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3470, 382, 'Bahertala South', 'বহেরাতলা দক্ষিণ', 0, 'bahertalasouthup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3471, 382, 'Baheratala North', 'বহেরাতলা উত্তর', 0, 'baheratalanorthup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3472, 382, 'Baskandi', 'বাঁশকান্দি', 0, 'baskandiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3473, 382, 'Umedpur', 'উমেদপুর', 0, 'umedpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3474, 382, 'Vhadrasion', 'ভদ্রাসন', 0, 'vhadrasionup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3475, 382, 'Shiruail', 'শিরুয়াইল', 0, 'shiruailup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3476, 382, 'Dattapara', 'দত্তপাড়া', 0, 'dattaparaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3477, 383, 'Alinagar', 'আলীনগর', 0, 'alinagarup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3478, 383, 'Baligram', 'বালীগ্রাম', 0, 'baligramup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3479, 383, 'Basgari', 'বাঁশগাড়ী', 0, 'basgariup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3480, 383, 'Chardoulatkhan', 'চরদৌলতখান', 0, 'chardoulatkhanup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3481, 383, 'Dashar', 'ডাসার', 0, 'dasharup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3482, 383, 'Enayetnagor', 'এনায়েতনগর', 0, 'enayetnagorup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3483, 383, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3484, 383, 'Koyaria', 'কয়ারিয়া', 0, 'koyariaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3485, 383, 'Kazibakai', 'কাজীবাকাই', 0, 'kazibakaiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3486, 383, 'Laxmipur', 'লক্ষীপুর', 0, 'laxmipurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3487, 383, 'Nabogram', 'নবগ্রাম', 0, 'nabogramup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3488, 383, 'Ramjanpur', 'রমজানপুর', 0, 'ramjanpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3489, 383, 'Shahebrampur', 'সাহেবরামপুর', 0, 'shahebrampurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3490, 383, 'Shikarmongol', 'শিকারমঙ্গল', 0, 'shikarmongolup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3491, 384, 'Haridasdi-Mahendrodi', 'হরিদাসদী-মহেন্দ্রদী', 0, 'haridasdi-mahendrodiup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3492, 384, 'Kadambari', 'কদমবাড়ী', 0, 'kadambariup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3493, 384, 'Bajitpur', 'বাজিতপুর', 0, 'bajitpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3494, 384, 'Amgram', 'আমগ্রাম', 0, 'amgramup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3495, 384, 'Rajoir', 'রাজৈর', 0, 'rajoirup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3496, 384, 'Khaliya', 'খালিয়া', 0, 'khaliyaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3497, 384, 'Ishibpur', 'ইশিবপুর', 0, 'ishibpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3498, 384, 'Badarpasa', 'বদরপাশা', 0, 'badarpasaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3499, 384, 'Kabirajpur', 'কবিরাজপুর', 0, 'kabirajpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3500, 384, 'Hosenpur', 'হোসেনপুর', 0, 'hosenpurup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3501, 384, 'Paikpara', 'পাইকপাড়া', 0, 'paikparaup.madaripur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3502, 385, 'Jalalabad', 'জালালাবাদ', 1, 'jalalabadup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3503, 385, 'Shuktail', 'শুকতাইল', 3, 'shuktailup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3504, 385, 'Chandradighalia', 'চন্দ্রদিঘলিয়া', 4, 'chandradighaliaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3505, 385, 'Gopinathpur', 'গোপীনাথপুর', 5, 'gopinathpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3506, 385, 'Paikkandi', 'পাইককান্দি', 6, 'paikkandiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3507, 385, 'Urfi', 'উরফি', 7, 'urfiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3508, 385, 'Lotifpur', 'লতিফপুর', 8, 'lotifpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3509, 385, 'Satpar', 'সাতপাড়', 9, 'satparup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3510, 385, 'Sahapur', 'সাহাপুর', 10, 'sahapurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3511, 385, 'Horidaspur', 'হরিদাসপুর', 11, 'horidaspurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3512, 385, 'Ulpur', 'উলপুর', 12, 'ulpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3513, 385, 'Nizra', 'নিজড়া', 13, 'nizraup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3514, 385, 'Karpara', 'করপাড়া', 14, 'karparaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3515, 385, 'Durgapur', 'দুর্গাপুর', 15, 'durgapurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3516, 385, 'Kajulia', 'কাজুলিয়া', 16, 'kajuliaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3517, 385, 'Majhigati', 'মাঝিগাতী', 18, 'majhigatiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3518, 385, 'Roghunathpur', 'রঘুনাথপুর', 19, 'roghunathpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3519, 385, 'Gobra', 'গোবরা', 20, 'gobraup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3520, 385, 'Borashi', 'বোড়াশী', 21, 'borashiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3521, 385, 'Kati', 'কাঠি', 17, 'katiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3522, 385, 'Boultali', 'বৌলতলী', 2, 'boultaliup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3523, 386, 'Kashiani', 'কাশিয়ানী', 5, 'kashianiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3524, 386, 'Hatiara', 'হাতিয়াড়া', 10, 'hatiaraup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3525, 386, 'Fukura', 'ফুকরা', 7, 'fukuraup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3526, 386, 'Rajpat', 'রাজপাট', 9, 'rajpatup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3527, 386, 'Bethuri', 'বেথুড়ী', 12, 'bethuriup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3528, 386, 'Nijamkandi', 'নিজামকান্দি', 14, 'nijamkandiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3529, 386, 'Sajail', 'সাজাইল', 4, 'sajailup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3530, 386, 'Mamudpur', 'মাহমুদপুর', 3, 'mamudpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3531, 386, 'Maheshpur', 'মহেশপুর', 1, 'maheshpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3532, 386, 'Orakandia', 'ওড়াকান্দি', 8, 'orakandiaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3533, 386, 'Parulia', 'পারুলিয়া', 2, 'paruliaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3534, 386, 'Ratail', 'রাতইল', 6, 'ratailup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3535, 386, 'Puisur', 'পুইশুর', 13, 'puisurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3536, 386, 'Singa', 'সিংগা', 11, 'singaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3537, 387, 'Kushli', 'কুশলী', 1, 'kushliup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3538, 387, 'Gopalpur', 'গোপালপুর', 3, 'gopalpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3539, 387, 'Patgati', 'পাটগাতী', 4, 'patgatiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3540, 387, 'Borni', 'বর্ণি', 2, 'borniup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3541, 387, 'Dumaria', 'ডুমরিয়া', 5, 'dumariaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3542, 388, 'Sadullapur', 'সাদুল্লাপুর', 2, 'sadullapurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3543, 388, 'Ramshil', 'রামশীল', 3, 'ramshilup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3544, 388, 'Bandhabari', 'বান্ধাবাড়ী', 4, 'bandhabariup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3545, 388, 'Kolabari', 'কলাবাড়ী', 1, 'kolabariup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3546, 388, 'Kushla', 'কুশলা', 6, 'kushlaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3547, 388, 'Amtoli', 'আমতলী', 9, 'amtoliup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3548, 388, 'Pinjuri', 'পিঞ্জুরী', 11, 'pinjuriup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3549, 388, 'Suagram', 'শুয়াগ্রাম', 10, 'ghaghorup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3550, 388, 'Radhaganj', 'রাধাগঞ্জ', 5, 'radhaganjup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3551, 388, 'Hiron', 'হিরণ', 7, 'hironup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3552, 388, 'Kandi', 'কান্দি', 12, 'kandiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3553, 389, 'Ujani', 'উজানী', 0, 'ujaniup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3554, 389, 'Nanikhir', 'ননীক্ষীর', 0, 'nanikhirup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3555, 389, 'Dignagar', 'দিগনগর', 0, 'dignagarup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3556, 389, 'Poshargati', 'পশারগাতি', 0, 'poshargatiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3557, 389, 'Gobindopur', 'গোবিন্দপুর', 0, 'gobindopurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3558, 389, 'Khandarpara', 'খান্দারপাড়া', 0, 'khandarparaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3559, 389, 'Bohugram', 'বহুগ্রাম', 0, 'bohugramup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3560, 389, 'Banshbaria', 'বাশঁবাড়িয়া', 0, 'banshbariaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3561, 389, 'Vabrashur', 'ভাবড়াশুর', 0, 'vabrashurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3562, 389, 'Moharajpur', 'মহারাজপুর', 0, 'moharajpurup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3563, 389, 'Batikamari', 'বাটিকামারী', 0, 'batikamariup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3564, 389, 'Jalirpar', 'জলিরপাড়', 0, 'jalirparup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3565, 389, 'Raghdi', 'রাঘদী', 0, 'raghdiup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3566, 389, 'Gohala', 'গোহালা', 0, 'gohalaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3567, 389, 'Mochna', 'মোচনা', 0, 'mochnaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3568, 389, 'Kashalia', 'কাশালিয়া', 0, 'kashaliaup.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3569, 390, 'Ishangopalpur', 'ঈশানগোপালপুর', 0, 'ishangopalpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3570, 390, 'Charmadbdia', 'চরমাধবদিয়া', 0, 'charmadbdiaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3571, 390, 'Aliabad', 'আলিয়াবাদ', 0, 'aliabadup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3572, 390, 'Uttarchannel', 'নর্থচ্যানেল', 0, 'uttarchannelup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3573, 390, 'Decreerchar', 'ডিক্রিরচর', 0, 'decreercharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3574, 390, 'Majchar', 'মাচ্চর', 0, 'majcharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3575, 390, 'Krishnanagar', 'কৃষ্ণনগর', 0, 'krishnanagarup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3576, 390, 'Ambikapur', 'অম্বিকাপুর', 0, 'ambikapurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3577, 390, 'Kanaipur', 'কানাইপুর', 0, 'kanaipurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3578, 390, 'Kaijuri', 'কৈজুরী', 0, 'kaijuriup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3579, 390, 'Greda', 'গেরদা', 0, 'gredaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3580, 391, 'Buraich', 'বুড়াইচ', 0, 'buraichup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3581, 391, 'Alfadanga', 'আলফাডাঙ্গা', 0, 'alfadangaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3582, 391, 'Tagarbanda', 'টগরবন্দ', 0, 'tagarbandaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3583, 391, 'Bana', 'বানা', 0, 'banaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3584, 391, 'Panchuria', 'পাঁচুড়িয়া', 0, 'panchuriaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3585, 391, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3586, 392, 'Boalmari', 'বোয়ালমারী', 0, 'boalmariup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3587, 392, 'Dadpur', 'দাদপুর', 0, 'dadpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3588, 392, 'Chatul', 'চতুল', 0, 'chatulup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3589, 392, 'Ghoshpur', 'ঘোষপুর', 0, 'ghoshpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3590, 392, 'Gunbaha', 'গুনবহা', 0, 'gunbahaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3591, 392, 'Chandpur', 'চাঁদপুর', 0, 'chandpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3592, 392, 'Parameshwardi', 'পরমেশ্বরদী', 0, 'parameshwardiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3593, 392, 'Satair', 'সাতৈর', 0, 'satairup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3594, 392, 'Rupapat', 'রূপাপাত', 0, 'rupapatup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3595, 392, 'Shekhar', 'শেখর', 0, 'shekharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3596, 392, 'Moyna', 'ময়না', 0, 'moynaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3597, 393, 'Char Bisnopur', 'চর বিষ্ণুপুর', 0, 'charbisnopurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3598, 393, 'Akoter Char', 'আকোটের চর', 0, 'akotercharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3599, 393, 'Char Nasirpur', 'চর নাসিরপুর', 0, 'charnasirpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3600, 393, 'Narikel Bariya', 'নারিকেল বাড়িয়া', 0, 'narikelbariyaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3601, 393, 'Bhashanchar', 'ভাষানচর', 0, 'bhashancharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3602, 393, 'Krishnapur', 'কৃষ্ণপুর', 0, 'krishnapurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3603, 393, 'Sadarpur', 'সদরপুর', 0, 'sadarpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3604, 393, 'Char Manair', 'চর মানাইর', 0, 'charmanairup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3605, 393, 'Dhaukhali', 'ঢেউখালী', 0, 'dhaukhaliup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3606, 394, 'Charjashordi', 'চরযশোরদী', 0, 'charjashordiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3607, 394, 'Purapara', 'পুরাপাড়া', 0, 'puraparaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3608, 394, 'Laskardia', 'লস্করদিয়া', 0, 'laskardiaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3609, 394, 'Ramnagar', 'রামনগর', 0, 'ramnagarup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3610, 394, 'Kaichail', 'কাইচাইল', 0, 'kaichailup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3611, 394, 'Talma', 'তালমা', 0, 'talmaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3612, 394, 'Fulsuti', 'ফুলসুতি', 0, 'fulsutiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3613, 394, 'Dangi', 'ডাঙ্গী', 0, 'dangiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3614, 394, 'Kodalia Shohidnagar', 'কোদালিয়া শহিদনগর', 0, 'kodaliashohidnagarup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3615, 395, 'Gharua', 'ঘারুয়া', 0, 'gharuaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3616, 395, 'Nurullagonj', 'নুরুল্যাগঞ্জ', 0, 'nurullagonjup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3617, 395, 'Manikdha', 'মানিকদহ', 0, 'manikdhaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3618, 395, 'Kawlibera', 'কাউলিবেড়া', 0, 'kawliberaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3619, 395, 'Nasirabad', 'নাছিরাবাদ', 0, 'nasirabadup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3620, 395, 'Tujerpur', 'তুজারপুর', 0, 'tujerpurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3621, 395, 'Algi', 'আলগী', 0, 'algiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3622, 395, 'Chumurdi', 'চুমুরদী', 0, 'chumurdiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3623, 395, 'Kalamridha', 'কালামৃধা', 0, 'kalamridhaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3624, 395, 'Azimnagor', 'আজিমনগর', 0, 'azimnagorup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3625, 395, 'Chandra', 'চান্দ্রা', 0, 'chandraup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3626, 395, 'Hamirdi', 'হামিরদী', 0, 'hamirdiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3627, 396, 'Gazirtek', 'গাজীরটেক', 0, 'gazirtekup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3628, 396, 'Char Bhadrasan', 'চর ভদ্রাসন', 0, 'charbhadrasanup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3629, 396, 'Char Harirampur', 'চর হরিরামপুর', 0, 'charharirampurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3630, 396, 'Char Jahukanda', 'চর ঝাউকান্দা', 0, 'charjahukandaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3631, 397, 'Madhukhali', 'মধুখালী', 0, 'madhukhaliup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3632, 397, 'Jahapur', 'জাহাপুর', 0, 'jahapurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3633, 397, 'Gazna', 'গাজনা', 0, 'gaznaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3634, 397, 'Megchami', 'মেগচামী', 0, 'megchamiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3635, 397, 'Raipur', 'রায়পুর', 0, 'raipurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3636, 397, 'Bagat', 'বাগাট', 0, 'bagatup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3637, 397, 'Dumain', 'ডুমাইন', 0, 'dumainup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3638, 397, 'Nowpara', 'নওপাড়া', 0, 'nowparaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3639, 397, 'Kamarkhali', 'কামারখালী', 0, 'kamarkhaliup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3640, 398, 'Bhawal', 'ভাওয়াল', 0, 'bhawalup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3641, 398, 'Atghar', 'আটঘর', 0, 'atgharup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3642, 398, 'Mazadia', 'মাঝারদিয়া', 0, 'mazadiaup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3643, 398, 'Ballabhdi', 'বল্লভদী', 0, 'ballabhdiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3644, 398, 'Gatti', 'গট্টি', 0, 'gattiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3645, 398, 'Jadunandi', 'যদুনন্দী', 0, 'jadunandiup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3646, 398, 'Ramkantapur', 'রামকান্তপুর', 0, 'ramkantapurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3647, 398, 'Sonapur', 'সোনাপুর', 0, 'sonapurup.faridpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3648, 399, 'Panchagarh Sadar', 'পঞ্চগড় সদর', 0, 'panchagarhsadarup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3649, 399, 'Satmara', 'সাতমেরা', 0, 'satmaraup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3650, 399, 'Amarkhana', 'অমরখানা', 0, 'amarkhanaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3651, 399, 'Haribhasa', 'হাড়িভাসা', 0, 'haribhasaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3652, 399, 'Chaklahat', 'চাকলাহাট', 0, 'chaklahatup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3653, 399, 'Hafizabad', 'হাফিজাবাদ', 0, 'hafizabadup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3654, 399, 'Kamat Kajol Dighi', 'কামাত কাজল দীঘি', 0, 'kamatkajoldighiup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3655, 399, 'Dhakkamara', 'ধাক্কামারা', 0, 'dhakkamaraup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3656, 399, 'Magura', 'মাগুরা', 0, 'maguraup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3657, 399, 'Garinabari', 'গরিনাবাড়ী', 0, 'garinabariup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3658, 400, 'Chilahati', 'চিলাহাটি', 0, 'chilahatiup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3659, 400, 'Shaldanga', 'শালডাঙ্গা', 0, 'shaldangaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3660, 400, 'Debiganj Sadar', 'দেবীগঞ্জ সদর', 0, 'debiganjsadarup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3661, 400, 'Pamuli', 'পামুলী', 0, 'pamuliup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3662, 400, 'Sundardighi', 'সুন্দরদিঘী', 0, 'sundardighiup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3663, 400, 'Sonahar Mollikadaha', 'সোনাহার মল্লিকাদহ', 0, 'sonaharmollikadahaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3664, 400, 'Tepriganj', 'টেপ্রীগঞ্জ', 0, 'tepriganjup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3665, 400, 'Dandopal', 'দন্ডপাল', 0, 'dandopalup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3666, 400, 'Debiduba', 'দেবীডুবা', 0, 'debidubaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3667, 400, 'Chengthi Hazra Danga', 'চেংঠী হাজরা ডাঙ্গা', 0, 'chengthihazradangaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3668, 401, 'Jholaishal Shiri', 'ঝলইশাল শিরি', 0, 'jholaishalshiriup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3669, 401, 'Moidandighi', 'ময়দান দীঘি', 0, 'moidandighiup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3670, 401, 'Banghari', 'বেংহারী', 0, 'banghariup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3671, 401, 'Kajoldighi Kaligonj', 'কাজলদীঘি কালিগঞ্জ', 0, 'kajoldighikaligonjup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3672, 401, 'Boroshoshi', 'বড়শশী', 0, 'boroshoshiup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3673, 401, 'Chandanbari', 'চন্দনবাড়ী', 0, 'chandanbariup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3674, 401, 'Marea Bamonhat', 'মাড়েয়া বামনহাট', 0, 'mareabamonhatup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3675, 401, 'Boda', 'বোদা', 0, 'bodaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3676, 401, 'Sakoa', 'সাকোয়া', 0, 'sakoaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3677, 401, 'Pachpir', 'পাচপীর', 0, 'pachpirup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3678, 402, 'Mirgapur', 'মির্জাপুর', 0, 'mirgapurup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3679, 402, 'Radhanagar', 'রাধানগর', 0, 'radhanagarup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3680, 402, 'Toria', 'তোড়িয়া', 0, 'toriaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3681, 402, 'Balarampur', 'বলরামপুর', 0, 'balarampurup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3682, 402, 'Alowakhowa', 'আলোয়াখোয়া', 0, 'alowakhowaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3683, 402, 'Dhamor', 'ধামোর', 0, 'dhamorup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3684, 403, 'Banglabandha', 'বাংলাবান্ধা', 0, 'banglabandhaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3685, 403, 'Bhojoanpur', 'ভজনপুর', 0, 'bhojoanpurup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3686, 403, 'Bhojoanpur', 'ভজনপুর', 0, 'bhojoanpur.gazipur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3687, 403, 'Buraburi', 'বুড়াবুড়ী', 0, 'buraburi.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3688, 403, 'Debnagar', 'দেবনগর', 0, 'debnagarup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3689, 403, 'Salbahan', 'শালবাহান', 0, 'salbahanup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3690, 403, 'Tentulia', 'তেতুলিয়া', 0, 'tentuliaup.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3691, 403, 'Timaihat', 'তিমাইহাট', 0, 'timaihat.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3692, 404, 'Joypur', 'জয়পুর', 0, 'joypurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3693, 404, 'Binodnagar', 'বিনোদনগর', 0, 'binodnagarup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3694, 404, 'Golapgonj', 'গোলাপগঞ্জ', 0, 'golapgonjup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3695, 404, 'Shalkhuria', 'শালখুরিয়া', 0, 'shalkhuriaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3696, 404, 'Putimara', 'পুটিমারা', 0, 'putimaraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3697, 404, 'Bhaduria', 'ভাদুরিয়া', 0, 'bhaduriaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3698, 404, 'Daudpur', 'দাউদপুর', 0, 'daudpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3699, 404, 'Mahmudpur', 'মাহামুদপুর', 0, 'mahmudpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3700, 404, 'Kushdaha', 'কুশদহ', 0, 'kushdahaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3701, 405, 'Shibrampur', 'শিবরামপুর', 0, 'shibrampurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3702, 405, 'Polashbari', 'পলাশবাড়ী', 0, 'polashbariup2.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3703, 405, 'Shatagram', 'শতগ্রাম', 0, 'shatagramup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3704, 405, 'Paltapur', 'পাল্টাপুর', 0, 'paltapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3705, 405, 'Sujalpur', 'সুজালপুর', 0, 'sujalpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3706, 405, 'Nijpara', 'নিজপাড়া', 0, 'nijparaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3707, 405, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3708, 405, 'Bhognagar', 'ভোগনগর', 0, 'bhognagarup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3709, 405, 'Sator', 'সাতোর', 0, 'satorup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3710, 405, 'Mohonpur', 'মোহনপুর', 0, 'mohonpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3711, 405, 'Moricha', 'মরিচা', 0, 'morichaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3712, 406, 'Bulakipur', 'বুলাকীপুর', 0, 'bulakipurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3713, 406, 'Palsha', 'পালশা', 0, 'palshaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3714, 406, 'Singra', 'সিংড়া', 0, 'singraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3715, 406, 'Ghoraghat', 'ঘোড়াঘাট', 0, 'ghoraghatup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3716, 407, 'Mukundopur', 'মুকুন্দপুর', 0, 'mukundopurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3717, 407, 'Katla', 'কাটলা', 0, 'katlaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3718, 407, 'Khanpur', 'খানপুর', 0, 'khanpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3719, 407, 'Dior', 'দিওড়', 0, 'diorup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3720, 407, 'Binail', 'বিনাইল', 0, 'binailup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3721, 407, 'Jatbani', 'জোতবানী', 0, 'jatbaniup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3722, 407, 'Poliproyagpur', 'পলিপ্রয়াগপুর', 0, 'poliproyagpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3723, 408, 'Belaichandi', 'বেলাইচন্ডি', 0, 'belaichandiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3724, 408, 'Monmothopur', 'মন্মথপুর', 0, 'monmothopurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3725, 408, 'Rampur', 'রামপুর', 0, 'rampurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3726, 408, 'Polashbari', 'পলাশবাড়ী', 0, 'polashbariup4.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3727, 408, 'Chandipur', 'চন্ডীপুর', 0, 'chandipurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3728, 408, 'Mominpur', 'মোমিনপুর', 0, 'mominpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3729, 408, 'Mostofapur', 'মোস্তফাপুর', 0, 'mostofapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3730, 408, 'Habra', 'হাবড়া', 0, 'habraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3731, 408, 'Hamidpur', 'হামিদপুর', 0, 'hamidpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3732, 408, 'Harirampur', 'হরিরামপুর', 0, 'harirampurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3733, 409, 'Nafanagar', 'নাফানগর', 0, 'nafanagarup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3734, 409, 'Eshania', 'ঈশানিয়া', 0, 'eshaniaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3735, 409, 'Atgaon', 'আটগাঁও', 0, 'atgaonup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3736, 409, 'Shatail', 'ছাতইল', 0, 'shatailup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3737, 409, 'Rongaon', 'রনগাঁও', 0, 'rongaonup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3738, 409, 'Murshidhat', 'মুর্শিদহাট', 0, 'murshidhatup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3739, 410, 'Dabor', 'ডাবোর', 0, 'daborup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3740, 410, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3741, 410, 'Mukundapur', 'মুকুন্দপুর', 0, 'mukundapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3742, 410, 'Targao', 'তারগাঁও', 0, 'targaoup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3743, 410, 'Ramchandrapur', 'রামচন্দ্রপুর', 0, 'ramchandrapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3744, 410, 'Sundarpur', 'সুন্দরপুর', 0, 'sundarpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3745, 411, 'Aloary', 'এলুয়াড়ী', 0, 'aloaryup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3746, 411, 'Aladipur', 'আলাদিপুর', 0, 'aladipurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3747, 411, 'Kagihal', 'কাজীহাল', 0, 'kagihalup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3748, 411, 'Bethdighi', 'বেতদিঘী', 0, 'bethdighiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3749, 411, 'Khairbari', 'খয়েরবাড়ী', 0, 'khairbariup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3750, 411, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3751, 411, 'Shibnagor', 'শিবনগর', 0, 'shibnagorup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3752, 412, 'Chealgazi', 'চেহেলগাজী', 0, 'chealgaziup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3753, 412, 'Sundorbon', 'সুন্দরবন', 0, 'sundorbonup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3754, 412, 'Fazilpur', 'ফাজিলপুর', 0, 'fazilpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3755, 412, 'Shekpura', 'শেখপুরা', 0, 'shekpuraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3756, 412, 'Shashora', 'শশরা', 0, 'shashoraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3757, 412, 'Auliapur', 'আউলিয়াপুর', 0, 'auliapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3758, 412, 'Uthrail', 'উথরাইল', 0, 'uthrailup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3759, 412, 'Sankarpur', 'শংকরপুর', 0, 'sankarpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3760, 412, 'Askorpur', 'আস্করপুর', 0, 'askorpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3761, 412, 'Kamalpur', 'কমলপুর', 0, 'kamalpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3762, 413, 'Alihat', 'আলীহাট', 0, 'alihatup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3763, 413, 'Khattamadobpara', 'খট্টামাধবপাড়া', 0, 'khattamadobparaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3764, 413, 'Boalder', 'বোয়ালদার', 0, 'boalderup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3765, 414, 'Alokjhari', 'আলোকঝাড়ী', 0, 'alokjhariup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3766, 414, 'Bherbheri', 'ভেড়ভেড়ী', 0, 'bherbheriup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3767, 414, 'Angarpara', 'আঙ্গারপাড়া', 0, 'angarparaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3768, 414, 'Goaldihi', 'গোয়ালডিহি', 0, 'goaldihiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3769, 414, 'Bhabki', 'ভাবকী', 0, 'bhabkiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3770, 414, 'Khamarpara', 'খামারপাড়া', 0, 'khamarparaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3771, 415, 'Azimpur', 'আজিমপুর', 0, 'azimpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3772, 415, 'Farakkabad', 'ফরাক্কাবাদ', 0, 'farakkabadup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3773, 415, 'Dhamoir', 'ধামইর', 0, 'dhamoirup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3774, 415, 'Shohorgram', 'শহরগ্রাম', 0, 'shohorgramup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3775, 415, 'Birol', 'বিরল', 0, 'birolup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3776, 415, 'Bhandra', 'ভান্ডারা', 0, 'bhandraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3777, 415, 'Bijora', 'বিজোড়া', 0, 'bijoraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3778, 415, 'Dharmapur', 'ধর্মপুর', 0, 'dharmapurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3779, 415, 'Mongalpur', 'মঙ্গলপুর', 0, 'mongalpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3780, 415, 'Ranipukur', 'রাণীপুকুর', 0, 'ranipukurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3781, 415, 'Rajarampur', 'রাজারামপুর', 0, 'rajarampurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3782, 416, 'Nashratpur', 'নশরতপুর', 0, 'nashratpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3783, 416, 'Satnala', 'সাতনালা', 0, 'satnalaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3784, 416, 'Fatejangpur', 'ফতেজংপুর', 0, 'fatejangpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3785, 416, 'Isobpur', 'ইসবপুর', 0, 'isobpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3786, 416, 'Abdulpur', 'আব্দুলপুর', 0, 'abdulpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3787, 416, 'Amarpur', 'অমরপুর', 0, 'amarpurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3788, 416, 'Auliapukur', 'আউলিয়াপুকুর', 0, 'auliapukurup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3789, 416, 'Saitara', 'সাইতারা', 0, 'saitaraup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3790, 416, 'Viail', 'ভিয়াইল', 0, 'viailup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3791, 416, 'Punotti', 'পুনট্টি', 0, 'punottiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3792, 416, 'Tetulia', 'তেতুলিয়া', 0, 'tetuliaup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3793, 416, 'Alokdihi', 'আলোকডিহি', 0, 'alokdihiup.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3794, 417, 'Rajpur', 'রাজপুর', 0, 'rajpurup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3795, 417, 'Harati', 'হারাটি', 0, 'haratiup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3796, 417, 'Mogolhat', 'মোগলহাট', 0, 'mogolhatup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3797, 417, 'Gokunda', 'গোকুন্ডা', 0, 'gokundaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3798, 417, 'Barobari', 'বড়বাড়ী', 0, 'barobariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3799, 417, 'Kulaghat', 'কুলাঘাট', 0, 'kulaghatup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3800, 417, 'Mohendranagar', 'মহেন্দ্রনগর', 0, 'mohendranagarup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3801, 417, 'Khuniagachh', 'খুনিয়াগাছ', 0, 'khuniagachhup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3802, 417, 'Panchagram', 'পঞ্চগ্রাম', 0, 'panchagramup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3803, 418, 'Bhotmari', 'ভোটমারী', 0, 'bhotmariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3804, 418, 'Modati', 'মদাতী', 0, 'modatiup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3805, 418, 'Dologram', 'দলগ্রাম', 0, 'dologramup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3806, 418, 'Tushbhandar', 'তুষভান্ডার', 0, 'tushbhandarup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3807, 418, 'Goral', 'গোড়ল', 0, 'goralup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3808, 418, 'Chondropur', 'চন্দ্রপুর', 0, 'chondropurup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3809, 418, 'Cholbola', 'চলবলা', 0, 'cholbolaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3810, 418, 'Kakina', 'কাকিনা', 0, 'kakinaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3811, 419, 'Barokhata', 'বড়খাতা', 0, 'barokhataup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3812, 419, 'Goddimari', 'গড্ডিমারী', 0, 'goddimariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3813, 419, 'Singimari', 'সিংগীমারী', 0, 'singimariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3814, 419, 'Tongvhanga', 'টংভাঙ্গা', 0, 'tongvhangaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3815, 419, 'Sindurna', 'সিন্দুর্ণা', 0, 'sindurnaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3816, 419, 'Paticapara', 'পাটিকাপাড়া', 0, 'paticaparaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3817, 419, 'Nowdabas', 'নওদাবাস', 0, 'nowdabasup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3818, 419, 'Gotamari', 'গোতামারী', 0, 'gotamariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3819, 419, 'Vhelaguri', 'ভেলাগুড়ি', 0, 'vhelaguriup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3820, 419, 'Shaniajan', 'সানিয়াজান', 0, 'shaniajanup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3821, 419, 'Fakirpara', 'ফকিরপাড়া', 0, 'fakirparaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3822, 419, 'Dawabari', 'ডাউয়াবাড়ী', 0, 'dawabariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3823, 420, 'Sreerampur', 'শ্রীরামপুর', 0, 'sreerampurup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3824, 420, 'Patgram', 'পাটগ্রাম', 0, 'patgramup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3825, 420, 'Jagatber', 'জগতবেড়', 0, 'jagatberup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3826, 420, 'Kuchlibari', 'কুচলিবাড়ী', 0, 'kuchlibariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3827, 420, 'Jongra', 'জোংড়া', 0, 'jongraup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3828, 420, 'Baura', 'বাউড়া', 0, 'bauraup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3829, 420, 'Dahagram', 'দহগ্রাম', 0, 'dahagramup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3830, 420, 'Burimari', 'বুড়িমারী', 0, 'burimariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3831, 421, 'Bhelabari', 'ভেলাবাড়ী', 0, 'bhelabariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3832, 421, 'Bhadai', 'ভাদাই', 0, 'bhadaiup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3833, 421, 'Kamlabari', 'কমলাবাড়ী', 0, 'kamlabariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3834, 421, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3835, 421, 'Sarpukur', 'সারপুকুর', 0, 'sarpukurup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3836, 421, 'Saptibari', 'সাপ্টিবাড়ী', 0, 'saptibariup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3837, 421, 'Palashi', 'পলাশী', 0, 'palashiup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3838, 421, 'Mohishkhocha', 'মহিষখোচা', 0, 'mohishkhochaup.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3839, 422, 'Kamarpukur', 'কামারপুকুর', 0, 'kamarpukurup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3840, 422, 'Kasiram Belpukur', 'কাশিরাম বেলপুকুর', 0, 'kasirambelpukurup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3841, 422, 'Bangalipur', 'বাঙ্গালীপুর', 0, 'bangalipur.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3842, 422, 'Botlagari', 'বোতলাগাড়ী', 0, 'botlagariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3843, 422, 'Khata Madhupur', 'খাতা মধুপুর', 0, 'khatamadhupurup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3844, 423, 'Gomnati', 'গোমনাতি', 0, 'gomnati.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3845, 423, 'Bhogdaburi', 'ভোগডাবুড়ী', 0, 'bhogdaburiup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3846, 423, 'Ketkibari', 'কেতকীবাড়ী', 0, 'ketkibariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3847, 423, 'Jorabari', 'জোড়াবাড়ী', 0, 'jorabariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3848, 423, 'Bamunia', 'বামুনীয়া', 0, 'bamuniaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3849, 423, 'Panga Motukpur', 'পাংগা মটকপুর', 0, 'pangamotukpurup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3850, 423, 'Boragari', 'বোড়াগাড়ী', 0, 'boragariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3851, 423, 'Domar', 'ডোমার', 0, 'domarup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3852, 423, 'Sonaray', 'সোনারায়', 0, 'sonarayup2.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3853, 423, 'Harinchara', 'হরিণচরা', 0, 'harincharaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3854, 424, 'Paschim Chhatnay', 'পশ্চিম ছাতনাই', 0, 'paschimchhatnayup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3855, 424, 'Balapara', 'বালাপাড়া', 0, 'balaparaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3856, 424, 'Dimla Sadar', 'ডিমলা সদর', 0, 'dimlasadarup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3857, 424, 'Khogakharibari', 'খগা খড়িবাড়ী', 0, 'khogakharibariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3858, 424, 'Gayabari', 'গয়াবাড়ী', 0, 'gayabariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3859, 424, 'Noutara', 'নাউতারা', 0, 'noutaraup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3860, 424, 'Khalisha Chapani', 'খালিশা চাপানী', 0, 'khalishachapaniup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3861, 424, 'Jhunagach Chapani', 'ঝুনাগাছ চাপানী', 0, 'jhunagachhchapaniup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3862, 424, 'Tepa Khribari', 'টেপা খরীবাড়ী', 0, 'tepakhribariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3863, 424, 'Purba Chhatnay', 'পুর্ব ছাতনাই', 0, 'purbachhatnayup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3864, 425, 'Douabari', 'ডাউয়াবাড়ী', 0, 'douabariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3865, 425, 'Golmunda', 'গোলমুন্ডা', 0, 'golmunda.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3866, 425, 'Balagram', 'বালাগ্রাম', 0, 'balagram.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3867, 425, 'Golna', 'গোলনা', 0, 'golna.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3868, 425, 'Dharmapal', 'ধর্মপাল', 0, 'dharmapal.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3869, 425, 'Simulbari', 'শিমুলবাড়ী', 0, 'simulbari.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3870, 425, 'Mirganj', 'মীরগঞ্জ', 0, 'mirganj.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3871, 425, 'Kathali', 'কাঠালী', 0, 'kathaliup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3872, 425, 'Khutamara', 'খুটামারা', 0, 'khutamaraup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3873, 425, 'Shaulmari', 'শৌলমারী', 0, 'shaulmariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3874, 425, 'Kaimari', 'কৈমারী', 0, 'kaimariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3875, 426, 'Barabhita', 'বড়ভিটা', 0, 'barabhitaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3876, 426, 'Putimari', 'পুটিমারী', 0, 'putimariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3877, 426, 'Nitai', 'নিতাই', 0, 'nitaiup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3878, 426, 'Bahagili', 'বাহাগিলি', 0, 'bahagiliup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3879, 426, 'Chandkhana', 'চাঁদখানা', 0, 'chandkhanaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3880, 426, 'Kishoreganj', 'কিশোরগঞ্জ', 0, 'kishoreganjup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3881, 426, 'Ranachandi', 'রনচন্ডি', 0, 'ranachandiup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3882, 426, 'Garagram', 'গাড়াগ্রাম', 0, 'garagramup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3883, 426, 'Magura', 'মাগুরা', 0, 'maguraup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3884, 427, 'Chaora Bargacha', 'চওড়া বড়গাছা', 0, 'chaorabargachaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3885, 427, 'Gorgram', 'গোড়গ্রাম', 0, 'gorgramup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3886, 427, 'Khoksabari', 'খোকসাবাড়ী', 0, 'khoksabariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3887, 427, 'Palasbari', 'পলাশবাড়ী', 0, 'palasbariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3888, 427, 'Ramnagar', 'রামনগর', 0, 'ramnagarup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3889, 427, 'Kachukata', 'কচুকাটা', 0, 'kachukataup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3890, 427, 'Panchapukur', 'পঞ্চপুকুর', 0, 'panchapukurup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3891, 427, 'Itakhola', 'ইটাখোলা', 0, 'itakholaup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3892, 427, 'Kundapukur', 'কুন্দপুকুর', 0, 'kundapukur.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3893, 427, 'Sonaray', 'সোনারায়', 0, 'sonaray.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3894, 427, 'Songalsi', 'সংগলশী', 0, 'songalsiup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3895, 427, 'Charaikhola', 'চড়াইখোলা', 0, 'charaikhola.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3896, 427, 'Chapra Sarnjami', 'চাপড়া সরঞ্জানী', 0, 'chaprasarnjami.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3897, 427, 'Lakshmicha', 'লক্ষ্মীচাপ', 0, 'lakshmichapup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3898, 427, 'Tupamari', 'টুপামারী', 0, 'tupamariup.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3899, 428, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3900, 428, 'Noldanga', 'নলডাঙ্গা', 0, 'noldangaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3901, 428, 'Damodorpur', 'দামোদরপুর', 0, 'damodorpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3902, 428, 'Jamalpur', 'জামালপুর', 0, 'jamalpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3903, 428, 'Faridpur', 'ফরিদপুর', 0, 'faridpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3904, 428, 'Dhaperhat', 'ধাপেরহাট', 0, 'dhaperhatup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3905, 428, 'Idilpur', 'ইদিলপুর', 0, 'idilpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3906, 428, 'Vatgram', 'ভাতগ্রাম', 0, 'vatgramup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3907, 428, 'Bongram', 'বনগ্রাম', 0, 'bongramup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3908, 428, 'Kamarpara', 'কামারপাড়া', 0, 'kamarparaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3909, 428, 'Khodkomor', 'খোদকোমরপুর', 0, 'khodkomorup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3910, 429, 'Laxmipur', 'লক্ষ্মীপুর', 0, 'laxmipurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3911, 429, 'Malibari', 'মালীবাড়ী', 0, 'malibariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3912, 429, 'Kuptola', 'কুপতলা', 0, 'kuptolaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3913, 429, 'Shahapara', 'সাহাপাড়া', 0, 'shahaparaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3914, 429, 'Ballamjhar', 'বল্লমঝাড়', 0, 'ballamjharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3915, 429, 'Ramchandrapur', 'রামচন্দ্রপুর', 0, 'ramchandrapurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3916, 429, 'Badiakhali', 'বাদিয়াখালী', 0, 'badiakhaliup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3917, 429, 'Boali', 'বোয়ালী', 0, 'boaliup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3918, 429, 'Ghagoa', 'ঘাগোয়া', 0, 'ghagoaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3919, 429, 'Gidari', 'গিদারী', 0, 'gidariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3920, 429, 'Kholahati', 'খোলাহাটী', 0, 'kholahatiup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3921, 429, 'Mollarchar', 'মোল্লারচর', 0, 'mollarcharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3922, 429, 'Kamarjani', 'কামারজানি', 0, 'kamarjaniup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3923, 430, 'Kishoregari', 'কিশোরগাড়ী', 0, 'kishoregariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3924, 430, 'Hosenpur', 'হোসেনপুর', 0, 'hosenpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3925, 430, 'Palashbari', 'পলাশবাড়ী', 0, 'palashbariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3926, 430, 'Barisal', 'বরিশাল', 0, 'barisalup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3927, 430, 'Mohdipur', 'মহদীপুর', 0, 'mohdipurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3928, 430, 'Betkapa', 'বেতকাপা', 0, 'betkapaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3929, 430, 'Pobnapur', 'পবনাপুর', 0, 'pobnapurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3930, 430, 'Monohorpur', 'মনোহরপুর', 0, 'monohorpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3931, 430, 'Harinathpur', 'হরিণাথপুর', 0, 'harinathpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3932, 431, 'Padumsahar', 'পদুমশহর', 0, 'padumsaharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3933, 431, 'Varotkhali', 'ভরতখালী', 0, 'varotkhaliup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3934, 431, 'Saghata', 'সাঘাটা', 0, 'saghataup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3935, 431, 'Muktinagar', 'মুক্তিনগর', 0, 'muktinagarup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3936, 431, 'Kachua', 'কচুয়া', 0, 'kachuaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3937, 431, 'Ghuridah', 'ঘুরিদহ', 0, 'ghuridahup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3938, 431, 'Holdia', 'হলদিয়া', 0, 'holdiaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3939, 431, 'Jumarbari', 'জুমারবাড়ী', 0, 'jumarbariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3940, 431, 'Kamalerpara', 'কামালেরপাড়া', 0, 'kamalerparaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3941, 431, 'Bonarpara', 'বোনারপাড়া', 0, 'bonarparaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3942, 432, 'Kamdia', 'কামদিয়া', 0, 'kamdiaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3943, 432, 'Katabari', 'কাটাবাড়ী', 0, 'katabariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3944, 432, 'Shakhahar', 'শাখাহার', 0, 'shakhaharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3945, 432, 'Rajahar', 'রাজাহার', 0, 'rajaharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3946, 432, 'Sapmara', 'সাপমারা', 0, 'sapmaraup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3947, 432, 'Dorbosto', 'দরবস্ত ইয়নিয়ন', 0, 'dorbostoup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3948, 432, 'Talukkanupur', 'তালুককানুপুর', 0, 'talukkanupurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3949, 432, 'Nakai', 'নাকাই', 0, 'nakaiup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3950, 432, 'Harirampur', 'হরিরামপুর', 0, 'harirampurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3951, 432, 'Rakhalburuj', 'রাখালবুরুজ', 0, 'rakhalburujup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3952, 432, 'Phulbari', 'ফুলবাড়ী', 0, 'phulbariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3953, 432, 'Gumaniganj', 'গুমানীগঞ্জ', 0, 'gumaniganjup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3954, 432, 'Kamardoho', 'কামারদহ', 0, 'kamardohoup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3955, 432, 'Kochasahar', 'কোচাশহর', 0, 'kochasaharup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3956, 432, 'Shibpur', 'শিবপুর', 0, 'shibpurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3957, 432, 'Mahimaganj', 'মহিমাগঞ্জ', 0, 'mahimaganjup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3958, 432, 'Shalmara', 'শালমারা', 0, 'shalmaraup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3959, 433, 'Bamondanga', 'বামনডাঙ্গা', 0, 'bamondangaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3960, 433, 'Sonaroy', 'সোনারায়', 0, 'sonaroyup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3961, 433, 'Tarapur', 'তারাপুর', 0, 'tarapurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3962, 433, 'Belka', 'বেলকা', 0, 'belkaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3963, 433, 'Dohbond', 'দহবন্দ', 0, 'dohbondup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3964, 433, 'Sorbanondo', 'সর্বানন্দ', 0, 'sorbanondoup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3965, 433, 'Ramjibon', 'রামজীবন', 0, 'ramjibonup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3966, 433, 'Dhopadanga', 'ধোপাডাঙ্গা', 0, 'dhopadangaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3967, 433, 'Chaporhati', 'ছাপরহাটী', 0, 'chaporhatiup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3968, 433, 'Shantiram', 'শান্তিরাম', 0, 'shantiramup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3969, 433, 'Konchibari', 'কঞ্চিবাড়ী', 0, 'konchibariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3970, 433, 'Sreepur', 'শ্রীপুর', 0, 'sreepurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3971, 433, 'Chandipur', 'চন্ডিপুর', 0, 'chandipurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3972, 433, 'Kapasia', 'কাপাসিয়া', 0, 'kapasiaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3973, 433, 'Haripur', 'হরিপুর', 0, 'haripurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3974, 434, 'Kanchipara', 'কঞ্চিপাড়া', 0, 'kanchiparaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3975, 434, 'Uria', 'উড়িয়া', 0, 'uriaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3976, 434, 'Udakhali', 'উদাখালী', 0, 'udakhaliup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3977, 434, 'Gazaria', 'গজারিয়া', 0, 'gazariaup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3978, 434, 'Phulchari', 'ফুলছড়ি', 0, 'phulchariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3979, 434, 'Erendabari', 'এরেন্ডাবাড়ী', 0, 'erendabariup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3980, 434, 'Fazlupur', 'ফজলুপুর', 0, 'fazlupurup.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3981, 435, 'Ruhea', 'রুহিয়া', 0, 'ruheaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3982, 435, 'Akhanagar', 'আখানগর', 0, 'akhanagarup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3983, 435, 'Ahcha', 'আকচা', 0, 'ahchaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3984, 435, 'Baragaon', 'বড়গাঁও', 0, 'baragaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3985, 435, 'Balia', 'বালিয়া', 0, 'baliaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3986, 435, 'Auliapur', 'আউলিয়াপুর', 0, 'auliapurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3987, 435, 'Chilarang', 'চিলারং', 0, 'chilarangup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3988, 435, 'Rahimanpur', 'রহিমানপুর', 0, 'rahimanpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3989, 435, 'Roypur', 'রায়পুর', 0, 'roypurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3990, 435, 'Jamalpur', 'জামালপুর', 0, 'jamalpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3991, 435, 'Mohammadpur', 'মোহাম্মদপুর', 0, 'mohammadpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3992, 435, 'Salandar', 'সালন্দর', 0, 'salandarup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3993, 435, 'Gareya', 'গড়েয়া', 0, 'gareyaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3994, 435, 'Rajagaon', 'রাজাগাঁও', 0, 'rajagaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3995, 435, 'Debipur', 'দেবীপুর', 0, 'debipurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3996, 435, 'Nargun', 'নারগুন', 0, 'nargunup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3997, 435, 'Jagannathpur', 'জগন্নাথপুর', 0, 'jagannathpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3998, 435, 'Sukhanpukhari', 'শুখানপুকুরী', 0, 'sukhanpukhariup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (3999, 435, 'Begunbari', 'বেগুনবাড়ী', 0, 'begunbariup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4000, 435, 'Ruhia Pashchim', 'রুহিয়া পশ্চিম', 0, 'ruhiapashchimup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4001, 435, 'Dholarhat', 'ঢোলারহাট', 0, 'dholarhatup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4002, 436, 'Bhomradaha', 'ভোমরাদহ', 0, 'bhomradahaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4003, 436, 'Kosharaniganj', 'কোষারাণীগঞ্জ', 0, 'kosharaniganjup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4004, 436, 'Khangaon', 'খনগাঁও', 0, 'khangaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4005, 436, 'Saidpur', 'সৈয়দপুর', 0, 'saidpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4006, 436, 'Pirganj', 'পীরগঞ্জ', 0, 'pirganjup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4007, 436, 'Hajipur', 'হাজীপুর', 0, 'hajipurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4008, 436, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4009, 436, 'Sengaon', 'সেনগাঁও', 0, 'sengaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4010, 436, 'Jabarhat', 'জাবরহাট', 0, 'jabarhatup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4011, 436, 'Bairchuna', 'বৈরচুনা', 0, 'bairchunaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4012, 437, 'Dhormogarh', 'ধর্মগড়', 0, 'dhormogarhup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4013, 437, 'Nekmorod', 'নেকমরদ', 0, 'nekmorodup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4014, 437, 'Hosengaon', 'হোসেনগাঁও', 0, 'hosengaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4015, 437, 'Lehemba', 'লেহেম্বা', 0, 'lehembaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4016, 437, 'Bachor', 'বাচোর', 0, 'bachorup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4017, 437, 'Kashipur', 'কাশিপুর', 0, 'kashipurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4018, 437, 'Ratore', 'রাতোর', 0, 'ratoreup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4019, 437, 'Nonduar', 'নন্দুয়ার', 0, 'nonduarup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4020, 438, 'Gedura', 'গেদুড়া', 0, 'geduraup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4021, 438, 'Amgaon', 'আমগাঁও', 0, 'amgaonup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4022, 438, 'Bakua', 'বকুয়া', 0, 'bakuaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4023, 438, 'Dangipara', 'ডাঙ্গীপাড়া', 0, 'dangiparaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4024, 438, 'Haripur', 'হরিপুর', 0, 'haripurup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4025, 438, 'Bhaturia', 'ভাতুরিয়া', 0, 'bhaturiaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4026, 439, 'Paria', 'পাড়িয়া', 0, 'pariaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4027, 439, 'Charol', 'চারোল', 0, 'charolup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4028, 439, 'Dhontola', 'ধনতলা', 0, 'dhontolaup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4029, 439, 'Boropalashbari', 'বড়পলাশবাড়ী', 0, 'boropalashbariup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4030, 439, 'Duosuo', 'দুওসুও', 0, 'duosuoup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4031, 439, 'Vanor', 'ভানোর', 0, 'vanorup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4032, 439, 'Amjankhore', 'আমজানখোর', 0, 'amjankhoreup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4033, 439, 'Borobari', 'বড়বাড়ী', 0, 'borobariup.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4034, 440, 'Mominpur', 'মমিনপুর', 0, 'mominpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4035, 440, 'Horidebpur', 'হরিদেবপুর', 0, 'horidebpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4036, 440, 'Uttam', 'উত্তম', 0, 'uttamup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4037, 440, 'Porshuram', 'পরশুরাম', 0, 'porshuramup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4038, 440, 'Topodhan', 'তপোধন', 0, 'topodhanup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4039, 440, 'Satgara', 'সাতগারা', 0, 'satgaraup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4040, 440, 'Rajendrapur', 'রাজেন্দ্রপুর', 0, 'rajendrapurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4041, 440, 'Sadwapuskoroni', 'সদ্যপুস্করনী', 0, 'sadwapuskoroniup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4042, 440, 'Chandanpat', 'চন্দনপাট', 0, 'chandanpatup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4043, 440, 'Dorshona', 'দর্শানা', 0, 'dorshonaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4044, 440, 'Tampat', 'তামপাট', 0, 'tampatup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4045, 441, 'Betgari', 'বেতগাড়ী', 0, 'betgariup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4046, 441, 'Kholeya', 'খলেয়া', 0, 'kholeyaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4047, 441, 'Borobil', 'বড়বিল', 0, 'borobilup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4048, 441, 'Kolcondo', 'কোলকোন্দ', 0, 'kolcondoup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4049, 441, 'Gongachora', 'গংগাচড়া', 0, 'gongachoraup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4050, 441, 'Gojoghonta', 'গজঘন্টা', 0, 'gojoghontaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4051, 441, 'Morneya', 'মর্ণেয়া', 0, 'morneyaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4052, 441, 'Alambiditor', 'আলমবিদিতর', 0, 'alambiditorup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4053, 441, 'Lakkhitari', 'লক্ষীটারী', 0, 'lakkhitariup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4054, 441, 'Nohali', 'নোহালী', 0, 'nohaliup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4055, 442, 'Kurshatara', 'কুর্শা', 0, 'kurshataraup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4056, 442, 'Alampur', 'আলমপুর', 0, 'alampurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4057, 442, 'Soyar', 'সয়ার', 0, 'soyarup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4058, 442, 'Ikorchali', 'ইকরচালী', 0, 'ikorchaliup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4059, 442, 'Hariarkuthi', 'হাড়িয়ারকুঠি', 0, 'hariarkuthiup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4060, 443, 'Radhanagar', 'রাধানগর', 0, 'radhanagarup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4061, 443, 'Gopinathpur', 'গোপীনাথপুর', 0, 'gopinathpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4062, 443, 'Modhupur', 'মধুপুর', 0, 'modhupurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4063, 443, 'Kutubpur', 'কুতুবপুর', 0, 'kutubpurup.ranpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4064, 443, 'Bishnapur', 'বিষ্ণপুর', 0, 'bishnapurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4065, 443, 'Kalupara', 'কালুপাড়া', 0, 'kaluparaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4066, 443, 'Lohanipara', 'লোহানীপাড়া', 0, 'lohaniparaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4067, 443, 'Gopalpur', 'গোপালপুর', 0, 'gopalpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4068, 443, 'Damodorpur', 'দামোদরপুর', 0, 'damodorpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4069, 443, 'Ramnathpurupb', 'রামনাথপুর', 0, 'ramnathpurupb.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4070, 444, 'Khoragach', 'খোরাগাছ', 0, 'khoragachup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4071, 444, 'Ranipukur', 'রাণীপুকুর', 0, 'ranipukurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4072, 444, 'Payrabond', 'পায়রাবন্দ', 0, 'payrabondup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4073, 444, 'Vangni', 'ভাংনী', 0, 'vangniup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4074, 444, 'Balarhat', 'বালারহাট', 0, 'balarhatup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4075, 444, 'Kafrikhal', 'কাফ্রিখাল', 0, 'kafrikhalup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4076, 444, 'Latibpur', 'লতিবপুর', 0, 'latibpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4077, 444, 'Chengmari', 'চেংমারী', 0, 'chengmariup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4078, 444, 'Moyenpur', 'ময়েনপুর', 0, 'moyenpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4079, 444, 'Baluya Masimpur', 'বালুয়া মাসিমপুর', 0, 'baluyamasimpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4080, 444, 'Borobala', 'বড়বালা', 0, 'borobalaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4081, 444, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4082, 444, 'Imadpur', 'ইমাদপুর', 0, 'imadpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4083, 444, 'Milonpur', 'মিলনপুর', 0, 'milonpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4084, 444, 'Mgopalpur', 'গোপালপুর', 0, 'mgopalpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4085, 444, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4086, 444, 'Boro Hazratpur', 'বড় হযরতপুর', 0, 'borohazratpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4087, 445, 'Chattracol', 'চৈত্রকোল', 0, 'chattracolup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4088, 445, 'Vendabari', 'ভেন্ডাবাড়ী', 0, 'vendabariup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4089, 445, 'Borodargah', 'বড়দরগাহ', 0, 'borodargahup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4090, 445, 'Kumedpur', 'কুমেদপুর', 0, 'kumedpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4091, 445, 'Modankhali', 'মদনখালী', 0, 'modankhaliup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4092, 445, 'Tukuria', 'টুকুরিয়া', 0, 'tukuriaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4093, 445, 'Boro Alampur', 'বড় আলমপুর', 0, 'boroalampurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4094, 445, 'Raypur', 'রায়পুর', 0, 'raypurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4095, 445, 'Pirgonj', 'পীরগঞ্জ', 0, 'pirgonjup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4096, 445, 'Shanerhat', 'শানেরহাট', 0, 'shanerhatup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4097, 445, 'Mithipur', 'মিঠিপুর', 0, 'mithipurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4098, 445, 'Ramnathpur', 'রামনাথপুর', 0, 'ramnathpurup1.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4099, 445, 'Chattra', 'চতরা', 0, 'chattraup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4100, 445, 'Kabilpur', 'কাবিলপুর', 0, 'kabilpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4101, 445, 'Pachgachi', 'পাঁচগাছী', 0, 'pachgachiup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4102, 446, 'Sarai', 'সারাই', 0, 'saraiup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4103, 446, 'Balapara', 'বালাপাড়া', 0, 'balaparaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4104, 446, 'Shahidbag', 'শহীদবাগ', 0, 'shahidbagup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4105, 446, 'Haragach', 'হারাগাছ', 0, 'haragachup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4106, 446, 'Tepamodhupur', 'টেপামধুপুর', 0, 'tepamodhupurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4107, 446, 'Kurshaupk', 'কুর্শা', 0, 'kurshaupk.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4108, 447, 'Kollyani', 'কল্যাণী', 0, 'kollyaniup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4109, 447, 'Parul', 'পারুল', 0, 'parulup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4110, 447, 'Itakumari', 'ইটাকুমারী', 0, 'itakumariup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4111, 447, 'Saula', 'ছাওলা', 0, 'saulaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4112, 447, 'Kandi', 'কান্দি', 0, 'kandiup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4113, 447, 'Pirgacha', 'পীরগাছা', 0, 'pirgachaup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4114, 447, 'Annodanagar', 'অন্নদানগর', 0, 'annodanagarup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4115, 447, 'Tambulpur', 'তাম্বুলপুর', 0, 'tambulpurup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4116, 447, 'Koikuri', 'কৈকুড়ী', 0, 'koikuriup.rangpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4117, 448, 'Holokhana', 'হলোখানা', 0, 'holokhanaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4118, 448, 'Ghogadhoh', 'ঘোগাদহ', 0, 'ghogadhohup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4119, 448, 'Belgacha', 'বেলগাছা', 0, 'belgachaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4120, 448, 'Mogolbasa', 'মোগলবাসা', 0, 'mogolbasaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4121, 448, 'Panchgachi', 'পাঁচগাছি', 0, 'panchgachiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4122, 448, 'Jatrapur', 'যাত্রাপুর', 0, 'jatrapurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4123, 448, 'Kanthalbari', 'কাঁঠালবাড়ী', 0, 'kanthalbariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4124, 448, 'Bhogdanga', 'ভোগডাঙ্গা', 0, 'bhogdangaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4125, 449, 'Ramkhana', 'রামখানা', 0, 'ramkhanaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4126, 449, 'Raigonj', 'রায়গঞ্জ', 0, 'raigonjup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4127, 449, 'Bamondanga', 'বামনডাঙ্গা', 0, 'bamondangaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4128, 449, 'Berubari', 'বেরুবাড়ী', 0, 'berubariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4129, 449, 'Sontaspur', 'সন্তোষপুর', 0, 'sontaspurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4130, 449, 'Hasnabad', 'হাসনাবাদ', 0, 'hasnabadup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4131, 449, 'Newyashi', 'নেওয়াশী', 0, 'newyashiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4132, 449, 'Bhitorbond', 'ভিতরবন্দ', 0, 'bhitorbondup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4133, 449, 'Kaligonj', 'কালীগঞ্জ', 0, 'kaligonjup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4134, 449, 'Noonkhawa', 'নুনখাওয়া', 0, 'noonkhawaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4135, 449, 'Narayanpur', 'নারায়নপুর', 0, 'narayanpurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4136, 449, 'Kedar', 'কেদার', 0, 'kedarup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4137, 449, 'Kachakata', 'কঁচাকাঁটা', 0, 'kachakataup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4138, 449, 'Bollobherkhas', 'বল্লভেরখাস', 0, 'bollobherkhasup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4139, 450, 'Pathordubi', 'পাথরডুবি', 0, 'pathordubiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4140, 450, 'Shilkhuri', 'শিলখুড়ি', 0, 'shilkhuriup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4141, 450, 'Tilai', 'তিলাই', 0, 'tilaiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4142, 450, 'Paikarchara', 'পাইকেরছড়া', 0, 'paikarcharaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4143, 450, 'Bhurungamari', 'ভূরুঙ্গামারী', 0, 'bhurungamariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4144, 450, 'Joymonirhat', 'জয়মনিরহাট', 0, 'joymonirhatup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4145, 450, 'Andharirjhar', 'আন্ধারীরঝাড়', 0, 'andharirjharup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4146, 450, 'Char-Bhurungamari', 'চর-ভূরুঙ্গামারী', 0, 'charbhurungamariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4147, 450, 'Bangasonahat', 'বঙ্গসোনাহাট', 0, 'bangasonahatup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4148, 450, 'Boldia', 'বলদিয়া', 0, 'boldiaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4149, 451, 'Nawdanga', 'নাওডাঙ্গা', 0, 'nawdangaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4150, 451, 'Shimulbari', 'শিমুলবাড়ী', 0, 'shimulbariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4151, 451, 'Phulbari', 'ফুলবাড়ী', 0, 'phulbariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4152, 451, 'Baravita', 'বড়ভিটা', 0, 'baravitaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4153, 451, 'Bhangamor', 'ভাঙ্গামোড়', 0, 'bhangamorup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4154, 451, 'Kashipur', 'কাশিপুর', 0, 'kashipurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4155, 452, 'Chinai', 'ছিনাই', 0, 'chinaiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4156, 452, 'Rajarhat', 'রাজারহাট', 0, 'rajarhatup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4157, 452, 'Nazimkhan', 'নাজিমখাঁন', 0, 'nazimkhanup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4158, 452, 'Gharialdanga', 'ঘড়িয়ালডাঙ্গা', 0, 'gharialdangaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4159, 452, 'Chakirpashar', 'চাকিরপশার', 0, 'chakirpasharup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4160, 452, 'Biddanondo', 'বিদ্যানন্দ', 0, 'biddanondoup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4161, 452, 'Umarmajid', 'উমর মজিদ', 0, 'umarmajidup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4162, 453, 'Daldalia', 'দলদলিয়া', 0, 'daldaliaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4163, 453, 'Durgapur', 'দুর্গাপুর', 0, 'durgapurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4164, 453, 'Pandul', 'পান্ডুল', 0, 'pandulup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4165, 453, 'Buraburi', 'বুড়াবুড়ী', 0, 'buraburiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4166, 453, 'Dharanibari', 'ধরণীবাড়ী', 0, 'dharanibariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4167, 453, 'Dhamsreni', 'ধামশ্রেণী', 0, 'dhamsreniup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4168, 453, 'Gunaigas', 'গুনাইগাছ', 0, 'gunaigasup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4169, 453, 'Bazra', 'বজরা', 0, 'bazraup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4170, 453, 'Tobockpur', 'তবকপুর', 0, 'tobockpurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4171, 453, 'Hatia', 'হাতিয়া', 0, 'hatiaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4172, 453, 'Begumgonj', 'বেগমগঞ্জ', 0, 'begumgonjup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4173, 453, 'Shahabiar Alga', 'সাহেবের আলগা', 0, 'shahabiaralgaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4174, 453, 'Thetrai', 'থেতরাই', 0, 'thetraiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4175, 454, 'Ranigonj', 'রাণীগঞ্জ', 0, 'ranigonjup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4176, 454, 'Nayarhat', 'নয়ারহাট', 0, 'nayarhatup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4177, 454, 'Thanahat', 'থানাহাট', 0, 'thanahatup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4178, 454, 'Ramna', 'রমনা', 0, 'ramnaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4179, 454, 'Chilmari', 'চিলমারী', 0, 'chilmariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4180, 454, 'Austomirchar', 'অষ্টমীর চর', 0, 'austomircharup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4181, 455, 'Dadevanga', 'দাঁতভাঙ্গা', 0, 'dadevangaup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4182, 455, 'Shoulemari', 'শৌলমারী', 0, 'shoulemariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4183, 455, 'Bondober', 'বন্দবেড়', 0, 'bondoberup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4184, 455, 'Rowmari', 'রৌমারী', 0, 'rowmariup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4185, 455, 'Jadurchar', 'যাদুরচর', 0, 'jadurcharup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4186, 456, 'Rajibpur', 'রাজিবপুর', 0, 'rajibpurup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4187, 456, 'Kodalkati', 'কোদালকাটি', 0, 'kodalkatiup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4188, 456, 'Mohongonj', 'মোহনগঞ্জ', 0, 'mohongonjup.kurigram.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4189, 457, 'Kamararchor', 'কামারের চর', 0, 'kamararchorup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4190, 457, 'Chorsherpur', 'চরশেরপুর', 0, 'chorsherpurup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4191, 457, 'Bajitkhila', 'বাজিতখিলা', 0, 'bajitkhilaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4192, 457, 'Gajir Khamar', 'গাজির খামার', 0, 'gajirkhamarup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4193, 457, 'Dhola', 'ধলা', 0, 'dholaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4194, 457, 'Pakuriya', 'পাকুরিয়া', 0, 'pakuriyaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4195, 457, 'Vatshala', 'ভাতশালা', 0, 'vatshalaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4196, 457, 'Losmonpur', 'লছমনপুর', 0, 'losmonpurup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4197, 457, 'Rouha', 'রৌহা', 0, 'rouhaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4198, 457, 'Kamariya', 'কামারিয়া', 0, 'kamariyaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4199, 457, 'Chor Mochoriya', 'চর মোচারিয়া', 0, 'chormochoriyaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4200, 457, 'Chorpokhimari', 'চর পক্ষীমারি', 0, 'chorpokhimariup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4201, 457, 'Betmari Ghughurakandi', 'বেতমারি ঘুঘুরাকান্দি', 0, 'betmarighughurakandiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4202, 457, 'Balairchar', 'বলাইরচর', 0, 'balaircharup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4203, 458, 'Puraga', 'পোড়াগাও', 0, 'puragauup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4204, 458, 'Nonni', 'নন্নী', 0, 'nonniup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4205, 458, 'Morichpuran', 'মরিচপুরাণ', 0, 'morichpuranup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4206, 458, 'Rajnogor', 'রাজনগর', 0, 'rajnogorup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4207, 458, 'Nayabil', 'নয়াবীল', 0, 'nayabilup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4208, 458, 'Ramchondrokura', 'রামচন্দ্রকুড়া', 0, 'ramchondrokuraup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4209, 458, 'Kakorkandhi', 'কাকরকান্দি', 0, 'kakorkandhiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4210, 458, 'Nalitabari', 'নালিতাবাড়ী', 0, 'nalitabariup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4211, 458, 'Juganiya', 'যোগনীয়া', 0, 'juganiyaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4212, 458, 'Bagber', 'বাঘবেড়', 0, 'bagberup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4213, 458, 'Koloshpar', 'কলসপাড়', 0, 'koloshparup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4214, 458, 'Rupnarayankura', 'রূপনারায়নকুড়া', 0, 'rupnarayankuraup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4215, 459, 'Ranishimul', 'রানীশিমুল', 0, 'ranishimulup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4216, 459, 'Singabaruna', 'সিংগাবরুনা', 0, 'singabarunaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4217, 459, 'Kakilakura', 'কাকিলাকুড়া', 0, 'kakilakuraup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4218, 459, 'Tatihati', 'তাতীহাটি', 0, 'tatihatiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4219, 459, 'Gosaipur', 'গোশাইপুর', 0, 'gosaipurup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4220, 459, 'Sreebordi', 'শ্রীবরদী', 0, 'sreebordiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4221, 459, 'Bhelua', 'ভেলুয়া', 0, 'bheluaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4222, 459, 'Kharia Kazirchar', 'খড়িয়া কাজিরচর', 0, 'khariakazircharup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4223, 459, 'Kurikahonia', 'কুড়িকাহনিয়া', 0, 'kurikahoniaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4224, 459, 'Garjaripa', 'গড়জরিপা', 0, 'garjaripaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4225, 460, 'Gonopoddi', 'গণপদ্দী', 0, 'gonopoddiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4226, 460, 'Nokla', 'নকলা', 0, 'noklaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4227, 460, 'Urpha', 'উরফা', 0, 'urphaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4228, 460, 'Gourdwar', 'গৌড়দ্বার', 0, 'gourdwarup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4229, 460, 'Baneshwardi', 'বানেশ্বর্দী', 0, 'baneshwardiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4230, 460, 'Pathakata', 'পাঠাকাটা', 0, 'pathakataup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4231, 460, 'Talki', 'টালকী', 0, 'talkiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4232, 460, 'Choraustadhar', 'চরঅষ্টধর', 0, 'choraustadharup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4233, 460, 'Chandrakona', 'চন্দ্রকোনা', 0, 'chandrakonaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4234, 461, 'Kansa', 'কাংশা', 0, 'kansaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4235, 461, 'Dansail', 'ধানশাইল', 0, 'dansailup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4236, 461, 'Nolkura', 'নলকুড়া', 0, 'nolkuraup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4237, 461, 'Gouripur', 'গৌরিপুর', 0, 'gouripurup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4238, 461, 'Jhenaigati', 'ঝিনাইগাতী', 0, 'jhenaigatiup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4239, 461, 'Hatibandha', 'হাতিবান্দা', 0, 'hatibandhaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4240, 461, 'Malijhikanda', 'মালিঝিকান্দা', 0, 'malijhikandaup.sherpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4241, 462, 'Deukhola', 'দেওখোলা', 0, 'deukholaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4242, 462, 'Naogaon', 'নাওগাঁও', 0, 'naogaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4243, 462, 'Putijana', 'পুটিজানা', 0, 'putijanaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4244, 462, 'Kushmail', 'কুশমাইল', 0, 'kushmailup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4245, 462, 'Fulbaria', 'ফুলবাড়ীয়া', 0, 'fulbariaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4246, 462, 'Bakta', 'বাক্তা', 0, 'baktaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4247, 462, 'Rangamatia', 'রাঙ্গামাটিয়া', 0, 'rangamatiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4248, 462, 'Enayetpur', 'এনায়েতপুর', 0, 'enayetpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4249, 462, 'Kaladaha', 'কালাদহ', 0, 'kaladahaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4250, 462, 'Radhakanai', 'রাধাকানাই', 0, 'radhakanaiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4251, 462, 'Asimpatuli', 'আছিমপাটুলী', 0, 'asimpatuliup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4252, 462, 'Vobanipur', 'ভবানীপুর', 0, 'vobanipurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4253, 462, 'Balian', 'বালিয়ান', 0, 'balianup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4254, 463, 'Dhanikhola', 'ধানীখোলা', 0, 'dhanikholaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4255, 463, 'Bailor', 'বৈলর', 0, 'bailorup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4256, 463, 'Kanthal', 'কাঁঠাল', 0, 'kanthalup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4257, 463, 'Kanihari', 'কানিহারী', 0, 'kanihariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4258, 463, 'Trishal', 'ত্রিশাল', 0, 'trishalup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4259, 463, 'Harirampur', 'হরিরামপুর', 0, 'harirampurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4260, 463, 'Sakhua', 'সাখুয়া', 0, 'www.sakhuaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4261, 463, 'Balipara', 'বালিপাড়া', 0, 'baliparaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4262, 463, 'Mokshapur', 'মোক্ষপুর', 0, 'mokshapurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4263, 463, 'Mathbari', 'মঠবাড়ী', 0, 'mathbariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4264, 463, 'Amirabari', 'আমিরাবাড়ী', 0, 'amirabariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4265, 463, 'Rampur', 'রামপুর', 0, 'rampurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4266, 464, 'Uthura', 'উথুরা', 0, 'uthuraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4267, 464, 'Meduari', 'মেদুয়ারী', 0, 'meduariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4268, 464, 'Varadoba', 'ভরাডোবা', 0, 'varadobaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4269, 464, 'Dhitpur', 'ধীতপুর', 0, 'dhitpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4270, 464, 'Dakatia', 'ডাকাতিয়া', 0, 'dakatiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4271, 464, 'Birunia', 'বিরুনিয়া', 0, 'biruniaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4272, 464, 'Bhaluka', 'ভালুকা', 0, 'bhalukaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4273, 464, 'Mallikbari', 'মল্লিকবাড়ী', 0, 'mallikbariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4274, 464, 'Kachina', 'কাচিনা', 0, 'kachinaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4275, 464, 'Habirbari', 'হবিরবাড়ী', 0, 'habirbariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4276, 464, 'Rajoi', 'রাজৈ', 0, 'rajoiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4277, 465, 'Dulla', 'দুল্লা', 0, 'dullaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4278, 465, 'Borogram', 'বড়গ্রাম', 0, 'borogramup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4279, 465, 'Tarati', 'তারাটি', 0, 'taratiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4280, 465, 'Kumargata', 'কুমারগাতা', 0, 'kumargataup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4281, 465, 'Basati', 'বাশাটি', 0, 'basatiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4282, 465, 'Mankon', 'মানকোন', 0, 'mankonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4283, 465, 'Ghoga', 'ঘোগা', 0, 'ghogaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4284, 465, 'Daogaon', 'দাওগাঁও', 0, 'daogaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4285, 465, 'Kashimpur', 'কাশিমপুর', 0, 'kashimpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4286, 465, 'Kheruajani', 'খেরুয়াজানী', 0, 'kheruajaniup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4287, 466, 'Austadhar', 'অষ্টধার', 0, 'austadharup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4288, 466, 'Bororchar', 'বোররচর', 0, 'bororcharup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4289, 466, 'Dapunia', 'দাপুনিয়া', 0, 'dapuniaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4290, 466, 'Aqua', 'আকুয়া', 0, 'aquaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4291, 466, 'Khagdohor', 'খাগডহর', 0, 'khagdohorup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4292, 466, 'Charnilaxmia', 'চরনিলক্ষিয়া', 0, 'charnilaxmiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4293, 466, 'Kushtia', 'কুষ্টিয়া', 0, 'kushtiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4294, 466, 'Paranganj', 'পরানগঞ্জ', 0, 'paranganjup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4295, 466, 'Sirta', 'সিরতা', 0, 'sirtaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4296, 466, 'Char Ishwardia', 'চর ঈশ্বরদিয়া', 0, 'charishwardiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4297, 466, 'Ghagra', 'ঘাগড়া', 0, 'ghagraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4298, 466, 'Vabokhali', 'ভাবখালী', 0, 'vabokhaliup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4299, 466, 'Boyra', 'বয়ড়া', 0, 'boyraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4300, 467, 'Dakshin Maijpara', 'দক্ষিণ মাইজপাড়া', 0, 'dakshinmaijparaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4301, 467, 'Gamaritola', 'গামারীতলা', 0, 'gamaritolaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4302, 467, 'Dhobaura', 'ধোবাউড়া', 0, 'dhobauraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4303, 467, 'Porakandulia', 'পোড়াকান্দুলিয়া', 0, 'porakanduliaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4304, 467, 'Goatala', 'গোয়াতলা', 0, 'goatalaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4305, 467, 'Ghoshgaon', 'ঘোষগাঁও', 0, 'ghoshgaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4306, 467, 'Baghber', 'বাঘবেড়', 0, 'baghberup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4307, 468, 'Rambhadrapur', 'রামভদ্রপুর', 0, 'rambhadrapurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4308, 468, 'Sondhara', 'ছনধরা', 0, 'sondharaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4309, 468, 'Vaitkandi', 'ভাইটকান্দি', 0, 'vaitkandiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4310, 468, 'Singheshwar', 'সিংহেশ্বর', 0, 'singheshwarup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4311, 468, 'Phulpur', 'ফুলপুর', 0, 'phulpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4312, 474, 'Banihala', 'বানিহালা', 0, 'banihalaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4313, 474, 'Biska', 'বিস্কা', 0, 'biskaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4314, 468, 'Baola', 'বওলা', 0, 'baolaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4315, 468, 'Payari', 'পয়ারী', 0, 'payariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4316, 468, 'Balia', 'বালিয়া', 0, 'baliaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4317, 468, 'Rahimganj', 'রহিমগঞ্জ', 0, 'rahimganjup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4318, 474, 'Balikha', 'বালিখা', 0, 'balikhaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4319, 474, 'Kakni', 'কাকনী', 0, 'kakniup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4320, 474, 'Dhakua', 'ঢাকুয়া', 0, 'dhakuaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4321, 468, 'Rupasi', 'রূপসী', 0, 'rupasiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4322, 474, 'Tarakanda', 'তারাকান্দা', 0, 'tarakandaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4323, 474, 'Galagaon', 'গালাগাঁও', 0, 'galagaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4324, 474, 'Kamargaon', 'কামারগাঁও', 0, 'kamargaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4325, 474, 'Kamaria', 'কামারিয়া', 0, 'kamariaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4326, 474, 'Rampur', 'রামপুর', 0, 'rampurup2.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4327, 469, 'Bhubankura', 'ভূবনকুড়া', 0, 'bhubankuraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4328, 469, 'Jugli', 'জুগলী', 0, 'jugliup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4329, 469, 'Kaichapur', 'কৈচাপুর', 0, 'kaichapurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4330, 469, 'Haluaghat', 'হালুয়াঘাট', 0, 'haluaghatup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4331, 469, 'Gazirbhita', 'গাজিরভিটা', 0, 'gazirbhitaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4332, 469, 'Bildora', 'বিলডোরা', 0, 'bildoraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4333, 469, 'Sakuai', 'শাকুয়াই', 0, 'sakuaiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4334, 469, 'Narail', 'নড়াইল', 0, 'narailup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4335, 469, 'Dhara', 'ধারা', 0, 'dharaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4336, 469, 'Dhurail', 'ধুরাইল', 0, 'dhurailup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4337, 469, 'Amtoil', 'আমতৈল', 0, 'amtoilup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4338, 469, 'Swadeshi', 'স্বদেশী', 0, 'swadeshiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4339, 470, 'Sahanati', 'সহনাটি', 0, 'sahanatiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4340, 470, 'Achintapur', 'অচিন্তপুর', 0, 'achintapurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4341, 470, 'Mailakanda', 'মইলাকান্দা', 0, 'mailakandaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4342, 470, 'Bokainagar', 'বোকাইনগর', 0, 'bokainagarup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4343, 470, 'Gouripur', 'গৌরীপুর', 0, 'gouripurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4344, 470, 'Maoha', 'মাওহা', 0, 'maohaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4345, 470, 'Ramgopalpur', 'রামগোপালপুর', 0, 'ramgopalpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4346, 470, 'Douhakhola', 'ডৌহাখলা', 0, 'douhakholaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4347, 470, 'Bhangnamari', 'ভাংনামারী', 0, 'bhangnamariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4348, 470, 'Sidhla', 'সিধলা', 0, 'sidhlaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4349, 471, 'Rasulpur', 'রসুলপুর', 0, 'rasulpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4350, 471, 'Barobaria', 'বারবারিয়া', 0, 'barobariaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4351, 471, 'Charalgi', 'চরআলগী', 0, 'charalgiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4352, 471, 'Saltia', 'সালটিয়া', 0, 'saltiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4353, 471, 'Raona', 'রাওনা', 0, 'raonaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4354, 471, 'Longair', 'লংগাইর', 0, 'longairup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4355, 471, 'Paithol', 'পাইথল', 0, 'paitholup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4356, 471, 'Gafargaon', 'গফরগাঁও', 0, 'gafargaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4357, 471, 'Josora', 'যশরা', 0, 'josoraup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4358, 471, 'Moshakhali', 'মশাখালী', 0, 'moshakhaliup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4359, 471, 'Panchbagh', 'পাঁচবাগ', 0, 'panchbaghup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4360, 471, 'Usthi', 'উস্থি', 0, 'usthiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4361, 471, 'Dotterbazar', 'দত্তেরবাজার', 0, 'dotterbazarup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4362, 471, 'Niguari', 'নিগুয়ারী', 0, 'niguariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4363, 471, 'Tangabo', 'টাংগাব', 0, 'tangaboup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4364, 472, 'Iswarganj', 'ঈশ্বরগঞ্জ', 0, 'iswarganjup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4365, 472, 'Sarisha', 'সরিষা', 0, 'sarishaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4366, 472, 'Sohagi', 'সোহাগী', 0, 'sohagiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4367, 472, 'Atharabari', 'আঠারবাড়ী', 0, 'atharabariup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4368, 472, 'Rajibpur', 'রাজিবপুর', 0, 'rajibpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4369, 472, 'Maijbagh', 'মাইজবাগ', 0, 'maijbaghup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4370, 472, 'Magtula', 'মগটুলা', 0, 'magtulaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4371, 472, 'Jatia', 'জাটিয়া', 0, 'jatiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4372, 472, 'Uchakhila', 'উচাখিলা', 0, 'uchakhilaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4373, 472, 'Tarundia', 'তারুন্দিয়া', 0, 'tarundiaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4374, 472, 'Barahit', 'বড়হিত', 0, 'barahitup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4375, 473, 'Batagoir', 'বেতাগৈর', 0, 'batagoirup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4376, 473, 'Nandail', 'নান্দাইল', 0, 'nandailup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4377, 473, 'Chandipasha', 'চন্ডীপাশা', 0, 'chandipashaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4378, 473, 'Gangail', 'গাংগাইল', 0, 'gangailup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4379, 473, 'Rajgati', 'রাজগাতী', 0, 'rajgatiup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4380, 473, 'Muajjempur', 'মোয়াজ্জেমপুর', 0, 'muajjempurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4381, 473, 'Sherpur', 'শেরপুর', 0, 'sherpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4382, 473, 'Singroil', 'সিংরইল', 0, 'singroilup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4383, 473, 'Achargaon', 'আচারগাঁও', 0, 'achargaonup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4384, 473, 'Mushulli', 'মুশুল্লী', 0, 'mushulliup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4385, 473, 'Kharua', 'খারুয়া', 0, 'kharuaup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4386, 473, 'Jahangirpur', 'জাহাঙ্গীরপুর', 0, 'jahangirpurup.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4387, 475, 'Kendua', 'কেন্দুয়া', 0, 'kenduaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4388, 475, 'Sharifpur', 'শরিফপুর', 0, 'sharifpurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4389, 475, 'Laxirchar', 'লক্ষীরচর', 0, 'laxircharup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4390, 475, 'Tolshirchar', 'তুলশীরচর', 0, 'tolshircharup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4391, 475, 'Itail', 'ইটাইল', 0, 'itailup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4392, 475, 'Narundi', 'নরুন্দী', 0, 'narundiup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4393, 475, 'Ghorada', 'ঘোড়াধাপ', 0, 'ghoradapup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4394, 475, 'Bashchara', 'বাশঁচড়া', 0, 'bashcharaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4395, 475, 'Ranagacha', 'রানাগাছা', 0, 'ranagachaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4396, 475, 'Sheepur', 'শ্রীপুর', 0, 'sheepurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4397, 475, 'Shahbajpur', 'শাহবাজপুর', 0, 'shahbajpurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4398, 475, 'Titpalla', 'তিতপল্লা', 0, 'titpallaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4399, 475, 'Mesta', 'মেষ্টা', 0, 'mestaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4400, 475, 'Digpait', 'দিগপাইত', 0, 'digpaitup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4401, 475, 'Rashidpur', 'রশিদপুর', 0, 'rashidpurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4402, 476, 'Durmot', 'দুরমুট', 0, 'durmotup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4403, 476, 'Kulia', 'কুলিয়া', 0, 'kuliaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4404, 476, 'Mahmudpur', 'মাহমুদপুর', 0, 'mahmudpurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4405, 476, 'Nangla', 'নাংলা', 0, 'nanglaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4406, 476, 'Nayanagar', 'নয়ানগর', 0, 'nayanagarup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4407, 476, 'Adra', 'আদ্রা', 0, 'adraup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4408, 476, 'Charbani Pakuria', 'চরবানী পাকুরিয়া', 0, 'charbanipakuriaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4409, 476, 'Fulkucha', 'ফুলকোচা', 0, 'fulkuchaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4410, 476, 'Ghuserpara', 'ঘোষেরপাড়া', 0, 'ghuserparaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4411, 476, 'Jhaugara', 'ঝাউগড়া', 0, 'jhaugaraup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4412, 476, 'Shuampur', 'শ্যামপুর', 0, 'shuampurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4413, 477, 'Kulkandi', 'কুলকান্দি', 0, 'kulkandiup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4414, 477, 'Belghacha', 'বেলগাছা', 0, 'belghachaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4415, 477, 'Chinaduli', 'চিনাডুলী', 0, 'chinaduliup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4416, 477, 'Shapdari', 'সাপধরী', 0, 'shapdariup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4417, 477, 'Noarpara', 'নোয়ারপাড়া', 0, 'noarparaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4418, 477, 'Islampur', 'ইসলামপুর', 0, 'islampurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4419, 477, 'Partharshi', 'পাথশী', 0, 'partharshiup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4420, 477, 'Palabandha', 'পলবান্ধা', 0, 'palabandhaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4421, 477, 'Gualerchar', 'গোয়ালেরচর', 0, 'gualercharup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4422, 477, 'Gaibandha', 'গাইবান্ধা', 0, 'gaibandhaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4423, 477, 'Charputimari', 'চরপুটিমারী', 0, 'charputimariup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4424, 477, 'Chargualini', 'চরগোয়ালীনি', 0, 'chargualiniup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4425, 478, 'Dungdhara', 'ডাংধরা', 0, 'dungdharaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4426, 478, 'Char Amkhawa', 'চর আমখাওয়া', 0, 'charamkhawaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4427, 478, 'Parram Rampur', 'পাররাম রামপুর', 0, 'parramrampurup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4428, 478, 'Hatibanga', 'হাতীভাঙ্গা', 0, 'hatibangaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4429, 478, 'Bahadurabad', 'বাহাদুরাবাদ', 0, 'bahadurabadup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4430, 478, 'Chikajani', 'চিকাজানী', 0, 'chikajaniup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4431, 478, 'Chukaibari', 'চুকাইবাড়ী', 0, 'chukaibariup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4432, 478, 'Dewangonj', 'দেওয়ানগঞ্জ', 0, 'dewangonjup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4433, 479, 'Satpoa', 'সাতপোয়া', 0, 'satpoaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4434, 479, 'Pogaldigha', 'পোগলদিঘা', 0, 'pogaldighaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4435, 479, 'Doail', 'ডোয়াইল', 0, 'doailup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4436, 479, 'Aona', 'আওনা', 0, 'aonaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4437, 479, 'Pingna', 'পিংনা', 0, 'pingnaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4438, 479, 'Bhatara', 'ভাটারা', 0, 'bhataraup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4439, 479, 'Kamrabad', 'কামরাবাদ', 0, 'kamrabadup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4440, 479, 'Mahadan', 'মহাদান', 0, 'mahadanup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4441, 480, 'Char Pakerdah', 'চর পাকেরদহ', 0, 'charpakerdahup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4442, 480, 'Karaichara', 'কড়ইচড়া', 0, 'karaicharaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4443, 480, 'Gunaritala', 'গুনারীতলা', 0, 'gunaritalaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4444, 480, 'Balijuri', 'বালিজুড়ী', 0, 'balijuriup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4445, 480, 'Jorekhali', 'জোড়খালী', 0, 'jorekhaliup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4446, 480, 'Adarvita', 'আদারভিটা', 0, 'adarvitaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4447, 480, 'Sidhuli', 'সিধুলী', 0, 'sidhuliup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4448, 481, 'Danua', 'ধানুয়া', 0, 'danuaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4449, 481, 'Bagarchar', 'বগারচর', 0, 'bagarcharup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4450, 481, 'Battajore', 'বাট্রাজোড়', 0, 'battajoreup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4451, 481, 'Shadurpara', 'সাধুরপাড়া', 0, 'shadurparaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4452, 481, 'Bakshigonj', 'বকসীগঞ্জ', 0, 'bakshigonjup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4453, 481, 'Nilakhia', 'নিলক্ষিয়া', 0, 'nilakhiaup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4454, 481, 'Merurchar', 'মেরুরচর', 0, 'merurcharup.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4455, 482, 'Asma', 'আসমা', 0, 'asma.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4456, 482, 'Chhiram', 'চিরাম', 0, 'chhiram.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4457, 482, 'Baushi', 'বাউশী', 0, 'baushiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4458, 482, 'Barhatta', 'বারহাট্টা', 0, 'barhattaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4459, 482, 'Raypur', 'রায়পুর', 0, 'raypurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4460, 482, 'Sahata', 'সাহতা', 0, 'sahataup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4461, 482, 'Singdha', 'সিংধা', 0, 'singdhaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4462, 483, 'Durgapur', 'দূর্গাপুর', 0, 'durgapurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4463, 483, 'Kakoirgora', 'কাকৈরগড়া', 0, 'kakoirgoraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4464, 483, 'Kullagora', 'কুল্লাগড়া', 0, 'kullagoraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4465, 483, 'Chandigarh', 'চণ্ডিগড়', 0, 'chandigarhup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4466, 483, 'Birisiri', 'বিরিশিরি', 0, 'birisiriup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4467, 483, 'Bakaljora', 'বাকলজোড়া', 0, 'bakaljoraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4468, 483, 'Gawkandia', 'গাঁওকান্দিয়া', 0, 'gawkandiaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4469, 484, 'Asujia', 'আশুজিয়া', 0, 'asujiaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4470, 484, 'Dalpa', 'দলপা', 0, 'dalpaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4471, 484, 'Goraduba', 'গড়াডোবা', 0, 'goradubaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4472, 484, 'Gonda', 'গণ্ডা', 0, 'gondaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4473, 484, 'Sandikona', 'সান্দিকোনা', 0, 'sandikonaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4474, 484, 'Maska', 'মাসকা', 0, 'maskaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4475, 484, 'Bolaishimul', 'বলাইশিমুল', 0, 'bolaishimulup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4476, 484, 'Noapara', 'নওপাড়া', 0, 'noaparaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4477, 484, 'Kandiura', 'কান্দিউড়া', 0, 'kandiuraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4478, 484, 'Chirang', 'চিরাং', 0, 'chirangup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4479, 484, 'Roailbari Amtala', 'রোয়াইলবাড়ী আমতলা', 0, 'roailbariamtalaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4480, 484, 'Paikura', 'পাইকুড়া', 0, 'paikuraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4481, 484, 'Muzafarpur', 'মোজাফরপুর', 0, 'muzafarpurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4482, 485, 'Shormushia', 'স্বরমুশিয়া', 0, 'shormushiaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4483, 485, 'Shunoi', 'শুনই', 0, 'shunoiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4484, 485, 'Lunesshor', 'লুনেশ্বর', 0, 'lunesshorup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4485, 485, 'Baniyajan', 'বানিয়াজান', 0, 'baniyajanup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4486, 485, 'Teligati', 'তেলিগাতী', 0, 'teligatiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4487, 485, 'Duoj', 'দুওজ', 0, 'duojup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4488, 485, 'Sukhari', 'সুখারী', 0, 'sukhariup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4489, 486, 'Fathepur', 'ফতেপুর', 0, 'fathepurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4490, 486, 'Nayekpur', 'নায়েকপুর', 0, 'nayekpurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4491, 486, 'Teosree', 'তিয়শ্রী', 0, 'teosreeup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4492, 486, 'Magan', 'মাঘান', 0, 'maganup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4493, 486, 'Gobindasree', 'গেবিন্দশ্রী', 0, 'gobindasreeup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4494, 486, 'Madan', 'মদন', 0, 'madanup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4495, 486, 'Chandgaw', 'চানগাঁও', 0, 'chandgawup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4496, 486, 'Kytail', 'কাইটাল', 0, 'kytailup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4497, 487, 'Krishnapur', 'কৃষ্ণপুর', 0, 'krishnapurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4498, 487, 'Nogor', 'নগর', 0, 'nogorup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4499, 487, 'Chakua', 'চাকুয়া', 0, 'chakuaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4500, 487, 'Khaliajuri', 'খালিয়াজুরী', 0, 'khaliajuriup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4501, 487, 'Mendipur', 'মেন্দিপুর', 0, 'mendipurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4502, 487, 'Gazipur', 'গাজীপুর', 0, 'gazipurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4503, 488, 'Koilati', 'কৈলাটী', 0, 'koilatiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4504, 488, 'Najirpur', 'নাজিরপুর', 0, 'najirpurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4505, 488, 'Pogla', 'পোগলা', 0, 'poglaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4506, 488, 'Kolmakanda', 'কলমাকান্দা', 0, 'kolmakandaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4507, 488, 'Rongchati', 'রংছাতি', 0, 'rongchatiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4508, 488, 'Lengura', 'লেংগুরা', 0, 'lenguraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4509, 488, 'Borokhapon', 'বড়খাপন', 0, 'borokhaponup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4510, 488, 'Kharnoi', 'খারনৈ', 0, 'kharnoiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4511, 489, 'Borokashia Birampur', 'বড়কাশিয়া বিরামপুর', 0, 'borokashiabirampurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4512, 489, 'Borotoli Banihari', 'বড়তলী বানিহারী', 0, 'borotolibanihariup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4513, 489, 'Tetulia', 'তেতুলিয়া', 0, 'tetuliaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4514, 489, 'Maghan Siadar', 'মাঘান সিয়াদার', 0, 'maghansiadarup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4515, 489, 'Somaj Sohildeo', 'সমাজ সহিলদেও', 0, 'somajsohildeoup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4516, 489, 'Suair', 'সুয়াইর', 0, 'suairup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4517, 489, 'Gaglajur', 'গাগলাজুর', 0, 'gaglajurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4518, 490, 'Khalishaur', 'খলিশাউড়', 0, 'khalishaurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4519, 490, 'Ghagra', 'ঘাগড়া', 0, 'ghagraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4520, 490, 'Jaria', 'জারিয়া', 0, 'jariaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4521, 490, 'Narandia', 'নারান্দিয়া', 0, 'narandiaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4522, 490, 'Bishkakuni', 'বিশকাকুনী', 0, 'bishkakuniup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4523, 490, 'Bairaty', 'বৈরাটী', 0, 'bairaty.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4524, 490, 'Hogla', 'হোগলা', 0, 'hoglaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4525, 490, 'Gohalakanda', 'গোহালাকান্দা', 0, 'gohalakandaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4526, 490, 'Dhalamulgaon', 'ধলামুলগাঁও', 0, 'dhalamulgaonup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4527, 490, 'Agia', 'আগিয়া', 0, 'agia.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4528, 490, 'Purbadhala', 'পূর্বধলা', 0, 'purbadhalaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4529, 491, 'Chollisha', 'চল্লিশা', 0, 'chollishaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4530, 491, 'Kailati', 'কাইলাটি', 0, 'kailatiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4531, 491, 'Dokkhin Bishiura', 'দক্ষিণ বিশিউড়া', 0, 'dokkhinbishiuraup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4532, 491, 'Modonpur', 'মদনপুর', 0, 'modonpurup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4533, 491, 'Amtola', 'আমতলা', 0, 'amtolaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4534, 491, 'Lokkhiganj', 'লক্ষীগঞ্জ', 0, 'lokkhiganj.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4535, 491, 'Singher Bangla', 'সিংহের বাংলা', 0, 'singherbanglaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4536, 491, 'Thakurakona', 'ঠাকুরাকোণা', 0, 'thakurakonaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4537, 491, 'Mougati', 'মৌগাতি', 0, 'mougatiup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4538, 491, 'Rouha', 'রৌহা', 0, 'rouhaup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4539, 491, 'Medni', 'মেদনী', 0, 'medniup.netrokona.gov.bd', NULL, NULL);
INSERT INTO `unions` VALUES (4540, 491, 'Kaliara Babragati', 'কালিয়ারা গাবরাগাতি', 0, 'kaliaragabragatiup.netrokona.gov.bd', NULL, NULL);

-- ----------------------------
-- Table structure for upazilas
-- ----------------------------
DROP TABLE IF EXISTS `upazilas`;
CREATE TABLE `upazilas`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `district_id` int NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bn_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `up_code` int NOT NULL DEFAULT 0,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `district_id`(`district_id` ASC) USING BTREE,
  CONSTRAINT `upazilas_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 492 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of upazilas
-- ----------------------------
INSERT INTO `upazilas` VALUES (1, 1, 'Debidwar', 'দেবিদ্বার', 0, 'debidwar.comilla.gov.bd', NULL, '2020-02-18 04:29:30');
INSERT INTO `upazilas` VALUES (2, 1, 'Barura', 'বরুড়া', 0, 'barura.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (3, 1, 'Brahmanpara', 'ব্রাহ্মণপাড়া', 0, 'brahmanpara.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (4, 1, 'Chandina', 'চান্দিনা', 0, 'chandina.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (5, 1, 'Chauddagram', 'চৌদ্দগ্রাম', 0, 'chauddagram.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (6, 1, 'Daudkandi', 'দাউদকান্দি', 0, 'daudkandi.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (7, 1, 'Homna', 'হোমনা', 0, 'homna.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (8, 1, 'Laksam', 'লাকসাম', 0, 'laksam.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (9, 1, 'Muradnagar', 'মুরাদনগর', 0, 'muradnagar.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (10, 1, 'Nangalkot', 'নাঙ্গলকোট', 0, 'nangalkot.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (11, 1, 'Comilla Sadar', 'কুমিল্লা সদর', 0, 'comillasadar.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (12, 1, 'Meghna', 'মেঘনা', 0, 'meghna.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (13, 1, 'Monohargonj', 'মনোহরগঞ্জ', 0, 'monohargonj.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (14, 1, 'Sadarsouth', 'সদর দক্ষিণ', 0, 'sadarsouth.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (15, 1, 'Titas', 'তিতাস', 0, 'titas.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (16, 1, 'Burichang', 'বুড়িচং', 0, 'burichang.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (17, 1, 'Lalmai', 'লালমাই', 0, 'lalmai.comilla.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (18, 2, 'Chhagalnaiya', 'ছাগলনাইয়া', 0, 'chhagalnaiya.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (19, 2, 'Feni Sadar', 'ফেনী সদর', 0, 'sadar.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (20, 2, 'Sonagazi', 'সোনাগাজী', 0, 'sonagazi.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (21, 2, 'Fulgazi', 'ফুলগাজী', 0, 'fulgazi.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (22, 2, 'Parshuram', 'পরশুরাম', 0, 'parshuram.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (23, 2, 'Daganbhuiyan', 'দাগনভূঞা', 0, 'daganbhuiyan.feni.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (24, 3, 'Brahmanbaria Sadar', 'ব্রাহ্মণবাড়িয়া সদর', 0, 'sadar.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (25, 3, 'Kasba', 'কসবা', 0, 'kasba.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (26, 3, 'Nasirnagar', 'নাসিরনগর', 0, 'nasirnagar.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (27, 3, 'Sarail', 'সরাইল', 0, 'sarail.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (28, 3, 'Ashuganj', 'আশুগঞ্জ', 0, 'ashuganj.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (29, 3, 'Akhaura', 'আখাউড়া', 0, 'akhaura.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (30, 3, 'Nabinagar', 'নবীনগর', 0, 'nabinagar.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (31, 3, 'Bancharampur', 'বাঞ্ছারামপুর', 0, 'bancharampur.brahmanbaria.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (32, 3, 'Bijoynagar', 'বিজয়নগর', 0, 'bijoynagar.brahmanbaria.gov.bd    ', NULL, NULL);
INSERT INTO `upazilas` VALUES (33, 4, 'Rangamati Sadar', 'রাঙ্গামাটি সদর', 0, 'sadar.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (34, 4, 'Kaptai', 'কাপ্তাই', 0, 'kaptai.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (35, 4, 'Kawkhali', 'কাউখালী', 0, 'kawkhali.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (36, 4, 'Baghaichari', 'বাঘাইছড়ি', 0, 'baghaichari.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (37, 4, 'Barkal', 'বরকল', 0, 'barkal.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (38, 4, 'Langadu', 'লংগদু', 0, 'langadu.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (39, 4, 'Rajasthali', 'রাজস্থলী', 0, 'rajasthali.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (40, 4, 'Belaichari', 'বিলাইছড়ি', 0, 'belaichari.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (41, 4, 'Juraichari', 'জুরাছড়ি', 0, 'juraichari.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (42, 4, 'Naniarchar', 'নানিয়ারচর', 0, 'naniarchar.rangamati.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (43, 5, 'Noakhali Sadar', 'নোয়াখালী সদর', 0, 'sadar.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (44, 5, 'Companiganj', 'কোম্পানীগঞ্জ', 0, 'companiganj.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (45, 5, 'Begumganj', 'বেগমগঞ্জ', 0, 'begumganj.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (46, 5, 'Hatia', 'হাতিয়া', 0, 'hatia.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (47, 5, 'Subarnachar', 'সুবর্ণচর', 0, 'subarnachar.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (48, 5, 'Kabirhat', 'কবিরহাট', 0, 'kabirhat.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (49, 5, 'Senbug', 'সেনবাগ', 0, 'senbug.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (50, 5, 'Chatkhil', 'চাটখিল', 0, 'chatkhil.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (51, 5, 'Sonaimori', 'সোনাইমুড়ী', 0, 'sonaimori.noakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (52, 6, 'Haimchar', 'হাইমচর', 0, 'haimchar.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (53, 6, 'Kachua', 'কচুয়া', 0, 'kachua.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (54, 6, 'Shahrasti', 'শাহরাস্তি	', 0, 'shahrasti.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (55, 6, 'Chandpur Sadar', 'চাঁদপুর সদর', 0, 'sadar.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (56, 6, 'Matlab South', 'মতলব দক্ষিণ', 0, 'matlabsouth.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (57, 6, 'Hajiganj', 'হাজীগঞ্জ', 0, 'hajiganj.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (58, 6, 'Matlab North', 'মতলব উত্তর', 0, 'matlabnorth.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (59, 6, 'Faridgonj', 'ফরিদগঞ্জ', 0, 'faridgonj.chandpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (60, 7, 'Lakshmipur Sadar', 'লক্ষ্মীপুর সদর', 0, 'sadar.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (61, 7, 'Kamalnagar', 'কমলনগর', 0, 'kamalnagar.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (62, 7, 'Raipur', 'রায়পুর', 0, 'raipur.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (63, 7, 'Ramgati', 'রামগতি', 0, 'ramgati.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (64, 7, 'Ramganj', 'রামগঞ্জ', 0, 'ramganj.lakshmipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (65, 8, 'Rangunia', 'রাঙ্গুনিয়া', 0, 'rangunia.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (66, 8, 'Sitakunda', 'সীতাকুন্ড', 0, 'sitakunda.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (67, 8, 'Mirsharai', 'মীরসরাই', 0, 'mirsharai.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (68, 8, 'Patiya', 'পটিয়া', 0, 'patiya.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (69, 8, 'Sandwip', 'সন্দ্বীপ', 0, 'sandwip.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (70, 8, 'Banshkhali', 'বাঁশখালী', 0, 'banshkhali.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (71, 8, 'Boalkhali', 'বোয়ালখালী', 0, 'boalkhali.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (72, 8, 'Anwara', 'আনোয়ারা', 0, 'anwara.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (73, 8, 'Chandanaish', 'চন্দনাইশ', 0, 'chandanaish.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (74, 8, 'Satkania', 'সাতকানিয়া', 0, 'satkania.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (75, 8, 'Lohagara', 'লোহাগাড়া', 0, 'lohagara.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (76, 8, 'Hathazari', 'হাটহাজারী', 0, 'hathazari.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (77, 8, 'Fatikchhari', 'ফটিকছড়ি', 0, 'fatikchhari.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (78, 8, 'Raozan', 'রাউজান', 0, 'raozan.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (79, 8, 'Karnafuli', 'কর্ণফুলী', 0, 'karnafuli.chittagong.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (80, 9, 'Coxsbazar Sadar', 'কক্সবাজার সদর', 0, 'sadar.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (81, 9, 'Chakaria', 'চকরিয়া', 0, 'chakaria.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (82, 9, 'Kutubdia', 'কুতুবদিয়া', 0, 'kutubdia.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (83, 9, 'Ukhiya', 'উখিয়া', 0, 'ukhiya.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (84, 9, 'Moheshkhali', 'মহেশখালী', 0, 'moheshkhali.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (85, 9, 'Pekua', 'পেকুয়া', 0, 'pekua.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (86, 9, 'Ramu', 'রামু', 0, 'ramu.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (87, 9, 'Teknaf', 'টেকনাফ', 0, 'teknaf.coxsbazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (88, 10, 'Khagrachhari Sadar', 'খাগড়াছড়ি সদর', 0, 'sadar.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (89, 10, 'Dighinala', 'দিঘীনালা', 0, 'dighinala.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (90, 10, 'Panchari', 'পানছড়ি', 0, 'panchari.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (91, 10, 'Laxmichhari', 'লক্ষীছড়ি', 0, 'laxmichhari.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (92, 10, 'Mohalchari', 'মহালছড়ি', 0, 'mohalchari.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (93, 10, 'Manikchari', 'মানিকছড়ি', 0, 'manikchari.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (94, 10, 'Ramgarh', 'রামগড়', 0, 'ramgarh.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (95, 10, 'Matiranga', 'মাটিরাঙ্গা', 0, 'matiranga.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (96, 10, 'Guimara', 'গুইমারা', 0, 'guimara.khagrachhari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (97, 11, 'Bandarban Sadar', 'বান্দরবান সদর', 0, 'sadar.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (98, 11, 'Alikadam', 'আলীকদম', 0, 'alikadam.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (99, 11, 'Naikhongchhari', 'নাইক্ষ্যংছড়ি', 0, 'naikhongchhari.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (100, 11, 'Rowangchhari', 'রোয়াংছড়ি', 0, 'rowangchhari.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (101, 11, 'Lama', 'লামা', 0, 'lama.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (102, 11, 'Ruma', 'রুমা', 0, 'ruma.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (103, 11, 'Thanchi', 'থানচি', 0, 'thanchi.bandarban.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (104, 12, 'Belkuchi', 'বেলকুচি', 0, 'belkuchi.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (105, 12, 'Chauhali', 'চৌহালি', 0, 'chauhali.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (106, 12, 'Kamarkhand', 'কামারখন্দ', 0, 'kamarkhand.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (107, 12, 'Kazipur', 'কাজীপুর', 0, 'kazipur.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (108, 12, 'Raigonj', 'রায়গঞ্জ', 0, 'raigonj.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (109, 12, 'Shahjadpur', 'শাহজাদপুর', 0, 'shahjadpur.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (110, 12, 'Sirajganj Sadar', 'সিরাজগঞ্জ সদর', 0, 'sirajganjsadar.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (111, 12, 'Tarash', 'তাড়াশ', 0, 'tarash.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (112, 12, 'Ullapara', 'উল্লাপাড়া', 0, 'ullapara.sirajganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (113, 13, 'Sujanagar', 'সুজানগর', 0, 'sujanagar.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (114, 13, 'Ishurdi', 'ঈশ্বরদী', 0, 'ishurdi.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (115, 13, 'Bhangura', 'ভাঙ্গুড়া', 0, 'bhangura.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (116, 13, 'Pabna Sadar', 'পাবনা সদর', 0, 'pabnasadar.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (117, 13, 'Bera', 'বেড়া', 0, 'bera.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (118, 13, 'Atghoria', 'আটঘরিয়া', 0, 'atghoria.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (119, 13, 'Chatmohar', 'চাটমোহর', 0, 'chatmohar.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (120, 13, 'Santhia', 'সাঁথিয়া', 0, 'santhia.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (121, 13, 'Faridpur', 'ফরিদপুর', 0, 'faridpur.pabna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (122, 14, 'Kahaloo', 'কাহালু', 0, 'kahaloo.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (123, 14, 'Bogra Sadar', 'বগুড়া সদর', 0, 'sadar.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (124, 14, 'Shariakandi', 'সারিয়াকান্দি', 0, 'shariakandi.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (125, 14, 'Shajahanpur', 'শাজাহানপুর', 0, 'shajahanpur.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (126, 14, 'Dupchanchia', 'দুপচাচিঁয়া', 0, 'dupchanchia.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (127, 14, 'Adamdighi', 'আদমদিঘি', 0, 'adamdighi.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (128, 14, 'Nondigram', 'নন্দিগ্রাম', 0, 'nondigram.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (129, 14, 'Sonatala', 'সোনাতলা', 0, 'sonatala.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (130, 14, 'Dhunot', 'ধুনট', 0, 'dhunot.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (131, 14, 'Gabtali', 'গাবতলী', 0, 'gabtali.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (132, 14, 'Sherpur', 'শেরপুর', 0, 'sherpur.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (133, 14, 'Shibganj', 'শিবগঞ্জ', 0, 'shibganj.bogra.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (134, 15, 'Paba', 'পবা', 0, 'paba.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (135, 15, 'Durgapur', 'দুর্গাপুর', 0, 'durgapur.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (136, 15, 'Mohonpur', 'মোহনপুর', 0, 'mohonpur.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (137, 15, 'Charghat', 'চারঘাট', 0, 'charghat.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (138, 15, 'Puthia', 'পুঠিয়া', 0, 'puthia.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (139, 15, 'Bagha', 'বাঘা', 0, 'bagha.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (140, 15, 'Godagari', 'গোদাগাড়ী', 0, 'godagari.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (141, 15, 'Tanore', 'তানোর', 0, 'tanore.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (142, 15, 'Bagmara', 'বাগমারা', 0, 'bagmara.rajshahi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (143, 16, 'Natore Sadar', 'নাটোর সদর', 0, 'natoresadar.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (144, 16, 'Singra', 'সিংড়া', 0, 'singra.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (145, 16, 'Baraigram', 'বড়াইগ্রাম', 0, 'baraigram.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (146, 16, 'Bagatipara', 'বাগাতিপাড়া', 0, 'bagatipara.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (147, 16, 'Lalpur', 'লালপুর', 0, 'lalpur.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (148, 16, 'Gurudaspur', 'গুরুদাসপুর', 0, 'gurudaspur.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (149, 16, 'Naldanga', 'নলডাঙ্গা', 0, 'naldanga.natore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (150, 17, 'Akkelpur', 'আক্কেলপুর', 0, 'akkelpur.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (151, 17, 'Kalai', 'কালাই', 0, 'kalai.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (152, 17, 'Khetlal', 'ক্ষেতলাল', 0, 'khetlal.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (153, 17, 'Panchbibi', 'পাঁচবিবি', 0, 'panchbibi.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (154, 17, 'Joypurhat Sadar', 'জয়পুরহাট সদর', 0, 'joypurhatsadar.joypurhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (155, 18, 'Chapainawabganj Sadar', 'চাঁপাইনবাবগঞ্জ সদর', 0, 'chapainawabganjsadar.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (156, 18, 'Gomostapur', 'গোমস্তাপুর', 0, 'gomostapur.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (157, 18, 'Nachol', 'নাচোল', 0, 'nachol.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (158, 18, 'Bholahat', 'ভোলাহাট', 0, 'bholahat.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (159, 18, 'Shibganj', 'শিবগঞ্জ', 0, 'shibganj.chapainawabganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (160, 19, 'Mohadevpur', 'মহাদেবপুর', 0, 'mohadevpur.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (161, 19, 'Badalgachi', 'বদলগাছী', 0, 'badalgachi.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (162, 19, 'Patnitala', 'পত্নিতলা', 0, 'patnitala.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (163, 19, 'Dhamoirhat', 'ধামইরহাট', 0, 'dhamoirhat.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (164, 19, 'Niamatpur', 'নিয়ামতপুর', 0, 'niamatpur.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (165, 19, 'Manda', 'মান্দা', 0, 'manda.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (166, 19, 'Atrai', 'আত্রাই', 0, 'atrai.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (167, 19, 'Raninagar', 'রাণীনগর', 0, 'raninagar.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (168, 19, 'Naogaon Sadar', 'নওগাঁ সদর', 0, 'naogaonsadar.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (169, 19, 'Porsha', 'পোরশা', 0, 'porsha.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (170, 19, 'Sapahar', 'সাপাহার', 0, 'sapahar.naogaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (171, 20, 'Manirampur', 'মণিরামপুর', 0, 'manirampur.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (172, 20, 'Abhaynagar', 'অভয়নগর', 0, 'abhaynagar.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (173, 20, 'Bagherpara', 'বাঘারপাড়া', 0, 'bagherpara.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (174, 20, 'Chougachha', 'চৌগাছা', 0, 'chougachha.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (175, 20, 'Jhikargacha', 'ঝিকরগাছা', 0, 'jhikargacha.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (176, 20, 'Keshabpur', 'কেশবপুর', 0, 'keshabpur.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (177, 20, 'Jessore Sadar', 'যশোর সদর', 0, 'sadar.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (178, 20, 'Sharsha', 'শার্শা', 0, 'sharsha.jessore.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (179, 21, 'Assasuni', 'আশাশুনি', 0, 'assasuni.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (180, 21, 'Debhata', 'দেবহাটা', 0, 'debhata.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (181, 21, 'Kalaroa', 'কলারোয়া', 0, 'kalaroa.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (182, 21, 'Satkhira Sadar', 'সাতক্ষীরা সদর', 0, 'satkhirasadar.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (183, 21, 'Shyamnagar', 'শ্যামনগর', 0, 'shyamnagar.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (184, 21, 'Tala', 'তালা', 0, 'tala.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (185, 21, 'Kaliganj', 'কালিগঞ্জ', 0, 'kaliganj.satkhira.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (186, 22, 'Mujibnagar', 'মুজিবনগর', 0, 'mujibnagar.meherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (187, 22, 'Meherpur Sadar', 'মেহেরপুর সদর', 0, 'meherpursadar.meherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (188, 22, 'Gangni', 'গাংনী', 0, 'gangni.meherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (189, 23, 'Narail Sadar', 'নড়াইল সদর', 0, 'narailsadar.narail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (190, 23, 'Lohagara', 'লোহাগড়া', 0, 'lohagara.narail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (191, 23, 'Kalia', 'কালিয়া', 0, 'kalia.narail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (192, 24, 'Chuadanga Sadar', 'চুয়াডাঙ্গা সদর', 0, 'chuadangasadar.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (193, 24, 'Alamdanga', 'আলমডাঙ্গা', 0, 'alamdanga.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (194, 24, 'Damurhuda', 'দামুড়হুদা', 0, 'damurhuda.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (195, 24, 'Jibannagar', 'জীবননগর', 0, 'jibannagar.chuadanga.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (196, 25, 'Kushtia Sadar', 'কুষ্টিয়া সদর', 0, 'kushtiasadar.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (197, 25, 'Kumarkhali', 'কুমারখালী', 0, 'kumarkhali.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (198, 25, 'Khoksa', 'খোকসা', 0, 'khoksa.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (199, 25, 'Mirpur', 'মিরপুর', 0, 'mirpurkushtia.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (200, 25, 'Daulatpur', 'দৌলতপুর', 0, 'daulatpur.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (201, 25, 'Bheramara', 'ভেড়ামারা', 0, 'bheramara.kushtia.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (202, 26, 'Shalikha', 'শালিখা', 0, 'shalikha.magura.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (203, 26, 'Sreepur', 'শ্রীপুর', 0, 'sreepur.magura.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (204, 26, 'Magura Sadar', 'মাগুরা সদর', 0, 'magurasadar.magura.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (205, 26, 'Mohammadpur', 'মহম্মদপুর', 0, 'mohammadpur.magura.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (206, 27, 'Paikgasa', 'পাইকগাছা', 0, 'paikgasa.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (207, 27, 'Fultola', 'ফুলতলা', 0, 'fultola.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (208, 27, 'Digholia', 'দিঘলিয়া', 0, 'digholia.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (209, 27, 'Rupsha', 'রূপসা', 0, 'rupsha.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (210, 27, 'Terokhada', 'তেরখাদা', 0, 'terokhada.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (211, 27, 'Dumuria', 'ডুমুরিয়া', 0, 'dumuria.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (212, 27, 'Botiaghata', 'বটিয়াঘাটা', 0, 'botiaghata.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (213, 27, 'Dakop', 'দাকোপ', 0, 'dakop.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (214, 27, 'Koyra', 'কয়রা', 0, 'koyra.khulna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (215, 28, 'Fakirhat', 'ফকিরহাট', 0, 'fakirhat.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (216, 28, 'Bagerhat Sadar', 'বাগেরহাট সদর', 0, 'sadar.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (217, 28, 'Mollahat', 'মোল্লাহাট', 0, 'mollahat.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (218, 28, 'Sarankhola', 'শরণখোলা', 0, 'sarankhola.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (219, 28, 'Rampal', 'রামপাল', 0, 'rampal.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (220, 28, 'Morrelganj', 'মোড়েলগঞ্জ', 0, 'morrelganj.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (221, 28, 'Kachua', 'কচুয়া', 0, 'kachua.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (222, 28, 'Mongla', 'মোংলা', 0, 'mongla.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (223, 28, 'Chitalmari', 'চিতলমারী', 0, 'chitalmari.bagerhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (224, 29, 'Jhenaidah Sadar', 'ঝিনাইদহ সদর', 0, 'sadar.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (225, 29, 'Shailkupa', 'শৈলকুপা', 0, 'shailkupa.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (226, 29, 'Harinakundu', 'হরিণাকুন্ডু', 0, 'harinakundu.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (227, 29, 'Kaliganj', 'কালীগঞ্জ', 0, 'kaliganj.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (228, 29, 'Kotchandpur', 'কোটচাঁদপুর', 0, 'kotchandpur.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (229, 29, 'Moheshpur', 'মহেশপুর', 0, 'moheshpur.jhenaidah.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (230, 30, 'Jhalakathi Sadar', 'ঝালকাঠি সদর', 0, 'sadar.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (231, 30, 'Kathalia', 'কাঠালিয়া', 0, 'kathalia.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (232, 30, 'Nalchity', 'নলছিটি', 0, 'nalchity.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (233, 30, 'Rajapur', 'রাজাপুর', 0, 'rajapur.jhalakathi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (234, 31, 'Bauphal', 'বাউফল', 0, 'bauphal.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (235, 31, 'Patuakhali Sadar', 'পটুয়াখালী সদর', 0, 'sadar.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (236, 31, 'Dumki', 'দুমকি', 0, 'dumki.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (237, 31, 'Dashmina', 'দশমিনা', 0, 'dashmina.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (238, 31, 'Kalapara', 'কলাপাড়া', 0, 'kalapara.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (239, 31, 'Mirzaganj', 'মির্জাগঞ্জ', 0, 'mirzaganj.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (240, 31, 'Galachipa', 'গলাচিপা', 0, 'galachipa.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (241, 31, 'Rangabali', 'রাঙ্গাবালী', 0, 'rangabali.patuakhali.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (242, 32, 'Pirojpur Sadar', 'পিরোজপুর সদর', 0, 'sadar.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (243, 32, 'Nazirpur', 'নাজিরপুর', 0, 'nazirpur.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (244, 32, 'Kawkhali', 'কাউখালী', 0, 'kawkhali.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (245, 32, 'Zianagar', 'জিয়ানগর', 0, 'zianagar.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (246, 32, 'Bhandaria', 'ভান্ডারিয়া', 0, 'bhandaria.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (247, 32, 'Mathbaria', 'মঠবাড়ীয়া', 0, 'mathbaria.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (248, 32, 'Nesarabad', 'নেছারাবাদ', 0, 'nesarabad.pirojpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (249, 33, 'Barisal Sadar', 'বরিশাল সদর', 0, 'barisalsadar.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (250, 33, 'Bakerganj', 'বাকেরগঞ্জ', 0, 'bakerganj.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (251, 33, 'Babuganj', 'বাবুগঞ্জ', 0, 'babuganj.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (252, 33, 'Wazirpur', 'উজিরপুর', 0, 'wazirpur.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (253, 33, 'Banaripara', 'বানারীপাড়া', 0, 'banaripara.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (254, 33, 'Gournadi', 'গৌরনদী', 0, 'gournadi.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (255, 33, 'Agailjhara', 'আগৈলঝাড়া', 0, 'agailjhara.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (256, 33, 'Mehendiganj', 'মেহেন্দিগঞ্জ', 0, 'mehendiganj.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (257, 33, 'Muladi', 'মুলাদী', 0, 'muladi.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (258, 33, 'Hizla', 'হিজলা', 0, 'hizla.barisal.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (259, 34, 'Bhola Sadar', 'ভোলা সদর', 0, 'sadar.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (260, 34, 'Borhan Sddin', 'বোরহান উদ্দিন', 0, 'borhanuddin.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (261, 34, 'Charfesson', 'চরফ্যাশন', 0, 'charfesson.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (262, 34, 'Doulatkhan', 'দৌলতখান', 0, 'doulatkhan.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (263, 34, 'Monpura', 'মনপুরা', 0, 'monpura.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (264, 34, 'Tazumuddin', 'তজুমদ্দিন', 0, 'tazumuddin.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (265, 34, 'Lalmohan', 'লালমোহন', 0, 'lalmohan.bhola.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (266, 35, 'Amtali', 'আমতলী', 0, 'amtali.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (267, 35, 'Barguna Sadar', 'বরগুনা সদর', 0, 'sadar.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (268, 35, 'Betagi', 'বেতাগী', 0, 'betagi.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (269, 35, 'Bamna', 'বামনা', 0, 'bamna.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (270, 35, 'Pathorghata', 'পাথরঘাটা', 0, 'pathorghata.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (271, 35, 'Taltali', 'তালতলি', 0, 'taltali.barguna.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (272, 36, 'Balaganj', 'বালাগঞ্জ', 0, 'balaganj.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (273, 36, 'Beanibazar', 'বিয়ানীবাজার', 0, 'beanibazar.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (274, 36, 'Bishwanath', 'বিশ্বনাথ', 0, 'bishwanath.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (275, 36, 'Companiganj', 'কোম্পানীগঞ্জ', 0, 'companiganj.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (276, 36, 'Fenchuganj', 'ফেঞ্চুগঞ্জ', 0, 'fenchuganj.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (277, 36, 'Golapganj', 'গোলাপগঞ্জ', 0, 'golapganj.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (278, 36, 'Gowainghat', 'গোয়াইনঘাট', 0, 'gowainghat.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (279, 36, 'Jaintiapur', 'জৈন্তাপুর', 0, 'jaintiapur.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (280, 36, 'Kanaighat', 'কানাইঘাট', 0, 'kanaighat.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (281, 36, 'Sylhet Sadar', 'সিলেট সদর', 0, 'sylhetsadar.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (282, 36, 'Zakiganj', 'জকিগঞ্জ', 0, 'zakiganj.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (283, 36, 'Dakshinsurma', 'দক্ষিণ সুরমা', 0, 'dakshinsurma.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (284, 36, 'Osmaninagar', 'ওসমানী নগর', 0, 'osmaninagar.sylhet.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (285, 37, 'Barlekha', 'বড়লেখা', 0, 'barlekha.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (286, 37, 'Kamolganj', 'কমলগঞ্জ', 0, 'kamolganj.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (287, 37, 'Kulaura', 'কুলাউড়া', 0, 'kulaura.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (288, 37, 'Moulvibazar Sadar', 'মৌলভীবাজার সদর', 0, 'moulvibazarsadar.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (289, 37, 'Rajnagar', 'রাজনগর', 0, 'rajnagar.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (290, 37, 'Sreemangal', 'শ্রীমঙ্গল', 0, 'sreemangal.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (291, 37, 'Juri', 'জুড়ী', 0, 'juri.moulvibazar.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (292, 38, 'Nabiganj', 'নবীগঞ্জ', 0, 'nabiganj.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (293, 38, 'Bahubal', 'বাহুবল', 0, 'bahubal.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (294, 38, 'Ajmiriganj', 'আজমিরীগঞ্জ', 0, 'ajmiriganj.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (295, 38, 'Baniachong', 'বানিয়াচং', 0, 'baniachong.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (296, 38, 'Lakhai', 'লাখাই', 0, 'lakhai.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (297, 38, 'Chunarughat', 'চুনারুঘাট', 0, 'chunarughat.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (298, 38, 'Habiganj Sadar', 'হবিগঞ্জ সদর', 0, 'habiganjsadar.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (299, 38, 'Madhabpur', 'মাধবপুর', 0, 'madhabpur.habiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (300, 39, 'Sunamganj Sadar', 'সুনামগঞ্জ সদর', 0, 'sadar.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (301, 39, 'South Sunamganj', 'দক্ষিণ সুনামগঞ্জ', 0, 'southsunamganj.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (302, 39, 'Bishwambarpur', 'বিশ্বম্ভরপুর', 0, 'bishwambarpur.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (303, 39, 'Chhatak', 'ছাতক', 0, 'chhatak.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (304, 39, 'Jagannathpur', 'জগন্নাথপুর', 0, 'jagannathpur.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (305, 39, 'Dowarabazar', 'দোয়ারাবাজার', 0, 'dowarabazar.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (306, 39, 'Tahirpur', 'তাহিরপুর', 0, 'tahirpur.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (307, 39, 'Dharmapasha', 'ধর্মপাশা', 0, 'dharmapasha.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (308, 39, 'Jamalganj', 'জামালগঞ্জ', 0, 'jamalganj.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (309, 39, 'Shalla', 'শাল্লা', 0, 'shalla.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (310, 39, 'Derai', 'দিরাই', 0, 'derai.sunamganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (311, 40, 'Belabo', 'বেলাবো', 0, 'belabo.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (312, 40, 'Monohardi', 'মনোহরদী', 0, 'monohardi.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (313, 40, 'Narsingdi Sadar', 'নরসিংদী সদর', 0, 'narsingdisadar.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (314, 40, 'Palash', 'পলাশ', 0, 'palash.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (315, 40, 'Raipura', 'রায়পুরা', 0, 'raipura.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (316, 40, 'Shibpur', 'শিবপুর', 0, 'shibpur.narsingdi.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (317, 41, 'Kaliganj', 'কালীগঞ্জ', 0, 'kaliganj.gazipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (318, 41, 'Kaliakair', 'কালিয়াকৈর', 0, 'kaliakair.gazipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (319, 41, 'Kapasia', 'কাপাসিয়া', 0, 'kapasia.gazipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (320, 41, 'Gazipur Sadar', 'গাজীপুর সদর', 0, 'sadar.gazipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (321, 41, 'Sreepur', 'শ্রীপুর', 0, 'sreepur.gazipur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (322, 42, 'Shariatpur Sadar', 'শরিয়তপুর সদর', 0, 'sadar.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (323, 42, 'Naria', 'নড়িয়া', 0, 'naria.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (324, 42, 'Zajira', 'জাজিরা', 0, 'zajira.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (325, 42, 'Gosairhat', 'গোসাইরহাট', 0, 'gosairhat.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (326, 42, 'Bhedarganj', 'ভেদরগঞ্জ', 0, 'bhedarganj.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (327, 42, 'Damudya', 'ডামুড্যা', 0, 'damudya.shariatpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (328, 43, 'Araihazar', 'আড়াইহাজার', 0, 'araihazar.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (329, 43, 'Bandar', 'বন্দর', 0, 'bandar.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (330, 43, 'Narayanganj Sadar', 'নারায়নগঞ্জ সদর', 0, 'narayanganjsadar.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (331, 43, 'Rupganj', 'রূপগঞ্জ', 0, 'rupganj.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (332, 43, 'Sonargaon', 'সোনারগাঁ', 0, 'sonargaon.narayanganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (333, 44, 'Basail', 'বাসাইল', 0, 'basail.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (334, 44, 'Bhuapur', 'ভুয়াপুর', 0, 'bhuapur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (335, 44, 'Delduar', 'দেলদুয়ার', 0, 'delduar.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (336, 44, 'Ghatail', 'ঘাটাইল', 0, 'ghatail.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (337, 44, 'Gopalpur', 'গোপালপুর', 0, 'gopalpur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (338, 44, 'Madhupur', 'মধুপুর', 0, 'madhupur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (339, 44, 'Mirzapur', 'মির্জাপুর', 0, 'mirzapur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (340, 44, 'Nagarpur', 'নাগরপুর', 0, 'nagarpur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (341, 44, 'Sakhipur', 'সখিপুর', 0, 'sakhipur.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (342, 44, 'Tangail Sadar', 'টাঙ্গাইল সদর', 0, 'tangailsadar.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (343, 44, 'Kalihati', 'কালিহাতী', 0, 'kalihati.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (344, 44, 'Dhanbari', 'ধনবাড়ী', 0, 'dhanbari.tangail.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (345, 45, 'Itna', 'ইটনা', 0, 'itna.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (346, 45, 'Katiadi', 'কটিয়াদী', 0, 'katiadi.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (347, 45, 'Bhairab', 'ভৈরব', 0, 'bhairab.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (348, 45, 'Tarail', 'তাড়াইল', 0, 'tarail.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (349, 45, 'Hossainpur', 'হোসেনপুর', 0, 'hossainpur.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (350, 45, 'Pakundia', 'পাকুন্দিয়া', 0, 'pakundia.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (351, 45, 'Kuliarchar', 'কুলিয়ারচর', 0, 'kuliarchar.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (352, 45, 'Kishoreganj Sadar', 'কিশোরগঞ্জ সদর', 0, 'kishoreganjsadar.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (353, 45, 'Karimgonj', 'করিমগঞ্জ', 0, 'karimgonj.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (354, 45, 'Bajitpur', 'বাজিতপুর', 0, 'bajitpur.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (355, 45, 'Austagram', 'অষ্টগ্রাম', 0, 'austagram.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (356, 45, 'Mithamoin', 'মিঠামইন', 0, 'mithamoin.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (357, 45, 'Nikli', 'নিকলী', 0, 'nikli.kishoreganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (358, 46, 'Harirampur', 'হরিরামপুর', 0, 'harirampur.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (359, 46, 'Saturia', 'সাটুরিয়া', 0, 'saturia.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (360, 46, 'Manikganj Sadar', 'মানিকগঞ্জ সদর', 0, 'sadar.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (361, 46, 'Gior', 'ঘিওর', 0, 'gior.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (362, 46, 'Shibaloy', 'শিবালয়', 0, 'shibaloy.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (363, 46, 'Doulatpur', 'দৌলতপুর', 0, 'doulatpur.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (364, 46, 'Singiar', 'সিংগাইর', 0, 'singiar.manikganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (365, 47, 'Savar', 'সাভার', 0, 'savar.dhaka.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (366, 47, 'Dhamrai', 'ধামরাই', 0, 'dhamrai.dhaka.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (367, 47, 'Keraniganj', 'কেরাণীগঞ্জ', 0, 'keraniganj.dhaka.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (368, 47, 'Nawabganj', 'নবাবগঞ্জ', 0, 'nawabganj.dhaka.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (369, 47, 'Dohar', 'দোহার', 0, 'dohar.dhaka.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (370, 48, 'Munshiganj Sadar', 'মুন্সিগঞ্জ সদর', 0, 'sadar.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (371, 48, 'Sreenagar', 'শ্রীনগর', 0, 'sreenagar.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (372, 48, 'Sirajdikhan', 'সিরাজদিখান', 0, 'sirajdikhan.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (373, 48, 'Louhajanj', 'লৌহজং', 0, 'louhajanj.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (374, 48, 'Gajaria', 'গজারিয়া', 0, 'gajaria.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (375, 48, 'Tongibari', 'টংগীবাড়ি', 0, 'tongibari.munshiganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (376, 49, 'Rajbari Sadar', 'রাজবাড়ী সদর', 0, 'sadar.rajbari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (377, 49, 'Goalanda', 'গোয়ালন্দ', 0, 'goalanda.rajbari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (378, 49, 'Pangsa', 'পাংশা', 0, 'pangsa.rajbari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (379, 49, 'Baliakandi', 'বালিয়াকান্দি', 0, 'baliakandi.rajbari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (380, 49, 'Kalukhali', 'কালুখালী', 0, 'kalukhali.rajbari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (381, 50, 'Madaripur Sadar', 'মাদারীপুর সদর', 0, 'sadar.madaripur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (382, 50, 'Shibchar', 'শিবচর', 0, 'shibchar.madaripur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (383, 50, 'Kalkini', 'কালকিনি', 0, 'kalkini.madaripur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (384, 50, 'Rajoir', 'রাজৈর', 0, 'rajoir.madaripur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (385, 51, 'Gopalganj Sadar', 'গোপালগঞ্জ সদর', 32, 'sadar.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (386, 51, 'Kashiani', 'কাশিয়ানী', 43, 'kashiani.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (387, 51, 'Tungipara', 'টুংগীপাড়া', 91, 'tungipara.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (388, 51, 'Kotalipara', 'কোটালীপাড়া', 51, 'kotalipara.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (389, 51, 'Muksudpur', 'মুকসুদপুর', 58, 'muksudpur.gopalganj.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (390, 52, 'Faridpur Sadar', 'ফরিদপুর সদর', 0, 'sadar.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (391, 52, 'Alfadanga', 'আলফাডাঙ্গা', 0, 'alfadanga.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (392, 52, 'Boalmari', 'বোয়ালমারী', 0, 'boalmari.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (393, 52, 'Sadarpur', 'সদরপুর', 0, 'sadarpur.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (394, 52, 'Nagarkanda', 'নগরকান্দা', 0, 'nagarkanda.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (395, 52, 'Bhanga', 'ভাঙ্গা', 0, 'bhanga.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (396, 52, 'Charbhadrasan', 'চরভদ্রাসন', 0, 'charbhadrasan.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (397, 52, 'Madhukhali', 'মধুখালী', 0, 'madhukhali.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (398, 52, 'Saltha', 'সালথা', 0, 'saltha.faridpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (399, 53, 'Panchagarh Sadar', 'পঞ্চগড় সদর', 0, 'panchagarhsadar.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (400, 53, 'Debiganj', 'দেবীগঞ্জ', 0, 'debiganj.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (401, 53, 'Boda', 'বোদা', 0, 'boda.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (402, 53, 'Atwari', 'আটোয়ারী', 0, 'atwari.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (403, 53, 'Tetulia', 'তেতুলিয়া', 0, 'tetulia.panchagarh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (404, 54, 'Nawabganj', 'নবাবগঞ্জ', 0, 'nawabganj.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (405, 54, 'Birganj', 'বীরগঞ্জ', 0, 'birganj.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (406, 54, 'Ghoraghat', 'ঘোড়াঘাট', 0, 'ghoraghat.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (407, 54, 'Birampur', 'বিরামপুর', 0, 'birampur.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (408, 54, 'Parbatipur', 'পার্বতীপুর', 0, 'parbatipur.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (409, 54, 'Bochaganj', 'বোচাগঞ্জ', 0, 'bochaganj.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (410, 54, 'Kaharol', 'কাহারোল', 0, 'kaharol.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (411, 54, 'Fulbari', 'ফুলবাড়ী', 0, 'fulbari.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (412, 54, 'Dinajpur Sadar', 'দিনাজপুর সদর', 0, 'dinajpursadar.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (413, 54, 'Hakimpur', 'হাকিমপুর', 0, 'hakimpur.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (414, 54, 'Khansama', 'খানসামা', 0, 'khansama.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (415, 54, 'Birol', 'বিরল', 0, 'birol.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (416, 54, 'Chirirbandar', 'চিরিরবন্দর', 0, 'chirirbandar.dinajpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (417, 55, 'Lalmonirhat Sadar', 'লালমনিরহাট সদর', 0, 'sadar.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (418, 55, 'Kaliganj', 'কালীগঞ্জ', 0, 'kaliganj.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (419, 55, 'Hatibandha', 'হাতীবান্ধা', 0, 'hatibandha.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (420, 55, 'Patgram', 'পাটগ্রাম', 0, 'patgram.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (421, 55, 'Aditmari', 'আদিতমারী', 0, 'aditmari.lalmonirhat.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (422, 56, 'Syedpur', 'সৈয়দপুর', 0, 'syedpur.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (423, 56, 'Domar', 'ডোমার', 0, 'domar.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (424, 56, 'Dimla', 'ডিমলা', 0, 'dimla.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (425, 56, 'Jaldhaka', 'জলঢাকা', 0, 'jaldhaka.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (426, 56, 'Kishorganj', 'কিশোরগঞ্জ', 0, 'kishorganj.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (427, 56, 'Nilphamari Sadar', 'নীলফামারী সদর', 0, 'nilphamarisadar.nilphamari.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (428, 57, 'Sadullapur', 'সাদুল্লাপুর', 0, 'sadullapur.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (429, 57, 'Gaibandha Sadar', 'গাইবান্ধা সদর', 0, 'gaibandhasadar.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (430, 57, 'Palashbari', 'পলাশবাড়ী', 0, 'palashbari.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (431, 57, 'Saghata', 'সাঘাটা', 0, 'saghata.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (432, 57, 'Gobindaganj', 'গোবিন্দগঞ্জ', 0, 'gobindaganj.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (433, 57, 'Sundarganj', 'সুন্দরগঞ্জ', 0, 'sundarganj.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (434, 57, 'Phulchari', 'ফুলছড়ি', 0, 'phulchari.gaibandha.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (435, 58, 'Thakurgaon Sadar', 'ঠাকুরগাঁও সদর', 0, 'thakurgaonsadar.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (436, 58, 'Pirganj', 'পীরগঞ্জ', 0, 'pirganj.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (437, 58, 'Ranisankail', 'রাণীশংকৈল', 0, 'ranisankail.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (438, 58, 'Haripur', 'হরিপুর', 0, 'haripur.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (439, 58, 'Baliadangi', 'বালিয়াডাঙ্গী', 0, 'baliadangi.thakurgaon.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (440, 59, 'Rangpur Sadar', 'রংপুর সদর', 0, 'rangpursadar.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (441, 59, 'Gangachara', 'গংগাচড়া', 0, 'gangachara.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (442, 59, 'Taragonj', 'তারাগঞ্জ', 0, 'taragonj.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (443, 59, 'Badargonj', 'বদরগঞ্জ', 0, 'badargonj.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (444, 59, 'Mithapukur', 'মিঠাপুকুর', 0, 'mithapukur.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (445, 59, 'Pirgonj', 'পীরগঞ্জ', 0, 'pirgonj.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (446, 59, 'Kaunia', 'কাউনিয়া', 0, 'kaunia.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (447, 59, 'Pirgacha', 'পীরগাছা', 0, 'pirgacha.rangpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (448, 60, 'Kurigram Sadar', 'কুড়িগ্রাম সদর', 0, 'kurigramsadar.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (449, 60, 'Nageshwari', 'নাগেশ্বরী', 0, 'nageshwari.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (450, 60, 'Bhurungamari', 'ভুরুঙ্গামারী', 0, 'bhurungamari.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (451, 60, 'Phulbari', 'ফুলবাড়ী', 0, 'phulbari.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (452, 60, 'Rajarhat', 'রাজারহাট', 0, 'rajarhat.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (453, 60, 'Ulipur', 'উলিপুর', 0, 'ulipur.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (454, 60, 'Chilmari', 'চিলমারী', 0, 'chilmari.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (455, 60, 'Rowmari', 'রৌমারী', 0, 'rowmari.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (456, 60, 'Charrajibpur', 'চর রাজিবপুর', 0, 'charrajibpur.kurigram.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (457, 61, 'Sherpur Sadar', 'শেরপুর সদর', 0, 'sherpursadar.sherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (458, 61, 'Nalitabari', 'নালিতাবাড়ী', 0, 'nalitabari.sherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (459, 61, 'Sreebordi', 'শ্রীবরদী', 0, 'sreebordi.sherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (460, 61, 'Nokla', 'নকলা', 0, 'nokla.sherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (461, 61, 'Jhenaigati', 'ঝিনাইগাতী', 0, 'jhenaigati.sherpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (462, 62, 'Fulbaria', 'ফুলবাড়ীয়া', 0, 'fulbaria.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (463, 62, 'Trishal', 'ত্রিশাল', 0, 'trishal.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (464, 62, 'Bhaluka', 'ভালুকা', 0, 'bhaluka.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (465, 62, 'Muktagacha', 'মুক্তাগাছা', 0, 'muktagacha.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (466, 62, 'Mymensingh Sadar', 'ময়মনসিংহ সদর', 0, 'mymensinghsadar.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (467, 62, 'Dhobaura', 'ধোবাউড়া', 0, 'dhobaura.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (468, 62, 'Phulpur', 'ফুলপুর', 0, 'phulpur.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (469, 62, 'Haluaghat', 'হালুয়াঘাট', 0, 'haluaghat.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (470, 62, 'Gouripur', 'গৌরীপুর', 0, 'gouripur.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (471, 62, 'Gafargaon', 'গফরগাঁও', 0, 'gafargaon.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (472, 62, 'Iswarganj', 'ঈশ্বরগঞ্জ', 0, 'iswarganj.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (473, 62, 'Nandail', 'নান্দাইল', 0, 'nandail.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (474, 62, 'Tarakanda', 'তারাকান্দা', 0, 'tarakanda.mymensingh.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (475, 63, 'Jamalpur Sadar', 'জামালপুর সদর', 0, 'jamalpursadar.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (476, 63, 'Melandah', 'মেলান্দহ', 0, 'melandah.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (477, 63, 'Islampur', 'ইসলামপুর', 0, 'islampur.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (478, 63, 'Dewangonj', 'দেওয়ানগঞ্জ', 0, 'dewangonj.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (479, 63, 'Sarishabari', 'সরিষাবাড়ী', 0, 'sarishabari.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (480, 63, 'Madarganj', 'মাদারগঞ্জ', 0, 'madarganj.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (481, 63, 'Bokshiganj', 'বকশীগঞ্জ', 0, 'bokshiganj.jamalpur.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (482, 64, 'Barhatta', 'বারহাট্টা', 0, 'barhatta.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (483, 64, 'Durgapur', 'দুর্গাপুর', 0, 'durgapur.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (484, 64, 'Kendua', 'কেন্দুয়া', 0, 'kendua.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (485, 64, 'Atpara', 'আটপাড়া', 0, 'atpara.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (486, 64, 'Madan', 'মদন', 0, 'madan.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (487, 64, 'Khaliajuri', 'খালিয়াজুরী', 0, 'khaliajuri.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (488, 64, 'Kalmakanda', 'কলমাকান্দা', 0, 'kalmakanda.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (489, 64, 'Mohongonj', 'মোহনগঞ্জ', 0, 'mohongonj.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (490, 64, 'Purbadhala', 'পূর্বধলা', 0, 'purbadhala.netrokona.gov.bd', NULL, NULL);
INSERT INTO `upazilas` VALUES (491, 64, 'Netrokona Sadar', 'নেত্রকোণা সদর', 0, 'netrokonasadar.netrokona.gov.bd', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user_type` int NOT NULL DEFAULT 3,
  `institute_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Akib Md. Sadiqul Islam', 'laureal.seu@gmail.com', '2022-12-20 18:16:40', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bKXquTE0D7', 1, 0, NULL, NULL);

-- ----------------------------
-- Table structure for users_permissions
-- ----------------------------
DROP TABLE IF EXISTS `users_permissions`;
CREATE TABLE `users_permissions`  (
  `user_id` int UNSIGNED NOT NULL,
  `permission_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `permission_id`) USING BTREE,
  INDEX `users_permissions_permission_id_foreign`(`permission_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of users_permissions
-- ----------------------------
INSERT INTO `users_permissions` VALUES (1, 1);
INSERT INTO `users_permissions` VALUES (1, 2);
INSERT INTO `users_permissions` VALUES (1, 3);

-- ----------------------------
-- Table structure for users_roles
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles`  (
  `user_id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `users_roles_role_id_foreign`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of users_roles
-- ----------------------------
INSERT INTO `users_roles` VALUES (1, 1);

SET FOREIGN_KEY_CHECKS = 1;
