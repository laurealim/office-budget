ALTER TABLE `items`
    MODIFY COLUMN `item_type`  tinyint(4) NOT NULL COMMENT '1=Products, 2=Services' AFTER `id`;

ALTER TABLE `budget_types`
    MODIFY COLUMN `budget_type`  tinyint(4) NOT NULL COMMENT '1=Salary, 2=Contingency, 3=Training, 4=Seminar' AFTER `id`;

ALTER TABLE `institutes`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `budget_types`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`,
    ADD COLUMN `budget_type_code`  int(11) NOT NULL AFTER `budget_type`;

ALTER TABLE `budget_types`
DROP COLUMN `budget_type_status`;

ALTER TABLE `budget_types`
    MODIFY COLUMN `budget_type`  varchar(200) NOT NULL COMMENT '1=Salary, 2=Contingency, 3=Training, 4=Seminar' AFTER `id`;

ALTER TABLE `budget_types`
    MODIFY COLUMN `budget_type_code`  int(11) NOT NULL DEFAULT 0 AFTER `budget_type`;

ALTER TABLE `budget_types`
    MODIFY COLUMN `budget_type_code`  int(11) NULL DEFAULT 0 AFTER `budget_type`;

ALTER TABLE `budget_codes`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `users`
    ADD COLUMN `institute_id`  int(11) NOT NULL AFTER `user_type`;

ALTER TABLE `budget_codes`
    ADD COLUMN `vat`  double(16,2) NOT NULL DEFAULT 0.00 AFTER `budget_title`,
    ADD COLUMN `tax`  double(16,2) NOT NULL DEFAULT 0.00 AFTER `vat`;

ALTER TABLE `budget_codes`
    ADD COLUMN `is_item_parent`  tinyint(2) NOT NULL DEFAULT 0 AFTER `tax`;

ALTER TABLE `items`
    MODIFY COLUMN `item_type`  integer(11) NOT NULL COMMENT '1=Products, 2=Services' AFTER `id`;

ALTER TABLE `items`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `budget_codes`
    ADD COLUMN `bgt_type`  int(11) NOT NULL AFTER `id`;

ALTER TABLE `office_budgets`
    ADD COLUMN `status`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved' AFTER `institute_id`;

ALTER TABLE `office_budgets`
    MODIFY COLUMN `file_name`  varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `budget_details`;

ALTER TABLE `office_budgets` CHANGE `budget_details` `budget_details` JSON NOT NULL;

ALTER TABLE `office_budgets`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `office_budgets`
    MODIFY COLUMN `status`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject' AFTER `institute_id`;

ALTER TABLE `office_budget_balances` ADD `budget_codes` JSON NOT NULL AFTER `budget_type`;

ALTER TABLE `office_budget_balances`
    DROP `budget_codes`;

ALTER TABLE `office_budget_balances` CHANGE `total_balances` `total_balances` JSON NOT NULL COMMENT 'Including vat tax', CHANGE `usable_balances` `usable_balances` JSON NOT NULL COMMENT 'Without vat tax';

ALTER TABLE `office_budget_balances`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `internal_budget_balances`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `office_budget_expenses`
    MODIFY COLUMN `useable_expense`  int(11) NOT NULL DEFAULT 0 AFTER `expn_date`;

ALTER TABLE `office_budget_expenses`
    ADD COLUMN `status`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Rejected' AFTER `file_name`;

ALTER TABLE `office_budget_expenses`
    MODIFY COLUMN `details`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT 'item,desc,unit_price,quantity,amount' AFTER `useable_expense`,
    MODIFY COLUMN `file_name`  varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `institute_id`;

ALTER TABLE `office_budget_expenses`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `internal_budgets`
    ADD COLUMN `status`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject' AFTER `institute_id`;

ALTER TABLE `internal_budgets`
    ADD COLUMN `budget_code`  int(11) NOT NULL DEFAULT 0 AFTER `budget_type`;

ALTER TABLE `internal_budgets`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`;

ALTER TABLE `internal_budgets`
    MODIFY COLUMN `file_name`  varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `total_amount`;

ALTER TABLE `internal_budget_expenses`
    MODIFY COLUMN `updated_by`  int(11) NULL AFTER `created_by`,
    ADD COLUMN `status`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject' AFTER `file_name`;

ALTER TABLE `internal_budget_expenses`
    MODIFY COLUMN `file_name`  varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `institute_id`;


