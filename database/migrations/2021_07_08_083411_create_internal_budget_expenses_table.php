<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalBudgetExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_budget_expenses', function (Blueprint $table) {
            $table->id();
            $table->string('financial_year');
            $table->integer('budget_type');
            $table->date('expn_date');
            $table->json('details')->comment('title,unit_price,quantity,price');
            $table->integer('total_price');
            $table->integer('institute_id');
            $table->string('file_name');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_budget_expenses');
    }
}
