<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->id();
            $table->integer('flat_id');
            $table->integer('tenant_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('f_rent');
            $table->integer('f_charge');
            $table->integer('other_charge');
            $table->integer('prv_due');
            $table->integer('total_bill');
            $table->integer('pay_rent_amount');
            $table->integer('pay_other_amount');
            $table->integer('pay_adv_amount');
            $table->integer('total_payment');
            $table->integer('cur_due_adv');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledgers');
    }
}
