<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('flat_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('rent');
            $table->integer('s_charge');
            $table->integer('other_charge');
            $table->integer('prv_due');
            $table->integer('total_bill');
            $table->integer('total_pay');
            $table->integer('cur_due');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
