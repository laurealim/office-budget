<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeBudgetExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_budget_expenses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('financial_year');
            $table->integer('budget_type');
            $table->integer('budget_code');
            $table->integer('vat');
            $table->integer('tax');
            $table->date('expn_date');
            $table->integer('useable_expense');
            $table->json('details')->comment('item,desc,unit_price,quantity,amount');
            $table->integer('total_expense');
            $table->integer('institute_id');
            $table->string('file_name');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_budget_expenses');
    }
}
