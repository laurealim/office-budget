<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_vouchers', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_code_id');
            $table->integer('voucher_no')->nullable()->default(0);
            $table->integer('year');
            $table->string('voucher_items');
            $table->integer('total_cost');
            $table->integer('institute_id');
            $table->integer('office_exp_id')->nullable()->default(0);
            $table->integer('status')->nullable()->default(0);
            $table->string('attachment')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_vouchers');
    }
};
