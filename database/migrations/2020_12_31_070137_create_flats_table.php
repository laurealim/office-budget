<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flats', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('dist');
            $table->integer('upaz');
            $table->integer('union');
            $table->integer('word');
            $table->string('address');
            $table->integer('face');
            $table->string('flat');
            $table->integer('floor');
            $table->integer('size');
            $table->integer('room');
            $table->integer('bath');
            $table->integer('veranda');
            $table->tinyInteger('gas');
            $table->tinyInteger('lift');
            $table->tinyInteger('generator');
            $table->tinyInteger('parking');
            $table->tinyInteger('intercom');
            $table->tinyInteger('garbage');
            $table->tinyInteger('comn_elec');
            $table->tinyInteger('comn_clean');
            $table->tinyInteger('comn_water');
            $table->integer('rent');
            $table->integer('service_charge');
            $table->integer('monthly_bill');
            $table->integer('total_due');
            $table->integer('advance_month');
            $table->integer('advance_bill');
            $table->integer('advance_pay');
            $table->tinyInteger('status');
            $table->string('pic');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flats');
    }
}
