<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_budgets', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('financial_year');
            $table->integer('budget_type');
            $table->double('total_amount',15,2);
            $table->string('file_name');
            $table->integer('institute_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_budgets');
    }
}
