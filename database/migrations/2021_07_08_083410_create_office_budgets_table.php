<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_budgets', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('financial_year');
            $table->integer('budget_type');
            $table->json('budget_details');
            $table->string('file_name');
            $table->integer('institute_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_budgets');
    }
}
