<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeBudgetBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_budget_balances', function (Blueprint $table) {
            $table->id();
            $table->string('financial_year');
            $table->integer('budget_type');
            $table->json('total_updatable_balances')->comment('Json formatted updatable data');
            $table->json('total_balances')->comment('Including vat tax');
            $table->json('usable_balances')->comment('Without vat tax');
            $table->integer('institute_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_budget_balances');
    }
}
