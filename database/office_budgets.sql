/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : office_budget

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/02/2023 17:24:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for office_budgets
-- ----------------------------
DROP TABLE IF EXISTS `office_budgets`;
CREATE TABLE `office_budgets`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `budget_type` int NOT NULL,
  `budget_details` json NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `institute_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1 = Approved, 2 = Reject',
  `created_by` int NOT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of office_budgets
-- ----------------------------
INSERT INTO `office_budgets` VALUES (1, 'জেলা কর্মকর্তা ও কর্মচারীদের নগদ মঞ্জুরি ও বেতন ২২-২৩', '2022', 1, '{\"3111101\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"742500\", \"useable_amount\": \"742500\"}, \"3111201\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"154600\", \"useable_amount\": \"154600\"}, \"3111301\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111302\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111306\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"5500\", \"useable_amount\": \"5500\"}, \"3111309\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}, \"3111310\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"343200\", \"useable_amount\": \"343200\"}, \"3111311\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"49500\", \"useable_amount\": \"49500\"}, \"3111314\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"2200\", \"useable_amount\": \"2200\"}, \"3111325\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"163100\", \"useable_amount\": \"163100\"}, \"3111335\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"16400\", \"useable_amount\": \"16400\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-01 03:49:49', '2023-01-01 05:59:43');
INSERT INTO `office_budgets` VALUES (2, 'ভ্রমণ ব্যয় ও বদলী ব্যয়', '2022', 6, '{\"3244101\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"25000\", \"useable_amount\": \"25000\"}, \"3244102\": {\"tax\": \"0\", \"vat\": \"0\", \"amount\": \"0\", \"useable_amount\": \"0\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-22 09:45:49', '2023-01-22 10:23:20');
INSERT INTO `office_budgets` VALUES (3, 'পণ্য ও সেবার ব্যবহার', '2022', 2, '{\"3211106\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"11000\", \"useable_amount\": \"9900\"}, \"3211117\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"12000\", \"useable_amount\": \"10800\"}, \"3211119\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"700\", \"useable_amount\": \"644\"}, \"3211120\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"8000\", \"useable_amount\": \"7360\"}, \"3211129\": {\"tax\": \"5\", \"vat\": \"15\", \"amount\": \"200000\", \"useable_amount\": \"160000\"}, \"3255102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3255104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"1500\", \"useable_amount\": \"1350\"}, \"3255105\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"6000\", \"useable_amount\": \"5520\"}, \"3258102\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258103\": {\"tax\": \"12\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4250\"}, \"3258104\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258105\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"3258107\": {\"tax\": \"5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4600\"}, \"4112303\": {\"tax\": \"7.5\", \"vat\": \"5\", \"amount\": \"2500\", \"useable_amount\": \"2200\"}, \"4112310\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"5000\", \"useable_amount\": \"4500\"}, \"4112314\": {\"tax\": \"7.5\", \"vat\": \"3\", \"amount\": \"2500\", \"useable_amount\": \"2250\"}}', 'budget.pdf', 1, 0, 1, 1, '2023-01-22 10:28:22', '2023-02-01 11:21:50');

SET FOREIGN_KEY_CHECKS = 1;
