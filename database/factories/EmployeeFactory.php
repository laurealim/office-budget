<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'salary' => $faker->numberBetween(30000, 90000),
        'department' =>$faker->randomElement($array = ['Accounting', 'Management','HR','Sales','Marketing','QA'])
    ];
});
