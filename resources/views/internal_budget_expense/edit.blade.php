@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('internal-budget-expense.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Office Budget --}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"> সাধারন তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> বাজেটের ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_type" name="budget_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetTypeList as $id => $val){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $internalBudgetExpenseData->budget_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                                <option value="0"> অন্যন্য</option>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <!-- Date -->
                        <div class="form-group">
                            <label>তারিখ <span class="reqrd"> * </span></label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" class="form-control" data-inputmask-alias="datetime"
                                       data-inputmask-inputformat="mm/dd/yyyy" id="expn_date" name="expn_date" data-mask
                                       value="{{ date('m/d/Y',strtotime($internalBudgetExpenseData->expn_date)) }}"/>
                                <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('expn_date'))
                                        <span class="help-block middle">
                                    <strong>{{ $errors->first('expn_date') }}</strong>
                                </span>
                                    @endif
                        </span>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আর্থিক বছর <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="financial_year" name="financial_year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($financialList as $year => $val){ ?>
                                <option
                                    value="{{ $year }}" {{ $year == $internalBudgetExpenseData->financial_year ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('financial_year'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('financial_year') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{-- Office Budget Details--}}
        <div class="card card-info bgt_dels">
            <div class="card-header">
                <h3 class="card-title"> ব্যায় বিভাজন </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap" id="budget_tbl">
                            <thead>
                            <tr>
                                <th>নাম</th>
                                <th>একক মূল্য</th>
                                <th>পরিমান</th>
                                <th>মূল্য</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="dynamic_field">
                            <?php
                            foreach ($detailsDecodedData as $key=>$vals){ ?>
                            <tr class="dynamic-added" id="row{{ $key }}">
                                <td>
                                    <input type="tex" class="form-control title" name="details[{{ $key }}][title]" id="title_{{ $key }}"
                                           n_val="{{ $key }}" placeholder="ব্যায়ের খাত" value="{{ $vals["title"] }}"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control unit_price" min="0"
                                           name="details[{{ $key }}][unit_price]" n_val="{{ $key }}" id="unit_price_{{ $key }}"
                                           value="{{ $vals["unit_price"] }}"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control quantity" min="0"
                                           name="details[{{ $key }}][quantity]" n_val="{{ $key }}" id="quantity_{{ $key }}"
                                           value="{{ $vals["quantity"] }}"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control price" min="0" name="details[{{ $key }}][price]"
                                           id="price_{{ $key }}" readonly value="{{ $vals["price"] }}"/>
                                </td>
                                <?php
                                if($key <= 1){ ?>
                                <td>
                                    <a title="Edit" class="btn btn-success" id="row_add" n_val="{{ $key }}" href='#'><i
                                            class="fas fa-plus"></i></a>
                                </td>
                                <?php } else{ ?>
                                <td>
                                    <button type="button" n_val="{{ $key }}" name="remove" id="{{ $key }}"
                                            class="btn btn-danger btn_remove">X
                                    </button>
                                </td>
                                <?php }
                                ?>
                            </tr>
                            <?php }
                            ?>

                            </tbody>
                            <tfooter>
                                <tr>
                                    <td colspan="3" style="text-align:right"><b>Total Price</b></td>
                                    <td><b><input type="number" class="form-control" id="total_price" name="total_price"
                                                  min="0" value="{{ $internalBudgetExpenseData["total_price"] }}"
                                                  readonly="readonly"/></b></td>
                                    <td></td>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

        </div>

        {{-- Office Budget Images--}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট ফাইল সংযুক্তি</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">বাজেট ফাইল নির্বাচন করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="file_name" class="col-xs-10 col-sm-5" name="file_name"/>
                                <?php
                                if (!empty($internalBudgetExpenseData->file_name)) {
                                    $path = asset("images/budget/internal_budget_expense/" . $internalBudgetExpenseData->id . "/" . $internalBudgetExpenseData->file_name);
                                    $icon = '<a target="_blank" title="Attachment" class="btn btn-success" id="edit-office-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i> ' . $internalBudgetExpenseData->file_name . ' </a>';
                                    echo $icon;
                                }
                                ?>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('file_name'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('file_name') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

        });

        var i = '<?php echo sizeof($detailsDecodedData);?>';
        $('#row_add').on('click', function (e) {
            e.preventDefault();
            // alert("clicked");
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added">' +
                '<td><input type="text" name="details[' + i + '][title]" placeholder="ব্যায়ের খাত" class="form-control title" n_val="' + i + '" id="title_' + i + '" /></td>' +
                '<td><input type="number" name="details[' + i + '][unit_price]" value="0.0" class="form-control unit_price" n_val="' + i + '" id="unit_price_' + i + '" min="0" /></td>' +
                '<td><input type="number" name="details[' + i + '][quantity]" value="0.0" class="form-control quantity" n_val="' + i + '" id="quantity_' + i + '" min="0" /></td>' +
                '<td><input type="number" name="details[' + i + '][price]" value="0.0" n_val="' + i + '" class="form-control price" id="price_' + i + '" min="0" readonly/></td>' +
                '<td><button type="button" n_val="' + i + '" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td>' +
                '</tr>');
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
            totalPriceCalculate();
        });

        $(document).on('change', '.unit_price', function () {
            var quantity = 0;
            var unitVal = parseInt($(this).val());
            var id = parseInt($(this).attr('n_val'));
            quantity = parseInt($("#quantity_" + id).val());
            calculatePrice(unitVal, id, quantity);
        });

        $(document).on('change', '.quantity', function () {
            var unitVal = 0;
            var quantity = parseInt($(this).val());
            var id = parseInt($(this).attr('n_val'));
            unitVal = parseInt($("#unit_price_" + id).val());
            calculatePrice(unitVal, id, quantity);
        });

        function calculatePrice(unitVal, nid, quantity) {
            console.log(unitVal);
            console.log(quantity);

            if (isNaN(unitVal)) {
                $("#unit_price_" + nid).val(0.0);
                unitVal = 0;
            }
            if (isNaN(quantity)) {
                $("#quantity_" + nid).val(0.0);
                quantity = 0;
            }

            var price = parseInt(unitVal * quantity);
            $("#price_" + nid).val(price);

            totalPriceCalculate();
        }

        function totalPriceCalculate() {
            var totalPrice = 0;

            $('.dynamic-added').each(function () {
                var price = parseInt($(this).find(".price").val());
                console.log(price);
                totalPrice += price;
            });
            $("#total_price").val(totalPrice);
        }

        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "100px",
                                "height": "100px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }

    </script>
@endsection
