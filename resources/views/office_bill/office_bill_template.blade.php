<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" media="all"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(empty($data['file_path']))
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bill_template.css') }}">
    @else
            <?php //'<style>' . file_get_contents($data['file_path']['css']['bootstrap']) . '</style>'; ?>
            <?php //'<style>' . file_get_contents($data['file_path']['css']['style']) . '</style>'; ?>
            <?php '<style>' . file_get_contents("public/css/bootstrap.min.css") . '</style>'; ?>
            <?php '<style>' . file_get_contents("public/css/bill_template.css") . '</style>'; ?>
    @endif

    <style>

        @font-face {
            font-family: "Nikosh";
            src: url({{ base_path('resources/fonts/Nikosh.ttf') }}) format('truetype');
        }

        body {
            font-family: "Nikosh", sans-serif;
        }

    </style>
</head>

<body class="hold-transition sidebar-mini">
<div class="container">
    <div class="wrappe">
        <div class="content">
            <div class="container-fluid">
                <?php //pr($data); ?>
                <?php

                use Rakibhstu\Banglanumber\NumberToBangla;

                $numto = new NumberToBangla();
//                $text = $numto->bnMoney($data['count']);  // $data variable for DOMPDF
                if (!isset($data))
                    $text = $numto->bnMoney($count); // $data array key for MPDF
                else
                    $text = $numto->bnMoney($data['count']);
                echo $text;
                ?>


                <div class="row" id='DivIdToPrint'>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive borderless writeup">
                                    <table class="table head1 mgrless">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="align-middle fw-bold" colspan="2">
                                                <label class="form-control-plaintext code_name" type="text" id="code">টি,
                                                    আর, ফর্ম নং ২১</label>
                                                <label class="form-control-plaintext code_name" type="text"
                                                       id="code_name">( এস, আর ১৮৩ দ্রষ্টব্য )</label>
                                            </td>
                                            <td class="align-middle fw-bold" colspan="2">
                                                <span class="head_title form-control-plaintext" type="text"
                                                      id="code">ক্রয়, সরবরাহ ও সেবা বাবদ ব্যয়ের বিল</span>
                                            </td>
                                            <td class="align-middle fw-bold" colspan="2">
                                                <label class="form-control-plaintext code_name" type="text"
                                                       id="code"><span>বিল নংঃ</span><span
                                                        class="fw-bold">12596438</span></label>
                                                <label class="form-control-plaintext code_name" type="text"
                                                       id="code_name"><span>তারিখঃ- </span><span class="fw-bold">১০/০৯/২০২৩</span></label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive borderless">
                                    <table class="table head2 mgrless">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr class="mgrless">
                                            <td class="align-middle fw-bold mgrless">
                                                <span class="head2_title form-control-plaintext mgrless"
                                                      type="text" id="code">তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর, জেলা কার্যালয়, গোপালগঞ্জ</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive borderless">
                                    <table class="table head3 mgrless">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="30%" class="text-end align-middle info-text">
                                                <span>প্রাতিষ্ঠানিক / অপারেশন কোড * &nbsp;</span>
                                            </td>
                                            <td width="40%" class="table-responsive-md" id="office_code">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr class="">
                                                        <th>-</th>
                                                        <th>1</th>
                                                        <th>1</th>
                                                        <th>6</th>
                                                        <th>6</th>
                                                        <th>2</th>
                                                        <th>2</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </td>
                                            <td width="15%" class="info-text text-center align-middle">
                                                <span>টোকেন নংঃ</span>
                                                <span class="fw-bold"></span>
                                            </td>
                                            <td width="15%" class="info-text text-center align-middle">
                                                <span>তারিখঃ</span>
                                                <span class="fw-bold"></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive borderless">
                                    <table class="table head4 mgrless">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="info-text text-center align-middle text-md-center" >
                                                <span>ভাউচার নংঃ-</span>
                                                <span class="fw-bold">০৮/২২-২৩</span>
                                            </td>
                                            <td width="20%" class="info-text text-center align-middle text-md-center">
                                                <span>তারিখঃ-</span>
                                                <span class="fw-bold">৩০-১০-২০২২</span>
                                            </td>
                                            <td width="30%" class="info-text text-center align-middle text-md-center">
                                                <span>টি,আই, এন, নংঃ-</span>
                                                <span class="fw-bold">১৪৬০০২২৯০৬</span>
                                            </td>
                                            <td width="30%" class="info-text text-center align-middle text-md-center">
                                                <span>বি, আই, এন নংঃ-</span>
                                                <span class="fw-bold">১৪৬০০২২৯০৬</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row" id="table_content">
                            <div class="col">
                                <div class="table-responsive table table-bordered">
                                    <table class="table">
                                        <thead>

                                        </thead>
                                        <tbody>
                                        <tr class="table-active fw-bold fs-8">
                                            <td style="width: 20%;" colspan="2">অর্থনৈতিক কোড</td>
                                            <td style="width: 50%;">বিবরণ</td>
                                            <td style="width: 15%;">টাকার পরিমান</td>
                                            <td style="width: 15%;">পয়সা</td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle fw-bold" colspan="2">
                                                <span id="f3" class="form-control-plaintext code_name" type="text"
                                                      id="code">৪১১২৩০৩</span>
                                                <span id="f4" class="form-control-plaintext code_name" type="text"
                                                      id="code_name">বৈদ্যুতিক সরঞ্জামাদি</span>
                                            </td>
                                            <td>
                                                <span
                                                    class="d-flex fw-bold justify-content-center align-items-center description"
                                                    id="desc">
                                                    তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর, জেলা কার্যালয়, গোপালগঞ্জ এর&nbsp;বৈদ্যুতিক সরঞ্জামাদি বাবদ ব্যয় বিল
                                                </span>
                                                <span
                                                    class="d-flex fw-bold justify-content-center align-items-center description"
                                                    id="desc2">
                                                    তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর,ঢাকা’র ২৬ ডিসেম্বর, ২০১৮ তারিখের ৫৬.০৪.০০০০.০১০.২০.০০১.১৭.৫৬০ নং স্মারকের বরাদ্দপত্র
                                                </span>
                                            </td>
                                            <td class="align-middle fw-bold">
                                                <label id="f5" class="form-control-plaintext code_name fw-bold"
                                                       type="text"
                                                       id="amount"
                                                       value=""
                                                       readonly="">৪৫০০</label>
                                            </td>
                                            <td class="align-middle fw-bold">
                                                <label id="f6" class="form-control-plaintext code_name" type="text"
                                                       id="float_amount">০০</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-end fw-bold" colspan="3">মোট টাকা (ক) :</td>
                                            <td class="fw-bold"><span>৪১১২৩০৩</span></td>
                                            <td class="fw-bold"><span>০০</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-start" colspan="5">কর্তন ও পরিশোধ</td>
                                        </tr>
                                        <tr>
                                            <td><input id="f7" type="checkbox" checked=""></td>
                                            <td>১১১১১০১</td>
                                            <td class="text-start">ব্যক্তি কর্তিক দেয় আয়কর (&nbsp;&nbsp;<span
                                                    class="fw-bold">৭.৫ %</span>&nbsp;
                                                )
                                            </td>
                                            <td class="fw-bold">৮৫০</td>
                                            <td class="fw-bold">০০</td>
                                        </tr>
                                        <tr>
                                            <td><input id="f8" type="checkbox" checked=""></td>
                                            <td>১১৪১১০১</td>
                                            <td class="text-start">দেশজ পন্য ও সেবার উপর মুসক (&nbsp;&nbsp;<span
                                                    class="fw-bold">৩.০%</span>&nbsp;
                                                )
                                            </td>
                                            <td class="fw-bold">১৫০</td>
                                            <td class="fw-bold">০০</td>
                                        </tr>
                                        <tr>
                                            <td><input id="f9" type="checkbox" checked=""></td>
                                            <td>৮১১৩৩০১</td>
                                            <td class="text-start">ঠিকাদারের নিরাপত্তা জমা</td>
                                            <td class="fw-bold">০০</td>
                                            <td class="fw-bold">০০</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-end" colspan="3">কর্তন ও পরিশোধ বাবদ মোট আদায় (খ)&nbsp;ঃ
                                            </td>
                                            <td class="fw-bold">১০০০</td>
                                            <td class="fw-bold">০০</td>
                                        </tr>
                                        <tr>
                                            <td><input id="f10" type="checkbox"></td>
                                            <td>৮১৭২১০৮</td>
                                            <td class="text-start">প্রদেয় বিল (ক - খ)</td>
                                            <td class="fw-bold">৩৫০০</td>
                                            <td class="fw-bold">০০</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold" colspan="2">টাকা ( কথায় )&nbsp;ঃ</td>
                                            <td class="fw-bold table-active" colspan="3">তিন হাজার পাঁচশত টাকা মাত্র।
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row" id="oth">
                            <div class="col">
                                <div class="table-responsive borderless writeup pg1">
                                    <table class="table">
                                        <thead>
                                        <tr></tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5%;">১।</td>
                                            <td style="width: 95%;">প্রত্যায়ন করা যাইতেছে যে, জনস্বার্থে এই ব্যয়
                                                অপরিহার্য ছিল। আমি আরও প্রত্যায়ন করিতেছি যে, আমার জ্ঞান ও বিশ্বাস মতে,
                                                এই বিলে উল্লেখিত প্রদত্ত অর্থ নিম্নবর্ণিত ক্ষেত্র ব্যতীত প্রকৃত প্রাপককে
                                                প্রদান করা হইয়াছে, তবে স্থায়ী অগ্রিমের টাকা অপেক্ষা দায় বেশী হওয়ায়,
                                                অবশিষ্ট পাওনা এই বিলে দাবিকৃত টাকা প্রাপ্তির পর প্রদান করা হবে। আমি
                                                যথাসম্ভব সকল ভাউচার গ্রহণ করেছি এবং সেগুলো এমনভাবে বাতিল করা হয়েছে যেন
                                                পুনরায় ব্যবহার করা না যায়। ২৫ টাকার ঊর্ধ্বের সকল ভাউচারসমূহ এমনভাবে
                                                সংরক্ষণ করা হয়েছে, যেন প্রয়োজনমত ৩ বৎসরের মধ্যে এইগুলো পেশ করা যায়। সকল
                                                পূর্ত কর্মের বিল এর সঙ্গে সংযুক্ত করা হলো।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;">২।</td>
                                            <td style="width: 95%;">প্রত্যায়ন করা যাচ্ছে যে, যে সকল দ্রব্যের জন্য স্টোর
                                                একাউন্টস
                                                সংরক্ষণ
                                                করা
                                                হয় সে সব দ্রব্যাদি স্টক রেজিস্টারে অন্তর্ভুক্ত করা হয়েছে।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;">৩।</td>
                                            <td style="width: 95%;">প্রত্যায়ন করা যাচ্ছে যে, যে সব দ্রব্যাদি ক্রয়ের বিল
                                                পেশ
                                                করা
                                                হয়েছে,
                                                সে সব
                                                দ্রব্যের পরিমাণ সঠিক গুণগতমান ভাল, যে দরে ক্রয় করা হয়েছে, তাহা বাজার
                                                দরের
                                                অধিক
                                                নয়,
                                                এবং
                                                কার্যাদেশ বা চালান (ইনভয়েস)- এর যথাস্থানে লিপিবদ্ধ করা হয়েছে, যাতে একই
                                                দ্রব্যের
                                                জন্য
                                                দ্বিতীয়
                                                বার (ডুপ্লিকেট) অর্থ প্রদান এড়ানো যায়।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;"></td>
                                            <td style="width: 95%;">
                                                <span class="next_page"> [ অপর পৃষ্ঠা দ্রষ্টব্য ]</span>
                                                <hr id="seperator"/>
                                                <span
                                                    class="clearfix conds d-md-flex justify-content-start align-items-start align-content-start">* প্রতিষ্ঠানের ক্ষেত্রে ০৬ ডিজিট এবং বিশেষ কার্যক্রম/ প্রোজেক্ট/ স্কিম - এর ক্ষেত্রে সাত (০৭) ডিজিটের কোড ব্যবহার করতে হবে।</span>
                                                <hr style="margin-top: 0;margin-bottom: 0;border-style: none;">
                                                <span
                                                    class="conds d-md-flex justify-content-start align-items-start align-content-start">বিঃ দ্রঃ- ক্রয়, সরবরাহ ও সেবা বাবদ ব্যয়ের বিল দাখিলের ক্ষেত্রে একটি নির্দিষ্ট অর্থনৈতিক কোডে দাবি করতে হবে।</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                {{--                                <div class="page-break"></div>--}}
                                <P style="page-break-before: always">
                                <div class="table-responsive borderless writeup pg2">
                                    <table class="table">
                                        <thead>
                                        <tr></tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5%;">৪।</td>
                                            <td style="width: 95%;">প্রত্যয়ন করা যাচ্ছে যে---<br>&nbsp;&nbsp;
                                                (ক) এই বিলে দাবিকৃত প্রকৃত পরিবহন ভাড়া দেওয়া হয়েছে এবং এটি অপরিহার্য ছিল
                                                এবং
                                                ভাড়ার
                                                হার
                                                প্রচলিত যানবাহন ভাড়ার হারের মধ্যেই; এবং<br>&nbsp;&nbsp;
                                                (খ) সংশ্লিষ্ট সরকারী কর্মচারী সাধারণ বিধিবলে এই ভ্রমণের জন্য ব্যয়
                                                প্রাপ্য হন
                                                না,
                                                এবং
                                                এর
                                                অতিরিক্ত কোন বিশেষ পারিশ্রমিক, এই দায়িত্ব পালনের জন্য প্রাপ্য হইবেন না।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;">৫।</td>
                                            <td style="width: 95%;">প্রত্যয়ন করা যাচ্ছে যে, যে সকল অধস্তন কর্মচারীর বেতন
                                                এই
                                                বিলে
                                                দাবী
                                                করা
                                                হয়েছে তারা ঐ সময়ে প্রকৃতই সরকারী কাজে নিয়োজিত ছিলেন (এস, আর, ১৭১)।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;">৬।</td>
                                            <td style="width: 95%;">প্রত্যয়ন করা যাচ্ছে যে----<br>
                                                (ক) মনিহারি দ্রব্য বা স্ট্যাম্প বাবত ২০ টাকার অধিক কোন ক্রয় স্থানীয়ভাবে
                                                করা
                                                হয়নি।<br>&nbsp;&nbsp;
                                                (খ) ব্যক্তিগত কাজে ব্যবহৃত তাঁবু বহনের কোন ব্যয় এই বিলে অন্তর্ভুক্ত করা
                                                হয়নি।<br>&nbsp;&nbsp;
                                                (গ) আবাসিক ভবনে ব্যবহৃত কোন বিদ্যুৎ বাবদ ব্যয় এই বিলে অন্তর্ভুক্ত করা
                                                হয়নি।<br>
                                                (ঘ) এই বৎসরে প্রসেস প্রদত্ত পারিতোষিক টাকা .................... (যা গত ৩
                                                বৎসরের
                                                জরিমানা
                                                বাবদ
                                                প্রাপ্তি গড় টাকার সামান্য অধিক হবে না)।
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;">৭।</td>
                                            <td style="width: 95%;">যাহার নামে চেক ইস্যু করা হবে (প্রযোজ্য ক্ষেত্রে):&nbsp;
                                                ...............................................................
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive borderless pg2" id="ddoTable">
                                    <table class="table">
                                        <thead>
                                        <tr></tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>*নিয়ন্ত্রণকারী/প্রতিস্বাক্ষরকারী কর্মকর্তার স্বাক্ষর</td>
                                            <td>বুঝে পেয়েছি..........................................</td>
                                        </tr>
                                        <tr>
                                            <td>.......................................................</td>
                                            <td>আয়ন কর্মকর্তার স্বাক্ষর................................</td>
                                        </tr>
                                        <tr>
                                            <td>নাম..................................................</td>
                                            <td>নাম..................................................</td>
                                        </tr>
                                        <tr>
                                            <td>পদবি...............................................&nbsp;&nbsp;</td>
                                            <td>পদবি...............................................&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr class="seal">
                                            <td>সিল</td>
                                            <td>সিল</td>
                                        </tr>
                                        <tr>
                                            <td>স্থান..................................................</td>
                                            <td>স্থান..................................................</td>
                                        </tr>
                                        <tr>
                                            <td>তারিখ...............................................&nbsp;&nbsp;</td>
                                            <td>তারিখ...............................................&nbsp;&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive borderless pg2" id="ddoCal">
                                    <table class="table">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="" width="50%">
                                                <table class="table table-bordered ext_amount">
                                                    <tbody>
                                                    <tr class="table-active fw-bold fs-7 text-center">
                                                        <td style="width: 70%;">বরাদ্দের হিসাব</td>
                                                        <td style="width: 20%;">টাকা</td>
                                                        <td style="width: 10%;">পয়সা</td>
                                                    </tr>
                                                    <tr>
                                                        <td>১। শেষ বিলের টাকার অংক</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>২। এ যাবত অতিরিক্ত বরাদ্দ<br>(পত্র নং………….)</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>৩। এ যাবত যে অংকের বরাদ্দ কমানো হইয়াছে। (পত্র
                                                            নং.......................)
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>৪। নীট মোট</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="" width="50%">
                                                <table class="table table-bordered ext_amount">
                                                    <tbody>
                                                    <tr class="table-active fw-bold fs-7 text-center">
                                                        <td style="width: 60%;">ব্যয়ের হিসাব</td>
                                                        <td style="width: 30%;">টাকা</td>
                                                        <td style="width: 10%;">পয়সা</td>
                                                    </tr>
                                                    <tr>
                                                        <td>গত বিলের মোট জের (+)</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>এই বিলের মোট (+)</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>সংযুক্ত--পূর্তকর্মের বিলের টাকা</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>মোট (পরবর্তী বিলে জের টেনে নেওয়া হবে)</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <hr id="seperator">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" id="ag_office">
                            <div>
                                <span class="head3_title form-control-plaintext mgrless">হিসাব রক্ষণ অফিসে ব্যবহারের জন্য</span>
                            </div>
                            <span>
                                <p class="amountTk">প্রদানের জন্য পাস করা হল, টাকা&nbsp; ..................................................&nbsp;
                                    &nbsp;কথায়&nbsp;..................................................................................................
                            </span>
                            <div id="all_sign">
                                <div class="table-responsive borderless" id="sign_table">
                                    <table class="table">
                                        <thead>
                                        <tr></tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><span>____________________</span>
                                                <p>অডিটর (স্বাক্ষর)</p>
                                            </td>
                                            <td><span>____________________</span>
                                                <p>সুপার (স্বাক্ষর)</p>
                                            </td>
                                            <td><span>________________________________</span>
                                                <p>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>নামঃ .....................................................&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>নামঃ .....................................................&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>নামঃ .....................................................&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><br>তারিখঃ&nbsp;...................................................</td>
                                            <td>তারিখঃ ...................................................</td>
                                            <td>তারিখঃ ...................................................</td>
                                        </tr>
                                        <tr class="seal">
                                            <td>সিল&nbsp;&nbsp;</td>
                                            <td>সিল&nbsp;&nbsp;</td>
                                            <td>সিল&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>চেক নংঃ ...................................................</td>
                                            <td>তারিখঃ&nbsp;...................................................</td>
                                        </tr>
                                        <tr id="blank_row">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td id="check_sign"><span>____________________</span>
                                                <p>চেক প্রদানকারীর স্বাক্ষর</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td>নামঃ ...................................................</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td>তারিখঃ&nbsp;...................................................</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><span>____________________________</span>
                                                <p>*কেবল মাত্র প্রযোজ্য ক্ষেত্রে</p>
                                            </td>
                                            <td>সিল</td>
                                        </tr>
                                        <tr>
                                            <td class="nb_text nb_last_text" colspan="3">
                                                <span>বিঃদ্রঃ--- এটি স্পষ্টভাবে স্মরণ রাখতে হবে যে, বরাদ্দের অতিরিক্ত ব্যয়ের জন্য আয়ন কর্মকর্তা ব্যক্তিগতভাবে দায়ী থাকবেন। বরাদ্দের অতিরিক্ত ব্যয়ের বিপরীতে যদি তিনি অতিরিক্ত বরাদ্দ মঞ্জুর করতে না পারেন, তবে অতিরিক্ত ব্যয়িত অর্থ তাঁর ব্যক্তিগত ভাতাদি হতে আদায় করা হবে।</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-print-none mt-4 ">
                    <div class="float-end hide_div">
                        {{--                                                        <a class="btn btn-primary" href="{{ URL::to('#') }}">Export to PDF</a>--}}
                        <a class="btn btn-success me-1" href="{{ URL::to('/office-budget/pdf') }}"><i
                                class="fa fa-file-pdf"></i>PDF</a>
                        <input id="f11" type='button' class="btn btn-info w-md print_btn" id='btn' value='Print'>
                        {{--            <a href="#" class="btn btn-primary w-md">Print</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
            integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"
            integrity="sha512-d5Jr3NflEZmFDdFHZtxeJtBzk0eB+kkRXWFQqEc1EKmolXjHm2IKCA7kTvXBNjIYzjXfD5XzIjaaErpkZHCkBg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.print_btn').on('click', function (e) {
                $(".hide_div").hide();
                $("#DivIdToPrint").printThis();
                $(".hide_div").show();
            });
        });

    </script>
</div>
</body>
</html>
