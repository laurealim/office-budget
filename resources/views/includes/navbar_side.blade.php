<!-- Brand Logo -->
<a href="#" class="brand-link">
    {{--    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">--}}
    <span class="brand-text font-weight-light">জমির বন্দবস্ত</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    {{--    <div class="user-panel mt-3 pb-3 mb-3 d-flex">--}}
    {{--        <div class="image">--}}
    {{--            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">--}}
    {{--        </div>--}}
    {{--        <div class="info">--}}
    {{--            <a href="#" class="d-block">Alexander Pierce</a>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
            data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{ route('dashboard') }}" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>ড্যাসবোর্ড
                    </p>
                </a>
            </li>
            @can('see_all')
                <li class="nav-item">
                    <a href="{{ route('lands.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>জমির তথ্য
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('applicants.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>বন্দবস্ত গ্রহণকারীর তথ্য
                        </p>
                    </a>
                </li>
            @endcan
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tools"></i>
                    <p>
                        সাধারন সেটিংস
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('institutes.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>প্রতিষ্ঠান</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('budget-type.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেটের ধরন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('budget-sub-type.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেটের উপ-ধরন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('budget-code.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেট কোড</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('items.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>আইটেম</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-hand-holding-usd"></i>
                    <p>
                        কেন্দ্রিও বাজেট ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('office-budget.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেট সংযোজন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('office-budget-expense.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেট বিয়োজন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        {{--                        <a href="{{ route('office-budget.budgetApproveReject') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>বাজেট অনুমোদন</p>--}}
                        {{--                        </a>--}}
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{--                    <i class="nav-icon fas fa-hand-holding-usd"></i>--}}
                    <i class="nav-icon fas fa-solid fa-receipt"></i>
                    <p>
                        ভাউচার ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('create-office-voucher')
                        <li class="nav-item">
                            <a href="{{ route('office-voucher.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>অফিস ভাউচার</p>
                            </a>
                        </li>
                    @endcan
                    @can('create-internal-voucher')
                        <li class="nav-item">
                            <a href="{{ route('internal-voucher.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>অভ্যন্তরিন ভাউচার</p>
                            </a>
                        </li>
                    @endcan
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{ route('office-budget-expense.index') }}" class="nav-link active">--}}
                    {{--                            <i class="far fa-circle nav-icon"></i>--}}
                    {{--                            <p>বাজেট বিয়োজন</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    <li class="nav-item">
                        {{--                        <a href="{{ route('office-budget.budgetApproveReject') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>বাজেট অনুমোদন</p>--}}
                        {{--                        </a>--}}
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file-invoice-dollar"></i>
                    <p>
                        আভ্যন্তরীণ বাজেট ব্যবস্থাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('internal-budget.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেট সংযোজন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('internal-budget-expense.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বাজেট বিয়োজন</p>
                        </a>
                    </li>
                </ul>
            </li>
            @can('see-all')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            জমির বন্দোবস্ত
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('assign_lands.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>বন্দোবস্তের ব্যবস্তা</p>
                            </a>
                        </li>
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="#" class="nav-link">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>Inactive Page</p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                    </ul>
                </li>
            @endcan

            <!--            --><?php
                                   //            if (check_view_role('super-admin')){
                                   //            ?>
            @role(array('super-admin','admin'))
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        অনুমতি ব্যবস্তাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('create-role')
                        <li class="nav-item">
                            <a href="{{ route('roles.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>ইউজার রোল</p>
                            </a>
                        </li>
                    @endcan
                    @can('create-permission')
                        <li class="nav-item">
                            <a href="{{ route('permissions.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>ইউজার পারমিশন</p>
                            </a>
                        </li>
                    @endcan
                    @can('role-setting')
                        <li class="nav-item">
                            <a href="{{ route('roles.roleSetting') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>রোল সেটিং</p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        ইউজার ব্যবস্তাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('user-list')
                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>সকল ইউজার</p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endrole
            @can('see-all')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            ফ্ল্যাট ব্যবস্তাপনা
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('flat-list')
                            <li class="nav-item">
                                <a href="{{ route('flats.index') }}" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ফ্ল্যাট লিস্ট</p>
                                </a>
                            </li>
                        @endcan
                        @can('assign-tenant')
                            <li class="nav-item">
                                <a href="{{ route('flats.assignTenant') }}" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ভাড়াটিয়া এসাইন</p>
                                </a>
                            </li>
                        @endcan
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="{{ route('roles.roleSetting') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>রোল সেটিং</p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            ভাড়াটিয়া ব্যবস্তাপনা
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('tenant-list')
                            <li class="nav-item">
                                <a href="{{ route('tenants.index') }}" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ভাড়াটিয়ার লিস্ট</p>
                                </a>
                            </li>
                        @endcan
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="{{ route('permissions.index') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>ফ্ল্যাট এসাইন </p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="{{ route('roles.roleSetting') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>রোল সেটিং</p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            বাড়ি ভাড়া ব্যবস্তাপনা
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('bill-list')
                            <li class="nav-item">
                                <a href="{{ route('bills.index') }}" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ইনভয়েস</p>
                                </a>
                            </li>
                        @endcan
                        @can('payment-list')
                            <li class="nav-item">
                                <a href="{{ route('payments.index') }}" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>পেমেন্ট</p>
                                </a>
                            </li>
                        @endcan
                        {{--                    @can('create-task')--}}
                        <li class="nav-item">
                            <a href="{{ route('tenants.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>লেজার</p>
                            </a>
                        </li>
                        {{--                    @endcan--}}
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="{{ route('permissions.index') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>ফ্ল্যাট এসাইন </p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                        {{--                    <li class="nav-item">--}}
                        {{--                        <a href="{{ route('roles.roleSetting') }}" class="nav-link active">--}}
                        {{--                            <i class="far fa-circle nav-icon"></i>--}}
                        {{--                            <p>রোল সেটিং</p>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                    </ul>
                </li>
            @endcan
            <!--            --><?php //} ?>
            {{--            <li class="nav-item">--}}
            {{--                <a href="#" class="nav-link">--}}
            {{--                    <i class="nav-icon fas fa-th"></i>--}}
            {{--                    <p>--}}
            {{--                        Simple Link--}}
            {{--                        <span class="right badge badge-danger">New</span>--}}
            {{--                    </p>--}}
            {{--                </a>--}}
            {{--            </li>--}}
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
