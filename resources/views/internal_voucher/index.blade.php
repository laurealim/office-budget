@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @can('create-internal-voucher')
                        <h3 class="card-title">
                            <div class="pad_space">
                                <a href="{{ route('internal-voucher.create') }}"
                                   class="btn btn-block btn-secondary btn-md"
                                   title="Add">
                                    <i class="far fa-plus-square fa-lg"></i>
                                </a>
                            </div>
                        </h3>
                    @endcan
                    @can('export-flat-list')
                        {{--                        <h3 class="card-title">--}}
                        {{--                            <div class="pad_space">--}}
                        {{--                                <a href="{{ route('institutes.export') }}" class="btn btn-block btn-success btn-md"--}}
                        {{--                                   id="export_list" title="Download List">--}}
                        {{--                                    <i class="fas fa-file-download fa-lg"></i>--}}
                        {{--                                </a>--}}
                        {{--                            </div>--}}
                        {{--                        </h3>--}}
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered data-table">
                        {{--                    <table id="example1" class="table table-bordered table-striped">--}}
                        <thead>
                        <tr>
                            <th>ক্রমিক নং</th>
                            {{--                            <th> প্রতিষ্ঠানের নাম </th>--}}
                            <th> অর্থ বছর</th>
                            <th> খরচের খাতের নাম</th>
                            <th> মোট খরচ</th>
                            <th> প্রতিষ্ঠানের নাম</th>
                            <th> তারিখ </th>
                            <th> স্ট্যাটাস</th>
                            <th> ব্যবস্থা</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript">
        // $(function () {
        //     $(".data-table").DataTable({
        //         "responsive": true, "lengthChange": false, "autoWidth": false,
        //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        // });
        $(document).ready(function () {
            // $(".data-table").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


            var table = $('.data-table').DataTable({
                lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                ajax: "{{ route('internal-voucher.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: "5%"},
                    {data: 'year', name: 'year', width: "10%"},
                    {data: 'voucher_items', name: 'voucher_items', width: "25%"},
                    {
                        data: 'total_cost', name: 'total_cost', width: "7%", render: $.fn.dataTable.render.number
                        (',', '.', 2, '')
                    },
                    {data: 'institute_id', name: 'institute_id', width: "23%"},
                    {data: 'v_date', name: 'v_date', width: "10%"},
                    {data: 'status', name: 'status', width: "5%"},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width: "15%"},
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $(document).on("click", "a.delete-budget-voucher", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                const { value: accept } = Swal.fire({
                    text: 'তথ্য মুছে ফেলতে চাচ্ছেন ?',
                    icon: 'info',
                    // text: 'Something went wrong!',
                    focusConfirm: false,
                    showCancelButton: true,
                    confirmButtonText: 'হ্যাঁ',
                    confirmButtonAriaLabel: 'Thumbs up, great!',
                    cancelButtonText: 'না',
                    cancelButtonAriaLabel: 'Thumbs down',
                    confirmButtonColor: '#ff2424',
                    cancelButtonColor: 'rgb(33,79,171)',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: 'DELETE',
                            url: url,
                            dataType: 'json',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {id: id, "_token": "{{ csrf_token() }}"},

                            success: function (data) {
                                if (data.status == 'success') {
                                    Swal.fire({
                                        text: 'তথ্য সফলভাবে মুছে ফেলা হয়েছে...',
                                        icon: 'success',
                                        confirmButtonText: 'ফিরে যান',
                                    }).then((result) => {
                                        window.location.reload();
                                    });
                                } else if (data.status == 'error') {
                                    Swal.fire({
                                        text: 'তথ্য মুছা সম্ভব হয় নি...',
                                        icon: 'error',
                                        confirmButtonText: 'ফিরে যান',
                                    })
                                    console.log('error')
                                }
                            },
                            error: function (data) {
                                Swal.fire({
                                    text: data,
                                    icon: 'error',
                                    confirmButtonText: 'ফিরে যান',
                                })
                                console.log(data)
                            }
                        });
                    }
                });


{{--                if (confirm("Do you wat to delete?")) {--}}
{{--                    $.ajax({--}}
{{--                        type: 'DELETE',--}}
{{--                        url: url,--}}
{{--                        dataType: 'json',--}}
{{--                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
{{--                        data: {id: id, "_token": "{{ csrf_token() }}"},--}}

{{--                        success: function (data) {--}}
{{--                            if (data.status == 'success') {--}}
{{--                                var flashMessage = '{{ session('success') }}';--}}
{{--                                if (flashMessage) {--}}
{{--                                    // Display the message in your HTML--}}
{{--                                    $('#message').html(flashMessage).fadeIn();--}}
{{--                                }--}}
{{--                                window.location.reload();--}}
{{--                            } else if (data.status == 'error') {--}}
{{--                                console.log('error')--}}
{{--                            }--}}
{{--//                    data.request->session()->flash('status', 'Task was successful!');--}}
{{--//                    setInterval(function() {--}}
{{--//                    }, 5900);--}}
{{--                        },--}}
{{--                        error: function (data) {--}}
{{--                            console.log(data)--}}
{{--                        }--}}
{{--                    });--}}

{{--                } else {--}}
{{--                    return false;--}}
{{--                }--}}
            });

            $(document).on("click", "a.bgt_voucher_approve", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                let value = $(this).attr("value");
                var statusName = 'অনুমোদন';
                if(value == 2){
                    statusName = 'বাতিল';
                }
                const { value: accept } = Swal.fire({
                    text: 'আপনি কি '+statusName+' করতে চান ?',
                    icon: 'info',
                    // text: 'Something went wrong!',
                    focusConfirm: false,
                    showCancelButton: true,
                    confirmButtonText: 'হ্যাঁ',
                    confirmButtonAriaLabel: 'Thumbs up, great!',
                    cancelButtonText: 'না',
                    cancelButtonAriaLabel: 'Thumbs down',
                    confirmButtonColor: '#ff2424',
                    cancelButtonColor: 'rgb(33,79,171)',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {id: id, value: value, "_token": "{{ csrf_token() }}"},

                            success: function (data) {
                                if (data.status == 'success') {
                                    Swal.fire({
                                        text: 'তথ্য সফলভাবে '+statusName+' করা হয়েছে...',
                                        icon: 'success',
                                        confirmButtonText: 'ফিরে যান',
                                    }).then((result) => {
                                        window.location.reload();
                                    });
                                } else if (data.status == 'error') {
                                    Swal.fire({
                                        text: 'তথ্য অনুমোদন করা সম্ভব হয় নি...',
                                        icon: 'error',
                                        confirmButtonText: 'ফিরে যান',
                                    })
                                    console.log('error')
                                }
                            },
                            error: function (data) {
                                Swal.fire({
                                    text: data,
                                    icon: 'error',
                                    confirmButtonText: 'ফিরে যান',
                                })
                                console.log(data)
                            }
                        });
                    }
                });
{{--                if (confirm("আপনি কি অনুমোদন প্রদান করতে চান ?")) {--}}
{{--                    $.ajax({--}}
{{--                        type: 'POST',--}}
{{--                        url: url,--}}
{{--                        dataType: 'json',--}}
{{--                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
{{--                        data: {id: id, value: value, "_token": "{{ csrf_token() }}"},--}}

{{--                        success: function (data) {--}}
{{--                            if (data.status == 'success') {--}}
{{--                                window.location.reload();--}}
{{--                            } else if (data.status == 'error') {--}}
{{--                            }--}}
{{--//                    data.request->session()->flash('status', 'Task was successful!');--}}
{{--//                    setInterval(function() {--}}
{{--//                    }, 5900);--}}
{{--                        },--}}
{{--                        error: function (data) {--}}
{{--                        }--}}
{{--                    });--}}

{{--                } else {--}}
{{--                    return false;--}}
{{--                }--}}
            });
        });
    </script>
@endsection
