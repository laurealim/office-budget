@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('internal-voucher.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Office Budget --}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"> সাধারন তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> প্রতিষ্ঠান <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="institute_id" name="institute_id" required>
                                <option value="">--- বাছাই করুণ ---</option>

                                @foreach ($budgetInstituteList as $id => $val)
                                    <option
                                        value="{{ $id }}" {{ $id == $internalVoucherData->institute_id ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                @endforeach
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('institute_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('institute_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> তারিখঃ <span class="reqrd"> *</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" class="form-control" name="v_date" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask="" inputmode="numeric" value="{{ $internalVoucherData->v_date }}" >
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আর্থিক বছর <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="year" name="year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                @foreach ($financialList as $year => $val)
                                    <option
                                        value="{{ $year }}" {{ $year == $internalVoucherData->year ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                @endforeach
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('year'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('year') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{-- Office voucher Details--}}
        <div class="card card-info bgt_dels">
            <div class="card-header">
                <h3 class="card-title"> আর্থিক তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="card-body table-responsive p-0">
                        <?php
                        $itemArrData = json_decode($internalVoucherData->voucher_items, true);
//                            pr($itemArrData);
                        ?>
                        <table class="table table-hover text-nowrap table-bordered" id="dynamic-table">
                            <thead class="table-active">
                            <tr class="active">
                                <th>পণ্য</th>
                                <th>পরিমাণ</th>
                                <th>একক</th>
                                <th>মূল্য</th>
                                <th>মোট মূল্য</th>
                                <th>ব্যাবস্থা</th>
                            </tr>
                            </thead>
                            <tbody id="budgetAjaxData">

                            @if(!empty($itemArrData))
                                @foreach($itemArrData as $no=>$items)
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" name="voucher[items][]"
                                                   value="{{ $items['items'] }}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="voucher[amount][]"
                                                   value="{{ $items['amount'] }}" data-calculate="true">
                                        </td>
                                        <td>
                                            <select class="form-control select2 required" style="width: 100%" name="voucher[unit][]"
                                                    required>
                                                    <?php
                                                    $unitListArr = config('constants.unit_value.arr');
                                                    ?>
                                                <option value="">--- বাছাই করুণ ---</option>
                                                @foreach ($unitListArr as $id => $val)
                                                    <option
                                                        value="{{ $id }}" {{ $id == $items['unit'] ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="voucher[price][]"
                                                   value="{{ $items['price'] }}" data-calculate="true">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control"
                                                   value="{{ $items['total_price'] }}" name="voucher[total_price][]">
                                        </td>
                                        <td>
                                            <button class="btn btn-danger remove-row">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr class="fw-bold text-bold">
                                <td colspan="4" class="text-right">সর্বমোট খরচ:</td>
                                <td colspan="2" class="text-center"><input class="form-control" type="number" name="total_cost" id="total_cost" value="{{ $internalVoucherData->total_cost }}" readonly/></td>
                            </tr>
                            </tfoot>
                        </table>
                        <a href="#" id="add_row" class="btn btn-info"><i class="fas fa-plus-square"></i></a>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

        </div>


        {{--        Flat Images--}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট ফাইল সংযুক্তি</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">বাজেট ফাইল নির্বাচন করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="file_name" class="col-xs-10 col-sm-5" name="file_name"/>
                                <?php
                                if (!empty($internalVoucherData->file_name)) {
                                    $pathFromEnv = env('BUDGET_FILE_LOCATION');
                                    $path = asset($pathFromEnv . "budget/office_budget_expense/" . $internalVoucherData->id . "/" .
                                        $internalVoucherData->file_name);

//                                    $path = asset("images/budget/office_budget_expense/" . $officeBudgetExpenseData->id . "/" . $officeBudgetExpenseData->file_name);
                                    $icon = '<a target="_blank" title="Edit" class="btn btn-success" id="edit-office-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i> ' . $internalVoucherData->file_name . ' </a>';
                                    echo $icon;
                                }
                                ?>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('file_name'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('file_name') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>


        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button id="btn_sub" type="submit" class="btn btn-info">সংরক্ষন</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    var itemList;
    var unitList;

    <script type="text/javascript">
        var itemList;
        var unitList;
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

            // $('.bgt_dels').hide();

            $('#institute_id').on('change', function (e) {
            });

            $('#year').on('change', function (e) {
            });

            // initFunction();
            getUnitList();
        });

        // Add Row
        $("#add_row").on("click", function (e) {
            // alert("asdasd");
            e.preventDefault();

            var year = $("#year").val();
            var institute_id = $("#institute_id").val();

            if (year === undefined || year === null || year === "") {
                Swal.fire({
                    text: 'অর্থ বছর বাছাই করতে হবে...',
                    icon: 'info',
                })
                return false;
            }
            if (institute_id === undefined || institute_id === null || institute_id === "") {
                Swal.fire({
                    text: 'প্রতিষ্ঠান বাছাই করতে হবে...',
                    icon: 'info',
                })
                return false;
            }

            var unitListSelect = '<select class="form-control select2 required" style="width: 100%;" name="voucher[unit][]">';
            unitListSelect += '<option value="">' + "--- বাছাই করুণ ---" + '</option>';
            $.each(unitList, function (key, value) {
                unitListSelect += '<option value="' + key + '">' + value + '</option>';
            });
            unitListSelect += '</select>';

            var newRow = '<tr>' +
                '<td><input type="text" class="form-control" name="voucher[items][]"></td>' +
                '<td><input type="number" class="form-control" name="voucher[amount][]" data-calculate="true"></td>' +
                '<td>' + unitListSelect + '</td>' +
                '<td><input type="number" class="form-control" name="voucher[price][]" data-calculate="true"></td>' +
                '<td><input type="number" class="form-control" name="voucher[total_price][]"></td>' +
                '<td><button class="btn btn-danger remove-row">Remove</button></td>' +
                '</tr>';
            $("#dynamic-table tbody").append(newRow);
            $('#dynamic-table tbody .select2').select2();
            $('.required').prop('required', true);
        });

        // Remove Row
        $("#dynamic-table").on("click", ".remove-row", function () {
            $(this).closest("tr").remove();
            calculateTotalSum();
        });

        // Calculate total_price (amount x price)
        $("#dynamic-table").on("input", "[data-calculate='true']", function () {
            var row = $(this).closest("tr");
            var amount = parseFloat(row.find("input[name='voucher[amount][]']").val()) || 0;
            var price = parseFloat(row.find("input[name='voucher[price][]']").val()) || 0;
            var result = amount * price;
            row.find("input[name='voucher[total_price][]']").val(result);

            calculateTotalSum();
        });

        function initFunction(){
            var year = $('#year').val();
            var institute_id = $("#institute_id").val();
            if (year === undefined || year === null || year === "") {
                Swal.fire({
                    text: 'অর্থ বছর বাছাই করতে হবে...',
                    icon: 'info',
                })
                return false;
            }
            if (institute_id === undefined || institute_id === null || institute_id === "") {
                Swal.fire({
                    text: 'প্রতিষ্ঠান বাছাই করতে হবে...',
                    icon: 'info',
                })
                return false;
            }
            getUnitList();
        }

        function calculateTotalSum() {
            var totalSum = 0;
            var rows = document.querySelectorAll('tbody tr');
            rows.forEach(function (row) {
                var total_price = parseFloat(row.querySelector('[name="voucher[total_price][]"]').value) || 0;
                totalSum += total_price;
            });
            // document.getElementById('total_cost').textContent = totalSum;
            document.getElementById('total_cost').value = totalSum;
        }

        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "100px",
                                "height": "100px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }



        function getUnitList() {
            var token = $("input[name='_token']").val();
            var url = "{{ route('ajax-helper.getUnitList') }}";
            $.ajax({
                url: url,
                method: 'POST',
                dataType: "json",
                data: {_token: token},
                success: function (data) {
                    console.log(data)
                    if (data.status == 200) {
                        unitList = data.unitList;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }

        function getItemList() {
            var token = $("input[name='_token']").val();
            var url = "{{ route('ajax-helper.getItemList') }}";
            $.ajax({
                url: url,
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (data.status == 200) {
                        itemList = data.itemList;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }

    </script>
@endsection
