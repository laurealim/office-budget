{{--<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">--}}
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="5%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                </label>
            </th>
            <th width="14%">Union Name</th>
            <th width="15%">Union Name (Bangla)</th>
            <th width="14%">Upazila Name</th>
            <th width="14%">District Name</th>
            <th width="14%">Division Name</th>
            <th width="14%">URL</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($unionLists->currentPage() - 1) * $unionLists->perPage()) + 1 ?>

        @foreach ($unionLists as $unionList)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $unionList->name}}
                </td>
                <td>
                    {{ $unionList->bn_name}}
                </td>
                <td>
                    {{ $unionList->upazila_name}}
                </td>
                <td>
                    {{ $unionList->district_name}}
                </td>
                <td>
                    {{ $unionList->division_name }}
                </td>
                <td>
                    {{ $unionList->url }}
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('union.adminEdit',$unionList->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('union.adminDelete',$unionList->id) }}"
                           id={{ $unionList->id }}>
                            <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($unionLists->currentPage() - 1) * $unionLists->perPage()) + 1 }}
                to {{ (($unionLists->currentPage() - 1) * $unionLists->perPage()) + $unionLists->perPage() }} of
                {{ $unionLists->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $unionLists->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
{{--</div>--}}
