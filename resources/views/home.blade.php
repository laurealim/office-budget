@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">Starter Page</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                        <?php
                        if (check_view_permission('developer'))
                            echo 'Hello developer';
                        if (check_view_permission('manager'))
                            echo 'Hello manager';

                        ?>
                    </p>

                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div>

            <div class="card card-primary card-outline">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                        Some quick example text to build on the card title and make up the bulk of the
                        card's
                        content.
                    </p>
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div><!-- /.card -->
        </div>
        <!-- /.col-md-6 -->
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="m-0">Featured</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional
                        content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>

            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">Featured</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional
                        content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        <!-- /.col-md-6 -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Dashboard') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            {{ __('You are logged in!') }}
                            <br>

                            <?php
                            if (check_view_role('super-admin'))
                                echo 'Hello Super Admin';
                            if (check_view_role('admin'))
                                echo 'Hello Admin';
                            if (check_view_role('user'))
                                echo 'Hello User';
                            if (check_view_role('ap-user'))
                                echo 'Hello AP User';

                            ?>
{{--                            @can('create-task')--}}
{{--                                Hello lllll--}}
{{--                            @endcan--}}

{{--                                                @role(array('super-admin'))--}}
{{--                                                    Hello Dododod--}}
{{--                                                @endrole--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
