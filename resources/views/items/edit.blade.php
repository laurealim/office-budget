@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('items.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Institute --}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">আইটেম</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আইটেমের ধরন <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="item_type" name="item_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($parentItemListArr as $id => $val){ ?>
{{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <option value="{{ $id }}" {{ $id == $itemData->item_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
{{--                            <input type="text" id="budget_type" name="budget_type" class="form-control"--}}
{{--                                   placeholder="বাজেট ধরন" required value="{{ $bgtTypData->budget_type }}">--}}
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আইটেমের নাম <span class="reqrd"> *</span> </label>
                            <input type="text" id="item_name" name="item_name" class="form-control"
                                   placeholder="আইটেমের নাম" required value="{{ $itemData->item_name }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('item_name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('item_name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>স্ট্যাটাস <span class="reqrd"> *</span></label>
                            <?php $status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="item_status" name="item_status" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($status as $id => $val){ ?>
                                {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <option value="{{ $id }}" {{ $id == $itemData->item_status ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('item_status'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('item_status') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

        });

    </script>
@endsection
