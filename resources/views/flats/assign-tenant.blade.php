@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('flats.saveAssignTenant') }}" method="POST" id="reg_form">
    {{ csrf_field() }}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ভাড়াটিয়া এসাইন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ফ্লাটের তালিকা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="flat_id" name="flat_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($unassignedFlatList as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('flat_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('flat_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ভাড়াটিয়ার তালিকা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="tenant_id" name="tenant_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($tenantList as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('tenant_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('tenant_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>
        <div class="card card-success">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Assign</button>
            </div>
        </div>
    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

        });
    </script>
@endsection
