@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @can('create-flat')
                        <h3 class="card-title">
                            <div class="pad_space">
                                <a href="{{ route('flats.create') }}" class="btn btn-block btn-secondary btn-md"
                                   title="Add">
                                    <i class="far fa-plus-square fa-lg"></i>
                                </a>
                            </div>
                        </h3>
                    @endcan
                    @can('export-flat-list')
                        <h3 class="card-title">
                            <div class="pad_space">
                                <a href="{{ route('flats.export') }}" class="btn btn-block btn-success btn-md"
                                   id="export_list" title="Download List">
                                    <i class="fas fa-file-download fa-lg"></i>
                                </a>
                            </div>
                        </h3>
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered data-table">
                        {{--                    <table id="example1" class="table table-bordered table-striped">--}}
                        <thead>
                        <tr>
                            <th>ক্রমিক নং</th>
                            <th> ফ্ল্যটের মালিক</th>
                            <th> ফ্ল্যাট নং</th>
                            <th> ফ্ল্যাট আইডি</th>
                            <th> ঠিকানা</th>
                            <th> ফ্ল্যাটের বিবরন</th>
                            <th> সুবিধা সমূহ</th>
                            <th> ভাড়া</th>
                            <th> সার্ভিস চার্জ</th>
                            <th> অগ্রিম ভাড়া মাস</th>
                            <th> স্ট্যাটাস</th>
                            <th> ব্যবস্থা</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body  card-body">
                    <form id="tenantForm" name="tenantForm" class="form-horizontal">
                        <input type="hidden" name="flat_id" id="flat_id">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tenant_name" class="col-sm-12 control-label">ভাড়াটিয়ার তালিকা</label>
                                    <select class="form-control select2bs4" id="tenant_id" name="tenant_id">
                                        <option value="">--- বাছাই করুণ ---</option>
                                        <?php
                                        foreach ($tenantList as $id => $name){ ?>
                                        <option value="{{ $id }}">{{ $name }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info" id="saveBtn" value="create">সংরক্ষণ করুন
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript">
        // $(function () {
        //     $(".data-table").DataTable({
        //         "responsive": true, "lengthChange": false, "autoWidth": false,
        //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        // });
        $(document).ready(function () {
            // $(".data-table").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                ajax: "{{ route('flats.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'created_by', name: 'created_by'},
                    {data: 'flat', name: 'flat'},
                    {data: 'flat_id', name: 'flat_id'},
                    {data: 'full_address', name: 'full_address'},
                    {data: 'flat_description', name: 'flat_description'},
                    {data: 'facilities', name: 'facilities'},
                    {data: 'rent', name: 'rent'},
                    {data: 'service_charge', name: 'service_charge'},
                    {data: 'advance_month', name: 'advance_month'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $(document).on("click", "a.delete-flat", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("Do you wat to delete?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $('body').on('click', '.assignTenant', function () {
                var flat_id = $(this).data('id');
                var tenant_id = $('#tenant_id').val();
                {{--var base_url = "{!! url() !!}";--}}
                var base_url = "{{url("flats/assign-tenant")}}";
                $('#ajaxModel').modal('show');
                $('#flat_id').val(flat_id);
                // alert(Customer_id);
                // $.get(base_url + '/' + flat_id +'/'+tenant_id, function (data) {
                // $('#modelHeading').html("ভাড়াটিয়া বাছাই করুন");
                // $('#saveBtn').val("edit-user");
                // $('#tenant_id').val(data.tenant_id);
                // $('#Customer_id').val(data.id);
                // $('#detail').val(data.detail);
                // })
            });

            $('body').on('click', '.unAssignTenant', function () {
                var flat_id = $(this).data('id');
                var base_url = "{{url("flats/unassign-tenant")}}";
                var token = "{{ csrf_token() }}";
                if (confirm("Do you wat to Un Assign Tenant?")) {
                    $.ajax({
                        // data: $('#tenantForm').serialize(),
                        url: base_url,
                        type: "GET",
                        data: {flat_id: flat_id, _token: token},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            // $('#CustomerForm').trigger("reset");
                            // $('#ajaxModel').modal('hide');
                            // $('#example1').ajax.reload();
                            window.location.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            // $('#saveBtn').html('Save Changes');
                        }
                    });
                } else {
                    return false;
                }
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).html('Saving..');
                var base_url = "{{url("flats/assign-tenant")}}";
                var flat_id = tenant_id = "";
                flat_id = $('#flat_id').val();
                tenant_id = $('#tenant_id').val();
                var token = "{{ csrf_token() }}";
                $.ajax({
                    // data: $('#tenantForm').serialize(),
                    url: base_url,
                    type: "GET",
                    data: {flat_id: flat_id, tenant_id: tenant_id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        // $('#example1').ajax.reload();
                        window.location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                    }
                });
            });

        });
    </script>
@endsection


{{--@extends('layout/master')--}}
{{--@section('content')--}}
{{--    <!-- Main content -->--}}
{{--    <section class="content">--}}
{{--      <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--          <div class="col-12">--}}
{{--            <div class="card card-primary">--}}
{{--              <div class="card-header">--}}
{{--                <h3 class="card-title">{!! $title !!}</h3>--}}
{{--              </div>--}}
{{--              <!-- /.card-header -->--}}
{{--              <div class="card-body">--}}
{{--                <table id="role_lists" class="table table-bordered table-striped">--}}
{{--                  <thead>--}}
{{--                  <tr>--}}
{{--                    <th>নাম</th>--}}
{{--                    <th> স্লাগ </th>--}}
{{--                    <th style="width:17%!important;">Action</th>--}}
{{--                  </tr>--}}
{{--                  </thead>--}}
{{--                  <tbody>--}}
{{--                    @if(count($role_list) > 0)--}}
{{--                       @foreach($role_list as $role)--}}
{{--                  <tr onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">--}}
{{--                    <td>{!! $role->role_name !!}</td>--}}
{{--                    <td>{!! $role->role_slug !!}</td>--}}


{{--                    <td style="width:17%!important;">--}}
{{--                      <a href="{{route('role.edit',$role->id)}}"><button class="btn-success">Edit</button> &nbsp;</a>--}}

{{--                      <a onclick="delete_data('role','{!! $role->id !!}','{!! $role->role_name !!}');" type="submit" name="submit"  data-toggle="modal" data-target="#modal-default"><button class="btn-danger">Delete</button></a>--}}


{{--                    </td>--}}


{{--                  </tr>--}}
{{--                </tbody>--}}
{{--                    @endforeach--}}
{{--                   @endif--}}
{{--                  </tfoot>--}}
{{--                </table>--}}
{{--              </div>--}}
{{--              @include('layout.delete-data-modal')--}}
{{--              <!-- /.card-body -->--}}
{{--            </div>--}}
{{--            <!-- /.card -->--}}
{{--          </div>--}}
{{--          <!-- /.col -->--}}
{{--        </div>--}}
{{--        <!-- /.row -->--}}
{{--      </div>--}}
{{--@stop--}}

{{--@section('script')--}}
{{--<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
{{--<script>--}}
{{--  $(function () {--}}
{{--    $("#role_lists").DataTable({--}}
{{--      "responsive": true,--}}
{{--      "autoWidth": false,--}}
{{--    });--}}
{{--  });--}}


{{--</script>--}}

{{--@extends('layout/common-js')--}}
{{--@stop--}}
