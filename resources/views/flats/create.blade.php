@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('flats.store') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}

        {{--        Flat Address / Location...--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ঠিকানা</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="dist" name="dist" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($districtListArr as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('dist'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('dist') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>উপজেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="upaz" name="upaz" required>
                                <option value="">--- বাছাই করুণ ---</option>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('upaz'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('upaz') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ইউনিয়ন / পৌরসভা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="union" name="union" required>
                                <option value="">--- বাছাই করুণ ---</option>

                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('union'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('union') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ওয়ার্ড নং </label>
                            <input type="text" id="word" name="word" class="form-control" placeholder="ওয়ার্ড নং">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('word'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('word') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>মহল্লা/রোড,হোল্ডিং নং,গ্রাম/বাসা নং <span class="reqrd"> *</span> </label>
                            <input type="text" id="address" name="address" class="form-control"
                                   placeholder="মহল্লা/রোড,হোল্ডিং নং,গ্রাম/বাসা নং" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('address'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ফ্লাট নং <span class="reqrd"> *</span> </label>
                            <input type="text" id="flat" name="flat" class="form-control" placeholder="ফ্লাট নং"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('flat'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('flat') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Flat Description--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ফ্ল্যাটের বিবরন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>বিল্ডিঙের দিক <span class="reqrd"> *</span></label>
                            <?php $buildingFate = config('constants.flat_face'); ?>
                            <select class="form-control select2bs4" id="face" name="face" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($buildingFate as $id => $face){ ?>
                                <option value="{{ $id }}">{{ $face }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('face'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('face') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>ফ্ল্যাটের সাইজ (sq. ft.) <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="size" name="size" class="form-control"
                                   placeholder="i.e. 1200,1580,2000...."
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('size'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('size') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>বাথরুম সংখ্যা <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="bath" name="bath" class="form-control"
                                   placeholder="i.e. 2,3....."
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('bath'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('bath') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->

                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ফ্ল্যাটের অবস্থান (তলা) <span class="reqrd"> *</span> </label>
                            <?php $flatFloor = config('constants.flat_floor'); ?>
                            <select class="form-control select2bs4" id="floor" name="floor" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($flatFloor as $id => $floor){ ?>
                                <option value="{{ $id }}">{{ $floor }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('floor'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('floor') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>বেডরুম সংখ্যা <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="room" name="room" class="form-control"
                                   placeholder="i.e. 3,4....."
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('room'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('room') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>বারান্দা সংখ্যা <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="veranda" name="veranda" class="form-control"
                                   placeholder="i.e. 1,2....."
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('veranda'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('veranda') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Flat Facilities--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ফ্ল্যাটের সুবিধাসমুহ</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="gas" name="gas" value="0">
                                <input type="checkbox" id="gas" name="gas" value="1">
                                <label for="gas">
                                    গ্যাস সরবরাহ
                                </label>
                            </div>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="intercom" name="intercom" value="0">
                                <input type="checkbox" id="intercom" name="intercom" value="1">
                                <label for="intercom">
                                    ইন্টারকম ফোন
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="generator" name="generator" value="0">
                                <input type="checkbox" id="generator" name="generator" value="1">
                                <label for="generator">
                                    জেনারেটর
                                </label>
                            </div>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="geyser" name="geyser" value="0">
                                <input type="checkbox" id="geyser" name="geyser" value="1">
                                <label for="geyser">
                                    গিজার
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="lift" name="lift" value="0">
                                <input type="checkbox" id="lift" name="lift" value="1">
                                <label for="lift">
                                    লিফট
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="parking" name="parking" value="0">
                                <input type="checkbox" id="parking" name="parking" value="1">
                                <label for="parking">
                                    কার পার্কিং
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Service Charge Includes--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">সার্ভিস চার্জের মধ্যে অন্তর্ভুক্ত </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="garbage" name="garbage" value="0">
                                <input type="checkbox" id="garbage" name="garbage" value="1">
                                <label for="garbage">
                                    ময়লার বিল
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="comn_water" name="comn_water" value="0">
                                <input type="checkbox" id="comn_water" name="comn_water" value="1">
                                <label for="comn_water">
                                    কমন পানির বিল
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="comn_elec" name="comn_elec" value="0">
                                <input type="checkbox" id="comn_elec" name="comn_elec" value="1">
                                <label for="comn_elec">
                                    কমন বিদ্যুত বিল
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-sm-3">
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="hidden" id="comn_clean" name="comn_clean" value="0">
                                <input type="checkbox" id="comn_clean" name="comn_clean" value="1">
                                <label for="comn_clean">
                                    পরিচ্ছন্নকারীর বিল
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Flat Rants and Bills--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ভাড়া ও বিল সংক্রান্ত তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>মাসিক ভাড়া <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="rent" name="rent" class="form-control"
                                   placeholder="i.e. 20000, 25000...."
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('rent'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('rent') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form group -->
                        <div class="form-group">
                            <label>মাসিক সার্ভিস চার্জ <span class="reqrd"> *</span></label>

                            <input type="number" min="0" id="service_charge" name="service_charge" class="form-control"
                                   placeholder="i.e. 2000,5000...." required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('service_charge'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('service_charge') }}</strong>
                                </span>
                                @endif
                        </span>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>মাসিক মোট ভাড়া (মাসিক ভাড়া + সার্ভিস চার্জ) <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="monthly_bill" name="monthly_bill" class="form-control"
                                   readonly>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('monthly_bill'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('monthly_bill') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">


                        <div class="form-group">
                            <label>অগ্রিম ভাড়া কত মাসের <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="advance_month" name="advance_month" class="form-control"
                                   placeholder="i.e. 2,3,4......"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('advance_month'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('advance_month') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>অগ্রিম ভাড়া (মাসিক ভাড়া * অগ্রিম ভাড়ার মাস) <span class="reqrd"> *</span></label>
                            <input type="number" min="0" id="advance_bill" name="advance_bill" class="form-control"
                                   readonly>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('advance_bill'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('advance_bill') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Flat Images--}}
        <div class="card  card-success">
            <div class="card-header">
                <h3 class="card-title">ফ্ল্যাটের ছবি যুক্ত করুন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">ছবি বাছাই করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="image" class="col-xs-10 col-sm-5" name="image[]"
                                       onchange="loadPreview(this)" multiple/>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('image'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "100px",
                                "height": "100px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
                upazillaList(districtID, token, url);
            } else {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
            }
        });

        $('#upaz').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.unionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="union"]').empty();
                unionList(upazilaID, token, url);
            } else {
                $('select[name="union"]').empty();
            }
        });

        $('#rent').on('change', function () {
            var rentValue = $(this).val();
            var service_charge = $('#service_charge').val();
            var advance_month = $('#advance_month').val();

            monthly_bill_calculation(rentValue, service_charge);
            advance_bill_calculation(rentValue, advance_month);
        });

        $('#service_charge').on('change', function () {
            var service_charge = $(this).val();
            var rentValue = $('#rent').val();
            monthly_bill_calculation(rentValue, service_charge);
        });

        $('#advance_month').on('change', function (){
            var advance_month = $(this).val();
            var rentValue = $('#rent').val();
            advance_bill_calculation(rentValue, advance_month)
        });

        function upazillaList(districtID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="upaz"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="upaz"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');

                    $.each(data, function (key, value) {
                        $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function monthly_bill_calculation(rentValue, service_charge){
            if ($.trim(rentValue) != '') {
                if ($.trim(service_charge) != '') {
                    // var monthly_bill = parseInt(rentValue) + parseInt(service_charge);
                    $('#monthly_bill').val(parseInt(rentValue) + parseInt(service_charge));
                }else{
                    // var monthly_bill = parseInt(rentValue) + parseInt(service_charge);
                    $('#monthly_bill').val(parseInt(rentValue));
                }
            }
        }

        function advance_bill_calculation(rentValue, advance_month){
            if ($.trim(rentValue) != '') {
                if ($.trim(advance_month) != '') {
                    // var monthly_bill = parseInt(rentValue) * parseInt(service_charge);
                    $('#advance_bill').val(parseInt(rentValue) * parseInt(advance_month));
                }else{
                    // var monthly_bill = parseInt(rentValue) * 0;
                    $('#advance_bill').val(0);
                }
            }
        }

    </script>
@endsection
