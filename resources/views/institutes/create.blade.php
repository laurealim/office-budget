@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('institutes.store') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}

        {{-- Institute --}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">প্রতিষ্ঠান</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="dist" name="dist" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($districtListArr as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('dist'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('dist') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>উপজেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="upaz" name="upaz" required>
                                <option value="">--- বাছাই করুণ ---</option>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('upaz'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('upaz') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>প্রতিষ্ঠানের নামে <span class="reqrd"> *</span> </label>
                            <input type="text" id="office_name" name="office_name" class="form-control"
                                   placeholder="প্রতিষ্ঠানের নামে" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('office_name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('office_name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>প্রতিষ্ঠানের কোড (শেষ ৬ ডিজিট) <span class="reqrd"> *</span> </label>
                            <input type="number" id="office_code" name="office_code" class="form-control"
                                   placeholder="প্রতিষ্ঠানের কোড (শেষ ৬ ডিজিট)" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('office_code'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('office_code') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>স্ট্যাটাস <span class="reqrd"> *</span></label>
                            <?php $status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="status" name="status" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($status as $id => $val){ ?>
                                <option value="{{ $id }}">{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('status'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
                upazillaList(districtID, token, url);
            } else {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
            }
        });

        function upazillaList(districtID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="upaz"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="upaz"]').append('<option value="-1">' + " জেলা কার্যালয় " + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="upaz"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

    </script>
@endsection
