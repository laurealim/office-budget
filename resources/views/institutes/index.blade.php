@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @can('create-institute')
                        <h3 class="card-title">
                            <div class="pad_space">
                                <a href="{{ route('institutes.create') }}" class="btn btn-block btn-secondary btn-md"
                                   title="Add">
                                    <i class="far fa-plus-square fa-lg"></i>
                                </a>
                            </div>
                        </h3>
                    @endcan
                    @can('export-flat-list')
{{--                        <h3 class="card-title">--}}
{{--                            <div class="pad_space">--}}
{{--                                <a href="{{ route('institutes.export') }}" class="btn btn-block btn-success btn-md"--}}
{{--                                   id="export_list" title="Download List">--}}
{{--                                    <i class="fas fa-file-download fa-lg"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </h3>--}}
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered data-table">
                        {{--                    <table id="example1" class="table table-bordered table-striped">--}}
                        <thead>
                        <tr>
                            <th>ক্রমিক নং</th>
                            <th> প্রতিষ্ঠানের নাম</th>
                            <th> প্রতিষ্ঠানের কোড (শেষ ৬ ডিজিট) </th>
                            <th> স্ট্যাটাস</th>
                            <th> ব্যবস্থা</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript">
        // $(function () {
        //     $(".data-table").DataTable({
        //         "responsive": true, "lengthChange": false, "autoWidth": false,
        //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        // });
        $(document).ready(function () {
            // $(".data-table").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            var table = $('.data-table').DataTable({
                lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                ajax: "{{ route('institutes.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'office_name', name: 'office_name'},
                    {data: 'office_code', name: 'office_code'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $(document).on("click", "a.delete-institute", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("Do you wat to delete?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });
        });
    </script>
@endsection
