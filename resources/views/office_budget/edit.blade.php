@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('office-budget.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Office Budget --}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"> সাধারন তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> বাজেট শিরনাম <span class="reqrd"> *</span> </label>
                            <input type="text" id="title" name="title" class="form-control"
                                   placeholder="বাজেট শিরনাম" required value="{{ $officeBudgetData->title }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('title'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> বাজেটের ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_type" name="budget_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetTypeList as $id => $val){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $officeBudgetData->budget_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আর্থিক বছর <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="financial_year" name="financial_year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($financialList as $year => $val){ ?>
                                <option
                                    value="{{ $year }}" {{ $year == $officeBudgetData->financial_year ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                {{--                                <option value="{{ $year }}">{{ $val }}</option>--}}
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('financial_year'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('financial_year') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> প্রতিষ্ঠান <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="institute_id" name="institute_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetInstituteList as $id => $val){ ?>
                                {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <option
                                    value="{{ $id }}" {{ $id == $officeBudgetData->institute_id ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('institute_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('institute_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        {{--                        <div class="form-group">--}}
                        {{--                            <label>স্ট্যাটাস <span class="reqrd"> *</span></label>--}}
                        {{--                            <?php $status = config('constants.budget_status.arr'); ?>--}}
                        {{--                            <select class="form-control select2bs4" id="status" name="status" required>--}}
                        {{--                                <option value="">--- বাছাই করুণ ---</option>--}}
                        {{--                                <?php--}}
                        {{--                                foreach ($status as $id => $val){ ?>--}}
                        {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                        {{--                                <?php }--}}
                        {{--                                ?>--}}
                        {{--                            </select>--}}
                        {{--                            <span class="help-inline col-xs-12 col-sm-7">--}}
                        {{--                            @if ($errors->has('status'))--}}
                        {{--                                    <span class="help-block middle">--}}
                        {{--                                    <strong>{{ $errors->first('status') }}</strong>--}}
                        {{--                                </span>--}}
                        {{--                                @endif--}}
                        {{--                        </span>--}}
                        {{--                        </div>--}}
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{-- Office Budget Images--}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট ফাইল সংযুক্তি</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">বাজেট ফাইল নির্বাচন করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="file_name" class="col-xs-10 col-sm-5" name="file_name"/>
                                <?php
                                if (!empty($officeBudgetData->file_name)) {
                                    $path = asset("images/budget/office_budget/" . $officeBudgetData->id . "/" . $officeBudgetData->file_name);
                                    $icon = '<a target="_blank" title="Edit" class="btn btn-success" id="edit-office-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i> ' . $officeBudgetData->file_name . ' </a>';
                                    echo $icon;
                                }
                                ?>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('file_name'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('file_name') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{-- Office Budget Details--}}
        <div class="card card-info bgt_dels">
            <div class="card-header">
                <h3 class="card-title"> আর্থিক তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap" id="budget_tbl">
                            <thead>
                            <tr>
                                <th>নাম</th>
                                <th>পূর্ন বাজেট কোড</th>
                                <th>অর্থের পরিমান</th>
                                <th>ভ্যাট</th>
                                <th>ট্যাক্স</th>
                                <th>প্রকৃত ব্যায়যোগ্য অর্থের পরিমান</th>
                            </tr>
                            </thead>
                            <tbody id="budgetAjaxData">
                            <?php
                            $decodedBudgetDetails = json_decode($budget_details, true);
                            $newCode = false;
//                            pr($decodedBudgetDetails);
//                            pr($newBudgetCodeListArr);
                            foreach ($newBudgetCodeListArr as $keys => $vals){ ?>
                            <tr class='inner_tr' style='font-weight: bold; text-align: center; font-size: large'>
                                <td colspan='6'>{{ $budgetSubType[$keys] }}</td>
                            </tr>
                            <?php foreach ($vals as $key => $val){

                                $bgtId = $val['id'];
                                ?>
                            <tr>
                                <td>
                                    <input name='budget[id][]' custId="{{ $bgtId }}" id="title_{{ $bgtId }}"
                                           value="{{ $val['budget_title'] }}"
                                           readonly/>
                                </td>
                                <td>
                                    <input name='budget[full_budget_code][]' custId="{{ $bgtId }}"
                                           id="bgt_code_{{ $bgtId }}"
                                           value="{{ $val['full_budget_code'] }}"
                                           readonly/>
                                </td>
                                <td>
                                        <?php
                                        if (array_key_exists($val['full_budget_code'], $decodedBudgetDetails)) {
                                            $newCode = $decodedBudgetDetails[$val['full_budget_code']]['amount'];
                                        } else {
                                            $newCode = 0;
                                        }
                                        ?>
                                    <input type="number" name='budget[amount][]' custId="{{ $bgtId }}"
                                           class='main_amount' id="amnt_{{ $bgtId }}" min='0'
                                           value="{{ $newCode }}"/>
                                </td>
                                <td>
                                    <input name='budget[vat][]' custId="{{ $bgtId }}" id="vat_{{ $bgtId }}"
                                           value="{{ $val['vat'] }}"
                                           readonly/>
                                </td>
                                <td>
                                    <input name='budget[tax][]' custId="{{ $bgtId }}" id="tax_{{ $bgtId }}"
                                           value="{{ $val['tax'] }}"
                                           readonly/>
                                </td>
                                <td>
                                        <?php
                                        if (array_key_exists($val['full_budget_code'], $decodedBudgetDetails)) {
                                            $newCode = $decodedBudgetDetails[$val['full_budget_code']]['useable_amount'];
                                        } else {
                                            $newCode = 0;
                                        }
                                        ?>
                                    <input name='budget[useable_amount][]' custId="{{ $bgtId }}"
                                           id="use_amnt_{{ $bgtId }}"
                                           value="{{ $newCode}}"
                                           readonly/>
                                </td>
                            </tr>
                            <?php }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

            $('#budget_type').on('change', function (e) {
                $('#budgetAjaxData').html("");
                var budgetType = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('office-budge.getBudgetCodeByType') }}";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {budgetType: budgetType, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        var htmlTag = "";
                        if (data.status == 200) {
                            $.each(data.resData, function (indexs, valuess) {
                                console.log(data.budgetSubType[3]);
                                htmlTag += "<tr class='inner_tr' style='font-weight: bold; text-align: center; font-size: large'>";
                                htmlTag += "<td colspan='6'>" + data.budgetSubType[indexs] + "</td>";
                                htmlTag += "</tr>";
                                $.each(valuess, function (index, values) {
                                    console.log(index);
                                    console.log(values);
                                    htmlTag += "<tr>";
                                    htmlTag += "<td><input name='budget[id][]' custId='" + values.id + "' id='title_" + values.id + "' value='" + values.budget_title + "' readonly /></td>"
                                    htmlTag += "<td><input name='budget[full_budget_code][]' custId='" + values.id + "' id='bgt_code_" + values.id + "' value='" + values.budget_code + "' readonly /></td>"
                                    htmlTag += "<td><input type='number' custId='" + values.id + "' class='main_amount' id='amnt_" + values.id + "' min='0' name='budget[amount][]'  value='0' /></td>"
                                    htmlTag += "<td><input name='budget[vat][]' custId='" + values.id + "' id='vat_" + values.id + "' value='" + values.vat + "' readonly /></td>"
                                    htmlTag += "<td><input name='budget[tax][]' custId='" + values.id + "' id='tax_" + values.id + "' value='" + values.tax + "' readonly /></td>"
                                    htmlTag += "<td><input name='budget[useable_amount][]' custId='" + values.id + "' id='use_amnt_" + values.id + "' value='0' readonly /></td>"
                                    htmlTag += "</tr>";
                                });
                            });
                        }
                        $('#budgetAjaxData').append(htmlTag);

                        $('.bgt_dels').show();
                        // $('select[name="division_id"]').empty();
                        // $.each(data, function (key, value) {
                        //     $('select[name="division_id"]').append('<option value="' + key + '">' + value + '</option>');
                        // });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
                // alert(values);
            });


            var $budget_details = '<?php echo $budget_details; ?>';
            const obj = JSON.parse($budget_details);
            // alert($budget_details);
            var htmlTag = "<tr>";
            $.each(obj, function (index, values) {
            });

        });

        $('#budget_tbl').on('change', 'input[type=number]', function (e) {
            var amount = parseInt($(this).val());
            var typeId = $(this).attr('custId');

            var vat = parseInt($("#vat_" + typeId).val());
            var tax = parseInt($("#tax_" + typeId).val());

            var usableAmount = 0;
            if (amount) {
                usableAmount = (amount - (amount * vat / 100) - (amount * tax / 100));
            }
            $("#use_amnt_" + typeId).val(usableAmount);
        });

    </script>
@endsection
