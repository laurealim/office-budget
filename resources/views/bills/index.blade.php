@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <div class="pad_space">
                            <a href="{{ route('bills.create') }}" class="btn btn-block btn-secondary btn-md"
                               title="Add">
                                <i class="far fa-plus-square fa-lg"></i>
                            </a>
                        </div>
                    </h3>
                    {{--                    <h3 class="card-title">--}}
                    {{--                        <div class="pad_space">--}}
                    {{--                            <a href="#" class="btn btn-block btn-success btn-md" title="Download List">--}}
                    {{--                                <i class="fas fa-file-download fa-lg"></i>--}}
                    {{--                            </a>--}}
                    {{--                        </div>--}}
                    {{--                    </h3>--}}
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered data-table">
                        {{--                    <table id="example1" class="table table-bordered table-striped">--}}
                        <thead>
                        <tr>
                            <th>ক্রমিক নং</th>
                            <th>ফ্ল্যাট আইডি</th>
                            <th>ফ্ল্যাট নং</th>
                            <th>ঠিকানা</th>
                            <th>ভাড়াটিয়া</th>
                            <th>বিলিং মাস</th>
                            <th>ভাড়া ৳</th>
                            <th>সার্ভিস চার্জ ৳</th>
                            <th>অন্যান্ন চার্জ ৳</th>
                            <th>বকেয়া ভাড়া ৳</th>
                            <th>মোট ভাড়া ৳</th>
                            <th>স্ট্যেটাস</th>
                            <th>ব্যবস্থাপনা</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body  card-body">
                    {{--                    <form id="tenantForm" name="tenantForm" class="form-horizontal">--}}
                    <input type="hidden" name="flat_id" id="flat_id">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="tenant_name" class="col-sm-12 control-label">ভাড়া বাড়ির তালিকা</label>
                                <div id="assignedFlatList"></div>
                                {{--                                    <select class="form-control select2bs4" id="tenant_id" name="tenant_id">--}}
                                {{--                                        <option value="">--- বাছাই করুণ ---</option>--}}
                                {{--                                        <?php--}}
                                {{--                                        foreach ($tenantList as $id => $name){ ?>--}}
                                {{--                                        <option value="{{ $id }}">{{ $name }}</option>--}}
                                {{--                                        <?php }--}}
                                {{--                                        ?>--}}
                                {{--                                    </select>--}}
                            </div>
                        </div>
                    </div>
                    {{--                        <div class="col-sm-offset-2 col-sm-10">--}}
                    {{--                            <button type="submit" class="btn btn-info" id="saveBtn" value="create">সংরক্ষণ করুন--}}
                    {{--                            </button>--}}
                    {{--                        </div>--}}
                    {{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_style')
    <style type="text/css">
    </style>
@endsection

@section('custom_script')
    <script type="text/javascript">
        // $(function () {
        //     $(".data-table").DataTable({
        //         "responsive": true, "lengthChange": false, "autoWidth": false,
        //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        // });
        $(document).ready(function () {
            // $(".data-table").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                ajax: "{{ route('bills.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'flat_code'},
                    {data: 'flat'},
                    {data: 'address'},
                    {data: 'name'},
                    {data: 'billing_time'},
                    {data: 'rent', render: $.fn.dataTable.render.number(',', '.', 2)},
                    {data: 's_charge', render: $.fn.dataTable.render.number(',', '.', 2)},
                    {data: 'other_charge', render: $.fn.dataTable.render.number(',', '.', 2)},
                    {data: 'prv_due', render: $.fn.dataTable.render.number(',', '.', 2)},
                    {data: 'total_bill', render: $.fn.dataTable.render.number(',', '.', 2)},
                    {data: 'bill_status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $(document).on("click", "a.delete-tenant", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("Do you wat to delete?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $('body').on('click', '.flatList', function () {
                var tenant_id = $(this).data('id');
                var base_url = "{{url("assigned-flat-lists/")}}";
                var token = "{{ csrf_token() }}";
                $.ajax({
                    url: base_url,
                    type: "GET",
                    data: {tenant_id: tenant_id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        var dataVal = jQuery.parseJSON(data.list);
                        var table = '<table id="example1" class="table table-bordered data-table">';
                        table += '<thead><tr>' +
                            '<th>ফ্ল্যাট আইডি</th>' +
                            '<th>ঠিকানা</th>' +
                            '<th>ফ্ল্যাট নং</th>' +
                            '<th>ফ্ল্যাট সাইজ</th>' +
                            '<th>ভাড়া</th>' +
                            '<th>সার্ভিস চার্জ</th>' +
                            '</tr></thead>' +
                            '<tbody>';
                        $.each(dataVal, function (id, key) {
                            table += '<tr>' +
                                '<td>' + key.flat_id + '</td>' +
                                '<td>' + key.address + '</td>' +
                                '<td>' + key.flat + '</td>' +
                                '<td>' + key.size + '</td>' +
                                '<td>' + key.rent + '</td>' +
                                '<td>' + key.service_charge + '</td>' +
                                '</tr>';
                        });
                        table += '</tbody>' +
                            '</table>';

                        $("#assignedFlatList").empty();
                        $("#assignedFlatList").append(table);
                        $('#ajaxModel').modal('show');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        // $('#saveBtn').html('Save Changes');
                    }
                });
            });

        });
    </script>
@endsection
