@extends('layouts.invoice')

@section('pagetitle')
        <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    {{--    <div class="container d-flex justify-content-center mt-50 mb-50">--}}
    <div class="row" id="printInvoice">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <h6 class="card-title">{{ $invoice_title }}</h6>
                    <div class="header-elements buttons">
                        {{--                        <a type="button" href="<?php echo route('bills.downloadPDF') ?>" class="btn btn-light btn-sm"><i--}}
                        {{--                                class="fa fa-file mr-2"></i> Save--}}
                        {{--                        </a>--}}
                        <a type="button" class="btn btn-light btn-sm ml-3" id="prnt_btn"><i
                                class="fa fa-print mr-2"></i>
                            প্রিন্ট
                        </a>
                    </div>
                </div>
<!--                --><?php //pr($billData);?>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-4 pull-left">
                                <h5>ফ্লাটের মালিক</h5>
                                <ul class="list list-unstyled mb-0 text-left">
                                    <li>নামঃ {{ $billData->owner_name }}</li>
                                    <li>ফোনঃ {{ $billData->phone }}</li>
                                    <li>ইমাইলঃ {{ $billData->email }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-4 ">
                                <div class="text-sm-right">
                                    <h4 class="invoice-color mb-2 mt-md-2">Invoice {{ $billData->invoice_code }}</h4>
                                    <ul class="list list-unstyled mb-0">
                                        <?php
                                        $cur_date = $billData->year.'-'.$billData->month.'-01';
                                        $last_date = date("M t, Y,", strtotime($cur_date));
                                        $billing_date = date("M d, Y,", strtotime($cur_date));
                                        ?>
                                        <li>বিলিং মাস: <span class="font-weight-semibold">{{ $billing_date }}</span></li>
{{--                                        <li>পরিশোধের শেষ তারিখ: <span class="font-weight-semibold">{{ $last_date }}</span></li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="d-md-flex flex-md-wrap">
                        <div class="mb-4 mb-md-2 text-left"><span class="text-muted">প্রাপক:</span>
                            <ul class="list list-unstyled mb-0">
                                <li>
                                    <h5 class="my-2"><?php echo $billData->name; ?></h5>
                                </li>
                                <li><span class="font-weight-semibold"><?php echo getTenantAddress($billData->tenant_id); ?></span></li>
                                <li><?php echo $billData->mobile; ?></li>
                            </ul>
                        </div>
                        <div class="mb-2 ml-auto"><span class="text-muted">বিলের বিবরন:</span>
                            <div class="d-flex flex-wrap wmin-md-400">
                                <ul class="list list-unstyled mb-0 text-left">
                                    <li>
                                        <h5 class="my-2">মোট বকেয়া:</h5>
                                    </li>
                                </ul>
                                <ul class="list list-unstyled text-right mb-0 ml-auto">
                                    <li>
                                        <h5 class="font-weight-semibold my-2 invoice-color">&#2547; <?php echo $billData->total_bill?></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>বিবরন</th>
                            <th>মোট</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h6 class="mb-0">মাসিক ভাড়া</h6> <span class="text-muted"></span>
                            </td>
                            <td><span class="font-weight-semibold float-right">&#2547; <?php echo $billData->rent; ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="mb-0">সার্ভিস চার্জ</h6> <span class="text-muted"></span>
                            </td>
                            <td><span class="font-weight-semibold float-right">&#2547; <?php echo $billData->s_charge; ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="mb-0">অগ্রিম ভাড়া</h6> <span class="text-muted"></span>
                            </td>
                            <td><span class="font-weight-semibold float-right">&#2547; <?php echo $billData->adv_rent; ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="mb-0">অন্যান্য চার্জ</h6> <span class="text-muted"></span>
                            </td>
                            <td><span class="font-weight-semibold float-right">&#2547; <?php echo $billData->other_charge; ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="mb-0">বকেয়া বিল</h6> <span class="text-muted"></span>
                            </td>
                            <td><span class="font-weight-semibold float-right">&#2547; <?php echo $billData->prv_due; ?></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body">
                    <div class="d-md-flex flex-md-wrap">
                        <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                            <h6 class="mb-3 text-left">মোট বকেয়া</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th class="text-left">উপমোট:</th>
                                        <td class="text-right">&#2547; <?php echo $billData->total_bill; ?></td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">ছাড়:
                                        </th>
                                        <td class="text-right">&#2547; 0.00</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">মোট:</th>
                                        <td class="text-right text-primary">
                                            <h5 class="font-weight-semibold">&#2547; <?php echo $billData->total_bill; ?></h5>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {{--                                <div class="text-right mt-3">--}}
                            {{--                                    <button type="button" class="btn btn-primary"><b><i--}}
                            {{--                                                class="fa fa-paper-plane-o mr-1"></i></b> Send invoice--}}
                            {{--                                    </button>--}}
                            {{--                                </div>--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--    </div>--}}
@endsection

@section('custom_script')
    <script type="text/javascript">
        // function printData() {
        //     var divToPrint = document.getElementById("printInvoice");
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.close();
        // }

        function printDiv() {
            var printContents = document.getElementById('printInvoice').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }

        $('#prnt_btn').on('click', function () {
            $('.buttons').hide();
            printDiv();
        })
    </script>

@endsection
