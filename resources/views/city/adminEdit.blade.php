@extends('layouts.admin')

@section('title')
    Edit City
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('city.adminList') }}">City List</a>
        </li>

        <li>
            <a href="{{ route('city.adminForm') }}">City Edit</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Edit Country</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('city.adminUpdate', $id) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> City Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="Event Type Name" name="name" value="{{ $cityData->name }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Country Name *</label>

                <div class="col-sm-9">
                    <select name='country_id' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="">-- Please Select One --</option>
                        @foreach($countryData as $countryName)
                            <option value="{{ $countryName->id }}" {{ $countryName->id == $cityData->country_id ? 'selected="selected"' : '' }} >{{ $countryName->name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('country_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('country_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1" {{ $cityData->status == 1 ? 'selected="selected"' : '' }} >Active</option>
                        <option value="0" {{ $cityData->status == 0 ? 'selected="selected"' : '' }} >Inactive</option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')

@stop