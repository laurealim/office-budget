@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('internal-budget.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Office Budget --}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"> সাধারন তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> বাজেট শিরনাম <span class="reqrd"> *</span> </label>
                            <input type="text" id="title" name="title" class="form-control"
                                   placeholder="বাজেট শিরনাম" required value="{{ $internalBudgetData->title }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('title'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> বাজেটের ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_type" name="budget_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetTypeList as $id => $val){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $internalBudgetData->budget_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আর্থিক বছর <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="financial_year" name="financial_year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($financialList as $year => $val){ ?>
                                <option
                                    value="{{ $year }}" {{ $year == $internalBudgetData->financial_year ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                {{--                                <option value="{{ $year }}">{{ $val }}</option>--}}
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('financial_year'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('financial_year') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>অর্থের পরিমান <span class="reqrd"> *</span></label>
                            <input type="number" id="total_amount" name="total_amount" class="form-control" min="0"
                                   value="{{ $internalBudgetData->total_amount }}" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('total_amount'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('total_amount') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{-- Office Budget Images--}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট ফাইল সংযুক্তি</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">বাজেট ফাইল নির্বাচন করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="file_name" class="col-xs-10 col-sm-5" name="file_name"/>
                                <?php
                                if (!empty($internalBudgetData->file_name)) {
                                    $path = asset("images/budget/internal_budget/" . $internalBudgetData->id . "/" . $internalBudgetData->file_name);
                                    $icon = '<a target="_blank" title="Edit" class="btn btn-success" id="edit-office-budget" href= "' . $path . '"><i class="fas fa-file-downloads fa-download"></i> ' . $internalBudgetData->file_name . ' </a>';
                                    echo $icon;
                                }
                                ?>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('file_name'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('file_name') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

        });

        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "100px",
                                "height": "100px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }

    </script>
@endsection
