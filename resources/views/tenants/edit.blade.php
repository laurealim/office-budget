@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('tenants.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">যোগাযোগ</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>এন,আই,ডি নং <span class="reqrd"> *</span></label>
                            <input type="text" id="nid" name="nid" class="form-control" placeholder="এন,আই,ডি নং"
                                   required value="{{ $tenamtData->nid }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('nid'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('nid') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
{{--                        <div class="form-group">--}}
{{--                            <label>পরিবার পরিচিতি কার্ড নং </label>--}}
{{--                            <input type="text" id="f_card" name="f_card" class="form-control" placeholder="পরিবার পরিচিতি কার্ড নং"--}}
{{--                                   required value="{{ $tenamtData->f_card }}">--}}
{{--                            <span class="help-inline col-xs-12 col-sm-7">--}}
{{--                            @if ($errors->has('f_card'))--}}
{{--                                    <span class="help-block middle">--}}
{{--                                    <strong>{{ $errors->first('f_card') }}</strong>--}}
{{--                                </span>--}}
{{--                                @endif--}}
{{--                        </span>--}}
{{--                        </div>--}}
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>মোবাইল নং <span class="reqrd"> *</span></label>
                            <input type="text" id="mobile" name="mobile" class="form-control" placeholder="মোবাইল নং"
                                   required value="{{ $tenamtData->mobile }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('mobile'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">সাধারন তথ্য</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>নাম <span class="reqrd"> *</span></label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="নাম"
                                   required value="{{ $tenamtData->name }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form group -->

                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>জন্ম তারিখ <span class="reqrd"> *</span></label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" id="dob" name="dob" data-mask value="{{ date('m/d/Y',strtotime($tenamtData->dob)) }}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>পিতা / স্বামীর নাম <span class="reqrd"> *</span></label>
                            <input type="text" id="g_name" name="g_name" class="form-control" placeholder="পিতা / স্বামীর নাম"
                                   required value="{{ $tenamtData->g_name }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('g_name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('g_name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label>লিঙ্গ <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="gender" name="gender" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                $gender = config('constants.gender.arr');
                                foreach ($gender as $id => $name){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $tenamtData->gender ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                <?php }
                                ?>
                            </select>

                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('gender'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">ঠিকানা</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="dist" name="dist" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($districtListArr as $id => $name){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $tenamtData->dist ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                {{--                                <option value="{{ $id }}">{{ $name }}</option>--}}
                                <?php }
                                ?>

                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('dist'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('dist') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>উপজেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="upaz" name="upaz" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                @foreach($upozillaListArr as $id => $name)
                                    <option
                                        value="{{ $id }}" {{ $id == $tenamtData->upaz ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                @endforeach
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('upaz'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('upaz') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ইউনিয়ন / পৌরসভা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="union" name="union" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                @foreach($unionListArr as $id => $name)
                                    <option
                                        value="{{ $id }}" {{ $id == $tenamtData->union ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                @endforeach
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('union'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('union') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>গ্রাম/ বাড়ী/ মহল্লা/ রাস্তা/ পাড়া <span class="reqrd"> *</span></label>
                            <input type="text" id="address" name="address" class="form-control" placeholder="গ্রাম/ বাড়ী/ মহল্লা/ রাস্তা/ পাড়া"
                                   required value="{{ $tenamtData->address }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('address'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

{{--            <div class="card-footer">--}}
{{--                <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--            </div>--}}
        </div>

        {{--        Flat Images--}}
        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">ভাড়াটিয়ার ছবি যুক্ত করুন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">ছবি বাছাই করুন </label>

                            <div class="col-xs-12 col-sm-6">
                                <input type="file" id="image" class="col-xs-10 col-sm-5" name="image"
                                       onchange="loadPreview(this)"/>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('image'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <?php
                                $imgArr = explode(",", $tenamtData->pic);
                                foreach ( $imgArr as $item) {
                                if(!empty($item)){ ?>

                                <div style="padding: 5px; border: #3ab0c3">
                                    <img src="{{url('/images/tenants/'.$tenamtData->id.'/'.$item)}}" class="msg-photo"
                                         alt="Kate's Avatar" width="120px" height="80px"/>
{{--                                    <a href="#" class="remov_img" id="{{ $item }}" tenant_id="{{ $tenamtData->id }}"> Remove </a>--}}
                                </div>
                                <?php }
                                }
                                ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "120px",
                                "height": "80px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
                upazillaList(districtID, token, url);
            } else {
                $('select[name="upaz"]').empty();
                $('select[name="union"]').empty();
            }
        });

        $('#upaz').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.unionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="union"]').empty();
                unionList(upazilaID, token, url);
            }
        });

        function upazillaList(districtID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="upaz"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="upaz"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');

                    $.each(data, function (key, value) {
                        $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        $('.remov_img').on('click',function (e){
            e.preventDefault();
            var imageId = $(this).attr('id');
            var tenantId = $(this).attr('tenent_id');
            var token = $("input[name='_token']").val();
            var url = "{{ route('tenants.removeTenantImage') }}";

            alert(imageId +" "+tenantId);

            $.ajax({
                url: url,
                method: 'POST',
                data: {imageId: imageId, tenantId: tenantId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (data.status == 'success') {
                        window.location.reload();
                    } else if (data.status == 'error') {
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        })

    </script>
@endsection
