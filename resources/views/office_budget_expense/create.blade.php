@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('office-budget-expense.store') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}

        {{-- Office Budget --}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"> সাধারন তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> বাজেটের ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_type" name="budget_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetTypeList as $id => $val){ ?>
                                <option value="{{ $id }}">{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> কোড <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_code" name="budget_code" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                {{--                                <?php--}}
                                {{--                                foreach ($financialList as $year => $val){ ?>--}}
                                {{--                                <option value="{{ $year }}">{{ $val }}</option>--}}
                                {{--                                <?php }--}}
                                {{--                                ?>--}}
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_code'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_code') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> মন্তব্য <span class="reqrd"> </span></label>
                            <textarea type="textarea" id="details" name="details" class="form-control"></textarea>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('details'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('details') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> আর্থিক বছর <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="financial_year" name="financial_year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($financialList as $year => $val){ ?>
                                <option value="{{ $year }}">{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('financial_year'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('financial_year') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> প্রতিষ্ঠান <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="institute_id" name="institute_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budgetInstituteList as $id => $val){ ?>
                                <option value="{{ $id }}">{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('institute_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('institute_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.card-body -->
        </div>

        {{-- Office Budget Details--}}
        <div class="card card-info bgt_dels">
            <div class="card-header">
                <h3 class="card-title"> আর্থিক তথ্য </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap" id="budget_tbl">
                            <thead>
                            <tr>
                                <th>নাম</th>
                                <th>পূর্ন বাজেট কোড</th>
{{--                                <th>ভ্যাট</th>--}}
{{--                                <th>ট্যাক্স</th>--}}
                                <th>মোট অর্থের পরিমান</th>
                                <th>ব্যায়যোগ্য অর্থের পরিমান</th>
                                <th>পূর্বের মোট ব্যয়</th>
                                <th>বর্তমান ব্যয়</th>
                            </tr>
                            </thead>
                            <tbody id="budgetAjaxData">
                            <tr>
                                <td><input name='title' id='title' readonly/></td>
                                <td><input name='bgt_code' id='bgt_code' readonly/>
                                </td>
{{--                                <td><input name='vat' id='vat' readonly/></td>--}}
{{--                                <td><input name='tax' id='tax' readonly/></td>--}}
                                <td><input name='total_cur_balance' id='total_cur_balance' readonly/></td>
                                <td><input name='useable_expense' id='useable_expense' readonly/></td>
                                <td><input class='total_expense' name='total_expense' id='total_expense' readonly/></td>
                                <td><input type='number' class='current_exp' id='current_exp' min='0' name='current_exp'
                                           value='0'/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

        </div>


        {{--        Flat Images--}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট ফাইল সংযুক্তি</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-12">
                        <div class="form-group col-sm-12">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                   for="image">বাজেট ফাইল নির্বাচন করুন </label>

                            <div class="col-xs-12 col-sm-7">
                                <input type="file" id="file_name" class="col-xs-10 col-sm-5" name="file_name"
                                       onchange="loadPreview(this)"/>
                                <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('file_name'))
                                        <span class="help-block middle">
                                <strong>{{ $errors->first('file_name') }}</strong>
                            </span>
                                    @endif
                                    {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>


        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button id="btn_sub" type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()

            // $('.bgt_dels').hide();

            $('#budget_code').on('change', function (e) {
                // $('#budgetAjaxData').html("");
                var budget_code_id = $(this).val();
                var budgetType = $("#budget_type").val();
                var financial_year = $("#financial_year").val();
                if (financial_year) {
                    getBudgetExpenseData(budgetType, budget_code_id, financial_year);
                } else {
                    alert('অর্থবছর বাছাই করুন');
                }
            });

            $('#institute_id').on('change', function (e) {
                // $('#budgetAjaxData').html("");
                var institute_id = $(this).val();
                var budgetType = $("#budget_type").val();
                var budget_code_id = $('#budget_code').val();
                var financial_year = $("#financial_year").val();
                if (financial_year && budget_code_id) {
                    getBudgetExpenseData(budgetType, budget_code_id, financial_year, institute_id);
                } else {
                    alert('অর্থবছর ও বাজেট কোড বাছাই করুন');
                }
            });

            $('#financial_year').on('change', function (e) {
                // $('#budgetAjaxData').html("");
                var financial_year = $(this).val();
                var budget_code_id = $('#budget_code').val();
                var budgetType = $("#budget_type").val();
                var institute_id = $("#institute_id").val();
                if (budget_code_id && institute_id) {
                    getBudgetExpenseData(budgetType, budget_code_id, financial_year, institute_id);
                } else {
                    alert('বাজেট কোড ও প্রতিষ্ঠান  বাছাই করুন');
                }
            });

            $('#budget_type').on('change', function () {
                // $('#budgetAjaxData').html("");
                var budgetType = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('budget-code.getBudgetCodeList') }}";
                if (budgetType) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {budgetType: budgetType, _token: token},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            if (data.status == 200) {
                                console.log(data.resData);
                            }
                            $('select[name="budget_code"]').empty();
                            $('input[name="title"]').empty();
                            $('select[name="budget_code"]').append('<option value="">' + "--- বাছাই করুণ ---" + '</option>');
                            $.each(data.resData, function (key, value) {
                                $('select[name="budget_code"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                } else {
                    $('select[name="district_id"]').empty();
                }
            });


        });
        // $('#cur_exp').on('change', 'input[type=number]', function(){
        //     var expamnt = $(this).val();
        //     alert(expamnt);
        // });

        $('#budget_tbl').on('change', 'input[name=current_exp]', function (e) {
            var amount = parseInt($(this).val());
            var useable_expense = parseInt($("#useable_expense").val());

            if (amount > useable_expense) {
                alert("বর্তমান ব্যায় প্রক্রত ব্যায় থেকে বেশি হতে পারবে না। সংশোধন করুন।");
                // $('#btn_sub').hide();
            } else {
                // alert("laurealim");
                // $('#btn_sub').show();
            }
        });

        function loadPreview(input) {
            var data = $(input)[0].files; //this file data
            console.log(data);
            $('#thumb-output').empty();
            $.each(data, function (index, file) {
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result).css({
                                "width": "100px",
                                "height": "100px",
                                "padding": "5px"
                            }); //create image thumb element
                            $('#thumb-output').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }

        function getBudgetExpenseData(budgetType, budget_code_id, financial_year, institute_id) {
            var token = $("input[name='_token']").val();
            var url = "{{ route('office-budget-expense.expenseDataAjax') }}";
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    budgetType: budgetType,
                    budget_code_id: budget_code_id,
                    financial_year: financial_year,
                    institute_id: institute_id,
                    _token: token
                },
                dataType: "json",
                success: function (data) {
                    // var htmlTag = "<tr>";
                    if (data.status == 200) {
                        var resData = data.resData;
                        console.log(resData);

                            $('#title').val(resData.budget_title);
                            $('#bgt_code').val(resData.full_budget_code);
                            // $('#vat').val(resData.vat);
                            // $('#tax').val(resData.tax);
                            $('#total_cur_balance').val(resData.total_balances);
                            $('#useable_expense').val(resData.real_usable_balances);
                            $('#total_expense').val(resData.total_prev_exp);

                        // htmlTag += "<td><input name='title' id='title' value='" + resData.budget_title + "' readonly /></td>"
                        // htmlTag += "<td><input name='bgt_code' id='bgt_code' value='" + resData.full_budget_code + "'" +
                        //     " readonly /></td>"
                        // htmlTag += "<td><input name='vat' id='vat' value='" + resData.vat + "' readonly /></td>"
                        // htmlTag += "<td><input name='tax' id='tax' value='" + resData.tax + "' readonly /></td>"
                        // htmlTag += "<td><input name='total_cur_balance' id='total_cur_balance'  value='" + resData.total_balances + "' readonly /></td>"
                        // htmlTag += "<td><input name='useable_expense' id='useable_expense'  value='" + resData.usable_balances + "' readonly /></td>"
                        // htmlTag += "<td><input class='total_expense' name='total_expense' id='total_expense' value='" + resData.total_exp
                        //     + "' readonly /></td>"
                        // htmlTag += "<td><input type='number' class='cur_exp' id='cur_exp' min='0' " +
                        //     "name='cur_exp' value='0' /></td>"
                        // htmlTag += "</tr>";
                    }
                    // $('#budgetAjaxData').append(htmlTag);

                    // $('.bgt_dels').show();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }

        function total_expense() {
        }

    </script>
@endsection
