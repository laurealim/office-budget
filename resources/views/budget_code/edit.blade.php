@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('budget-code.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        {{-- Institute --}}
        <div class="card  card-info">
            <div class="card-header">
                <h3 class="card-title">বাজেট কোড</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>বাজেটের ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="bgt_type" name="bgt_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budget_type as $id => $val){ ?>
                                {{--                                <option value="{{ $id }}">{{ $val }}</option>--}}
                                <option
                                    value="{{ $id }}" {{ $id == $bgtCodData->bgt_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('bgt_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('bgt_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>উপখাত বাজেট কোড <span class="reqrd"> *</span> </label>
                            <div class="row ">
                                <div class="col-md-2">
                                    <input type="text" id="bgt_type_code" name="bgt_type_code" class="form-control"
                                           readonly value="{{ $bgtCodData->bgt_type_code }}">
                                </div>

                                <div class="col-md-2">
                                    <input type="text" id="bgt_sub_type_code" name="bgt_sub_type_code"
                                           class="form-control" readonly value="{{ $bgtCodData->bgt_sub_type_code }}">
                                </div>

                                <div class="col-md-8">
                                    <input type="number" id="budget_code" name="budget_code" class="form-control"
                                           placeholder="বাজেট কোড" required value="{{ $bgtCodData->budget_code }}">
                                </div>
                                <span class="help-inline col-xs-12 col-sm-7">
                                    @if ($errors->has('budget_code'))
                                        <span class="help-block middle">
                                                <strong>{{ $errors->first('budget_code') }}</strong>
                                            </span>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>ভ্যাট(%) <span class="reqrd"> *</span></label>
                            <input type="number" id="vat" name="vat" class="form-control" min="0" step="0.25"
                                   placeholder="ভ্যাট (%)" required value="{{ $bgtCodData->vat}}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('vat'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('vat') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>স্ট্যাটাস <span class="reqrd"> *</span></label>
                            <?php $status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="budget_status" name="budget_status" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($status as $id => $val){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $bgtCodData->budget_status ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_status'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_status') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>বাজেটের উপ-ধরন <span class="reqrd"> *</span></label>
                            <!--                            --><?php //$status = config('constants.status.arr'); ?>
                            <select class="form-control select2bs4" id="bgt_sub_type" name="bgt_sub_type" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($budget_sub_type as $id => $val){ ?>
                                <option value="{{ $id }}" {{ $id == $bgtCodData->bgt_sub_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('bgt_sub_type'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('bgt_sub_type') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>উপখাতের নাম <span class="reqrd"> *</span></label>
                            <input type="text" id="budget_title" name="budget_title" class="form-control"
                                   placeholder="উপখাতের নাম" required value="{{ $bgtCodData->budget_title }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('budget_title'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('budget_title') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>আইটি/ট্যাক্স(%) <span class="reqrd"> *</span> </label>
                            <input type="number" min="0" step="0.25" id="tax" name="tax" class="form-control"
                                   placeholder="আইটি/ট্যাক্স(%)" required value="{{ $bgtCodData->tax }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('tax'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('tax') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> আইটেম পেরেন্ট <span class="reqrd"> *</span></label>
                            <?php $status = config('constants.is_item_parent.arr'); ?>
                            <select class="form-control select2bs4" id="is_item_parent" name="is_item_parent" required>
                                {{--                                <option value="">--- বাছাই করুণ ---</option>--}}
                                <?php
                                foreach ($status as $id => $val){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $bgtCodData->is_item_parent ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('is_item_parent'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('is_item_parent') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>

        {{--        Submit Form--}}
        <div class="card  card-primary">
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">

        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'});
            //Money Euro
            $('[data-mask]').inputmask();


            $('#bgt_type').on('change', function () {
                var bgt_type = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('budget-sub-type.ajaxBgtSubTypList') }}";
                if (bgt_type) {
                    $('select[name="bgt_sub_type"]').empty();
                    $('#bgt_type_code').val("");
                    $('#bgt_sub_type_code').val("");
                    bgtSubTypeList(bgt_type, token, url);
                } else {
                    $('select[name="bgt_sub_type"]').empty();
                    $('#bgt_type_code').val("");
                    $('#bgt_sub_type_code').val("");
                }
            });

            $('#bgt_sub_type').on('change', function () {
                var bgt_sub_type = $(this).val();
                var token = $("input[name='_token']").val();
                var url = "{{ route('budget-sub-type.ajaxBgtSubTypCode') }}";
                if (bgt_sub_type) {
                    $('#bgt_sub_type_code').val("");
                    bgtSubTypeCode(bgt_sub_type, token, url);
                } else {
                    $('#bgt_sub_type_code').val("");
                }
            });

            function bgtSubTypeList(bgt_type, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {bgt_type: bgt_type, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if (data.status === 200) {
                            $('select[name="bgt_sub_type"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                            $.each(data.budgetSubType, function (key, value) {
                                $('select[name="bgt_sub_type"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                            $('#bgt_type_code').val(data.budgetType.budget_type_code);
                        } else {
                            alert("ভুল হয়েছে");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

            function bgtSubTypeCode(bgt_sub_type, token, url) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {bgt_sub_type: bgt_sub_type, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if (data.status === 200) {
                            $('#bgt_sub_type_code').val(data.budgetSubTypeCode);
                        } else {
                            alert("ভুল হয়েছে");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            };

        });

    </script>
@endsection
