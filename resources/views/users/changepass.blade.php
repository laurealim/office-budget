@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('users.changepass') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">পাসওয়ার্ড পরিবর্তন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>নতুন পাসওয়ার্ড  <span class="reqrd"> *</span></label>
                            <input type="text" id="password" name="password" class="form-control"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('password'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>পূনরায় পাসওয়ার্ড দিন  <span class="reqrd"> *</span></label>
                            <input type="text" id="c_pass" name="c_pass" class="form-control" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('c_pass'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('c_pass') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection
