@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('payments.store') }}" method="POST"
          enctype="multipart/form-data" id="invoice_form">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">বিল পরিশোধ ফর্ম</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="hidden" id="tenant_id" name="tenant_id">
                            <input type="hidden" id="flat_id" name="flat_id">
                            <input type="hidden" id="year" name="year">
                            <input type="hidden" id="month" name="month">
                            <label>বিলের তালিকা <span class="reqrd"> *</span></label>
                            <select class="form-control flatList" id="bill_id" name="bill_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($unpaidBillList as $key => $vals){ ?>
                                <option value="{{ $key }}">{{ $vals }}
                                </option>
                                <?php }
                                ?>
                            </select>

                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('flat_list'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('flat_list') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <div class="form-group">
                            <label>বিলিং সময়ঃ <span id="b_time"></span></label>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>বিলের ধরন</th>
                                    <th style="width: 20%">বিলের পরিমাণ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>১.</td>
                                    <td>মাসিক ভাড়া</td>
                                    <td><input type="number" id="rent" name="rent" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td>২.</td>
                                    <td>সার্ভিস চার্জ</td>
                                    <td><input type="number" id="s_charge" name="s_charge" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td>৩.</td>
                                    <td>অন্যন্ন্য চার্জ</td>
                                    <td><input type="number" id="other_charge" name="other_charge" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td>৪.</td>
                                    <td>পুর্বের পরিশোধ</td>
                                    <td><input type="number" id="prev_pay" name="prev_pay" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td>৫.</td>
                                    <td>পুর্বের বকেয়া</td>
                                    <td><input type="number" id="prev_due" name="prev_due" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center"><h6><b> মোট বিল </b></h6></td>
                                    <td><input type="number" id="total_bill" name="total_bill" min="0" disabled></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center"><h6><b> মোট প্রদান <span class="reqrd"> *</span></b></h6></td>
                                    <td><input type="number" id="total_pay" name="total_pay" min="0" required></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-3">&nbsp;</div>
                </div>
            </div>
        </div>
        </div>

        {{--        Submit Form--}}
        <div class="card card-primary">
            <div class="card-footer">
                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            $('.flatList').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask();

            $(".hideable").hide();

        });

        $("#bill_id").on('change', function () {
            var billId = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('bills.ajaxGetUnpaidBillDetails') }}";
            $('.clear_field input[type="text"]').val('llkkiillkka');

            $.ajax({
                url: url,
                method: 'POST',
                data: {billId: billId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    console.log(data.flat_id);
                    var advPayment = 0;
                    var b_time = data.ban_month + ', '+data.ban_year;
                    $("#b_time").text(b_time);
                    $("#tenant_id").val(data.tenant_id);
                    $("#flat_id").val(data.flat_id);
                    $("#month").val(data.month);
                    $("#year").val(data.year);
                    $("#rent").val(data.rent);
                    $("#s_charge").val(data.s_charge);
                    $("#other_charge").val(data.other_charge);
                    $("#prev_pay").val(data.total_pay);
                    $("#prev_due").val(data.prv_due);

                    var total_bill = parseInt(data.total_bill) - parseInt(data.total_pay);
                    $("#total_bill").val(total_bill);
                    // if (data.advance_pay < 1) {
                    //     $(".hideable").show();
                    //     $("#advance_month").val(data.advance_month);
                    //     $("#advance_bill").val(data.advance_bill);
                    //     advPayment = parseInt(data.advance_bill);
                    // } else {
                    //     $(".hideable").hide();
                    // }
                    //
                    // var other_charge = parseInt($("#other_charge").val());
                    // var totalBill = parseInt(data.rent) + parseInt(data.service_charge) + parseInt(data.prev_due) + advPayment + parseInt(other_charge);
                    //
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        $("#other_charge").on('change', function () {
            var fltId = $('#flat_list').val();
            var other_charge = $("#other_charge").val();
            console.log(other_charge);
            if (other_charge) {
                other_charge = parseInt(other_charge);
            } else {
                other_charge = 0;
                $("#other_charge").val(0);
            }
            if (fltId) {
                var rent = $("#rent").val();
                var service_charge = $("#s_charge").val();
                var prv_due = $("#prv_due").val();
                var advance_bill = $("#advance_bill").val();
                if (!advance_bill) {
                    advance_bill = 0;
                }
                var totalBill = parseInt(rent) + parseInt(service_charge) + parseInt(prv_due) + parseInt(advance_bill) + parseInt(other_charge);
                $("#total_bill").val(totalBill);
            }
        });

        $('#month').on('change', function () {
            checkDuplicateMonthBill();
        });

        $('#year').on('change', function () {
            checkDuplicateMonthBill();
        });

        $('#flat_list').on('change', function () {
            checkDuplicateMonthBill();
        });

        function checkDuplicateMonthBill() {
            var month = $("#month").val();
            var year = $("#year").val();
            var flatId = $("#flat_list").val();

            if (month && year && flatId) {
                var token = $("input[name='_token']").val();
                var url = "{{ route('bills.ajaxCheckDuplicateBillMonth') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {month: month, year: year, flatId: flatId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if (data.code === 404) {
                            $('#submit').hide();
                            alert(data.message);
                        }
                        if (data.code === 200) {
                            $("#submit").show();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }
        };
    </script>
@endsection
