@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('bills.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="invoice_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">বিল সংশোধন</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ফ্ল্যাটের তালিকা <span class="reqrd"> *</span></label>
                            <select class="form-control flatList" id="flat_list" name="flat_list" required disabled>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($billableFlatList as $key => $vals){
                                if($vals->id == $billData->flat_id){
                                ?>
                                <option
                                    value="{{ $vals->id }}" {{ $vals->id == $billData->flat_id ? 'selected="selected"' : '' }}>{{ $vals->flat_id }}
                                    - {{ $vals->flat }}
                                    ( {{ $vals->address }} )
                                </option>
                                <?php }
                                }
                                ?>
                            </select>

                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('flat_list'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('flat_list') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>

                        <div class="col-md-3">&nbsp;</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ভাড়াটিয়ার নাম </label>
                            <input type="hidden" id="tenant_id" name="tenant_id" value="{{ $billData->tenant_id }}">
                            <input type="text" id="name" name="name" class="form-control clear_field"
                                   placeholder="ভাড়াটিয়ার নাম" readonly value="{{ $billData->name }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>মাসিক ভাড়া </label>
                            <input type="text" id="rent" name="rent" class="form-control clear_field"
                                   placeholder="মাসিক ভাড়া " readonly value="{{ $billData->rent }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('rent'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('rent') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>অন্যান্ন চার্জ </label>
                            <input type="number" min="0" id="other_charge" name="other_charge"
                                   class="form-control clear_field"
                                   value="{{ $billData->other_charge }}"
                                   placeholder="অন্যান্ন চার্জ ">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('other_charge'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('other_chargeS') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div
                            class="form-group <?php echo ($billData->advance_pay == config('constants.advance_pay.Unpaid')) ? "" : (($billData->adv_rent >= 1) ? "" : "hideable"); ?>">
                            <label>অগ্রিম মাস</label>
                            <input type="text" id="advance_month" name="advance_month" class="form-control clear_field"
                                   placeholder="অগ্রিম মাস" readonly value="{{ $billData->advance_month }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('advance_month'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('advance_month') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>মাস <span class="reqrd"> *</span></label>
                            <select class="form-control flatList" id="month" name="month" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                $monthList = config('constants.month_list.arr');
                                foreach ($monthList as $id => $name){ ?>
                                <option
                                    value="{{ $id }}" {{ $id == $billData->month ? 'selected="selected"' : '' }}>{{ $name }}
                                </option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('month'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('month') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>মোবাইল নং </label>
                            <input type="text" id="mobile" name="mobile" class="form-control clear_field"
                                   placeholder="মোবাইল নং" readonly value="{{ $billData->mobile }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('mobile'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>সার্ভিস চার্জ</label>
                            <input type="text" id="s_charge" name="s_charge" class="form-control clear_field"
                                   placeholder="সার্ভিস চার্জ " readonly value="{{ $billData->s_charge }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('s_charge'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('s_charge') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>বকেয়া বিল</label>
                            <input type="text" id="prv_due" name="prv_due" class="form-control clear_field"
                                   placeholder="বকেয়া বিল" readonly value="{{ $billData->prv_due }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('prv_due'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('prv_due') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div
                            class="form-group <?php echo ($billData->advance_pay == config('constants.advance_pay.Unpaid')) ? "" : (($billData->adv_rent >= 1) ? "" : "hideable"); ?>">
                            <label>অগ্রিম ভাড়া </label>
                            <input type="text" id="advance_bill" name="advance_bill" class="form-control clear_field"
                                   placeholder="অগ্রিম ভাড়া " readonly value="{{ $billData->advance_bill }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('advance_bill'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('advance_bill') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>বছর <span class="reqrd"> *</span></label>
                            <select class="form-control flatList" id="year" name="year" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                $yearList = (int)date('Y');
                                for($i = $yearList - 1; $i <= $yearList ; $i++){ ?>
                                <option
                                    value="{{ $i }}" {{ $i == $billData->year ? 'selected="selected"' : '' }}>{{ $i }}
                                </option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                                                @if ($errors->has('year'))
                                    <span class="help-block middle">
                                                        <strong>{{ $errors->first('year') }}</strong>
                                                    </span>
                                @endif
                                            </span>
                        </div>
                        <!-- /.form-group -->

                    </div>
                    <!-- /.col -->
                </div>

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="total_bill" class="col-sm-3 col-form-label">মোট বকেয়া বিল</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="total_bill" name="total_bill" readonly
                                       value="{{ $billData->total_bill }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>

        {{--        Submit Form--}}
        <div class="card card-primary">
            <div class="card-footer">
                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            $('.flatList').select2({
                theme: 'bootstrap4'
            });

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask();

            $(".hideable").hide();

        });

        $("#flat_list").on('change', function () {
            var flatId = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('bills.ajaxGetFlatBillData') }}";
            $('.clear_field input[type="text"]').val('llkkiillkka');

            $.ajax({
                url: url,
                method: 'POST',
                data: {flatId: flatId, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    console.log(data.flat_id);
                    var advPayment = 0;
                    $("#tenant_id").val(data.tenantId);
                    $("#name").val(data.name);
                    $("#mobile").val(data.mobile);
                    $("#rent").val(data.rent);
                    $("#s_charge").val(data.service_charge);
                    $("#prv_due").val(data.prev_due);
                    if (data.advance_pay < 1) {
                        $(".hideable").show();
                        $("#advance_month").val(data.advance_month);
                        $("#advance_bill").val(data.advance_bill);
                        advPayment = parseInt(data.advance_bill);
                    } else {
                        $(".hideable").hide();
                    }

                    var other_charge = parseInt($("#other_charge").val());
                    var totalBill = parseInt(data.rent) + parseInt(data.service_charge) + parseInt(data.prev_due) + advPayment + parseInt(other_charge);

                    $("#total_bill").val(totalBill);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        $("#other_charge").on('change', function () {
            var fltId = $('#flat_list').val();
            var other_charge = $("#other_charge").val();
            if (other_charge) {
                other_charge = parseInt(other_charge);
            } else {
                other_charge = 0;
                $("#other_charge").val(0);
            }
            if (fltId) {
                var rent = $("#rent").val();
                var service_charge = $("#s_charge").val();
                var prv_due = $("#prv_due").val();

                var advance_bill = '<?php echo $billData->adv_rent; ?>';
                var totalBill = parseInt(rent) + parseInt(service_charge) + parseInt(prv_due) + parseInt(advance_bill) + parseInt(other_charge);
                $("#total_bill").val(totalBill);
            }
        });

        $('#month').on('change', function () {
            checkDuplicateMonthBill();
        });

        $('#year').on('change', function () {
            checkDuplicateMonthBill();
        });

        $('#flat_list').on('change', function () {
            checkDuplicateMonthBill();
        });

        function checkDuplicateMonthBill() {
            var month = $("#month").val();
            var year = $("#year").val();
            var flatId = $("#flat_list").val();

            if (month && year && flatId) {
                var token = $("input[name='_token']").val();
                var url = "{{ route('bills.ajaxCheckDuplicateBillMonth') }}";

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {month: month, year: year, flatId: flatId, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if (data.code === 404) {
                            $('#submit').hide();
                            alert(data.message);
                        }
                        if (data.code === 200) {
                            $("#submit").show();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }
        };
    </script>
@endsection
