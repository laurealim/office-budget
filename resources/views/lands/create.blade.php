@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('lands.store') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">কেস নং</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>কেস নং <span class="reqrd"> *</span></label>
                            <input type="text" id="case_no" name="case_no" class="form-control" placeholder="কেস নং"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('case_no'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('case_no') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    {{--                    <div class="form-group">--}}
                    {{--                        <label>খতিয়ান</label>--}}
                    {{--                        <input type="text" class="form-control" placeholder="Enter ...">--}}
                    {{--                    </div>--}}
                    <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">জমির অবস্থান</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="dist" name="dist" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($districtListArr as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('dist'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('dist') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>মৌজা <span class="reqrd"> *</span></label>
                            <input type="text" id="mouja" name="mouja" class="form-control" placeholder="মৌজা"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('mouja'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('mouja') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->

                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>উপজেলা <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="upa" name="upa" required>
                                <option value="">--- বাছাই করুণ ---</option>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('upa'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('upa') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label>খতিয়ান <span class="reqrd"> *</span></label>
                            <input type="number" id="khotiyan" name="khotiyan" class="form-control" placeholder="খতিয়ান"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('khotiyan'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('khotiyan') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>দাগ নং <span class="reqrd"> *</span></label>
                            <input type="text" id="dag_no" name="dag_no" class="form-control" placeholder="দাগ নং"
                                   required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('dag_no'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('dag_no') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    {{--                    <div class="form-group">--}}
                    {{--                        <label>খতিয়ান</label>--}}
                    {{--                        <input type="text" class="form-control" placeholder="Enter ...">--}}
                    {{--                    </div>--}}
                    <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            {{--        <div class="card-footer">--}}
            {{--            Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about--}}
            {{--            the plugin.--}}
            {{--        </div>--}}
        </div>

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">জমির পরিমাণ ও মূল্য</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জমির শ্রেনী <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="lnd_cat" name="lnd_cat" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($landTypeListArr as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('lnd_cat'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('lnd_cat') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>জমির দাবী (মূল্য)</label>
                            <input type="number" id="lnd_price" name="lnd_price" class="form-control"
                                   placeholder="জমির দাবী (মূল্য)" disabled="disabled">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('lnd_price'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('lnd_price') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জমির পরিমাণ (শতাংশ) <span class="reqrd"> *</span></label>
                            <input type="number" id="lnd_size" name="lnd_size" class="form-control"
                                   placeholder="জমির পরিমাণ (শতাংশ)" required>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('lnd_size'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('lnd_size') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    {{--                    <div class="form-group">--}}
                    {{--                        <label>খতিয়ান</label>--}}
                    {{--                        <input type="text" class="form-control" placeholder="Enter ...">--}}
                    {{--                    </div>--}}
                    <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{--        </div>--}}
        </div>
    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upa"]').empty();
                upazillaList(districtID, token, url);
            } else {
                $('select[name="upa"]').empty();
            }
        });

        $('#lnd_cat').on('change', function () {
            var sizeVal = $('#lnd_size').val();
            var catVal = $(this).val();
            if (sizeVal != null && sizeVal != '') {
                var calculation = (parseInt(catVal) * parseInt(sizeVal));
                $('#lnd_price').val(calculation);
            }

        });

        $('#lnd_size').on('change', function () {
            var catVal = $('#lnd_cat').val();
            var sizeVal = $(this).val();
            if (catVal != null && catVal != '') {
                var calculation = (parseInt(catVal) * parseInt(sizeVal));
                $('#lnd_price').val(calculation);
            }
        });

        function upazillaList(districtID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="upa"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="upa"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };
    </script>
@endsection
